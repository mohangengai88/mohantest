import os
import urllib

from decouple import Csv, config

DEBUG = config('DEBUG', default=True, cast=bool)
IS_DEV_ENV = config('IS_DEV_ENV', default=True, cast=bool)

SECRET_KEY = config('SECRET_KEY')

SERVER_EMAIL = config('SERVER_EMAIL', default='ams.server.admin@renewbuy.com')
ADMINS = [
    ('Maulik', 'maulik.jain@renewbuy.com'),
    ('Server Errors', 'server.errors@renewbuy.com'),
]

DOMAIN = config('DOMAIN', default='https://accounts.rbstaging.in/')
TA_DOMAIN = config('TA_DOMAIN', default='https://www.travassured.com/')
RETAIL_DOMAIN = config('RETAIL_DOMAIN', default='https://www.renewbuypartners.com/')
RENEWBUY_DOMAINCODE = config('RENEWBUY_DOMAINCODE', default='https://www.renewbuy.com/')

ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=Csv())
ALLOWED_DOMAINS = config('ALLOWED_DOMAINS', cast=Csv())

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': config('DB_HOST'),
        'PORT': config('DB_PORT'),
    }
}

# Email Host Setup
DEFAULT_FROM_EMAIL = 'RenewBuy Support <support@renewbuy.com>'

EMAIL_HOST = config('EMAIL_HOST')
EMAIL_PORT = config('EMAIL_PORT')

TRAVASSURED_FROM_EMAIL = 'TravAssured Support <' + config(
    'TA_FROM_EMAIL',
    'travassured@renewbuy.com'
) + '>'
RENEWBUY_PARTNERS_FROM_EMAIL = 'RenewBuy Partners Support <' + config(
    'RT_FROM_EMAIL',
    'renewbuypartners@renewbuy.com'
) + '>'

# Current Environment's API Header Keys
CURRENT_ENV_API_KEY = config('CURRENT_API_KEY')
CURRENT_ENV_SECRET_KEY = config('CURRENT_SECRET_KEY')

# Custom S3 storage backends to store files in subfolders.
# The AWS access key used to access the storage buckets.
AWS_S3_ACCESS_KEY_ID = config('AWS_S3_ACCESS_KEY_ID')
# The AWS secret access key used to access the storage buckets.
AWS_S3_SECRET_ACCESS_KEY = config('AWS_S3_SECRET_ACCESS_KEY')
# The S3 bucket used to store uploaded files.
AWS_STORAGE_BUCKET_NAME = config('AWS_STORAGE_BUCKET_NAME')
AWS_S3_REGION_NAME = config('AWS_S3_REGION_NAME')
# Use AWS Signature version 4
AWS_S3_SIGNATURE_VERSION = 's3v4'
# https://rb-media-dev.s3.ap-south-1.amazonaws.com/media/file.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=0f641ad97c73257aa6f0bbbf8f0fd12c7ebf5f566a85d3acaa480a781f30324f&X-Amz-Date=20161023T165423Z&X-Amz-Credential=AKIAICQAILK7BGOYV7EA%2F20161023%2Fap-south-1%2Fs3%2Faws4_request
AWS_QUERYSTRING_AUTH = True
AWS_S3_ENCRYPTION = True
DEFAULT_FILE_STORAGE = 'ams.utils.s3.MediaRootS3Boto3Storage'
AWS_DEFAULT_ACL = None

# Celery Backend
AWS_SQS_ACCESS_KEY_ID = config('AWS_SQS_ACCESS_KEY_ID')
AWS_SQS_SECRET_ACCESS_KEY = config('AWS_SQS_SECRET_ACCESS_KEY')

BROKER_URL = 'sqs://{0}:{1}@'.format(
    urllib.parse.quote(AWS_SQS_ACCESS_KEY_ID, safe=''),
    urllib.parse.quote(AWS_SQS_SECRET_ACCESS_KEY, safe='')
)

result_backend = 'rpc://'
result_persistent = False
worker_pool_restarts = True

SQS_REGION = 'ap-south-1'
BROKER_TRANSPORT_OPTIONS = {
    'region': SQS_REGION,
    'wait_time_seconds': 10,
    'visibility_timeout': 3600,
    'queue_name_prefix': 'ams-uat-' if IS_DEV_ENV else 'ams-prod-',
}

# Route For Task
task_routes = {
    'ams.utils.tasks.send_otp_notification': {'queue': 'otp'},
    'ams.utils.tasks.send_otp_value_notification': {'queue': 'otp'},
    'ams.utils.tasks.send_sms_notification': {'queue': 'sms'},
    'ams.utils.tasks.send_event_task': {'queue': 'analytics'},
    'ams.utils.tasks.send_email_from_template_task': {'queue': 'email'},
    'ams.utils.tasks.create_pdf_and_send': {'queue': 'email_with_attachment'},
}

# Cryptography Settings
CRYPTOGRAPHY_KEY = config('CRYPTOGRAPHY_KEY')
CRYPTOGRAPHY_KEYS = config('CRYPTOGRAPHY_KEYS', cast=Csv())
CRYPTOGRAPHY_SALT = config('CRYPTOGRAPHY_SALT')

MAC_KEY = config('MAC_KEY')

GLUE = {
    'REMOTE_URL': config(
        'GLUE_REMOTE_URL',
        default='http://{namespace}.rbstaging.in/api/v1/{service_name}/',
    ),
    'accounts': {
        'API_KEY': config('ACCOUNTS_API_KEY'),
        'SECRET_KEY': config('ACCOUNTS_SECRET_KEY'),
    },
}

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
]

APPS = [
    'ams.accounts',
    'ams.base',
    'ams.data',
    # 'ams.notifications',
    'ams.otp',
    # 'ams.rbac',
    # 'ams.subscription',
    'ams.utils',

    'corsheaders',
    'glue',
    'rest_framework',
    'simple_history',
]

EXTRA_LIBS = config('EXTRA_LIBS', default='', cast=Csv())

INSTALLED_APPS = DJANGO_APPS + APPS + EXTRA_LIBS

INTERNAL_IPS = ['127.0.0.1']

WSGI_APPLICATION = 'ams.wsgi.application'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'simple_history.middleware.HistoryRequestMiddleware',
]

if 'debug_toolbar' in EXTRA_LIBS:
    MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]


ROOT_URLCONF = 'ams.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(PROJECT_DIR, 'ams/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'ams.utils.context_processors.allowed_user_context_processor',
            ],
        },
    },
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    'ams.accounts.backends.AMSAuthenticationBackend',
    # 'django.contrib.auth.backends.AllowAllUsersModelBackend'
]

REST_FRAMEWORK = {
    'NON_FIELD_ERRORS_KEY': 'error',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'ams.accounts.backends.JWTAuthentication',
    ),
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = '/static/ams'

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, 'static'),
)

AUTH_USER_MODEL = 'accounts.User'

LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/accounts/'
LOGOUT_REDIRECT_URL = None

MAX_UPLOAD_SIZE = '10485760'

# Logger Settings
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] [%(levelname)s] %(name)s: %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'generic': {
            'format': '%(asctime)s [%(process)d] [%(levelname)s] %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
            'class': 'logging.Formatter',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        }
    },
    'loggers': {
        'ams': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'glue.http.views': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'requests.packages.urllib3': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

CORS_ORIGIN_REGEX_WHITELIST = [
    r'^https://\w+\.rbstaging\.in$',
    r'^http://\w+\.rbstaging\.in$',
]

# Production environment settings
if not IS_DEV_ENV:
    CORS_ORIGIN_REGEX_WHITELIST = [
        r'^https://\w+\.renewbuy\.com$',
        r'^http://\w+\.renewbuy\.com$',
    ]

    LOG_DIR = config('AMS_LOG_DIR', default='/var/log/ams/')

    LOGGING['handlers'].update({
        'log_file': {
            'class': 'logging.handlers.WatchedFileHandler',
            'formatter': 'verbose',
            'filename': LOG_DIR + 'ams.log'
        },
        'error_file': {
            'class': 'logging.handlers.WatchedFileHandler',
            'formatter': 'generic',
            'filename': LOG_DIR + 'gunicorn.error.log'
        },
        'access_file': {
            'class': 'logging.handlers.WatchedFileHandler',
            'formatter': 'generic',
            'filename': LOG_DIR + 'gunicorn.access.log'
        }
    })

    # For celery workers, allow override logging
    default_handler = config('DEFAULT_LOG_HANDLER', default='log_file')
    override_logfile = config('AMS_OVERRIDE_LOGFILE', default=None)
    if override_logfile:
        LOGGING['handlers'].update({
            'override_logfile': {
                'class': 'logging.handlers.WatchedFileHandler',
                'formatter': 'verbose',
                'filename': override_logfile
            }
        })
        default_handler = 'override_logfile'

    LOGGING['loggers'].update({
        'gunicorn.access': {
            'level': 'INFO',
            'handlers': ['access_file'],
            'propagate': False
        },
        'gunicorn.error': {
            'level': 'INFO',
            'handlers': ['error_file'],
            'propagate': False
        },
        'ams': {
            'level': 'INFO',
            'handlers': [default_handler, 'mail_admins'],
            'propagate': False
        },
        'glue.http.views': {
            'level': 'ERROR',
            'handlers': [default_handler, 'mail_admins'],
            'propagate': False
        },
        'django.request': {
            'level': 'ERROR',
            'handlers': [default_handler, 'mail_admins'],
            'propagate': False
        },
        'request.packages.urllib3': {
            'level': 'DEBUG',
            'handlers': [default_handler, 'mail_admins'],
            'propagate': False
        }
    })

# SMS sending enabled
SMS_API_ENABLED = True

# OTP Settings
OTP_TOKEN_VALIDITY = 60 * 60 * 6  # 6 hours validity
OTP_CHALLENGE_MESSAGE = 'Sent by SMS. Token is {token}.'
OTP_ENABLED = True

SMS_CONF = {
    'OTPSOURCE': {
        'URL': 'http://sms.digialaya.com/API/SMSHttp.aspx?',
        'USERNAME': 'sandeep@renewbuy.com',
        'PASSWORD': 'sandeep07!',
        'SENDERID': 'RNWBUY',
    },
    'OTPINFINISOURCE': {
        'URL': 'https://alerts.solutionsinfini.com/api/v4/?api_key={}',
        'KEY': 'A40a56476403b8a1cc066694f0ac6bfac',
        'OTPKEY': 'Aa0798577bcab0ba20e104f7daf760855',
        'SENDERID': 'RNWBUY',
    },
    'OTPVALUESOURCE': {
        'URL': """http://api2.myvaluefirst.com/psms/servlet/psms.Eservice2?data=<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE MESSAGE SYSTEM 'http://127.0.0.1:80/psms/dtd/messagev12.dtd'><MESSAGE VER='1.2'><USER USERNAME='d2cxml' PASSWORD='d2ccspl123'/><SMS  UDH='0' CODING='1' TEXT='{SMSBODY}' PROPERTY='0' ID='1'><ADDRESS FROM='TRVSUR' TO='91{MOBILE}' SEQ='1' TAG='TravAssured OTP MESSAGE'/></SMS></MESSAGE>&action=send"""
    }
}

# SMS Templates
SMS_TEMPLATE = {
    'OTP_MESSAGE': '{token} is your One Time Password (OTP) to verify phone '
                   'number. Do not disclose OTP to anyone.',
    'TA': {
        'ACTIVATION': 'Dear Partner, your account with TravAssured has '
                      'been successfully activated. Click {}?next={} to login and '
                      'proceed.'.format(
                          DOMAIN, TA_DOMAIN
                      ),
        'FAILURE': 'Dear Partner, your application with TravAssured is '
                   'rejected because of incorrect PAN. Click {}profile to '
                   'upload the correct document.'.format(DOMAIN)
    }
}

RB_EXECUTIVE_SIGNUP_API = config(
    'RB_EXECUTIVE_SIGNUP_API',
    default='http://dev.renewbuy.com/api/v1/accounts/signup/'
)
RB_API_SECRET_KEY = config(
    'RB_API_SECRET_KEY',
    default='Sfhib3JpD7IEIPpwjFXNuLtkTXO9F3gX'
)
RB_APP_ID = config('RB_APP_ID', default='13425c72-0c7e-4248-9ffc-3e06b0a12632')

# Temporarily enable the sensitive fields.
DECRYPTED_GSTIN = config('DECRYPTED_GSTIN', default=False, cast=bool)

RENEWBUY_DOMAIN = {
    'URL': config('RB_URL', default='https://www.renewbuy.com/'),
    'API-SECRET-KEY': config('RB_SECRET_KEY', default='uFKJsEIoS5lIhn38pg88S03lTr8hcLHk'),
    'APP-ID': config('RB_APP_ID', default='9fa64858-b85a-483d-9c87-2890c41dbcc7'),
}

OC_SIGNUP_API = config(
    'OC_SIGNUP_API',
    default='http://uat.travassured.com/api/users/signup/'
)
OC_API_KEY = config(
    'OC_API_KEY',
    default='f290a76a-44e8-4ee3-9ed8-fcb943bf5786'
)
OC_SECRET_KEY = config(
    'OC_SECRET_KEY',
    default='Ep90HVQWxVvdwegMlaw50Mfro2eK0s35'
)

RB_PARTNER_CREATION_PASS = config('RB_PARTNER_CREATION_PASS', default='x3]39k0#JT11l9m')

DIALER_API_CHECK_AGENT_STATUS = config(
    'DIALER_API_CHECK_AGENT_STATUS',
    default='http://10.10.0.10/apps/appsHandler.php?transaction_id=CTI_CHECK_AGENT_STATE'
)
DIALER_API_CALL_CONNECT = config(
    'DIALER_API_CALL_CONNECT',
    default='http://10.10.0.10/apps/appsHandler.php?transaction_id=CTI_DIAL'
)

VALID_SOURCES = ['TA', 'RT', 'RB', 'DP', 'DPMOM', 'DPPAY1']

SEND_NOTIFICATION_BULK_REMAP = config('SEND_NOTIFICATION', default=True, cast=bool)
BULK_SEND_NOTIFICATION_FOR_BULK_REMAP = config('BULK_SEND_NOTIFICATION', default=False, cast=bool)

BULK_MAPPING_VIEW_ALLOWED_USERS = config('BULK_VIEW_ALLOWED_USER', cast=Csv())

GUPSHUP_CONF = {
    'URLS': {
        'WHATSAPP_OPT_IN_OUT_API': config('GUPSHUP_WHATSAPP_OPTIN_API', '')
    },
    'CREDENTIALS': {
        'USER_ID': config('GUPSHUP_API_USERID', '2000186644'),
        'PASSWORD': config('GUPSHUP_API_PASSWORD', 'wY53XX')
    }
}
SEND_EMAIL_FROM_SPEAR = config('SEND_EMAIL_FROM_SPEAR', default=False)
SPEAR = {
    'SPEAR_COMMUNICATION_API': config(
        'SPEAR_URL', 'http://spear.rbstaging.in/api/v2/perform_action/send_communications/'
    ),
    'HEADERS': {
        'API-SECRET-KEY': config(
            'SPEAR_API_SECRET_KEY', 'HTdtmtVjco0z3wIEhj1MsFPjOMRYKD0v'
        ),
        'App-Id': config(
            'SPEAR_APP_ID', '5a52b07f-d8ad-4b12-ba80-def5fcf16d8c'
        )
    }
}

DEFAULT_MAPPING_STATUS_EMAIL = config('DEFAULT_MAPPING_STATUS_EMAIL', cast=Csv())

WHATSAPP_OPTIN_REPORT_RECIPENT = config("WHATSAPP_OPTIN_REPORT_RECIPENT", cast=Csv())
DEFAULT_PAGINATOR_SIZE = config('DEFAULT_PAGINATOR_SIZE', default=1000, cast=int)


PARTNER_ACTIVATION_RULE = {
    'TOTAL_POLICY': config('TOTAL_POLICY', default=3),
    'TOTAL_BUSINESS': config('TOTAL_BUSINESS', default=10000),
    'PARTNER_BUSINESS_DATA_API': config(
        'DOSSIER_API',
        default='http://dossier.rbstaging.in/api/partner_data/'
    )
}
DOSSIER_DASHBOARD_URL = config(
    'DOSSIER_DASHBOARD_URL', default='http://dev.renewbuy.com/dossier/'
)

GENERATE_REFERAL = config('REFERAL_ALLOW', default=False, cast=bool)

HRONE_CONF = {
    'URL': config('HRONE_API_URL', default=''),
    'ACCESS_KEY': config('HRONE_API_ACCESS_KEY', default='qwertyuiop')
}

OPS_EMAIL = config('OPS_EMAIL', default='', cast=Csv())
MIS_EMAIL = config('MIS_EMAIL', default='', cast=Csv())

# general email id of `POS`
POS_MAILID = config('POS_MAIL', default='', cast=Csv())

# general email id of `FINANCE`
FINANCE_MAILID = config('FINANCE_MAIL', default='', cast=Csv())

# general email id of `HR`.
HR_GENERAL_MAIL = config('HR_GENERAL_MAIL', default='', cast=Csv())

RENEWBUY_SUPPORT = {
    'CONTACT_NO': config('CONTACT_NO', default='18004197852'),
    'EMAIL': config('EMAIL', default='support@renewbuy.com')
}

# Emails of muliple `HR` Person.
ALL_HR_MAIL = config('ALL_HR_MAIL', default='', cast=Csv())

BP_CONTACT_MAIL = config('BP_CONTACT_MAIL', default='sanjeev.kumar1@renewbuy.com', cast=Csv())

ANALYTICS = {
    'SNOWPLOW_COLLECTOR_URL': config(
        'SNOWPLOW_COLLECTOR_URL', default='//ec.rbstaging.in'
    ),
    'AMPLITUDE_API_KEY': config(
        'AMPLITUDE_API_KEY', default='fdab3ee6b34c920aa54bd0dc91e9d942'
    )
}

SEND_BP_REPORT = config(
    'SEND_BP_REPORT', default='ashwani.shakya@renewbuy.com', cast=Csv()
)
