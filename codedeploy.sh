#!/bin/bash
APPSPEC=$'version: 0.0
Resources:
  - TargetService:
      Type: AWS::ECS::Service
      Properties:
        TaskDefinition: arn:aws:ecs:ap-south-1:104072714890:task-definition/Phab_Build
        LoadBalancerInfo:
          ContainerName: 'web'
          ContainerPort: 26000
'

aws deploy create-deployment \
--application-name "AppECS-demo-ecs-demo-service" \
--deployment-group-name "DgpECS-demo-ecs-demo-service" \
--revision revisionType=String,string={content="$APPSPEC"}
