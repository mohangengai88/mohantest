from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import PasswordResetView
from django.urls import include, path, re_path

import ams.glue_registry  # noqa
from ams.accounts.forms import AMSAuthenticationForm, AMSPasswordResetForm
from ams.accounts.urls import hrurlpatterns, opsurlpatterns, t2urlpatterns
from ams.accounts.views import email_verify, frame, IndexView, profile, signUp

admin.site.site_header = 'AMS Administration'
admin.site.site_title = 'AMS Administration'
admin.site.index_title = 'AMS Administration'
admin.site.login_form = AMSAuthenticationForm

urlpatterns = [
    path('password_reset/', PasswordResetView.as_view(form_class=AMSPasswordResetForm), name='password_reset'),
    path('', include('django.contrib.auth.urls')),

    path('', IndexView.as_view(), name='index'),
    path('frame-page/', frame, name='frame'),
    path('profile', profile, name='profile'),
    path('sign-up', signUp, name='signUp'),

    path('admin/', admin.site.urls),
    path('api/', include('glue.http.urls')),
    path('accounts/', include('ams.accounts.urls')),
    path('ops/', include(opsurlpatterns)),
    path('hr/', include(hrurlpatterns)),
    path('t2/', include(t2urlpatterns)),

    re_path(
        r'^email_verification/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
        email_verify,
        name='email_verification'
    ),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
