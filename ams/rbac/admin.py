from django.contrib import admin

from ams.rbac.models import (Permission, PermissionRole, PermissionUser, Role,
                             UserRole)


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ('title', 'parent')


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = ('title', 'key')


@admin.register(UserRole)
class UserRoleAdmin(admin.ModelAdmin):
    list_display = ('user', 'role')
    raw_id_fields = ('user',)


@admin.register(PermissionUser)
class PermissionUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'permission')


@admin.register(PermissionRole)
class PermissionRoleAdmin(admin.ModelAdmin):
    list_display = ('role', 'permission')
