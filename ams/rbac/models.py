from django.conf import settings
from django.db import models

from ams.base.models import TimeStampModel


class Role(TimeStampModel):
    title = models.CharField(max_length=64, blank=True, null=True)
    parent = models.ForeignKey('self', blank=True, null=True, default=None, on_delete=models.PROTECT)

    def __str__(self):
        return self.title


class UserRole(TimeStampModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, default=None, on_delete=models.PROTECT)
    role = models.ForeignKey(Role, blank=True, null=True, default=None, on_delete=models.PROTECT)

    def __str__(self):
        return '{} - {}'.format(self.user, self.role)


class Permission(TimeStampModel):
    title = models.CharField(max_length=256, blank=True, null=True)
    key = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return self.title


class PermissionUser(TimeStampModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, default=None, on_delete=models.PROTECT)
    permission = models.ForeignKey(Permission, on_delete=models.DO_NOTHING, blank=True, null=True, default=None)

    def __str__(self):
        return '{} - {}'.format(self.user, self.permission)


class PermissionRole(TimeStampModel):
    role = models.ForeignKey(Role, blank=True, null=True, default=None, on_delete=models.PROTECT)
    permission = models.ForeignKey(Permission, on_delete=models.DO_NOTHING, blank=True, null=True, default=None)

    def __str__(self):
        return '{} - {}'.format(self.role, self.permission)
