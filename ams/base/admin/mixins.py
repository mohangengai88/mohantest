from django.contrib.admin.utils import flatten_fieldsets


class ReadonlyAdminMixin:
    can_delete = False
    extra = 0
    editable_fields = []

    def _user_can_write(self, request):
        groups = [x.name for x in request.user.groups.all()]
        return 'can_edit' in groups

    def has_add_permission(self, request):
        return request.user.is_superuser or self._user_can_write(request)

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser

    def get_actions(self, request):
        actions = super(ReadonlyAdminMixin, self).get_actions(request)
        if not self._user_can_write(request):
            actions.pop('delete_selected', None)

        return actions

    def _get_all_fields(self):
        if self.fieldsets:
            return flatten_fieldsets(self.fieldsets)
        all_fields = set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        )
        editable_fields = set(self.editable_fields)
        return list(all_fields - editable_fields)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser or self._user_can_write(request):
            return super(ReadonlyAdminMixin, self).get_readonly_fields(request, obj)
        return self._get_all_fields()
