from ams.subscription.exceptions import (InsufficientFloatBalanceException,
                                         ProductDoesNotExistException,
                                         UserSubscriptionNotFound)
from ams.subscription.models import AgreementType, Product, Subscription


def get_user_subscription(user):
    """
    Returns what products the given user has subscriptions to.
    """
    subs = Subscription.objects.filter(
        subscriber=user,
        is_active=True
    )
    return subs


def check_user_subscription(user, product):
    """
    Checks if a given user has a subscription for the selected product.
    """
    subs = get_user_subscription(user)

    if not isinstance(product, Product):
        try:
            product = Product.objects.get(
                product_slug=product
            )
        except Product.DoesNotExist:
            raise ProductDoesNotExistException

    try:
        sub = subs.get(
            product=product,
        )
        return sub
    except Subscription.DoesNotExist:
        raise UserSubscriptionNotFound


def get_user_commission_percentage(user, product):
    """
    Retrieves the commission percentage from the user subscription table
    for the given product.
    """
    sub = check_user_subscription(user, product)
    return {
        'type': sub.agreement_type,
        'percentage': sub.base_commission_percentage
    }


def get_user_float_subscription(user, product):
    """
    Returns what products the given user has subscriptions to
    and maintains a float with us for that.
    """
    subs = get_user_subscription(user)

    try:
        sub = subs.get(
            agreement_type=AgreementType.FLOAT,
            product=product
        )
        return sub
    except Subscription.DoesNotExist:
        raise UserSubscriptionNotFound


def check_and_update_float(user, product, amount):
    """
    Subtract the given amount from the user's current float account balance.
    """
    if not isinstance(product, Product):
        try:
            product = Product.objects.get(
                product_slug=product
            )
        except Product.DoesNotExist:
            raise ProductDoesNotExistException

    sub = get_user_float_subscription(
        user=user,
        product=product
    )

    old_float_amount = sub.float_amount

    if sub.float_amount >= amount:
        sub.float_amount -= round(amount)
        sub.save()
        return old_float_amount, sub.float_amount
    else:
        raise InsufficientFloatBalanceException
