from rest_framework.exceptions import APIException


class InsufficientFloatBalanceException(APIException):
    status_code = 400
    default_detail = 'Insufficient float account balance detected.'


class UserSubscriptionNotFound(APIException):
    status_code = 400
    default_detail = 'User subscription for this product not found.'


class ProductDoesNotExistException(APIException):
    status_code = 400
    default_detail = 'Invalid product selection. Please select a valid product.'
    default_code = 'error'
