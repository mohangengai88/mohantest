from django.contrib import admin

from ams.subscription.models import Subscription


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = ('id', 'subscriber', 'product', )
    search_fields = ('id', 'subscriber')
