from django.conf import settings
from django.db import models

from ams.base.models import TimeStampModel


class AgreementType:
    CUT_AND_PAY = 1
    FLOAT = 2


class Product(TimeStampModel):
    name = models.CharField(max_length=256, blank=True, null=True)
    product_slug = models.SlugField(max_length=64, unique=True, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Subscription(TimeStampModel):
    AGREEMENT_TYPE_CHOICES = (
        (AgreementType.CUT_AND_PAY, 'Cut and Pay'),
        (AgreementType.FLOAT, 'Float'),
    )
    subscriber = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    agreement_type = models.PositiveSmallIntegerField(choices=AGREEMENT_TYPE_CHOICES, blank=True, null=True)
    base_commission_percentage = models.FloatField(default=0.0, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Subscription'
        unique_together = ('subscriber', 'product')
