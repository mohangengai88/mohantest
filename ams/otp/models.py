import time
from binascii import unhexlify

import logging
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import six

from ams.base.validators import hex_validator
from ams.otp.oath import TOTP
from ams.otp.utils import random_hex
from ams.utils import import_class, sms

logger = logging.getLogger(__name__)


def default_key():
    return random_hex(20).decode()


def key_validator(value):
    return hex_validator(20)(value)


class DeviceManager(models.Manager):
    """
    The :class:'~django.db.models.Manager' object installed as
    ''Device.objects''.
    """

    def devices_for_user(self, user, confirmed=None):
        """
        Returns a queryset for all devices of this class that belong to the
        given user.

        :param user: The user.
        :type user: :class:'~django.contrib.auth.models.User'

        :param confirmed: If ''None'', all matching devices are returned.
            Otherwise, this can be any true or false value to limit the query
            to confirmed or unconfirmed devices, respectively.
        """
        devices = self.model.objects.filter(user=user)
        if confirmed is not None:
            devices = devices.filter(confirmed=bool(confirmed))
        return devices


class Device(models.Model):
    """
    Abstract base model for a :term:'device' attached to a user. Plugins must
    subclass this to define their OTP models.

    .. _unsaved_device_warning:

    .. warning::

        OTP devices are inherently stateful. For example, verifying a token is
        logically a mutating operation on the device, which may involve
        incrementing a counter or otherwise consuming a token. A device must be
        committed to the database before it can be used in any way.

    .. attribute:: user

        *ForeignKey*: Foreign key to your user model, as configured by
        :setting:'AUTH_USER_MODEL' (:class:'~django.contrib.auth.models.User'
        by default).

    .. attribute:: name

        *CharField*: A human-readable name to help the user identify their
        devices.

    .. attribute:: confirmed

        *BooleanField*: A boolean value that tells us whether this device has
        been confirmed as valid. It defaults to ''True'', but subclasses or
        individual deployments can force it to ''False'' if they wish to create
        a device and then ask the user for confirmation. As a rule, built-in
        APIs that enumerate devices will only include those that are confirmed.

    .. attribute:: objects

        A :class:'~django_otp.models.DeviceManager'.
    """
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        help_text='The user that this device belongs to.'
    )
    name = models.CharField(
        max_length=64,
        help_text='The human-readable name of this device.',
        null=True, blank=True, default=None
    )
    confirmed = models.BooleanField(
        default=True,
        help_text='Is this device ready for use?'
    )

    objects = DeviceManager()

    class Meta:
        abstract = True

    def __str__(self):
        if six.PY3:
            return self.__unicode__()
        else:
            return self.__unicode__().encode('utf-8')

    def __unicode__(self):
        try:
            user = self.user
        except ObjectDoesNotExist:
            user = None

        return six.u('{0} ({1})'.format(self.name, user))

    @property
    def persistent_id(self):
        return '{0}/{1}'.format(self.import_path, self.id)

    @property
    def import_path(self):
        return '{0}.{1}'.format(self.__module__, self.__class__.__name__)

    @classmethod
    def from_persistent_id(cls, path):
        """
        Loads a device from its persistent id::

            device == Device.from_persistent_id(device.persistent_id)
        """
        try:
            device_type, device_id = path.rsplit('/', 1)

            device_cls = import_class(device_type)
            device = device_cls.objects.get(id=device_id)
        except Exception:
            device = None

        return device

    def is_interactive(self):
        """
        Returns ''True'' if this is an interactive device. The default
        implementation returns ''True'' if
        :meth:'~django_otp.models.Device.generate_challenge' has been
        overridden, but subclasses are welcome to provide smarter
        implementations.

        :rtype: bool
        """
        return not hasattr(self.generate_challenge, 'stub')

    def generate_challenge(self):
        """
        Generates a challenge value that the user will need to produce a token.
        This method is permitted to have side effects, such as transmitting
        information to the user through some other channel (email or SMS,
        perhaps). And, of course, some devices may need to commit the
        challenge to the database.

        :returns: A message to the user. This should be a string that fits
            comfortably in the template '''OTP Challenge: {0}'''. This may
            return ''None'' if this device is not interactive.
        :rtype: string or ''None''

        :raises: Any :exc:'~exceptions.Exception' is permitted. Callers should
            trap ''Exception'' and report it to the user.
        """
        return None
    generate_challenge.stub = True

    def verify_token(self, token):
        """
        Verifies a token. As a rule, the token should no longer be valid if
        this returns ''True''.

        :param string token: The OTP token provided by the user.
        :rtype: bool
        """

        return False


class SMSDevice(Device):
    """
    A :class:'~django_otp.models.Device' that delivers codes via the SMS
    service. This uses TOTP to generate temporary tokens, which are valid for
    :setting:'OTP_TOKEN_VALIDITY' seconds. Once a given token has been
    accepted, it is no longer valid, nor is any other token generated at an
    earlier time.

    .. attribute:: number

        *CharField*: The mobile phone number to deliver to.

    .. attribute:: key

        *CharField*: The secret key used to generate TOTP tokens.

    .. attribute:: last_t

        *BigIntegerField*: The t value of the latest verified token.

    """
    number = models.CharField(
        max_length=16,
        help_text='The mobile number to deliver tokens to.'
    )

    key = models.CharField(
        max_length=40,
        validators=[key_validator],
        default=default_key,
        help_text='A random key used to generate tokens (hex-encoded).'
    )

    last_t = models.BigIntegerField(
        default=-1,
        help_text='The t value of the latest verified token. The next token must be at a higher time step.'
    )

    class Meta(Device.Meta):
        verbose_name = 'SMS Device'
        unique_together = ('user', 'number')

    @property
    def bin_key(self):
        return unhexlify(self.key.encode())

    def generate_challenge(self, **kwargs):
        """
        Sends the current TOTP token to ''self.number''.

        :returns: :setting:'OTP_CHALLENGE_MESSAGE' on success.
        :raises: Exception if delivery fails.

        """
        totp = self.totp_obj()
        token = format(totp.token(), '06d')
        message = settings.SMS_TEMPLATE.get('OTP_MESSAGE').format(token=token)   # noqa

        self._deliver_token(token, **kwargs)

        challenge = settings.OTP_CHALLENGE_MESSAGE.format(token=token)
        return challenge

    def _deliver_token(self, token, **kwargs):
        message = settings.SMS_TEMPLATE.get('OTP_MESSAGE').format(token=token)
        try:
            # if random.choice([True, False]):
            #     sms.send_otp_value(self.number, message)
            # else:
            sms.send_otp(self.number, message, **kwargs)
        except Exception:
            logger.exception('Unable to send an OTP.')

    def verify_token(self, token):
        try:
            token = int(token)
        except ValueError:
            verified = False
        else:
            totp = self.totp_obj()
            tolerance = settings.OTP_TOKEN_VALIDITY

            for offset in range(-tolerance, 1):
                totp.drift = offset
                if (totp.t() > self.last_t) and (totp.token() == token):
                    self.last_t = totp.t()
                    self.save()

                    verified = True
                    break
            else:
                verified = False

        return verified

    def totp_obj(self):
        totp = TOTP(self.bin_key, step=1)
        totp.time = time.time()
        return totp


class OTPDevice(models.Model):
    """
    A :class:'~django_otp.models.Device' that delivers codes via the SMS
    service. This uses TOTP to generate temporary tokens, which are valid for
    :setting:'OTP_TOKEN_VALIDITY' seconds. Once a given token has been
    accepted, it is no longer valid, nor is any other token generated at an
    earlier time.

    .. attribute:: number

        *CharField*: The mobile phone number to deliver to.

    .. attribute:: key

        *CharField*: The secret key used to generate TOTP tokens.

    .. attribute:: last_t

        *BigIntegerField*: The t value of the latest verified token.

    """
    number = models.CharField(
        max_length=16,
        unique=True,
        help_text='The mobile number to deliver tokens to.'
    )

    key = models.CharField(
        max_length=40,
        validators=[key_validator],
        default=default_key,
        help_text='A random key used to generate tokens (hex-encoded).'
    )

    last_t = models.BigIntegerField(
        default=-1,
        help_text='The t value of the latest verified token. '
                  'The next token must be at a higher time step.'
    )

    class Meta:
        verbose_name = 'OTP Device'

    @property
    def bin_key(self):
        return unhexlify(self.key)

    def generate_challenge(self, **kwargs):
        """
        Sends the current TOTP token to ''self.number''.

        :returns: :setting:'OTP_CHALLENGE_MESSAGE' on success.
        :raises: Exception if delivery fails.

        """
        totp = self.totp_obj()
        token = format(totp.token(), '06d')
        message = settings.SMS_TEMPLATE.get('OTP_MESSAGE').format(token=token)   # noqa

        self._deliver_token(token, **kwargs)

        challenge = settings.OTP_CHALLENGE_MESSAGE.format(token=token)
        return challenge

    def _deliver_token(self, token, **kwargs):
        message = settings.SMS_TEMPLATE.get('OTP_MESSAGE').format(token=token)
        try:
            # if random.choice([False]):
            #     sms.send_otp_value(self.number, message)
            # else:
            sms.send_otp(self.number, message, **kwargs)
        except Exception:
            logger.exception('Unable to send an OTP.')

    def verify_token(self, token):
        try:
            token = int(token)
        except ValueError:
            verified = False
        else:
            totp = self.totp_obj()
            tolerance = settings.OTP_TOKEN_VALIDITY

            for offset in range(-tolerance, 1):
                totp.drift = offset
                if (totp.t() > self.last_t) and (totp.token() == token):
                    self.last_t = totp.t()
                    self.save()

                    verified = True
                    break
            else:
                verified = False

        return verified

    def totp_obj(self):
        totp = TOTP(self.bin_key, step=1)
        totp.time = time.time()
        return totp
