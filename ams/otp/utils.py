from binascii import hexlify
from os import urandom


def random_hex(length=20):
    """
    Returns a string of random bytes encoded as hex. This uses
    :func:'os.urandom', so it should be suitable for generating cryptographic
    keys.

    :param int length: The number of (decoded) bytes to return.

    :returns: A string of hex digits.
    :rtype: str
    """
    return hexlify(urandom(length))
