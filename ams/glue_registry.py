from glue.registry import registry

from ams.accounts import services as accounts_services

from ams.data.services import (
    CitySearchService,
    PolicyAndContractFetchService,
    TransactionDataFetchService,
    TransactionDataInputService
)

registry.set_namespace('accounts')
# Global services.
registry.register(
    name='authenticate', provider=accounts_services.AuthenticationService
)
registry.register(
    name='logout_everywhere', provider=accounts_services.LogoutService
)
registry.register(
    name='password_reset', provider=accounts_services.PasswordResetService
)
registry.register(
    name='register', provider=accounts_services.RegistrationService
)
registry.register(
    name='partner_mapping', provider=accounts_services.PartnerMappingService
)
registry.register(
    name='approve_mapping', provider=accounts_services.MappingApproverViewService
)
registry.register(
    name='reportee_list', provider=accounts_services.MappingDetailApprovalViewService
)
registry.register(
    name='mapping_allocation', provider=accounts_services.MappingAcceptOrRejectService
)
registry.register(
    name='get_unmapped_partner', provider=accounts_services.GetUnmappedPartnerService
)
registry.register(
    name='partner_mapping_request', provider=accounts_services.SuggestMappingService
)
registry.register(
    name='update_profile', provider=accounts_services.ProfileUpdationService
)
registry.register(
    name='fetch_profile', provider=accounts_services.ProfileFetchingService
)
registry.register(
    name='feature_list', provider=accounts_services.FeatureListService
)
registry.register(
    name='send_otp', provider=accounts_services.SendOTPService
)
registry.register(
    name='verify_otp', provider=accounts_services.VerifyOTPService
)

registry.register(
    name='search_city', provider=CitySearchService
)

# Internal services.
registry.register(
    name='employee_search', provider=accounts_services.EmployeeSearchService
)
registry.register(
    name='referral_search', provider=accounts_services.ReferralSearchService
)

# Internal services using AMS ID.
registry.register(
    name='send_email', provider=accounts_services.SendEmailService
)
registry.register(
    name='send_sms', provider=accounts_services.SendSMSService
)
registry.register(
    name='create_customer', provider=accounts_services.InternalCustomerRegistrationService
)
registry.register(
    name='update_profile_with_id', provider=accounts_services.InternalProfileUpdationService
)
registry.register(
    name='fetch_profile_with_id', provider=accounts_services.InternalProfileFetchingService
)
registry.register(
    name='immediate_user_reporting', provider=accounts_services.ImmediateReportingService
)
registry.register(
    name='partners_list', provider=accounts_services.PartnersListService
)
registry.register(
    name='reporting_hierarchy', provider=accounts_services.ReportingHierarchyListService
)
registry.register(
    name='current_reporting_managers', provider=accounts_services.GetAllReportingManager
)
registry.register(
    name='reporting_check', provider=accounts_services.ReportingCheckService
)
registry.register(
    name='reporting_hierarchy_with_detail', provider=accounts_services.ReportingHierarchyDetailListService
)
registry.register(
    name='reporting_check', provider=accounts_services.ReportingCheckService
)
registry.register(
    name='reporting_manager', provider=accounts_services.ReportingManagerService
)
registry.register(
    name='recent_reporting_updates', provider=accounts_services.RecentReportingChangeFetchService
)
registry.register(
    name='set_user_reporting', provider=accounts_services.SetUserReporting
)
registry.register(
    name='bulk_partner_details', provider=accounts_services.BulkPartnerDetailsService
)
registry.register(
    name='bulk_partners_from_cluster', provider=accounts_services.FetchClusterMapping
)
registry.register(
    name='get_cluster_with_partner_id', provider=accounts_services.FetchPartnerClusterMapping
)
registry.register(
    name='reportings_on_daterange', provider=accounts_services.ReportingsOnDateRangeService
)
registry.register(
    name='get_partner_reportings', provider=accounts_services.GetPartnerReportingsService
)
registry.register(
    name='reporting_manager_details', provider=accounts_services.ReportingManagerDetailsService
)
registry.register(
    name='bulk_fetch_profile_with_id', provider=accounts_services.BulkProfileFetchingService
)
registry.register(
    name='fetch_cluster_details', provider=accounts_services.FetchClusterDetails
)
registry.register(
    name='direct_reportings_with_detail', provider=accounts_services.DirectReportingDetailService
)
registry.register(
    name='get_contract', provider=PolicyAndContractFetchService
)
registry.register(
    name='fetch_user_consent_status', provider=accounts_services.FetchConsentStatusService
)
registry.register(
    name='update_user_consent', provider=accounts_services.CreateUserConsentService
)

# Transactional confidential data storage for internal systems.
registry.register(
    name='transaction_data_input', provider=TransactionDataInputService
)
registry.register(
    name='transaction_data_fetch', provider=TransactionDataFetchService
)

# Temporary service for the internal systems
# until everything has been moved to AMS.
registry.register(
    name='data_backfill', provider=accounts_services.DataBackfillService
)
registry.register(
    name='communication_preferences', provider=accounts_services.CommunicationPreferenceService
)
