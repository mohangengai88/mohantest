from django.contrib.auth import views as auth_views
from django.urls import path, re_path

from ams.accounts.forms import AMSAuthenticationForm
from ams.accounts.views import (ChangeReportingView, ChangeUserStatus, DashboardView, DocAcceptView,
                                DocRejectView, DocsView, EmployeeProfileView, EmployeesView,
                                EmployeeTerminationView, ProfileView, SetReferralView, UsersView,
                                ChangeMappingView)

opsurlpatterns = [
    path('', DashboardView.as_view(), name='homepage'),
    path('docs/', DocsView.as_view(), name='docs'),
    path('doc_accept/', DocAcceptView.as_view(), name='doc_accept'),
    path('doc_reject/', DocRejectView.as_view(), name='doc_reject'),
    re_path(
        r'profile/(?P<pk>\d+)/$', ProfileView.as_view(),
        name='user_profile'
    ),
    path('users/', UsersView.as_view(), name='users'),
    path(
        'change_reporting/', ChangeReportingView.as_view(),
        name='change_reporting'
    ),
    path(
        'change_mapping', ChangeMappingView.as_view(),
        name='ops_change_mapping'
    )
]

hrurlpatterns = [
    path('', DashboardView.as_view(), name='homepage'),
    path('employees/', EmployeesView.as_view(), name='employees'),
    re_path(
        r'employee_profile/(?P<pk>\d+)/$', EmployeeProfileView.as_view(),
        name='employee_profile'
    ),
    path(
        'terminate_employee/', EmployeeTerminationView.as_view(),
        name='terminate_employee'
    ),
    path(
        'change_reporting/', ChangeReportingView.as_view(),
        name='change_reporting'
    ),
    path(
        'change_mapping', ChangeMappingView.as_view(), name='hr_change_mapping'
    )
]

t2urlpatterns = [
    path(
        'change_mapping', ChangeMappingView.as_view(), name='t2_change_mapping'
    )
]


urlpatterns = [
    path(
        'login/',
        auth_views.LoginView.as_view(
            form_class=AMSAuthenticationForm,
            template_name='accounts/login.html',
            redirect_authenticated_user=True,
        ),
        name='dash_login'
    ),
    path(
        'logout/',
        auth_views.LogoutView.as_view(
            template_name='accounts/login.html',
            next_page='../'
        ),
        name='dash_logout'
    ),
    path('set_referral/', SetReferralView.as_view(), name='set_referral'),
    path('user_status/', ChangeUserStatus.as_view(), name='change_user_status'),
]

urlpatterns.extend(opsurlpatterns)
urlpatterns.extend(hrurlpatterns)
