import json
import logging
from collections import Iterable
from datetime import date, datetime, timedelta

import jwt
import requests
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError
from django.core.management.utils import get_random_secret_key
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import DateField, F, IntegerField, Q, Value
from django.db.models.functions import Cast, Coalesce, Greatest, Least
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode
from django_cryptography.fields import encrypt
from simple_history.models import HistoricalRecords

from ams.accounts.managers import UserManager
from ams.accounts.tokens import email_verification_token
from ams.base.models import TimeStampModel
from ams.base.validators.form_validators import file_extension_validator
from ams.data.models import (
    Address, City, document_upload,
    PolicyAndContract, upload_image
)
from ams.utils import data_mask as security
from ams.utils import email, short_data, timezone, try_or
from ams.utils.analytics import send_event
from ams.utils.db import ExtractDaysFromPGInterval

logger = logging.getLogger(__name__)


class GenderType:
    MALE = 1
    FEMALE = 2


class MaritalStatus:
    SINGLE = 1
    MARRIED = 2


class UserTypes:
    EMPLOYEE = 1
    PARTNER = 2
    AGGREGATOR = 3
    CUSTOMER = 4
    CORPORATE = 5
    PYRAMID_MANAGER = 6


class ProfileVerificationStatus:
    PENDING = 1
    PARTIALLY_VERIFIED = 2
    FULLY_VERIFIED = 3


class OccupationType:
    TRAVEL_AGENCY = 1
    FOREX_AGENCY = 2
    VISA_CONSULTANT = 3
    ONLINE_TRAVEL_AGENT = 4
    TRAVEL_AGGREGATOR = 5
    GENERAL_SALES_AGENT = 6
    REGIONAL_SALES_AGENT = 7
    BUSINESS_PARTNER = 8
    OTHER = 9
    CHEMIST = 10
    SALARIED = 11
    AGRICULTURE = 12
    PROFESSIONAL = 13
    BUSINESS_OWNER = 14
    SELF_EMPLOYED = 15
    HOUSE_WIFE = 16
    STUDENT = 17
    INSURANCE_AGENT = 18
    CAR_DEALER = 19
    DEALER = 20
    MAX_AGENT = 21
    GARAGE_WORKSHOP_OWNER = 22
    FINANCIAL_SERVICE = 23


class AnnualIncome:
    BELOW_2_L = 1
    BETWEEN_2_5_L = 2
    BETWEEN_5_7_L = 3
    BETWEEN_7_10_L = 4
    ABOVE_10_L = 5


class EducationalQualification:
    BELOW_HIGH_SCHOOL = 1
    HIGH_SCHOOL = 2
    INTERMEDIATE = 3
    GRADUATE = 4
    POSTGRADUATE = 5
    ILLITERATE = 6
    OTHER = 7


class InsuranceBusiness:
    MOTOR_INSURANCE = 1
    LIFE_INSURANCE = 2
    HEALTH_INSURANCE = 3
    GENERAL_INSURANCE = 4
    NON_INSURANCE = 5


class Language:
    ENGLISH = 1
    HINDI = 2


class Channel(models.Model):
    """
    Model for storing user's channel related information
    """

    code = models.CharField(max_length=32, unique=True, blank=True, null=True)
    name = models.CharField(max_length=128, blank=True, null=True)
    description = models.CharField(max_length=512, blank=True, null=True)

    channel_head = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='channel_head',
    )

    def __str__(self):
        return f'{self.code} - {self.name}'


class BetaFeature(TimeStampModel):
    """
    Beta version feature mapping model.

    """

    feature_name = models.CharField(max_length=50, blank=True)
    feature_code = models.CharField(max_length=20, unique=True, blank=True)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return f'{self.feature_name}-{self.feature_code}'

    def save(self, *args, **kwargs):
        # If end date of the cluster is being changed, verify that it
        # isn't before the start date.
        if self.end_date:
            if self.end_date < self.start_date:
                raise ValidationError('End date of a feature cannot be before the start date')
        super().save(*args, **kwargs)


class User(AbstractBaseUser, PermissionsMixin, TimeStampModel):
    USER_TYPE_CHOICES = (
        (None, 'Please select a user type.'),
        (UserTypes.EMPLOYEE, 'Employee'),
        (UserTypes.PARTNER, 'Partner'),
        (UserTypes.AGGREGATOR, 'Aggregator'),
        (UserTypes.CUSTOMER, 'Customer'),
        (UserTypes.CORPORATE, 'Corporate'),
        (UserTypes.PYRAMID_MANAGER, 'Pyramid Manager'),
    )
    MARITAL_STATUS_CHOICES = (
        (None, 'Select the marital status.'),
        (MaritalStatus.SINGLE, 'Single'),
        (MaritalStatus.MARRIED, 'Married'),
    )
    GENDER_CHOICES = (
        (None, 'Please select the gender.'),
        (GenderType.MALE, 'Male'),
        (GenderType.FEMALE, 'Female'),
    )
    VERIFICATION_STATUS_CHOICES = (
        (ProfileVerificationStatus.PENDING, 'Pending'),
        (ProfileVerificationStatus.PARTIALLY_VERIFIED, 'Partially Verified'),
        (ProfileVerificationStatus.FULLY_VERIFIED, 'Fully Verified'),
    )
    OCCUPATION_TYPE_CHOICES = (
        (None, 'Please select an occupation type.'),
        (OccupationType.TRAVEL_AGENCY, 'Travel Agency'),
        (OccupationType.FOREX_AGENCY, 'Forex Agency'),
        (OccupationType.VISA_CONSULTANT, 'Visa Consultant'),
        (OccupationType.ONLINE_TRAVEL_AGENT, 'Online Travel Agent'),
        (OccupationType.TRAVEL_AGGREGATOR, 'Travel Aggregator'),
        (OccupationType.GENERAL_SALES_AGENT, 'General Sales Agent'),
        (OccupationType.REGIONAL_SALES_AGENT, 'Regional Sales Agent'),
        (OccupationType.BUSINESS_PARTNER, 'General Sales Agent'),
        (OccupationType.OTHER, 'Other'),
        (OccupationType.CHEMIST, 'Chemist'),
        (OccupationType.SALARIED, 'Salaried'),
        (OccupationType.AGRICULTURE, 'Agriculture'),
        (OccupationType.PROFESSIONAL, 'Professional'),
        (OccupationType.BUSINESS_OWNER, 'Business Owner'),
        (OccupationType.SELF_EMPLOYED, 'Self Employed'),
        (OccupationType.HOUSE_WIFE, 'House Wife'),
        (OccupationType.STUDENT, 'Student'),
        (OccupationType.INSURANCE_AGENT, 'Insurance Agent'),
        (OccupationType.CAR_DEALER, 'Car Dealer'),
        (OccupationType.DEALER, 'Dealer'),
        (OccupationType.MAX_AGENT, 'Max Agent'),
        (OccupationType.GARAGE_WORKSHOP_OWNER, 'Garage Workshop Owner'),
        (OccupationType.FINANCIAL_SERVICE, 'Financial Service'),
    )
    ANNUAL_INCOME = (
        (None, 'Please select an annual income.'),
        (AnnualIncome.BELOW_2_L, '< 2 Lacs'),
        (AnnualIncome.BETWEEN_2_5_L, '2 to 5 Lacs'),
        (AnnualIncome.BETWEEN_5_7_L, '5 to 7 Lacs'),
        (AnnualIncome.BETWEEN_7_10_L, '7 to 10 Lacs'),
        (AnnualIncome.ABOVE_10_L, '> 10 Lacs'),
    )
    EDUCATIONAL_QUALIFICATION = (
        (None, 'Please select the educational qualification.'),
        (EducationalQualification.BELOW_HIGH_SCHOOL, 'Below 10th'),
        (EducationalQualification.HIGH_SCHOOL, '10th'),
        (EducationalQualification.INTERMEDIATE, '12th'),
        (EducationalQualification.GRADUATE, 'Graduate'),
        (EducationalQualification.POSTGRADUATE, 'Post-Graduate'),
        (EducationalQualification.ILLITERATE, 'Illiterate'),
        (EducationalQualification.OTHER, 'Other'),
    )
    PAN_TYPE_CHOICES = (
        (None, 'Unable to detect PAN type.'),
        ('A', 'Association of Persons(AOP)'),
        ('B', 'Body of Individuals(BOI)'),
        ('C', 'Company'),
        ('F', 'Firm'),
        ('G', 'Government'),
        ('H', 'HUF(Hindu Undivided Family)'),
        ('L', 'Local Authority'),
        ('J', 'Artificial Juridical Person'),
        ('P', 'Individual(Proprietor)'),
        ('T', 'Trust(AOP)'),
    )
    LANGUAGE_CHOICES = (
        (None, 'Choose a preferred language.'),
        (Language.ENGLISH, 'English'),
        (Language.HINDI, 'Hindi'),
    )
    INSURANCE_BUSINESS_CHOICES = (
        (None, 'Select Business Type'),
        (InsuranceBusiness.MOTOR_INSURANCE, 'Motor Insurance'),
        (InsuranceBusiness.LIFE_INSURANCE, 'Life Insurance'),
        (InsuranceBusiness.HEALTH_INSURANCE, 'Health Insurance'),
        (InsuranceBusiness.GENERAL_INSURANCE, 'General Insurance'),
        (InsuranceBusiness.NON_INSURANCE, 'Non Insurance'),
    )

    # Hashed email to be used for database lookups.
    hashed_email = models.CharField(max_length=256, blank=True, null=True, unique=True)

    _email = encrypt(models.EmailField(max_length=256, blank=True, null=True, db_column='email'))

    _alternate_email = encrypt(
        models.EmailField(max_length=256, blank=True, null=True, db_column='alternate_email')
    )

    first_name = models.CharField(max_length=128, blank=True, null=True, default='')
    middle_name = models.CharField(max_length=128, blank=True, null=True, default='')
    last_name = models.CharField(max_length=128, blank=True, null=True, default='')
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, blank=True, null=True)

    dialer_agent_id = models.CharField(max_length=6, null=True, blank=True)

    is_staff = models.BooleanField(default=False)
    is_referral_active = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    token_secret_key = models.CharField(  # 50 character long random string.
        max_length=128, default=get_random_secret_key
    )

    channel = models.ForeignKey(
        Channel, blank=True, null=True, default=None, on_delete=models.DO_NOTHING
    )
    unique_code = models.CharField(
        max_length=64, unique=True, blank=True, null=True, editable=False, db_index=True
    )
    referral_code = models.CharField(
        max_length=32, unique=True, blank=True, null=True, default=None
    )

    # Hashed mobile to be used to database lookups.
    hashed_mobile = models.CharField(max_length=256, blank=True, null=True, db_index=True)

    _mobile = encrypt(models.CharField(max_length=10, blank=True, null=True, db_column='mobile'))

    _alternate_mobile = encrypt(
        models.CharField(max_length=10, blank=True, null=True, db_column='alternate_mobile')
    )

    landline = models.CharField(max_length=16, blank=True, null=True)

    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    photo = models.ImageField(
        max_length=256,
        upload_to=upload_image,
        null=True,
        blank=True,
        validators=[file_extension_validator],
    )

    primary_address = models.OneToOneField(
        Address,
        null=True,
        blank=True,
        default=None,
        related_name='primary_address',
        on_delete=models.DO_NOTHING,
    )
    secondary_address = models.OneToOneField(
        Address,
        null=True,
        blank=True,
        default=None,
        related_name='secondary_address',
        on_delete=models.DO_NOTHING,
    )
    gstin_address = models.OneToOneField(
        Address,
        null=True,
        blank=True,
        default=None,
        related_name='gstin_address',
        on_delete=models.DO_NOTHING,
    )

    agency_name = models.CharField(max_length=128, blank=True, null=True)
    occupation_type = models.PositiveSmallIntegerField(
        choices=OCCUPATION_TYPE_CHOICES, blank=True, null=True, default=None
    )

    marital_status = models.PositiveSmallIntegerField(
        choices=MARITAL_STATUS_CHOICES, blank=True, null=True, default=None
    )

    date_of_anniversary = models.DateField(blank=True, null=True, default=None)

    _aadhaar_number = encrypt(
        models.CharField(
            max_length=12,
            blank=True,
            null=True,
            db_column='aadhaar_number',
            validators=[
                RegexValidator(
                    regex=r'^[1-9]{1}' r'\d{11}$',
                    message='Please enter a valid 12 digit long aadhaar number.',
                )
            ],
        )
    )
    hashed_aadhaar_number = models.CharField(max_length=256, blank=True, null=True)

    _pan_number = encrypt(
        models.CharField(
            max_length=10,
            blank=True,
            null=True,
            db_column='pan_number',
            validators=[
                RegexValidator(
                    regex=r'^[A-Za-z]{3}'
                    r'[CPHFATBLJGcphfatbljg]{1}'
                    r'[A-Za-z]{1}'
                    r'\d{4}'
                    r'[A-Za-z]{1}$',
                    message='Please enter a valid 10 character long PAN number.',
                )
            ],
        )
    )
    hashed_pan_number = models.CharField(max_length=256, blank=True, null=True)

    pan_type = models.CharField(
        max_length=1, choices=PAN_TYPE_CHOICES, blank=True, null=True, editable=False
    )

    _gstin_number = encrypt(
        models.CharField(
            max_length=15,
            blank=True,
            null=True,
            db_column='gstin_number',
            validators=[
                RegexValidator(
                    regex=r'^\d{2}'
                    r'[A-Za-z]{3}'
                    r'[CPHFATBLJGcphfatbljg]{1}'
                    r'[A-Za-z]{1}'
                    r'\d{4}'
                    r'[A-Za-z]{1}'
                    r'\d{1}[Zz]{1}'
                    r'[0-9A-Za-z]{1}$',
                    message='Please enter a valid 15 character long GSTIN number.',
                )
            ],
        )
    )
    hashed_gstin_number = models.CharField(max_length=256, blank=True, null=True)

    _bank_account_number = encrypt(
        models.CharField(
            max_length=32,
            blank=True,
            null=True,
            db_column='bank_account_number',
        )
    )
    _ifsc_code = encrypt(
        models.CharField(
            max_length=11,
            blank=True,
            null=True,
            db_column='ifsc_code',
            validators=[
                RegexValidator(
                    regex=r'^[A-Za-z]{4}' r'[A-Za-z0-9]{7}$',
                    message='Please enter a valid 11 character long IFSC code.',
                )
            ],
        )
    )
    bank_name = models.CharField(max_length=128, blank=True, null=True)
    bank_branch = models.CharField(max_length=256, blank=True, null=True)

    _drivers_license_number = encrypt(
        models.CharField(
            max_length=32,
            blank=True,
            null=True,
            db_column='drivers_license_number',
            validators=[
                RegexValidator(
                    regex=r'^[A-Za-z]{2}' r'\d{13}$',
                    message='Please enter a valid 15 character long driving license' ' number.',
                )
            ],
        )
    )

    _education_cert_number = encrypt(
        models.CharField(
            max_length=32,
            blank=True,
            null=True,
            db_column='education_cert_number',
        )
    )

    _passport_number = encrypt(
        models.CharField(
            max_length=32,
            blank=True,
            null=True,
            db_column='passport_number',
            validators=[
                RegexValidator(
                    regex=r'^[A-Za-z]{1}' r'\d{7}$',
                    message='Please enter a valid 8 character long passport number.',
                )
            ],
        )
    )

    caller_id = models.CharField(max_length=16, blank=True, null=True)
    whatsapp_notification_enabled = models.BooleanField(default=False)

    is_email_verified = models.BooleanField(default=False)
    is_alternate_email_verified = models.BooleanField(default=False)
    is_mobile_verified = models.BooleanField(default=False)
    is_alternate_mobile_verified = models.BooleanField(default=False)
    is_photo_verified = models.BooleanField(default=False)

    is_aadhaar_verified = models.BooleanField(default=False)
    is_pan_verified = models.BooleanField(default=False)
    is_bank_account_verified = models.BooleanField(default=False)
    is_dl_verified = models.BooleanField(default=False)
    is_education_cert_verified = models.BooleanField(default=False)
    is_gst_verified = models.BooleanField(default=False)
    is_passport_verified = models.BooleanField(default=False)

    gi_pos_certified_date = models.DateField(blank=True, null=True, default=None)

    li_pos_status = models.BooleanField(default=False)

    gi_pos_status = models.BooleanField(default=False)

    li_pos_certified_date = models.DateField(blank=True, null=True, default=None)
    pos_code = models.CharField(max_length=32, blank=True, null=True, default=None)

    annual_income = models.PositiveSmallIntegerField(
        choices=ANNUAL_INCOME, default=None, blank=True, null=True
    )
    educational_qualification = models.PositiveSmallIntegerField(
        choices=EDUCATIONAL_QUALIFICATION, default=None, blank=True, null=True
    )

    preferred_lang_written = models.PositiveSmallIntegerField(
        choices=LANGUAGE_CHOICES, default=None, blank=True, null=True
    )
    preferred_lang_spoken = models.PositiveSmallIntegerField(
        choices=LANGUAGE_CHOICES, default=None, blank=True, null=True
    )

    profile_verification_status = models.PositiveSmallIntegerField(
        choices=VERIFICATION_STATUS_CHOICES,
        blank=True,
        null=True,
        default=ProfileVerificationStatus.PENDING,
    )

    insurance_business = models.PositiveSmallIntegerField(
        choices=INSURANCE_BUSINESS_CHOICES, blank=True, null=True, default=None
    )

    history = HistoricalRecords(history_change_reason_field=models.TextField(null=True))

    objects = UserManager()

    # M-2-M field mapping with the BetaFeature table
    #beta_enrollment = models.ManyToManyField(BetaFeature, blank=True, null=True)
    beta_enrollment = models.ManyToManyField(BetaFeature, blank=True)
    # Email address to be used as the username
    USERNAME_FIELD = 'hashed_email'

    class Meta:
        verbose_name_plural = 'Users'

    def __str__(self):
        """
        Returns the email of the User when it is printed in the console
        """
        return f'{self.email} - {self.unique_code}'

    # ==========================================================================
    # Setting properties on all the sensitive fields, to override the default
    # getter/setter functionality. All the gets on sensitive fields now reveal
    # the data in masked format.
    # ==========================================================================
    @property
    def email(self):
        return security.encrypt_email(self._email)

    @property
    def decrypted_email(self):
        return self._email

    @email.setter
    def email(self, value):
        self._email = value

    @property
    def alternate_email(self):
        return security.encrypt_email(self._alternate_email)

    @property
    def decrypted_alternate_email(self):
        return self._alternate_email

    @alternate_email.setter
    def alternate_email(self, value):
        self._alternate_email = value

    @property
    def mobile(self):
        return security.encrypt_mobile(self._mobile)

    @property
    def decrypted_mobile(self):
        return self._mobile

    @mobile.setter
    def mobile(self, value):
        self._mobile = value

    @property
    def alternate_mobile(self):
        return security.encrypt_mobile(self._alternate_mobile)

    @property
    def decrypted_alternate_mobile(self):
        return self._alternate_mobile

    @alternate_mobile.setter
    def alternate_mobile(self, value):
        self._alternate_mobile = value

    @property
    def aadhaar_number(self):
        return security.encrypt_aadhaar(self._aadhaar_number)

    @property
    def decrypted_aadhaar_number(self):
        return self._aadhaar_number

    @aadhaar_number.setter
    def aadhaar_number(self, value):
        self._aadhaar_number = value

    @property
    def pan_number(self):
        return security.encrypt_pan(self._pan_number)

    @property
    def decrypted_pan_number(self):
        return self._pan_number

    @pan_number.setter
    def pan_number(self, value):
        self._pan_number = value

    @property
    def gstin_number(self):
        if settings.DECRYPTED_GSTIN:
            return self._gstin_number
        return security.encrypt_gstin(self._gstin_number)

    @property
    def decrypted_gstin_number(self):
        return self._gstin_number

    @gstin_number.setter
    def gstin_number(self, value):
        self._gstin_number = value

    @property
    def bank_account_number(self):
        return security.encrypt_bank_acc(self._bank_account_number)

    @property
    def decrypted_bank_account_number(self):
        return self._bank_account_number

    @bank_account_number.setter
    def bank_account_number(self, value):
        self._bank_account_number = value

    @property
    def ifsc_code(self):
        return security.encrypt_ifsc(self._ifsc_code)

    @property
    def decrypted_ifsc_code(self):
        return self._ifsc_code

    @ifsc_code.setter
    def ifsc_code(self, value):
        self._ifsc_code = value

    @property
    def drivers_license_number(self):
        return security.encrypt_dl(self._drivers_license_number)

    @property
    def decrypted_drivers_license_number(self):
        return self._drivers_license_number

    @drivers_license_number.setter
    def drivers_license_number(self, value):
        self._drivers_license_number = value

    @property
    def education_cert_number(self):
        return security.encrypt_edu_cert(self._education_cert_number)

    @property
    def decrypted_education_cert_number(self):
        return self._education_cert_number

    @education_cert_number.setter
    def education_cert_number(self, value):
        self._education_cert_number = value

    @property
    def passport_number(self):
        return security.encrypt_passport(self._passport_number)

    @property
    def decrypted_passport_number(self):
        return self._passport_number

    @passport_number.setter
    def passport_number(self, value):
        self._passport_number = value

    # ==========================================================================

    @property
    def token(self):
        """
        Allows us to get a user's token by calling `user.token` instead of
        `user.generate_jwt_token().

        The `@property` decorator above makes this possible. `token` is
        called a 'dynamic property'.
        """
        return self._generate_jwt_token()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return try_or(lambda: self.first_name.title(), '') or try_or(
            lambda: short_data.get_first_name(self.decrypted_email).title(), ''
        )

    def get_full_name(self):
        """
        Returns the full name of the user.
        """
        full_name = self.first_name
        if self.middle_name:
            full_name = f'{full_name} {self.middle_name}'
        if self.last_name:
            full_name = f'{full_name} {self.last_name}'
        return try_or(lambda: full_name.title(), '')

    def update_token_secret_key(self):
        """
        Updates the unique secret key used for creating the user's token.
        """
        self.token_secret_key = get_random_secret_key()
        self.save()

    def _get_token_secret(self):
        """
        Generates the final secret key used to create the user's token,
        combining the token_secret_key stored in the database and django's
        secret key from the settings.
        """
        return self.token_secret_key.strip() + settings.SECRET_KEY

    def _generate_jwt_token(self):
        """
        Generates a JSON Web Token that stores this user's ID and has an
        expiry date set to 365 days into the future.
        """
        dt = datetime.now() + timedelta(days=365)

        token = jwt.encode(
            {'id': self.unique_code, 'exp': int(dt.timestamp())},
            self._get_token_secret(),
            algorithm='HS256',
        )

        return token.decode('utf-8')

    def save(self, *args, **kwargs):
        """
        https://docs.djangoproject.com/en/2.0/topics/db/models/#overriding-predefined-model-methods
        """
        is_new = False

        if self._email:
            self._email = UserManager().normalize_email(self._email)
            self.hashed_email = security.hash_field(self._email)

            if '@renewbuy' in self._email and '.retailers' not in self._email:
                self.user_type = UserTypes.EMPLOYEE

        if self._mobile:
            self.hashed_mobile = security.hash_field(self._mobile)

        if self._aadhaar_number:
            self.hashed_aadhaar_number = security.hash_field(self._aadhaar_number.upper())

        if self._pan_number:
            self.hashed_pan_number = security.hash_field(self._pan_number.upper())

        if self._gstin_number:
            self.hashed_gstin_number = security.hash_field(self._gstin_number.upper())

        if self.id is None:
            is_new = True
        elif not self.unique_code:
            self.unique_code = self.create_unique_code(
                'EM' if self.user_type == UserTypes.EMPLOYEE else None
            )

        if self._pan_number:
            # `__getitem__` to make the linter shut up.
            self.pan_type = self._pan_number.__getitem__(3).upper()

        if (self.gi_pos_status or self.li_pos_status) and self.is_referral_active:
            self.is_referral_active = False
            logger.info(
                'Disabling referral code for %s as they are POS certified and aren\'t allowed '
                'to have reportees under them',
                self,
            )

        try:
            if self.referral_code:
                response = requests.post(
                    '{}/api/v1/accounts/create_partner/'.format(settings.RENEWBUY_DOMAIN['URL']),
                    headers={
                        'API-SECRET-KEY': settings.RENEWBUY_DOMAIN['API-SECRET-KEY'],
                        'APP-ID': settings.RENEWBUY_DOMAIN['APP-ID'],
                        'Content-Type': 'application/json',
                    },
                    data=json.dumps(
                        {
                            'passkey': settings.RB_PARTNER_CREATION_PASS,
                            'email': self._email,
                            'referral_code': self.referral_code,
                            'is_active': self.is_referral_active,
                        }
                    ),
                    timeout=2,  # Setting 2 seconds long timeout.
                )
                logger.info('Set referral code[%s] for %s in RB', self.referral_code, self)
                if not response.status_code == 200:
                    raise ValidationError(response.json())
                else:
                    logger.info(
                        'Sucessfully set referral code[%s] for %s in RB', self.referral_code, self
                    )
        except requests.exceptions.ReadTimeout:
            logger.info(
                'Unable to set referral code for %s in RB because of request timeout.', self
            )
        except Exception as e:
            logger.exception('Unable to set referral code for %s in RB: %s', self, e)

        # We have to save the user object once because, it hasn't been assigned
        # a `pk` yet.
        super().save(*args, **kwargs)  # Call the 'real' save() method.

        # Checking if unique_code was provided at the time of making a new user.
        # Which would mean that we are backfilling the data from legacy systems.
        if is_new and not self.unique_code:
            if self.user_type == UserTypes.EMPLOYEE:
                self.unique_code = self.create_unique_code('EM')
            else:
                self.unique_code = self.create_unique_code()
            super().save()

    def create_unique_code(self, prefix='DI'):
        """
        Create a unique code for the user.
        Defaults to DI.
        """
        return f'{prefix}{str(self.pk).zfill(10 - len(prefix))}'

    def send_email_verification(self):
        """
        Send/resend email verification if email of the user isn't verified.
        """
        if self._email and not self.is_email_verified:
            # If the registration is from DP channel, don't send the
            # verification email.
            if self.unique_code.startswith('DP'):
                return True

            # Send a welcome email and verification link to the provided email
            # in the registration form.
            context = {
                'user_name': self.get_full_name(),
                'domain': settings.DOMAIN,
                'uid': force_str(urlsafe_base64_encode(force_bytes(self.pk))),
                'token': email_verification_token.make_token(self),
            }

            # Decide what email template to use based on the source.
            if self.unique_code[:2] == 'TA':
                template = 'ta_registration.html'
                subject = 'TravAssured - Email Verification'
                from_email = settings.TRAVASSURED_FROM_EMAIL
                context.update({'agency_name': self.agency_name})
            else:
                template = 'rt_registration.html'
                subject = 'RenewBuy - Email Verification'
                from_email = settings.RENEWBUY_PARTNERS_FROM_EMAIL

            to = self._email

            return email.send_from_template(to, subject, template, context, from_email=from_email)

        raise ValidationError(
            'Either email for the user doesn\'t exist or it has ' 'already been verified.'
        )

    def get_current_reporting(self, reporting_type=None):
        """
        Get the current reporting manager of this particular user.
        """
        try:
            reporting = self.manager.get(
                Q(end_date__gte=timezone.now_local(only_date=True)) | Q(end_date=None),
                start_date__lte=timezone.now_local(only_date=True),
                reporting_type=reporting_type,
            )
            return reporting
        except UserReporting.DoesNotExist:
            return self.get_latest_reporting()

    def get_latest_reporting(self, reporting_type=None):
        """
        Get the final reporting manager of this particular user.
        """
        try:
            reporting = self.manager.get(
                end_date=None,
                reporting_type=reporting_type,
            )
            return reporting
        except UserReporting.DoesNotExist:
            return None

    def get_current_and_future_reportings(self, reporting_type=None):
        """
        Get the current reporting manager of this particular user.
        """
        extra_kwargs = {}
        if reporting_type:
            if not isinstance(reporting_type, Iterable):
                reporting_type = [reporting_type]
            extra_kwargs.update(
                {
                    'reporting_type__in': reporting_type,
                }
            )
        reportings = self.manager.filter(
            Q(end_date__gte=timezone.now_local(only_date=True)) | Q(end_date=None),
            **extra_kwargs,
        ).order_by('start_date')
        return reportings

    def get_reporting_on_date(self, as_on_date, reporting_type=None):
        """
        Get the reporting manager of this particular user `as_on` date.
        """
        try:
            reporting = self.manager.get(
                Q(end_date__gte=as_on_date) | Q(end_date=None),
                start_date__lte=as_on_date,
                reporting_type=reporting_type,
            )
            return reporting
        except UserReporting.DoesNotExist:
            return None

    def get_reportings_in_date_range(self, start_date, end_date=None):
        """
        Get the reporting manager of this particular user in a date range.
        Insipiration:
        >>> from datetime import datetime
        >>> from collections import namedtuple
        >>> Range = namedtuple('Range', ['start', 'end'])

        >>> r1 = Range(start=datetime(2012, 1, 15), end=datetime(2012, 5, 10))
        >>> r2 = Range(start=datetime(2012, 2, 20), end=datetime(2012, 3, 15))
        >>> latest_start = max(r1.start, r2.start)
        >>> earliest_end = min(r1.end, r2.end)
        >>> delta = (earliest_end - latest_start).days + 1
        >>> overlap = max(0, delta)
        >>> overlap
        """
        if not isinstance(start_date, (datetime, date)):
            raise ValueError('Please provide `start_date` argument as an object of `datetime.date`')
        if end_date and not isinstance(end_date, (datetime, date)):
            raise ValueError('Please provide `end_date` argument as an object of `datetime.date`')
        if not end_date:
            end_date = (timezone.now_local(only_date=True) + relativedelta(months=1)).replace(day=1)
        if start_date > end_date:
            raise ValueError('`end_date` cannot be greater than the `start_date`')

        today = timezone.now_local(only_date=True)

        reportings = (
            self.manager.annotate(
                latest_start_date=Greatest('start_date', Value(start_date)),
                earliest_end_date=Coalesce(
                    Least('end_date' or today, Value(end_date)), end_date or F('end_date')
                ),
            )
            .annotate(
                diff=(
                    Cast(F('earliest_end_date'), DateField())
                    - Cast(F('latest_start_date'), DateField())
                )
            )
            .annotate(diff_days=ExtractDaysFromPGInterval(F('diff'), output_field=IntegerField()))
            .annotate(overlap=Greatest(0, F('diff_days') + 1))
            .filter(overlap__gt=0)
        )

        return reportings

    def non_discarded_docs(self):
        """
        Returns the documents of the user which haven't been discarded yet.
        """
        return Document.objects.filter(
            user=self,
            status__in=[DocumentVerificationStatus.PENDING, DocumentVerificationStatus.VERIFIED],
        )

    def indirectly_reports_to(self, user2, reporting_type=None):
        """
        Check whether an indirect relationship exists between a user and another user.
        """
        if self == user2:
            return True

        reporting = self.get_current_reporting(reporting_type)

        if not reporting:
            return False
        if user2 == reporting.manager:
            return True

        # Recursive call to check the entire hierarchy.
        return reporting.manager.indirectly_reports_to(user2, reporting_type)


# Make sure PartnerToEmployeeReportingType and ReportingType don't have clashes amongst each other
class PartnerToEmployeeReportingType:
    """
    Class containing different Partner-Employee reporting types
    """

    PARTNER_TELE = 1
    PARTNER_FIELD = 2
    PARTNER_KEY_RELATIONSHIP_FRM = 5
    PARTNER_ACTIVATION_FRM = 6
    PARTNER_ACQUISITION_FRM = 7
    PARTNER_HEALTH_FRM = 8
    PARTNER_LIFE_FRM = 9
    PARTNER_RENEWAL_TRM = 10
    PARTNER_EMPLOYEE = 11


# Make sure PartnerToEmployeeReportingType and ReportingType don't have clashes amongst each other
class ReportingType(PartnerToEmployeeReportingType):
    """
    Class containing all the reporting types there are
    """

    PARTNER_PARTNER = None
    EMPLOYEE = 3
    HEALTH_EXPERT = 4


class UserReporting(TimeStampModel):
    """
    This model will have relationship of user(reportee) to user reporting.
    Give us the start date to end date of user reporting.
    """

    PARTNER_TO_EMPLOYEE_REPORTING_CHOICES = [
        (ReportingType.PARTNER_TELE, 'Partner to Tele-RM Reporting'),
        (ReportingType.PARTNER_FIELD, 'Partner to Field-RM Reporting'),
        (ReportingType.PARTNER_KEY_RELATIONSHIP_FRM, 'Partner to Key Relationship-FRM Reporting'),
        (ReportingType.PARTNER_ACTIVATION_FRM, 'Partner to Activation-FRM Reporting'),
        (ReportingType.PARTNER_ACQUISITION_FRM, 'Partner to Acquisition-FRM Reporting'),
        (ReportingType.PARTNER_HEALTH_FRM, 'Partner to Health-FRM Reporting'),
        (ReportingType.PARTNER_LIFE_FRM, 'Partner to Life-FRM Reporting'),
        (ReportingType.PARTNER_RENEWAL_TRM, 'Partner to Renewal-TRM Reporting'),
    ]
    EMPLOYEE_TO_EMPLOYEE_REPORTING_CHOICES = [
        (ReportingType.EMPLOYEE, 'Employee to Employee Reporting'),
        (ReportingType.HEALTH_EXPERT, 'Employee to Health Expert Reporting'),
    ]
    TEMPORARY_REPORTING_CHOICES = [
        (ReportingType.PARTNER_EMPLOYEE, 'Partner to Employee Reporting'),
    ]
    REPORTING_CHOICES = [
        (ReportingType.PARTNER_PARTNER, 'Partner to Partner Reporting'),
    ]
    REPORTING_CHOICES.extend(PARTNER_TO_EMPLOYEE_REPORTING_CHOICES)
    REPORTING_CHOICES.extend(EMPLOYEE_TO_EMPLOYEE_REPORTING_CHOICES)
    REPORTING_CHOICES.extend(TEMPORARY_REPORTING_CHOICES)

    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    suggested_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, blank=True, null=True, default=None
    )

    manager = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='reportee'
    )

    reportee = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='manager'
    )

    reporting_type = models.PositiveSmallIntegerField(
        choices=REPORTING_CHOICES, blank=True, null=True, default=None
    )

    history = HistoricalRecords(history_change_reason_field=models.TextField(null=True))

    def save(self, *args, **kwargs):
        # If end date of the relationship is being changed, verify that it
        # isn't greater than the start date.
        if self.end_date:
            if self.end_date < self.start_date:
                raise ValidationError('End date of a relationship cannot be before the start date')

        # Validations on the data before saving to the database.
        if self.reportee == self.manager:
            raise ValidationError('Reportee cannot report to themselves.')

        if not self.id:
            # Same reportee's shouldn't have multiple manager for the
            # same reporting_type.
            if self.reportee.manager.filter(reporting_type=self.reporting_type, end_date=None):
                raise ValidationError(
                    'Cannot set reporting because this reportee already has a '
                    'manager for selected reporting_type.'
                )

            # If reportee have any reporting(except with TRM) then
            # `PARTNER_PARTNER` reporting_type shouln't allow for that reportee.
            if self.reporting_type == ReportingType.PARTNER_PARTNER:
                if self.reportee.manager.filter(end_date=None).exclude(
                    reporting_type=ReportingType.PARTNER_TELE
                ):
                    raise ValidationError(
                        'Partners already reporting to other Employee cannot be mapped to Partner.'
                    )

            qs = UserReporting.objects.filter(
                reportee=self.reportee,
                manager=self.manager,
                reporting_type=self.reporting_type,
                end_date=None,
            )

            if qs:
                user_reporting = qs.get()
                logger.info(
                    'Not creating a new reporting as %s already has %s as a manager for '
                    'reporting type %s.',
                    self.reportee.get_full_name(),
                    self.manager.get_full_name(),
                    self.get_reporting_type_display(),
                )
                return user_reporting

        # Check for updated mapping reportee's shouldn't have multiple manager for the
        # same reporting_type and for `PARTNER_PARTNER` reporting it shouldn't
        # have any other reporting.
        if self.id:
            if self.reportee.manager.filter(
                ~Q(id=self.id), reporting_type=self.reporting_type, end_date=None
            ):
                raise ValidationError(
                    'Cannot set reporting because this reportee already has a '
                    'manager for selected reporting_type.'
                )

            if self.reporting_type == ReportingType.PARTNER_PARTNER:
                if self.reportee.manager.filter(
                    ~Q(id=self.id), ~Q(reporting_type=ReportingType.PARTNER_TELE), end_date=None
                ):
                    raise ValidationError(
                        'Partners already reporting to other Employee cannot be mapped to Partner.'
                    )

        if (
            self.reporting_type in PartnerToEmployeeReportingType.__dict__.values()
            and self.manager.user_type != UserTypes.EMPLOYEE
        ):
            raise ValidationError(
                'Cannot set partner to employee reporting type for this relationship as '
                'the manager isn\'t an employee.'
            )

        # Check if the user reports to another non-employee user
        current_reporting_to_partner = self.reportee.get_current_reporting(
            reporting_type=ReportingType.PARTNER_PARTNER
        )
        # Partners already reporting to other partners cannot be
        # mapped to employees except TRM.
        if current_reporting_to_partner and current_reporting_to_partner.id != self.id:
            if (
                self.reporting_type in PartnerToEmployeeReportingType.__dict__.values()
                and self.reporting_type != ReportingType.PARTNER_TELE
            ):
                raise ValidationError(
                    'Partners already reporting to other partners cannot be mapped to employees.'
                )

        # Check if the reportee is an employee and is being mapped to
        # another employee as a partner.
        if (
            self.reportee.user_type == UserTypes.EMPLOYEE
            and self.reporting_type in PartnerToEmployeeReportingType.__dict__.values()
        ):
            raise ValidationError('Employees cannot be mapped to other employees as partners.')

        # If the reportee already reports to this manager during this time-frame
        # in any of the reporting types, throw a validation error.
        reportings = self.reportee.get_reportings_in_date_range(self.start_date, self.end_date)
        if reportings.exists():
            reportings_with_manager = reportings.filter(manager=self.manager).exclude(id=self.id)
            if reportings_with_manager.exists():
                raise ValidationError(
                    f'`{self.reportee.get_full_name()}` already reports to '
                    f'`{self.manager.get_full_name()}`.'
                )

        if self.manager.indirectly_reports_to(self.reportee, self.reporting_type):
            raise ValidationError(
                f'`{self.manager.get_full_name()}` reports to `{self.reportee.get_full_name()}` '
                f'indirectly for the reporting type ({self.get_reporting_type_display()}).'
            )

        try:
            if self._state.adding is True:  # for new object
                event_name = 'partner_mapping_created'
            else:  # self._state.adding is False: for existing object
                event_name = 'partner_mapping_modified'
            event_properties = {
                'event_bucket': ['Servicing'],
                'ams_id': self.reportee.unique_code,
                'effective_day': self.start_date.day,
                'effective_month': self.start_date.month,
                'effective_year': self.start_date.year,
                'mapping': [self.manager.unique_code, self.reportee.unique_code],
                'mapping_ams_id_resource': self.manager.unique_code
            }
            send_event(event_name, event_properties)
        except Exception:
            logger.exception('[ANALYTICS] Unbale to send report: %s', self.id)

        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'User Reportings'

    def __str__(self):
        return f'`{self.reportee}` reports to `{self.manager}`'


class DocumentType:
    PAN = 1
    AADHAAR = 2
    GST_CERT = 3
    PHOTO = 4
    CANCELLED_CHEQUE = 5
    DRIVERS_LICENSE = 6
    EDUCATION_CERTIFICATE = 7
    PASSPORT = 8


class DocumentVerificationStatus:
    PENDING = 1
    VERIFIED = 2
    DISCARDED = 3


class Document(TimeStampModel):  # Verifiable Documents
    """
    This model stores all the user related documents.
    """

    DOCUMENT_TYPE_CHOICES = (
        (None, '--'),
        (DocumentType.AADHAAR, 'Aadhaar Card'),
        (DocumentType.CANCELLED_CHEQUE, 'Cancelled Cheque'),
        (DocumentType.DRIVERS_LICENSE, 'Driver\'s License'),
        (DocumentType.EDUCATION_CERTIFICATE, 'Education Certificate'),
        (DocumentType.GST_CERT, 'GST Certificate'),
        (DocumentType.PAN, 'PAN Card'),
        (DocumentType.PHOTO, 'Photo'),
        (DocumentType.PASSPORT, 'Passport'),
    )
    STATUS_CHOICES = (
        (DocumentVerificationStatus.PENDING, 'Pending Verification'),
        (DocumentVerificationStatus.VERIFIED, 'Verified'),
        (DocumentVerificationStatus.DISCARDED, 'Discarded'),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='documents',
    )

    file = models.FileField(
        upload_to=document_upload,
        validators=[file_extension_validator],
        max_length=256,
    )
    type = models.IntegerField(choices=DOCUMENT_TYPE_CHOICES)

    name = models.CharField(max_length=128, blank=True, null=True, default=None)

    status = models.PositiveSmallIntegerField(
        choices=STATUS_CHOICES, blank=True, null=True, default=DocumentVerificationStatus.PENDING
    )

    verified_at = models.DateTimeField(blank=True, null=True, default=None)
    verified_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='verified_by',
    )
    discarded_at = models.DateTimeField(blank=True, null=True, default=None)
    discarded_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='discarded_by',
    )

    remarks = models.CharField(max_length=256, blank=True, null=True)

    def extension(self):
        return self.file.name.split('.')[-1]

    def __str__(self):
        return f'{self.get_type_display()} - {self.get_status_display()}'


class UserPincodeMapping(TimeStampModel):
    """
    This model holds the mapping between user and thier serving
    pincodes.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='user'
    )
    pincode = models.CharField(max_length=6, db_index=True)
    city = models.ForeignKey(
        City, blank=True, null=True, default=None, on_delete=models.DO_NOTHING, related_name='city'
    )

    class Meta:
        verbose_name = 'User Pincode Mapping'
        verbose_name_plural = 'User Pincode Mapping'
        unique_together = (
            'user',
            'pincode',
        )

    def __str__(self):
        return f'{self.user} serving in {self.pincode}'


class Cluster(TimeStampModel):
    """
    This model holds the cluster information.
    """

    cluster_code = models.CharField(
        max_length=64, unique=True, blank=True, null=True, editable=False, db_index=True
    )
    cluster_name = models.CharField(max_length=64, blank=True, null=True)
    group_by = models.CharField(max_length=64, blank=True, null=True)
    cluster_data = JSONField(blank=True, null=True)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    history = HistoricalRecords(history_change_reason_field=models.TextField(null=True))

    def __str__(self):
        return f'{self.cluster_name}-{self.cluster_code}'

    def save(self, *args, **kwargs):
        # If end date of the cluster is being changed, verify that it
        # isn't before the start date.
        if self.end_date:
            if self.end_date < self.start_date:
                raise ValidationError('End date of a cluster cannot be before the start date')

        super().save(*args, **kwargs)

        if not self.cluster_code:
            self.cluster_code = self.create_cluster_code()
            super().save()

    def create_cluster_code(self):
        """
        Create a cluster code for the cluster.
        Format: <clusterName/MM-YYYY/pk>
        """
        name = self.cluster_name.replace(' ', '_')
        date = datetime.strftime(self.start_date, '%m-%Y')
        return f'{name}/{date}/{self.pk}'


class UserCluster(TimeStampModel):
    """
    User Cluster mapping model.
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name='clusters'
    )
    cluster = models.ForeignKey(Cluster, on_delete=models.PROTECT, related_name='users')
    history = HistoricalRecords(history_change_reason_field=models.TextField(null=True))

    class Meta:
        verbose_name = 'UserCluster Mapping'
        verbose_name_plural = 'User Cluster Mapping'
        unique_together = (
            'user',
            'cluster',
        )

    def __str__(self):
        return f'{self.user.unique_code}-{self.cluster.cluster_code}'

    @property
    def is_mapping_active(self):
        if self.cluster.end_date is None:
            return True
        return False

    def save(self, *args, **kwargs):
        if self.user.user_type != UserTypes.PARTNER:
            raise ValidationError('Only partner can be mapped in cluster.')
        if self.cluster.end_date:
            raise ValidationError('Cluster is already deactivated.')
        try:
            # if user found, raise exception
            user_cluster = UserCluster.objects.get(user=self.user, cluster__end_date=None)
            # restrict user not to map to another cluster at same interval
            if self.cluster != user_cluster.cluster:
                raise ValidationError(
                    f'{user_cluster.user.unique_code} is already mapped to some other active cluster({user_cluster.cluster.cluster_code}).'
                )
        except UserCluster.DoesNotExist:
            pass

        if self.user.is_referral_active:
            raise ValidationError('Partner with active referrals are not allowed.')

        super().save(*args, **kwargs)


class Platform:
    WEB = 1
    APP = 2


class UserConsent(TimeStampModel):
    PLATFORM_CHOICES = (
        (Platform.WEB, 'Web'),
        (Platform.APP, 'App'),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT
    )
    consent_doc = models.ForeignKey(
        PolicyAndContract, on_delete=models.PROTECT
    )
    is_accepted = models.BooleanField(default=False)
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    platform = models.PositiveSmallIntegerField(
        choices=PLATFORM_CHOICES, blank=True, null=True
    )
    file = models.FileField(
        upload_to=document_upload,
        validators=[file_extension_validator],
        max_length=256, blank=True, null=True
    )

    history = HistoricalRecords(
        history_change_reason_field=models.TextField(null=True)
    )

    class Meta:
        verbose_name = 'User Consent'
        verbose_name_plural = 'User Consents'

    @property
    def type(self):
        return 'UserConsent'

    def save(self, *args, **kwargs):
        # Handle WhatsApp Consent due to dependency on User model
        if self.consent_doc.short_name == 'WhatsApp':
            self.user.whatsapp_notification_enabled = self.is_accepted
            self.user.save(update_fields=['whatsapp_notification_enabled'])
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.user.unique_code}-{self.consent_doc}'
