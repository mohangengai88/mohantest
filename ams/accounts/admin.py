from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjUserAdmin
from django.db.models import Q
from simple_history.admin import SimpleHistoryAdmin

from ams.accounts.forms import UserCreationForm
from ams.accounts.managers import UserManager
from ams.accounts.models import (
    Channel,
    Document,
    Cluster,
    UserCluster,
    UserConsent,
    UserReporting,
    UserPincodeMapping,
    BetaFeature,
)
from ams.base.admin import InputFilter
from ams.utils.data_mask import hash_field


class AMSAdmin(admin.ModelAdmin):
    def _user_can_write(self, request):
        groups = [x.name for x in request.user.groups.all()]
        return 'can_edit' in groups

    def has_add_permission(self, request):
        return request.user.is_superuser or self._user_can_write(request)

    def has_delete_permission(self, request, obj=None):
        return request.user.is_superuser


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'channel_head')


@admin.register(BetaFeature)
class BetaFeatureAdmin(admin.ModelAdmin):
    list_display = ('feature_name', 'feature_code', 'start_date', 'end_date')


class DocumentInlineAdmin(admin.StackedInline):
    model = Document
    raw_id_fields = ('verified_by', 'discarded_by')
    fk_name = 'user'
    extra = 0


class EmailFilter(InputFilter):
    parameter_name = 'email'
    title = 'Email'

    def queryset(self, request, queryset):
        if self.value() is not None:
            email = self.value()

            return queryset.filter(Q(hashed_email=hash_field(UserManager.normalize_email(email))))


class MobileFilter(InputFilter):
    parameter_name = 'mobile'
    title = 'Mobile'

    def queryset(self, request, queryset):
        if self.value() is not None:
            mobile = self.value()

            return queryset.filter(Q(hashed_mobile=hash_field(mobile)))


class ChangedDataSimpleHistoryAdmin(SimpleHistoryAdmin):
    """
    Custom history settings for displaying changed fields and changed values
    """

    history_list_display = ['changed_fields', 'changed_values']

    def changed_fields(self, obj):
        """
        Function for getting the changed fields
        """
        if obj.prev_record:
            delta = obj.diff_against(obj.prev_record)
            return delta.changed_fields
        return None

    def changed_values(self, obj):
        """
        Function for getting the changed values with respective fields
        """
        changed_fields = self.changed_fields(obj) or []
        changed_values = {}
        for fields in changed_fields:
            changed_values.update(
                {fields: [obj.__getattribute__(fields), obj.prev_record.__getattribute__(fields)]}
            )
        return changed_values


@admin.register(get_user_model())
class UserAdmin(AMSAdmin, DjUserAdmin, ChangedDataSimpleHistoryAdmin):
    add_form = UserCreationForm
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    date_hierarchy = 'created_at'
    list_display = (
        'email',
        'first_name',
        'last_name',
        'user_type',
        'unique_code',
        'pos_code',
        'agency_name',
        'is_active',
    )
    list_filter = (
        EmailFilter,
        MobileFilter,
        'user_type',
        'is_staff',
        'is_superuser',
        'is_active',
        'groups',
    )
    readonly_fields = (
        'created_at',
        'primary_address_address_line1',
        'primary_address_address_line2',
        'primary_address_address_line3',
        'primary_address_city',
        'primary_address_pincode',
        'secondary_address_address_line1',
        'secondary_address_address_line2',
        'secondary_address_address_line3',
        'secondary_address_city',
        'secondary_address_pincode',
        'unique_code',
        'referral_code',
        'pos_code',
        'gi_pos_certified_date',
        'li_pos_certified_date',
        'pan_type',
        'gstin_address_address_line1',
        'gstin_address_address_line2',
        'gstin_address_address_line3',
        'gstin_address_city',
        'gstin_address_pincode',
        'whatsapp_notification_enabled',
    )
    raw_id_fields = ('primary_address', 'secondary_address', 'gstin_address')

    fieldsets = (
        (None, {'fields': ('_email', 'password')}),
        (
            'Personal info',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': (
                    ('first_name', 'middle_name', 'last_name'),
                    'referral_code',
                    'is_referral_active',
                ),
            },
        ),
        (
            'Basic Details',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': (
                    'unique_code',
                    'channel',
                    'gender',
                    'dob',
                    '_mobile',
                    'caller_id',
                    'photo',
                    'agency_name',
                    'occupation_type',
                    'marital_status',
                    'educational_qualification',
                    'annual_income',
                    'preferred_lang_written',
                    'preferred_lang_spoken',
                    'date_of_anniversary',
                    'insurance_business',
                ),
            },
        ),
        ('Communication Preferences', {"fields": ('whatsapp_notification_enabled',)}),
        (
            'Roles/Permissions',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': (
                    'user_type',
                    'dialer_agent_id',
                    ('is_active', 'is_staff', 'is_superuser'),
                    'groups',
                    'user_permissions',
                    'beta_enrollment',
                ),
            },
        ),
        ('Important dates', {'fields': ('last_login', 'created_at')}),
        (
            'Verified Details',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': (
                    'is_email_verified',
                    'is_mobile_verified',
                    'is_photo_verified',
                    ('_pan_number', 'is_pan_verified', 'pan_type'),
                    ('_aadhaar_number', 'is_aadhaar_verified'),
                    ('_gstin_number', 'is_gst_verified'),
                    ('_drivers_license_number', 'is_dl_verified'),
                    ('_education_cert_number', 'is_education_cert_verified'),
                    (
                        '_bank_account_number',
                        '_ifsc_code',
                        'bank_name',
                        'bank_branch',
                        'is_bank_account_verified',
                    ),
                    ('_passport_number', 'is_passport_verified'),
                    'pos_code',
                    'gi_pos_certified_date',
                    'li_pos_certified_date',
                    'gi_pos_status',
                    'li_pos_status',
                ),
            },
        ),
        (
            'Addresses',
            {
                'classes': ('wide', 'extrapretty'),
                'fields': (
                    'primary_address',
                    'primary_address_address_line1',
                    'primary_address_address_line2',
                    'primary_address_address_line3',
                    ('primary_address_city', 'primary_address_pincode'),
                    'secondary_address',
                    'secondary_address_address_line1',
                    'secondary_address_address_line2',
                    'secondary_address_address_line3',
                    ('secondary_address_city', 'secondary_address_pincode'),
                    'gstin_address',
                    'gstin_address_address_line1',
                    'gstin_address_address_line2',
                    'gstin_address_address_line3',
                    ('gstin_address_city', 'gstin_address_pincode'),
                ),
            },
        ),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (
            None,
            {
                'classes': ('wide',),
                'fields': ('hashed_email', 'password1', 'password2'),
            },
        ),
    )

    search_fields = (
        'id',
        'first_name',
        'last_name',
        'agency_name',
        'unique_code',
        'pos_code',
        'referral_code',
    )
    ordering = ('-created_at',)
    filter_horizontal = ()
    inlines = [DocumentInlineAdmin]

    def primary_address_address_line1(self, obj):
        return obj.primary_address.address_line1

    primary_address_address_line1.short_description = 'Primary Address Line 1'
    primary_address_address_line1.empty_value_display = ''

    def primary_address_address_line2(self, obj):
        return obj.primary_address.address_line2

    primary_address_address_line2.short_description = 'Primary Address Line 2'
    primary_address_address_line2.empty_value_display = ''

    def primary_address_address_line3(self, obj):
        return obj.primary_address.address_line3

    primary_address_address_line3.short_description = 'Primary Address Line 3'
    primary_address_address_line3.empty_value_display = ''

    def primary_address_city(self, obj):
        return obj.primary_address.city

    primary_address_city.short_description = 'Primary Address City'
    primary_address_city.empty_value_display = ''

    def primary_address_pincode(self, obj):
        return obj.primary_address.pincode

    primary_address_pincode.short_description = 'Primary Address Pin Code'
    primary_address_pincode.empty_value_display = ''

    def secondary_address_address_line1(self, obj):
        return obj.secondary_address.address_line1

    secondary_address_address_line1.short_description = 'Secondary Address Line 1'
    secondary_address_address_line1.empty_value_display = ''

    def secondary_address_address_line2(self, obj):
        return obj.secondary_address.address_line2

    secondary_address_address_line2.short_description = 'Secondary Address Line 2'
    secondary_address_address_line2.empty_value_display = ''

    def secondary_address_address_line3(self, obj):
        return obj.secondary_address.address_line3

    secondary_address_address_line3.short_description = 'Secondary Address Line 3'
    secondary_address_address_line3.empty_value_display = ''

    def secondary_address_city(self, obj):
        return obj.secondary_address.city

    secondary_address_city.short_description = 'Secondary Address City'
    secondary_address_city.empty_value_display = ''

    def secondary_address_pincode(self, obj):
        return obj.secondary_address.pincode

    secondary_address_pincode.short_description = 'Secondary Address Pin Code'
    secondary_address_pincode.empty_value_display = ''

    def gstin_address_address_line1(self, obj):
        return obj.gstin_address.address_line1

    gstin_address_address_line1.short_description = 'GSTIN Address Line 1'
    gstin_address_address_line1.empty_value_display = ''

    def gstin_address_address_line2(self, obj):
        return obj.gstin_address.address_line2

    gstin_address_address_line2.short_description = 'GSTIN Address Line 2'
    gstin_address_address_line2.empty_value_display = ''

    def gstin_address_address_line3(self, obj):
        return obj.gstin_address.address_line3

    gstin_address_address_line3.short_description = 'GSTIN Address Line 3'
    gstin_address_address_line3.empty_value_display = ''

    def gstin_address_city(self, obj):
        return obj.gstin_address.city

    gstin_address_city.short_description = 'GSTIN Address City'
    gstin_address_city.empty_value_display = ''

    def gstin_address_pincode(self, obj):
        return obj.gstin_address.pincode

    gstin_address_pincode.short_description = 'GSTIN Address Pin Code'
    gstin_address_pincode.empty_value_display = ''


class ReportingManagerEmailFilter(InputFilter):
    parameter_name = 'manager_email'
    title = 'Manager Email'

    def queryset(self, request, queryset):
        if self.value() is not None:
            email = self.value()

            return queryset.filter(
                Q(manager__hashed_email=hash_field(UserManager.normalize_email(email)))
            )


class ReportingReporteeEmailFilter(InputFilter):
    parameter_name = 'reportee_email'
    title = 'Reportee Email'

    def queryset(self, request, queryset):
        if self.value() is not None:
            email = self.value()

            return queryset.filter(
                Q(reportee__hashed_email=hash_field(UserManager.normalize_email(email)))
            )


class IsMappingActiveFilter(admin.SimpleListFilter):
    title = 'Mapping Active'
    parameter_name = 'is_mapping_active'

    def lookups(self, request, model_admin):
        return (
            (True, True),
            (False, False),
        )

    def queryset(self, request, queryset):
        if self.value() in ['True', 'False']:
            return self.is_cluster_active(queryset)
        return queryset

    def is_mapping_active(self, queryset):
        if self.value() == 'True':
            reporting = queryset.filter(end_date=None)
        else:
            reporting = queryset.filter(~Q(end_date=None))
        return reporting


@admin.register(UserReporting)
class UserReportingAdmin(SimpleHistoryAdmin):
    list_display = ('manager', 'reportee', 'start_date', 'end_date', 'reporting_type')
    list_filter = (
        ReportingManagerEmailFilter,
        ReportingReporteeEmailFilter,
        IsMappingActiveFilter,
        'reporting_type',
    )
    search_fields = (
        'manager__first_name',
        'manager__last_name',
        'reportee__first_name',
        'reportee__last_name',
        'manager__unique_code',
        'reportee__unique_code',
        'reportee__pos_code',
    )
    readonly_fields = ('created_at', 'modified_at')
    raw_id_fields = ('manager', 'reportee', 'suggested_by')


@admin.register(UserPincodeMapping)
class UserPincodeMappingAdmin(SimpleHistoryAdmin):
    list_display = ('user', 'city', 'pincode')
    search_fields = (
        'user__unique_code',
        'user__pos_code',
        'user__first_name',
        'user__last_name',
        'pincode',
        'city__city_name',
    )
    readonly_fields = ('created_at', 'modified_at')
    raw_id_fields = ('user', 'city')


class IsClusterActiveFilter(admin.SimpleListFilter):
    title = 'Cluster Active'
    parameter_name = 'is_cluster_active'

    def lookups(self, request, model_admin):
        return (
            (True, True),
            (False, False),
        )

    def queryset(self, request, queryset):
        if self.value() in ['True', 'False']:
            return self.is_cluster_active(self.value())
        return queryset

    def is_cluster_active(self, value):
        if value == 'True':
            cluster = Cluster.objects.filter(end_date=None)
        else:
            cluster = Cluster.objects.filter(~Q(end_date=None))
        return cluster


class DataFilter(InputFilter):
    parameter_name = 'data'
    title = 'Cluster Data'

    def queryset(self, request, queryset):
        if self.value() is not None:
            data = self.value()

            return queryset.filter(cluster_data__contains={'primary_address__pincode__in': [data]})


@admin.register(Cluster)
class ClusterAdmin(ChangedDataSimpleHistoryAdmin):
    list_display = ('cluster_name', 'cluster_code', 'start_date', 'end_date', 'is_active')
    search_fields = ('cluster_name', 'cluster_code')
    list_filter = (
        DataFilter,
        IsClusterActiveFilter,
    )

    readonly_fields = ('cluster_code', 'created_at', 'modified_at')

    def is_active(self, obj):
        if obj.end_date is None:
            return True
        return False

    is_active.boolean = True


@admin.register(UserCluster)
class UserClusterAdmin(ChangedDataSimpleHistoryAdmin):
    list_display = ('user', 'cluster', 'is_mapping_active')
    search_fields = (
        'user__unique_code',
        'user__pos_code',
        'cluster__cluster_name',
        'cluster__cluster_code',
    )
    list_filter = ('cluster',)
    readonly_fields = ('created_at', 'modified_at', 'is_mapping_active')
    raw_id_fields = ('user', 'cluster')

    def is_mapping_active(self, obj):
        return obj.is_mapping_active

    is_mapping_active.boolean = True


class ConsentUserEmailFilter(InputFilter):
    parameter_name = 'email'
    title = 'Email'

    def queryset(self, request, queryset):
        if self.value() is not None:
            email = self.value()
            return queryset.filter(Q(user__hashed_email=hash_field(UserManager.normalize_email(email))))


@admin.register(UserConsent)
class UserConsentAdmin(ChangedDataSimpleHistoryAdmin):
    list_display = ('user', 'consent_doc', 'ip_address', 'is_accepted')
    raw_id_fields = ('user', 'consent_doc')
    list_filter = (ConsentUserEmailFilter, 'is_accepted',)

    search_fields = (
        'user__unique_code', 'user__pos_code',
        'consent_doc__title', 'consent_doc__short_name'
    )
    readonly_fields = (
        'ip_address', 'file', 'platform',
        'created_at', 'modified_at'
    )

    fieldsets = (
        (None, {
            'fields': (
                'user', 'consent_doc', 'is_accepted', 'platform',
                'ip_address', 'file', 'created_at', 'modified_at'
            )
        }),
    )
