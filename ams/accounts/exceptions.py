from rest_framework.exceptions import APIException


class InvalidLoginException(APIException):
    status_code = 400
    default_detail = 'Invalid login credentials provided.'


class InvalidUserTypeException(APIException):
    status_code = 400
    default_detail = 'Invalid user type selected.'


class UserRequiredException(APIException):
    status_code = 400
    default_detail = 'Please send a valid AMS ID.'


class RbSignupException(APIException):

    def __init__(self, detail=None, status_code=None):
        if detail is not None:
            self.default_detail = detail
        if status_code is not None:
            self.status_code = status_code

        super().__init__()


class OptUserException(APIException):
    status_code = 400
    default_detail = "Invalid request data."


class NotificationAPIException(APIException):
    status_code = 400
    default_detail = "Notification API error."
