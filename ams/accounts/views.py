import csv
import glob
import io
import json
import logging
import requests
from datetime import datetime
from subprocess import check_call

from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError, PermissionDenied
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import (DetailView, ListView, TemplateView,
                                  View)
from glue.registry import call

from ams.accounts.helper import manager_column_header, reporting_type_dict
from ams.accounts.managers import UserManager
from ams.accounts.models import (Document, DocumentType,
                                 DocumentVerificationStatus, ReportingType, User,
                                 UserReporting, UserTypes)
from ams.accounts.services import dashboard_login_permission
from ams.accounts.tokens import email_verification_token
from ams.utils import HttpResponseReload, data_mask, email, sms, timezone

logger = logging.getLogger(__name__)


def email_verify(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and email_verification_token.check_token(user, token):
        user.is_email_verified = True
        user.is_active = True
        user.save()
        return HttpResponse('Thank you for your email confirmation.')
    return HttpResponse('Activation link is invalid!')


# @xframe_options_exempt
class IndexView(TemplateView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        context = {
            'allowed_domains': settings.ALLOWED_DOMAINS
        }
        return render(request, 'account.html', context)

    def post(self, request):
        username = request.POST.get('email')
        password = request.POST.get('password')
        user = call(
            'accounts',
            'authenticate',
            data={
                'username': username,
                'password': password
            }
        )
        context = {
            'allowed_domains': settings.ALLOWED_DOMAINS,
            'login_response': user,
            'login_response_json': json.dumps(user)
        }
        return render(request, 'account.html', context)


@xframe_options_exempt
def frame(request):
    context = {'allowed_domains': settings.ALLOWED_DOMAINS}
    return render(request, 'windowFrame.html', context)


def profile(request):
    return render(request, 'user_profile_detail.html')


def signUp(request):
    return render(request, 'sign_up.html')


class DashboardView(TemplateView):
    template_name = 'accounts/index.html'

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = {
            'all_users': User.objects.all().count(),
            'new_registrations_yesterday': User.objects.filter(
                created_at__date=(
                    timezone.now_local(only_date=True) - timezone.timedelta(days=1)
                )
            ).count(),
            'new_registrations_today': User.objects.filter(
                created_at__date=timezone.now_local(only_date=True)
            ).count(),
        }
        return context


class UsersView(ListView):
    model = User
    template_name = 'accounts/users.html'
    paginate_by = 25

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        search_filter = Q()

        if self.request.method == 'GET':
            search = self.request.GET.get('search', '')
            search_parts = search.split(' ')
            for part in search_parts:
                q = Q(Q(first_name__icontains=part) |
                      Q(last_name__icontains=part) |
                      Q(middle_name__icontains=part) |
                      Q(pos_code__icontains=part) |
                      Q(referral_code__icontains=part) |
                      Q(hashed_email=data_mask.hash_field(UserManager.normalize_email(part))) |
                      Q(hashed_mobile=data_mask.hash_field(part)) |
                      Q(unique_code__icontains=part))
                search_filter = search_filter | q if search_filter else q

        users = User.objects.filter(
            search_filter
        ).exclude(user_type=UserTypes.EMPLOYEE)
        return users


class ProfileView(DetailView):
    model = User
    queryset = User.objects.exclude(user_type=UserTypes.EMPLOYEE)
    template_name = 'accounts/user_profile.html'

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = context.pop('user')
        docs_list = {}
        verified_docs = user.documents.filter(
            status=DocumentVerificationStatus.VERIFIED
        )
        for doc in verified_docs:
            docs_list[doc.type] = doc.file.url
        context.update({
            'docs': user.documents.filter(
                status=DocumentVerificationStatus.PENDING
            ).order_by('created_at'),
            'verified_docs': docs_list,
            'current_reportees': user.reportee.all().order_by('-start_date'),
            'reporting_history': user.manager.all().order_by('-start_date'),
            'max_date_allowed': (
                timezone.now_local(only_date=True) + relativedelta(months=1)
            ).replace(day=1).strftime('%Y-%m-%d'),
            'min_date_allowed': (
                timezone.now_local(only_date=True)
            ).strftime('%Y-%m-%d'),
            'reporting_types': {
                key: value for key, value in UserReporting.PARTNER_TO_EMPLOYEE_REPORTING_CHOICES if key
            }
        })
        return context


class EmployeeProfileView(ProfileView):
    model = User
    queryset = User.objects.filter(user_type=UserTypes.EMPLOYEE)
    template_name = 'accounts/employee_profile.html'

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'reporting_types': {
                key: value for key, value in UserReporting.EMPLOYEE_TO_EMPLOYEE_REPORTING_CHOICES if key
            }
        })
        return context


class DocsView(ListView):
    model = User
    template_name = 'accounts/docs.html'
    paginate_by = 10

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        search_filter = Q()

        if self.request.method == 'GET':
            search = self.request.GET.get('search', '')
            search_parts = search.split(' ')
            for part in search_parts:
                q = Q(Q(user__first_name__icontains=part) |
                      Q(user__last_name__icontains=part) |
                      Q(user__middle_name__icontains=part) |
                      Q(user__hashed_email=data_mask.hash_field(UserManager.normalize_email(part))) |
                      Q(user__hashed_mobile=data_mask.hash_field(part)) |
                      Q(user__unique_code__icontains=part) |
                      Q(user__agency_name__icontains=part))
                search_filter = search_filter | q if search_filter else q

        docs = Document.objects.filter(
            search_filter,
            status=DocumentVerificationStatus.PENDING,
        ).exclude(user__user_type=UserTypes.CUSTOMER).order_by('created_at')
        return docs


class SetReferralView(View):
    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        return redirect('docs')

    def post(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=request.POST.get('user'))
        code = request.POST.get('code', '').strip()
        is_bp_corporate = request.POST.get('user_type')
        deactivate_referral_code = request.POST.get('is_referral_active')

        if deactivate_referral_code:
            user.is_referral_active = False
            user.save(update_fields=['is_referral_active'])

        else:
            users_with_same_referral = User.objects.filter(referral_code=code).exclude(id=user.id)
            if users_with_same_referral.exists():
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Unable to set referral code `{code}` as same is already set '
                    f'for `{users_with_same_referral.first().unique_code}`'
                )
            elif user.gi_pos_status or user.li_pos_status:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Can\'t set referral code for {user} as they are POS certified and aren\'t allowed'
                    f' to have reportees under them.'
                )
            else:
                user.referral_code = code
                user.is_referral_active = True
                if is_bp_corporate:
                    user.user_type = UserTypes.PYRAMID_MANAGER
            user.save(update_fields=['referral_code', 'is_referral_active', 'user_type'])

        return HttpResponseReload(request)


class DocAcceptView(View):
    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        return redirect('docs')

    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(Document, pk=request.POST.get('doc'))

        # Update the user table with the new verified document values.
        user = doc.user
        if doc.type == DocumentType.PAN:
            user.pan_number = doc.name
            user.is_pan_verified = True
        if doc.type == DocumentType.AADHAAR:
            user.aadhaar_number = doc.name
            user.is_aadhaar_verified = True
        if doc.type == DocumentType.GST_CERT:
            user.gstin_number = doc.name
            user.is_gst_verified = True
        if doc.type == DocumentType.PHOTO:
            user.photo = doc.file
            user.is_photo_verified = True
        if doc.type == DocumentType.CANCELLED_CHEQUE:
            acc_no_ifsc = doc.name.split(',')
            user.bank_account_number = acc_no_ifsc[0].split(':')[-1].strip()
            user.ifsc_code = acc_no_ifsc[1].split(':')[-1].strip()
            user.is_bank_account_verified = True
        if doc.type == DocumentType.DRIVERS_LICENSE:
            user.drivers_license_number = doc.name
            user.is_dl_verified = True
        if doc.type == DocumentType.EDUCATION_CERTIFICATE:
            user.education_cert_number = doc.name
            user.is_education_cert_verified = True
        if doc.type == DocumentType.PASSPORT:
            user.passport_number = doc.name
            user.is_passport_verified = True

        try:
            if doc.type not in (DocumentType.EDUCATION_CERTIFICATE,
                                DocumentType.PHOTO):
                # If another document of the same type with the same name has
                # already been verified, raise a `ValidationError`.
                if Document.objects.filter(type=doc.type,
                                           status=DocumentVerificationStatus.VERIFIED,
                                           name__iexact=doc.name).exists():
                    raise ValidationError(
                        '`{}` cannot be verified because it has already been '
                        'verified for another user. Please reject it.'.format(
                            doc.get_type_display(),
                        )
                    )

            # Validate the user's information. If this fails, a
            # `ValidationError` exception will be raised
            user.full_clean()  # For all the field level validations in model.

            if user.bank_account_number in ('None', ''):
                raise ValidationError('Bank account number cannot be `None`.')

            # Update the user's accepted documents.
            user.save()

            # After we are done verifying that the document's values adhere to
            # the field level validations in the User table, mark the document
            # as verified.
            doc.status = DocumentVerificationStatus.VERIFIED
            doc.verified_at = timezone.now_local()
            # Set the user by whom the action was taken.
            doc.verified_by = request.user
            doc.save()

            # If the user is from the TravAssured channel and the document being
            # marked as verified is a PAN Card, send an email confirming the
            # activation of their account.
            if doc.user.unique_code.startswith('TA') and \
                    doc.type == DocumentType.PAN:
                context = {
                    'domain': settings.DOMAIN,
                    'ta_domain': settings.TA_DOMAIN,
                }
                email.send_from_template(
                    to=doc.user.decrypted_email,
                    subject='TravAssured - Account Activation',
                    template='ta_pan_request_accepted.html',
                    context=context,
                    from_email=settings.TRAVASSURED_FROM_EMAIL
                )
                sms.send(
                    doc.user.decrypted_mobile,
                    settings.SMS_TEMPLATE['TA']['ACTIVATION']
                )

        except ValidationError as e:
            # Some field level validation failed. User is trying to accept some
            # document where the value isn't adhering to basic format of the
            # document number/identifier.
            for message in e.messages:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Unable to accept `{}` of `{}`, because of: `{}`'.format(
                        doc.get_type_display(),
                        user.unique_code,
                        message,
                    )
                )
        except Exception:
            messages.add_message(
                request,
                messages.ERROR,
                'Oops! Something went wrong. '
                'Please contact the system administrator.'
            )

        return HttpResponseReload(request)


class DocRejectView(View):
    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        return redirect('docs')

    def post(self, request, *args, **kwargs):
        doc = get_object_or_404(Document, pk=request.POST.get('doc'))
        # Mark the document as discarded.
        doc.status = DocumentVerificationStatus.DISCARDED
        doc.discarded_at = timezone.now_local()
        # Set the user by whom the action was taken.
        doc.discarded_by = request.user
        doc.save()

        # If the user is from the TravAssured channel and the document being
        # marked as rejected is a PAN Card, send an email requiring a re-upload
        # of their document.
        if doc.user.unique_code.startswith('TA') and \
                doc.type == DocumentType.PAN:
            context = {
                'domain': settings.DOMAIN,
            }
            email.send_from_template(
                to=doc.user.decrypted_email,
                subject='TravAssured - PAN Card Re-Upload Required',
                template='ta_pan_request_rejected.html',
                context=context,
                from_email=settings.TRAVASSURED_FROM_EMAIL
            )
            sms.send(
                doc.user.decrypted_mobile,
                settings.SMS_TEMPLATE['TA']['FAILURE']
            )

        return HttpResponseReload(request)


class EmployeesView(ListView):
    model = User
    template_name = 'accounts/employees.html'
    paginate_by = 25

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        search_filter = Q()

        if self.request.method == 'GET':
            search = self.request.GET.get('search', '')
            search_parts = search.split(' ')
            for part in search_parts:
                q = Q(Q(first_name__icontains=part) |
                      Q(last_name__icontains=part) |
                      Q(middle_name__icontains=part) |
                      Q(hashed_email=data_mask.hash_field(UserManager.normalize_email(part))) |
                      Q(hashed_mobile=data_mask.hash_field(part)) |
                      Q(unique_code__icontains=part))
                search_filter = search_filter | q if search_filter else q

        emps = User.objects.filter(
            search_filter, user_type=UserTypes.EMPLOYEE, is_active=True
        ).order_by('first_name')
        return emps

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data.update({
            'max_date_allowed': (
                timezone.now_local(only_date=True) + relativedelta(months=1)
            ).replace(day=1).strftime('%Y-%m-%d'),
            'min_date_allowed': (
                timezone.now_local(only_date=True)
            ).strftime('%Y-%m-%d'),
        })
        return context_data


class ChangeReportingView(View):
    """
        View called while changing the reporting of some employee
    """

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        return redirect('employees')

    def post(self, request, *args, **kwargs):
        # Get the user making the change in the system.
        change_requesting_user = self.request.user

        # Get the user whose manager is being replaced.
        user = get_object_or_404(User, pk=request.POST.get('employee'))

        # Get the reporting type from the request if sent.
        reporting_type = request.POST.get('reporting_type', None)

        if reporting_type:
            reporting_type = int(reporting_type)

        # Create a datetime instance from the string received in the request.
        start_date_of_new_reporting = datetime.strptime(
            request.POST.get('start_date'), '%Y-%m-%d'
        ).date()

        # Get the new manager's ID from the request.
        new_reporting_manager = int(request.POST.get('manager_id'))

        # Raise an error if the user is being assigned to themselves.
        if user.id == new_reporting_manager:
            messages.add_message(
                request, messages.ERROR,
                f'`{user.first_name}` cannot be assigned to themselves.'
            )
        else:
            # Get the reporting of the user if it exists on the given
            # `start_date`.
            user_reporting_on_date = user.get_reporting_on_date(
                start_date_of_new_reporting, reporting_type
            )

            if not user_reporting_on_date or not \
                    user_reporting_on_date.end_date:
                # Only continue if the reporting of the user doesn't exist
                # on the given date or if the one that exists hasn't ended yet.
                try:
                    if user_reporting_on_date:
                        # TODO: After RBAC, check permissions of
                        # TODO: the `change_requesting_user`.
                        # TODO: If the user doesn't have the required permissions,
                        # TODO: throw an error.
                        if user_reporting_on_date.start_date >= start_date_of_new_reporting:
                            raise ValidationError(
                                'New reporting\'s start date has to be greater '
                                'than the old one\'s start date.'
                            )

                        # If the employee is changing their current reporting,
                        # they are only allowed to move this reportee to one of
                        # their other current reportees.
                        if user_reporting_on_date.manager == \
                                change_requesting_user:
                            all_current_reportees_of_requesting_user = \
                                change_requesting_user.reportee.filter(
                                    Q(end_date__gte=start_date_of_new_reporting) |
                                    Q(end_date=None),
                                    start_date__lte=start_date_of_new_reporting
                                ).values_list('reportee__id', flat=True)

                            if new_reporting_manager not in \
                                    all_current_reportees_of_requesting_user:
                                raise ValidationError(
                                    '`{}` is only allowed to change reporting '
                                    'of `{}` to one of their direct reportees '
                                    'on `{}`.'.format(
                                        change_requesting_user.first_name,
                                        user.first_name,
                                        start_date_of_new_reporting.strftime(
                                            '%b %d, %Y'
                                        )
                                    )
                                )

                        if user_reporting_on_date.manager.id == \
                                new_reporting_manager:
                            # Raise a ValidationError if the user is already
                            # reporting to the same person.
                            raise ValidationError(
                                '`{}` is already reporting to `{}` '
                                'on `{}`.'.format(
                                    user.first_name,
                                    user_reporting_on_date.manager.first_name,
                                    start_date_of_new_reporting.strftime(
                                        '%b %d, %Y'
                                    )
                                )
                            )

                        # End the current reporting of the user and set the end
                        # date as `start_date - 1`
                        user_reporting_on_date.end_date = \
                            start_date_of_new_reporting - \
                            timezone.timedelta(days=1)
                        user_reporting_on_date.save(update_fields=['end_date'])
                        reporting_type = user_reporting_on_date.reporting_type

                    # Create a new reporting from the given `start_date`.
                    user.manager.create(
                        start_date=start_date_of_new_reporting,
                        manager_id=new_reporting_manager,
                        reporting_type=reporting_type
                    )
                except ValidationError as e:
                    messages.add_message(request, messages.ERROR, e.message)
                except Exception:
                    logger.exception('Something went wrong while changing user hierarchy.')
                    messages.add_message(
                        request,
                        messages.ERROR,
                        'Oops! Something went wrong. '
                        'Please contact the system administrator.'
                    )
            else:
                # User already reports to someone on the given date.
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Reporting for `{}` already exists on `{}`.'.format(
                        user.first_name,
                        start_date_of_new_reporting.strftime('%b %d, %Y')
                    )
                )

        return HttpResponseReload(request)


class EmployeeTerminationView(View):
    """
        View called while terminating a given employee's employment.
    """

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        return redirect('employees')

    def post(self, request, *args, **kwargs):
        # Get the user who is being terminated.
        user = get_object_or_404(User, pk=request.POST.get('employee'))
        new_manager = get_object_or_404(User, pk=request.POST.get('new_manager'))

        today = timezone.now_local(only_date=True)

        # Initialize the current reporting as `None`.
        current_reporting = None

        # Get all the current reportees.
        reportees = user.reportee.filter(start_date__lte=today, end_date=None)

        # Get all the future reportees.
        future_reportees = user.reportee.filter(start_date__gt=today)

        # Get all the reportings from now.
        reportings_from_now = user.get_current_and_future_reportings()
        if reportings_from_now:
            try:
                # Get the current reporting.
                current_reporting = reportings_from_now.get(
                    start_date__lte=today
                )
            except UserReporting.DoesNotExist:
                pass

            # Get all the future reportings.
            future_reportings = reportings_from_now.exclude(
                start_date__lte=today
            )
            # Delete all the future reportings.
            future_reportings.delete()

            # Only end the current reporting, if one actually exists.
            if current_reporting:
                # Get the current manager.
                manager = new_manager or current_reporting.manager

                # Update the manager in all the future reportings to the
                # current supervisor.
                if future_reportees:
                    for report in future_reportees:
                        report.manager = manager
                        report.save(update_fields=['manager'])

                # Iterate over all the current reports, end the current
                # relationship with them. Start a new relationship with the
                # current supervisor.
                for reporting in reportees:
                    reportee = reporting.reportee
                    # Setting the `end_date` of the current reporting.
                    reporting.end_date = today
                    reporting.save(update_fields=['end_date'])
                    # Assign the reportee to the current supervisor.
                    reportee.manager.create(
                        manager=manager,
                        start_date=today + relativedelta(days=1),
                        reporting_type=reporting.reporting_type,
                    )

                # End the relationship with the current supervisor.
                current_reporting.end_date = today
                current_reporting.save(update_fields=['end_date'])

        # If the user doesn't have a supervisor to map the current reportees to.
        elif not reportings_from_now and not current_reporting and \
                (reportees or future_reportees):
            messages.add_message(
                request,
                messages.ERROR,
                f'`{user.first_name}` cannot be terminated as they have people reporting to '
                f'them and don\'t have a supervisor to map the current reportings to. '
                f'Please move all the current reportings to someone '
                f'else before proceeding any further.'
            )

            return HttpResponseReload(request)

        # Deactivate the user.
        user.is_active = False
        user.save(update_fields=['is_active'])

        return HttpResponseReload(request)


class ChangeMappingView(TemplateView):
    template_name = 'upload.html'

    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        if self.request.user.decrypted_email not in settings.BULK_MAPPING_VIEW_ALLOWED_USERS:
            raise PermissionDenied()
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ChangeMappingView, self).get_context_data(**kwargs)
        if 't2/' in self.request.path:
            fields = [(ReportingType.PARTNER_TELE, 'Partner to Tele-RM Reporting')]
        elif 'hr/' in self.request.path:
            fields = UserReporting.EMPLOYEE_TO_EMPLOYEE_REPORTING_CHOICES
        else:
            fields = UserReporting.PARTNER_TO_EMPLOYEE_REPORTING_CHOICES
            fields.extend(
                [(ReportingType.PARTNER_PARTNER, 'Partner to Partner Reporting')]
            )
            # Remove the Tele-RM reporting from Ops portal
            try:
                fields.remove(
                    (ReportingType.PARTNER_TELE, 'Partner to Tele-RM Reporting')
                )
            except Exception:
                pass

        # Below logic for return field value as thier respective names.
        # e.g. [('FRM', 'Partner to Field-RM Reporting')]
        result_fields = []
        for key, value in reporting_type_dict.items():
            if value in dict(fields):
                result_fields.append((
                    key, dict(fields).get(value)
                ))
        context = {
            'fields': result_fields
        }
        return context

    def get(self, request):
        return render(
            request, self.template_name, context=self.get_context_data()
        )

    def post(self, request, *args, **kwargs):
        reporting_type = request.POST.get('reporting_type')
        file = self.request.FILES['file']
        if file and reporting_type:
            if file.name.endswith('.csv'):
                status, msg = self.handle_uploaded_file(file, reporting_type)
                if status:
                    messages.success(self.request, msg)
                else:
                    messages.error(self.request, msg)
            else:
                messages.error(self.request, 'Please upload only csv file.')
        return render(
            request, self.template_name, context=self.get_context_data()
        )

    def handle_uploaded_file(self, file, reporting_type):
        """
        Description of File name saved which are uploaded by user.

        {reporting_type}_mapping_{original_file_name}-{email}-{counter}.csv

        where:
            reporting_type: Reporting type for which the mapping is selected.
            original_file_name: file name uploaded by user
            email: Email of uploader
            counter: Counts the no. of same files uploaded by user on same day.
        """
        file.seek(0)
        csv_file = csv.DictReader(io.StringIO(file.read().decode('utf-8')))
        if self.is_header_allowed(csv_file.fieldnames, reporting_type):
            current_date = timezone.now_local(only_date=True)
            override_dir = '/mapping/{}/{}/{}'.format(
                str(current_date.year).zfill(2),
                str(current_date.month).zfill(2),
                str(current_date.day).zfill(2)
            )
            partial_name = '{reporting_type}_mapping_{file_name}-{email}-'.format(
                reporting_type=reporting_type,
                file_name=file.name.replace('.csv', ''),
                email=self.request.user.decrypted_email
            )

            # Create a directory if not exists.
            try:
                check_call(['mkdir', '-p', override_dir])
            except Exception:
                logger.exception('Directory[%s] already exists.')

            # get the file number in override_dir if file exists.
            file_counter = max([
                int(
                    i.replace(
                        override_dir + partial_name, ''
                    ).replace('.csv', '')
                )
                for i in glob.glob(override_dir + partial_name + '[0-9]*.csv')
            ]) if glob.glob(override_dir + partial_name + '[0-9]*.csv') else 0
            target_file_name = '{partial_name}{counter}.csv'.format(
                partial_name=partial_name,
                counter=(file_counter + 1)
            )
            with open(
                '{}/{}'.format(override_dir, target_file_name), 'wb+'
            ) as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            return True, 'File Uploaded Successfully'
        else:
            return False, 'Columns not matched with mapping criteria.'

    def is_header_allowed(self, header, reporting_type):
        """
        Check the columns of csv file is allowed or not.
        """
        # Check the selected reporting_type is matched with file header.
        if not manager_column_header.get(
            reporting_type_dict.get(reporting_type, '')
        ) in header:
            return False

        for v in manager_column_header.values():
            columns = [
                'Partner ID', v, 'Approved By', 'Reason for Remapping'
            ]
            # Handling only for partner mapping request
            if reporting_type in ['PARTNER', 'EMP']:
                columns[0] = 'Reportee'
            if columns == header:
                return True
        return False


def get_emails(user, as_on):
    """
        This function gets the emails of next two layer
        down the hierarchy from channel head.
    """
    managers = user.manager.filter(
        Q(end_date__gte=as_on) | Q(end_date=None),
        start_date__lte=as_on,
    )
    if managers:
        if managers.count() > 1:
            try:
                manager = managers.filter(reporting_type=ReportingType.PARTNER_FIELD).first().manager
            except Exception:
                logger.exception(f'user ({user}) does not have any field manager')
        else:
            manager = managers.first().manager

        email_list, hierarchy_level = get_emails(manager, as_on)

    else:
        email_list = []
        hierarchy_level = -1

    hierarchy_level += 1

    if hierarchy_level == 1 or hierarchy_level == 2:
        email_list.append(user.decrypted_email)

    return email_list, hierarchy_level


class ChangeUserStatus(View):
    @method_decorator(login_required(login_url='dash_login'))
    @method_decorator(dashboard_login_permission)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        user = get_object_or_404(User, pk=request.POST.get('user'))

        try:
            if user.is_active:
                user.is_active = False
                user.is_referral_active = False
                messages.success(self.request, 'User deactivated successfully')
                user.save(update_fields=['is_active', 'is_referral_active'])

                bp_data = {'Date of cancellation': timezone.now_local().date(),
                           'BP Name': user.get_full_name(),
                           'BP Code': user.referral_code,
                           'EI Code': user.pos_code,
                           'Reports to': user.manager.filter(end_date=None).first().manager,
                           'Cancelled by User': request.user.get_full_name()
                           }

                context = {
                    'bp_data': bp_data,
                    'date': timezone.now_local().date(),
                    'contact_mail': settings.BP_CONTACT_MAIL
                }

                # Email send to next two layer down the hierarchy from channel head.
                sales_mail, _ = get_emails(user, timezone.now_local().date())

                if user.decrypted_email in sales_mail:
                    sales_mail.remove(user.decrypted_email)

                try:
                    email.send_from_template(
                        to=sales_mail + settings.OPS_EMAIL + settings.FINANCE_MAILID + settings.ALL_HR_MAIL,
                        subject=f'BP ({user.referral_code}) Deactivation confirmation _Dated: '
                                f'{timezone.now_local().date()}',
                        template='mappings/bp_deactivation_mail.html',
                        context=context,
                        from_email=settings.RENEWBUY_SUPPORT['EMAIL']
                    )
                except Exception as e:
                    logger.exception(f'Unable to send an email at the moment because {e}')
            else:
                user.is_active = True
                messages.success(self.request, 'User activated succesfully')
                user.save(update_fields=['is_active'])

            # To avoid cycling between `RB` & 'AMS` we call RB_backend service
            # from here insted of writting inside the `User` save method.
            try:
                data = json.dumps({
                        'passkey': settings.RB_PARTNER_CREATION_PASS,
                        'email': user._email,
                        'is_active': user.is_active
                    })
                response = requests.post(
                    f"{settings.RENEWBUY_DOMAIN['URL']}/api/v1/accounts/change_user_status/",
                    headers={
                        'API-SECRET-KEY': settings.RENEWBUY_DOMAIN['API-SECRET-KEY'],
                        'APP-ID': settings.RENEWBUY_DOMAIN['APP-ID'],
                        'Content-Type': 'application/json'
                    },
                    data=data,
                    timeout=2  # Setting 2 seconds long timeout.
                )

                if not response.status_code == 200:
                    raise ValidationError(response.json())

            except requests.exceptions.ReadTimeout:
                logger.info(
                    f'Unable to change status of user({self}) in RB because of request timeout.'
                )

            except Exception as e:
                logger.exception(
                    f'Unable to change status for {self} in RB: {e}'
                )
        except Exception as e:
            logger.exception(f'Unable to update the status of user {user} beacause {e}')

        return HttpResponseReload(request)
