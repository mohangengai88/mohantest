from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError


class PinCodeValidator:
    message = _('Please enter a valid pin code.')

    def __init__(self, message=None):
        self.message = message or self.message

    def __call__(self, value):

        try:
            int(value)
        except ValueError:
            raise ValidationError(self.message)
