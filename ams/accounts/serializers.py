import logging

import requests
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.core.exceptions import ValidationError as _DjangoValidationError
from django.core.validators import EmailValidator, RegexValidator
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ams.accounts.forms import AMSPasswordResetForm
from ams.accounts.managers import UserManager
from ams.accounts.models import (
    Address,
    Channel,
    Cluster,
    Document,
    DocumentType,
    DocumentVerificationStatus,
    ReportingType,
    User,
    UserCluster,
    UserReporting,
    UserTypes,
)
from ams.accounts import services
from ams.base.validators.form_validators import (
    file_extension_validator,
    source_validation,
    validate_mobile,
)
from ams.data.serializers import AddressSerializer
from ams.utils import data_mask, random_password, timezone, try_or

logger = logging.getLogger(__name__)


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    user_type = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    middle_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)
    registration_date = serializers.CharField(read_only=True)
    pos_code = serializers.CharField(read_only=True)
    is_active = serializers.CharField(read_only=True)
    referral_code = serializers.CharField(read_only=True)

    def validate(self, data):
        username = data.get('username', None)
        password = data.get('password', None)

        if username is None:
            raise serializers.ValidationError('Email/mobile is required to log in.')

        if password is None:
            raise serializers.ValidationError('Password is required to log in.')

        # The `authenticate` method is provided by Django and handles checking
        # for a user that matches this email/password combination.
        user = authenticate(username=username, password=password)

        if user is None:
            raise serializers.ValidationError('A user with this username and password not found.')

        if not user.is_active:
            raise serializers.ValidationError('The user has been deactivated.')

        user.last_login = timezone.now_local()
        user.save(update_fields=['last_login'])

        return {
            'username': user.unique_code,
            'pos_code': user.pos_code,
            'token': user.token,
            'user_type': user.user_type,
            'first_name': user.first_name,
            'middle_name': user.middle_name,
            'last_name': user.last_name,
            'is_active': user.is_active,
            'referral_code': user.referral_code,
            'registration_date': user.created_at.strftime('%d/%m/%Y'),
        }


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """

    email = serializers.EmailField()

    password_reset_form_class = AMSPasswordResetForm

    def get_email_options(self):
        """Override this method to change default e-mail options"""
        return {}

    def validate_email(self, value):
        # Create AMSPasswordResetForm with the serializer
        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        return value

    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }

        opts.update(self.get_email_options())
        self.reset_form.save(**opts)


class DocsSerializer(serializers.Serializer):
    aadhaar_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
        validators=[
            RegexValidator(
                regex=r'^[1-9]{1}\d{11}$',
                message='Please enter a valid 12 digit long aadhaar number.',
            )
        ],
    )
    aadhaar_card_image = serializers.FileField(
        required=False, validators=[file_extension_validator]
    )
    pan_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
        validators=[
            RegexValidator(
                regex=r'^[A-Za-z]{3}[CPHFATBLJGcphfatbljg]{1}[A-Za-z]{1}\d{4}[A-Za-z]{1}$',
                message='Please enter a valid 10 character long PAN number.',
            )
        ],
    )
    pan_card_image = serializers.FileField(required=False, validators=[file_extension_validator])
    gstin_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
        validators=[
            RegexValidator(
                regex=r'^\d{2}[A-Za-z]{3}[CPHFATBLJGcphfatbljg]{1}[A-Za-z]{1}\d{4}[A-Za-z]{1}\d{1}[Zz]{1}[0-9A-Za-z]{1}$',
                message='Please enter a valid 15 character long GSTIN number.',
            )
        ],
    )
    gst_cert_image = serializers.FileField(required=False, validators=[file_extension_validator])
    bank_account_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
    )
    ifsc_code = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
        validators=[
            RegexValidator(
                regex=r'^[A-Za-z]{4}[A-Za-z0-9]{7}$',
                message='Please enter a valid 11 character long IFSC code.',
            )
        ],
    )
    cancelled_cheque_image = serializers.FileField(
        required=False, validators=[file_extension_validator]
    )
    drivers_license_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
        validators=[
            RegexValidator(
                regex=r'^[A-Za-z]{2}\d{13}$',
                message='Please enter a valid 15 character long driving ' 'license number.',
            )
        ],
    )
    drivers_license_image = serializers.FileField(
        required=False, validators=[file_extension_validator]
    )
    education_cert_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
    )
    education_cert_image = serializers.FileField(
        required=False, validators=[file_extension_validator]
    )
    passport_number = serializers.CharField(
        required=False,
        allow_null=True,
        allow_blank=True,
        default=None,
        validators=[
            RegexValidator(
                regex=r'^[A-Za-z]{1}\d{7}$',
                message='Please enter a valid 8 character long passport number.',
            )
        ],
    )
    passport_image = serializers.FileField(required=False, validators=[file_extension_validator])
    photo = serializers.FileField(required=False, validators=[file_extension_validator])

    class Meta:
        fields = (
            'aadhaar_number',
            'aadhaar_card_image',
            'pan_number',
            'pan_card_image',
            'gstin_number',
            'gst_cert_image',
            'bank_account_number',
            'ifsc_code',
            'cancelled_cheque_image',
            'drivers_license_number',
            'drivers_license_image',
            'education_cert_number',
            'education_cert_image',
            'passport_number',
            'passport_image',
            'photo',
        )

    def validate_pan_number(self, pan_number):
        """
        new User(create): check for pan number in existing users
                          if exists then raise exception
        old user(update): check for pan number in existing users except itself.
                          if found then raise exception.
        """
        if pan_number:
            user = User.objects.filter(hashed_pan_number=data_mask.hash_field(pan_number.upper()))
            status = True
            if isinstance(self.context.get('user'), User):
                # exclude current user, if still pan number is associated
                # with any other user raise exception.
                status = user.exclude(unique_code=self.context.get('user').unique_code).exists()
            if user and status:
                raise ValidationError('This PAN number has already been verified for another user.')
            return pan_number

    def validate_ifsc_code(self, ifsc_code):
        """
        Validate the IFSC Code by calling an external API and fetching the bank
        branch details for the same.
        """
        try:
            request_url_ifsc = 'https://api.techm.co.in/api/v1/ifsc/' + ifsc_code
            ifsc_response = requests.get(request_url_ifsc, timeout=1)

            if not ifsc_response.json().get('status') == 'success':
                raise ValidationError('Invalid IFSC Code')
        except ValidationError:
            raise
        except Exception:
            return ifsc_code

        return ifsc_code


class DocumentVerificationStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = (
            'type',
            'status',
        )


class UserSerializer(serializers.ModelSerializer):
    """
    Handles serialization of User objects.
    """

    email = serializers.CharField(
        max_length=255,
        required=False,
        source='_email',
        # write_only=True,
    )

    mobile = serializers.CharField(
        max_length=10,
        required=False,
        source='_mobile',
        validators=[validate_mobile],
        # write_only=True,
    )

    # Passwords must be at least 8 characters, but no more than 128
    # characters. These values are the default provided by Django. We could
    # change them, but that would create extra work while introducing no real
    # benefit, so lets just stick with the defaults.
    password = serializers.CharField(max_length=128, min_length=8, write_only=True, required=False)

    primary_address = AddressSerializer(required=False, many=False, default=None, allow_null=True)
    secondary_address = AddressSerializer(required=False, many=False, default=None, allow_null=True)
    gstin_address = AddressSerializer(required=False, many=False, default=None, allow_null=True)

    docs = DocsSerializer(write_only=True, required=False)

    source = serializers.CharField(
        max_length=6, min_length=2, write_only=True, validators=[source_validation], required=False
    )

    registration_date = serializers.DateTimeField(
        required=False, source='created_at', read_only=True, format='%d/%m/%Y'
    )

    referral_code = serializers.CharField(required=False, max_length=32)

    channel = serializers.CharField(required=False, source='channel.name')

    doc_verification_status = DocumentVerificationStatusSerializer(
        read_only=True, many=True, source='non_discarded_docs'
    )

    beta_features = serializers.SerializerMethodField()

    def get_beta_features(self, obj):
        try:

            beta_feature = obj.beta_enrollment.filter(end_date=None).values_list(
                'feature_code', flat=True
            )

            return beta_feature

        except Exception:

            return None

    class Meta:
        model = User
        fields = (
            'email',
            'first_name',
            'middle_name',
            'last_name',
            'user_type',
            'unique_code',
            'pos_code',
            'mobile',
            'gender',
            'dob',
            'photo',
            'primary_address',
            'secondary_address',
            'gstin_address',
            'agency_name',
            'occupation_type',
            'docs',
            'aadhaar_number',
            'pan_number',
            'gstin_number',
            'bank_account_number',
            'ifsc_code',
            'drivers_license_number',
            'education_cert_number',
            'passport_number',
            'is_email_verified',
            'is_mobile_verified',
            'is_photo_verified',
            'is_aadhaar_verified',
            'is_pan_verified',
            'is_bank_account_verified',
            'is_dl_verified',
            'is_gst_verified',
            'is_passport_verified',
            'is_education_cert_verified',
            'profile_verification_status',
            'educational_qualification',
            'referral_code',
            'password',
            'gi_pos_certified_date',
            'li_pos_certified_date',
            'gi_pos_status',
            'li_pos_status',
            'source',
            'registration_date',
            'marital_status',
            'date_of_anniversary',
            'preferred_lang_written',
            'preferred_lang_spoken',
            'insurance_business',
            'channel',
            'is_active',
            'doc_verification_status',
            'created_at',
            'whatsapp_notification_enabled',
            'beta_features',
        )

        read_only_fields = (
            'is_active',
            'registration_date',
            'is_email_verified',
            'is_mobile_verified',
            'is_photo_verified',
            'is_aadhaar_verified',
            'is_pan_verified',
            'is_bank_account_verified',
            'is_dl_verified',
            'is_gst_verified',
            'is_passport_verified',
            'is_education_cert_verified',
            'profile_verification_status',
            'unique_code',
            'pos_code',
            'channel',
            'doc_verification_status',
            'beta_features',
        )

    def validate(self, data):
        email = data.get('_email')
        if email:
            # Django EmailValidator to validate email only if the source
            # isn't DP
            email_validator = EmailValidator()
            qs_filter = get_user_model().objects.filter(
                hashed_email=data_mask.hash_field(UserManager.normalize_email(email))
            )
            # This Try-Except block is to handle the email validations, if
            # source isn't DP
            try:
                email_validator(email)
            except _DjangoValidationError as e:
                if not data.get('source', '').startswith('DP'):
                    raise ValidationError(e)
                else:
                    data['dp_email_exception'] = True
            if qs_filter.exists():
                # If source is DP then bypass the Validators.
                # The validations will be handled in the create method.
                data['dp_email_exception'] = True
                if not data.get('source', '').startswith('DP'):
                    raise ValidationError('A user with this email already exists.')

        if self.instance and (
            self.instance.gi_pos_certified_date or self.instance.li_pos_certified_date
        ):
            if (
                data.get('first_name')
                or data.get('middle_name')
                or data.get('last_name')
                or (data.get('docs') and data.get('docs').get('photo'))
            ):
                raise ValidationError(
                    'Name or profile picture cannot be ' 'updated once POS certification is done.'
                )

        return data

    def create(self, validated_data):
        # We take the address data out from the validated_data
        # dictionary to update the user address data later on.
        primary_address = validated_data.pop('primary_address', {})
        secondary_address = validated_data.pop('secondary_address', {})
        gstin_address = validated_data.pop('gstin_address', {})
        docs = validated_data.pop('docs', {})

        ref_code = try_or(lambda: validated_data.pop('referral_code', 'DIRECT').upper(), 'DIRECT')
        source = validated_data.pop('source', None)

        # If source is DP then update the email by appending `+{mobile_number}`
        # in the email username to maintain the uniqueness of email field.
        if source and source.startswith('DP') and validated_data.pop('dp_email_exception', False):
            if not validated_data.get('_mobile'):
                raise ValidationError('Please provide mobile number for a successful registration.')
            if validated_data.get('_email'):
                domain = validated_data.get('_email').split('@')[-1]
                # If email domain is gmail then appending the `+{mobile_number}`
                # in the email username is enough.
                if domain.lower() == 'gmail.com':
                    validated_data.update(
                        {
                            '_email': '{}+{}@{}'.format(
                                validated_data.get('_email').split('@')[0],
                                validated_data.get('_mobile'),
                                domain,
                            )
                        }
                    )
                # For any other domain update the email with
                # `{partner}.retailers+{mobile_number}@renewbuy`.
                else:
                    validated_data.update(
                        {
                            '_email': '{partner}.retailers+{mobile_number}@renewbuy.com'.format(
                                partner=source.replace('DP', '').lower(),
                                mobile_number=validated_data.get('_mobile'),
                            )
                        }
                    )
                qs_filter = get_user_model().objects.filter(
                    hashed_email=data_mask.hash_field(
                        UserManager.normalize_email(validated_data['_email'])
                    )
                )
                if qs_filter.exists():
                    raise ValidationError(
                        'A user with this email and mobile number combination already exists.'
                    )

        # Check if a referral code was provided and it exists in the system.
        if ref_code not in ('DIRECT', ''):
            try:
                u = get_user_model().objects.get(unique_code=ref_code)
            except get_user_model().DoesNotExist:
                try:
                    u = get_user_model().objects.get(referral_code=ref_code)
                except get_user_model().DoesNotExist:
                    # Raise a validation error if the referral code provided
                    # doesn't exist.
                    raise serializers.ValidationError('Invalid referral code.')

        # Use the `create_user` method to create a new user.
        user = User.objects.create_user(**validated_data)

        # Create a reporting instance if a referral code was provided.
        if ref_code not in ('DIRECT', ''):
            try:
                UserReporting.objects.create(
                    start_date=timezone.now_local(only_date=True),
                    manager=u,
                    reportee=user,
                )
            except Exception as e:
                logger.exception('Unable to set user reporting for %s: %s', user, e)

        # If primary address was provided.
        if primary_address:
            primary_add_obj = Address.objects.create(**primary_address)
            user.primary_address = primary_add_obj

        # If secondary address was provided.
        if secondary_address:
            secondary_add_obj = Address.objects.create(**secondary_address)
            user.secondary_address = secondary_add_obj

        # If GSTIN address was provided.
        if gstin_address:
            gstin_add_obj = Address.objects.create(**gstin_address)
            user.gstin_address = gstin_add_obj

        # If docs are uploaded.
        if docs:
            for key, value in docs.items():
                doc_type = None
                if key == 'aadhaar_card_image':
                    doc_type = DocumentType.AADHAAR
                    name = docs.get('aadhaar_number')
                elif key == 'pan_card_image':
                    doc_type = DocumentType.PAN
                    name = docs.get('pan_number')
                elif key == 'gst_cert_image':
                    doc_type = DocumentType.GST_CERT
                    name = docs.get('gstin_number')
                # If Bank account details are being updated, only process the
                # document if both `bank_account_number` and `ifsc_code`
                # are present.
                elif key == 'cancelled_cheque_image':
                    if docs.get('bank_account_number') and docs.get('ifsc_code'):
                        doc_type = DocumentType.CANCELLED_CHEQUE
                        name = 'Account Number: {}, IFSC Code: {}'.format(
                            docs.get('bank_account_number'), docs.get('ifsc_code')
                        )
                    else:
                        continue
                elif key == 'photo':
                    doc_type = DocumentType.PHOTO
                    name = ''
                elif key == 'drivers_license_image':
                    doc_type = DocumentType.DRIVERS_LICENSE
                    name = docs.get('drivers_license_number')
                elif key == 'education_cert_image':
                    doc_type = DocumentType.EDUCATION_CERTIFICATE
                    name = docs.get('education_cert_number')
                elif key == 'passport_image':
                    doc_type = DocumentType.PASSPORT
                    name = docs.get('passport_number')
                elif key == 'gstin_number' and 'gst_cert_image' not in docs:
                    user.gstin_number = value
                else:
                    continue

                if doc_type:
                    doc = Document.objects.get_or_create(
                        user=user, type=doc_type, status=DocumentVerificationStatus.PENDING
                    )[0]
                    doc.file = value
                    doc.name = name
                    doc.save(update_fields=['file', 'name'])

        if source:
            user.unique_code = user.create_unique_code(
                prefix='EM' if user.user_type == UserTypes.EMPLOYEE else source
            )
            try:
                channel = Channel.objects.get(code=source)
                user.channel = channel
            except Channel.DoesNotExist:
                pass

            # For Digital Partner integration, call RenewBuy and Octarine APIs for
            # creating executives.
            if source and source.startswith('DP'):
                try:
                    # Renewbuy Executive creation
                    rb_request_data = {
                        'dpi_executive_code': user.unique_code,
                        'first_name': validated_data.get('first_name'),
                        'email': validated_data.get('_email'),
                        'mobile': validated_data.get('_mobile'),
                        'password': random_password(),
                        'partner_code': 'DIRECT',
                        'is_executive': True,
                        'type': 'public',
                    }

                    rb_headers = {
                        'API-SECRET-KEY': settings.RB_API_SECRET_KEY,
                        'APP-ID': settings.RB_APP_ID,
                    }

                    logger.info(
                        'DPI RB executive creation request data for user (%s): %s',
                        user.unique_code,
                        rb_request_data,
                    )

                    rb_response_data = requests.post(
                        settings.RB_EXECUTIVE_SIGNUP_API, data=rb_request_data, headers=rb_headers
                    )

                    logger.info(
                        'DPI RB executive creation response data for user (%s): %s',
                        user.unique_code,
                        rb_response_data.text,
                    )

                    # Octarine user creation
                    oc_request_data = {
                        'email': validated_data.get('_email'),
                        'mobile': validated_data.get('_mobile'),
                        'password': random_password(),
                        'through_dp_channel': 1,
                        'dp_code': user.unique_code,
                    }

                    oc_headers = {
                        'API-KEY': settings.OC_API_KEY,
                        'SECRET-KEY': settings.OC_SECRET_KEY,
                    }

                    logger.info(
                        'DPI Octarine user creation request data for user (%s): %s',
                        user.unique_code,
                        oc_request_data,
                    )

                    oc_response_data = requests.post(
                        settings.OC_SIGNUP_API, data=oc_request_data, headers=oc_headers
                    )

                    logger.info(
                        'DPI Octarine user creation response data for user (%s): %s',
                        user.unique_code,
                        oc_response_data.text,
                    )

                except Exception as e:
                    logger.exception(
                        'DPI executive creation request failed ' 'for user (%s): %s.',
                        user.unique_code,
                        str(e),
                    )

        user.save()

        return user

    def update(self, instance, validated_data):
        # Passwords should not be handled with `setattr`, unlike other fields.
        # Django provides a function that handles hashing and
        # salting passwords. That means we need to remove the password field
        # from the `validated_data` dictionary before iterating over it.
        password = validated_data.pop('password', None)

        # Like passwords, we have to handle addresses, docs separately.
        # To do that, we remove the address data from the `validated_data`.
        primary_address = validated_data.pop('primary_address', {})
        secondary_address = validated_data.pop('secondary_address', {})
        gstin_address = validated_data.pop('gstin_address', {})
        docs = validated_data.pop('docs', {})

        # Pop the `referral_code` out of validated_data
        validated_data.pop('referral_code', None)

        # Here, we pop the source out of the dictionary, as, no changes are
        # allowed to the unique code once a user has been registered.
        validated_data.pop('source', None)

        for (key, value) in validated_data.items():
            # For the keys remaining in `validated_data`, we will set them on
            # the current `User` instance one at a time.
            setattr(instance, key, value)

        if password is not None:
            # `.set_password()`  handles all
            # of the security stuff that we shouldn't be concerned with.
            instance.set_password(password)

        # After everything has been updated we must explicitly save
        # the model. It's worth pointing out that `.set_password()` does not
        # save the model.
        instance.save()

        if primary_address:
            if not instance.primary_address:
                primary_add_obj = Address.objects.create(**primary_address)
                instance.primary_address = primary_add_obj
            else:
                for (key, value) in primary_address.items():
                    setattr(instance.primary_address, key, value)
            instance.primary_address.save()

        if secondary_address:
            if not instance.secondary_address:
                secondary_add_obj = Address.objects.create(**secondary_address)
                instance.secondary_address = secondary_add_obj
            else:
                for (key, value) in secondary_address.items():
                    setattr(instance.secondary_address, key, value)
            instance.secondary_address.save()

        if gstin_address:
            if not instance.gstin_address:
                gstin_add_obj = Address.objects.create(**gstin_address)
                instance.gstin_address = gstin_add_obj
            else:
                for (key, value) in gstin_address.items():
                    setattr(instance.gstin_address, key, value)
            instance.gstin_address.save()

        if docs:
            for key, value in docs.items():
                if key == 'aadhaar_card_image':
                    doc_type = DocumentType.AADHAAR
                    name = docs.get('aadhaar_number')
                    instance.aadhaar_number = None
                    instance.is_aadhaar_verified = False
                elif key == 'pan_card_image':
                    doc_type = DocumentType.PAN
                    name = docs.get('pan_number')
                    instance.pan_number = None
                    instance.is_pan_verified = False
                elif key == 'gst_cert_image':
                    doc_type = DocumentType.GST_CERT
                    name = docs.get('gstin_number')
                    instance.gstin_number = None
                    instance.is_gst_verified = False
                elif key == 'cancelled_cheque_image':
                    if docs.get('bank_account_number') and docs.get('ifsc_code'):
                        doc_type = DocumentType.CANCELLED_CHEQUE
                        name = 'Account Number: {}, IFSC Code: {}'.format(
                            docs.get('bank_account_number'), docs.get('ifsc_code')
                        )
                        instance.bank_account_number = None
                        instance.ifsc_code = None
                        instance.is_bank_account_verified = False
                    else:
                        continue
                elif key == 'photo':
                    doc_type = DocumentType.PHOTO
                    name = ''
                    instance.is_photo_verified = False
                elif key == 'drivers_license_image':
                    doc_type = DocumentType.DRIVERS_LICENSE
                    name = docs.get('drivers_license_number')
                    instance.drivers_license_number = None
                    instance.is_dl_verified = False
                elif key == 'education_cert_image':
                    doc_type = DocumentType.EDUCATION_CERTIFICATE
                    name = docs.get('education_cert_number')
                    instance.education_cert_number = None
                    instance.is_education_cert_verified = False
                elif key == 'passport_image':
                    doc_type = DocumentType.PASSPORT
                    name = docs.get('passport_number')
                    instance.passport_number = None
                    instance.is_passport_verified = False
                else:
                    continue

                # There can be three cases while updating the user documents,
                # 1. If the document that is being updated, already was
                #    verified earlier, we discard the old document and create a
                #    new one to be verified.
                # 2. If the document with a pending verification status already
                #    exists, we just update the document values.
                # 3. Create a document if it doesn't exist.

                # Get all the verified documents of this type for this user.
                verified_docs = Document.objects.filter(
                    user=instance,
                    type=doc_type,
                    status=DocumentVerificationStatus.VERIFIED,
                )

                # Iterate over the verifed documents of this type.
                for d in verified_docs:
                    # Update and mark everything as discarded.
                    d.status = DocumentVerificationStatus.DISCARDED
                    d.discarded_at = timezone.now_local()
                    d.save(update_fields=['status', 'discarded_at'])

                # Create or update the document still in pending state.
                doc = Document.objects.get_or_create(
                    user=instance,
                    type=doc_type,
                    status=DocumentVerificationStatus.PENDING,
                )[0]
                doc.file = value
                doc.name = name
                doc.save(update_fields=['file', 'name'])

        instance.save()

        return instance


class RegistrationSerializer(UserSerializer):
    """
    Exact copy of the UserSerializer, the only difference being that this
    exposes the token of the user.
    """

    # token = serializers.CharField(max_length=255, read_only=True)

    class Meta(UserSerializer.Meta):
        extra_fields = ('token',)
        read_only_fields = ('token',)

    def get_field_names(self, declared_fields, info):
        expanded_fields = super().get_field_names(declared_fields, info)

        if getattr(self.Meta, 'extra_fields', None):
            return expanded_fields + self.Meta.extra_fields
        return expanded_fields


class BackfillSerializer(serializers.ModelSerializer):
    """
    Handles serialization of User objects for the backfill script.
    """

    email = serializers.EmailField(max_length=255, source='_email')

    mobile = serializers.CharField(
        max_length=10,
        required=False,
        source='_mobile',
        validators=[validate_mobile],
        allow_null=True,
    )

    token = serializers.CharField(max_length=255, read_only=True)

    primary_address = AddressSerializer(required=False, many=False, default=None, allow_null=True)
    secondary_address = AddressSerializer(required=False, many=False, default=None, allow_null=True)
    gstin_address = AddressSerializer(required=False, many=False, default=None, allow_null=True)

    docs = DocsSerializer(write_only=True, required=False)

    self_referral_code = serializers.CharField(max_length=32, allow_null=True, required=False)

    source = serializers.CharField(
        max_length=6, min_length=2, write_only=True, validators=[source_validation], required=False
    )

    kyc_submitted_at = serializers.DateTimeField(required=False, default=None, allow_null=True)

    kyc_closed_at = serializers.DateTimeField(required=False, default=None, allow_null=True)
    kyc_closed_by = serializers.CharField(required=False, default=None, allow_null=True)

    pos_document_status = serializers.IntegerField(required=False, default=None, allow_null=True)
    bank_document_status = serializers.IntegerField(required=False, default=None, allow_null=True)

    class Meta:
        model = User
        fields = (
            'email',
            'first_name',
            'middle_name',
            'last_name',
            'user_type',
            'dialer_agent_id',
            'unique_code',
            'mobile',
            'gender',
            'dob',
            'photo',
            'primary_address',
            'secondary_address',
            'gstin_address',
            'agency_name',
            'occupation_type',
            'aadhaar_number',
            'pan_number',
            'gstin_number',
            'bank_account_number',
            'drivers_license_number',
            'is_email_verified',
            'is_mobile_verified',
            'is_photo_verified',
            'is_aadhaar_verified',
            'is_pan_verified',
            'is_bank_account_verified',
            'is_dl_verified',
            'is_gst_verified',
            'profile_verification_status',
            'docs',
            'referral_code',
            'token',
            'password',
            'gi_pos_certified_date',
            'li_pos_certified_date',
            'gi_pos_status',
            'li_pos_status',
            'self_referral_code',
            'ifsc_code',
            'educational_qualification',
            'marital_status',
            'date_of_anniversary',
            'source',
            'kyc_submitted_at',
            'kyc_closed_at',
            'kyc_closed_by',
            'pos_document_status',
            'bank_document_status',
            'insurance_business',
            'bank_name',
            'bank_branch',
            'is_education_cert_verified',
            'pos_code',
            'is_referral_active',
        )

        read_only_fields = ('token',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['docs'].context.update({'user': self.instance})

    def validate_email(self, value):
        """
        Create a hash of the email and check the uniqueness of the said email
        in the database.
        """
        if value:
            qs_filter = get_user_model().objects.filter(
                hashed_email=data_mask.hash_field(UserManager.normalize_email(value))
            )
            if qs_filter.exists():
                # If the user instance is being updated and the user is the
                # only one in the queryset, don't throw a validation error.
                if self.instance and qs_filter.count() == 1 and self.instance in qs_filter:
                    return value
                raise ValidationError('A user with this email already exists.')
            return value

    def validate(self, data):
        email = data.get('_email')
        if email:
            data.update({'_email': UserManager.normalize_email(email)})
        return super().validate(data)

    def create(self, validated_data):
        # We take the address data out from the validated_data
        # dictinary to update the user address data later on.
        primary_address = validated_data.pop('primary_address', {})
        secondary_address = validated_data.pop('secondary_address', {})
        gstin_address = validated_data.pop('gstin_address', {})
        docs = validated_data.pop('docs', {})

        ref_code = try_or(lambda: validated_data.pop('referral_code', 'DIRECT').upper(), 'DIRECT')
        source = validated_data.pop('source', None)

        kyc_submitted_at = validated_data.pop('kyc_submitted_at', None)
        kyc_closed_at = validated_data.pop('kyc_closed_at', None)
        kyc_closed_by = validated_data.pop('kyc_closed_by', None)
        pos_document_status = validated_data.pop('pos_document_status', None)
        bank_document_status = validated_data.pop('bank_document_status', None)

        # If bank account details are not present, ignore `bank_name` and
        # `bank_branch` fields.
        if (
            not docs.get('ifsc_code')
            or not docs.get('bank_account_number')
            or not validated_data.get('is_bank_account_verified')
        ):
            validated_data.pop('bank_name', None)
            validated_data.pop('bank_branch', None)

        # Try to look for the KYC accepting operations personnel in the
        # database, if not found, save the email ID of the same in
        # the remarks of the document.
        if kyc_closed_by:
            try:
                kyc_closed_by = User.objects.get(
                    hashed_email=data_mask.hash_field(UserManager.normalize_email(kyc_closed_by))
                )
            except User.DoesNotExist:
                pass

        # Check if a referral code was provided and it exists in the system.
        if ref_code not in ('DIRECT', ''):
            try:
                u = get_user_model().objects.get(unique_code=ref_code)
            except get_user_model().DoesNotExist:
                try:
                    u = get_user_model().objects.get(referral_code=ref_code)
                except get_user_model().DoesNotExist:
                    # Raise a validation error if the referral code provided
                    # doesn't exist.
                    raise serializers.ValidationError('Invalid referral code.')

        # Pop the password out of the dictinary to manually update the
        # hash in the database.
        password = validated_data.pop('password')

        if validated_data.get('self_referral_code'):
            validated_data.update({'referral_code': validated_data.pop('self_referral_code')})
        else:
            try_or(lambda: validated_data.pop('self_referral_code'), '')

        # Use the `create_user` method to create a new user.
        user = User.objects.create_user(**validated_data)

        # Create a reporting instance if a referral code was provided.
        if ref_code not in ('DIRECT', ''):
            UserReporting.objects.create(
                start_date=timezone.now_local(only_date=True),
                manager=u,
                reportee=user,
            )

        # Manually set the password hash so that the user's password
        # remains the same as before.
        if password:
            user.password = password

        if primary_address:
            primary_add_obj = Address.objects.create(**primary_address)
            user.primary_address = primary_add_obj

        if secondary_address:
            secondary_add_obj = Address.objects.create(**secondary_address)
            user.secondary_address = secondary_add_obj

        if gstin_address:
            gstin_add_obj = Address.objects.create(**gstin_address)
            user.gstin_address = gstin_add_obj

        if docs:
            for key, value in docs.items():
                status = DocumentVerificationStatus.PENDING
                if key == 'aadhaar_card_image':
                    doc_type = DocumentType.AADHAAR
                    name = docs.get('aadhaar_number')
                    if (
                        validated_data.pop('is_aadhaar_verified', None)
                        or pos_document_status == DocumentVerificationStatus.VERIFIED
                    ):
                        user.is_aadhaar_verified = True
                        user.aadhaar_number = docs.get('aadhaar_number')
                        status = DocumentVerificationStatus.VERIFIED
                    elif pos_document_status == DocumentVerificationStatus.DISCARDED:
                        status = DocumentVerificationStatus.DISCARDED
                elif key == 'pan_card_image':
                    doc_type = DocumentType.PAN
                    name = docs.get('pan_number')
                    if (
                        validated_data.pop('is_pan_verified', None)
                        or pos_document_status == DocumentVerificationStatus.VERIFIED
                    ):
                        user.is_pan_verified = True
                        user.pan_number = docs.get('pan_number')
                        status = DocumentVerificationStatus.VERIFIED
                    elif pos_document_status == DocumentVerificationStatus.DISCARDED:
                        status = DocumentVerificationStatus.DISCARDED
                elif key == 'gst_cert_image':
                    doc_type = DocumentType.GST_CERT
                    name = docs.get('gstin_number')
                    if validated_data.pop('is_gst_verified', None):
                        user.gstin_number = docs.get('gstin_number')
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'cancelled_cheque_image':
                    doc_type = DocumentType.CANCELLED_CHEQUE
                    name = docs.get('bank_account_number')
                    if (
                        validated_data.pop('is_bank_account_verified', None)
                        or bank_document_status == DocumentVerificationStatus.VERIFIED
                    ):
                        user.is_bank_account_verified = True
                        user.bank_account_number = docs.get('bank_account_number')
                        user.ifsc_code = docs.get('ifsc_code')
                        status = DocumentVerificationStatus.VERIFIED
                    elif bank_document_status == DocumentVerificationStatus.DISCARDED:
                        status = DocumentVerificationStatus.DISCARDED
                elif key == 'photo':
                    doc_type = DocumentType.PHOTO
                    name = ''
                    if validated_data.pop('is_photo_verified', None):
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'drivers_license_image':
                    doc_type = DocumentType.DRIVERS_LICENSE
                    name = docs.get('drivers_license_number')
                    if validated_data.pop('is_dl_verified', None):
                        user.drivers_license_number = docs.get('drivers_license_number')
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'education_cert_image':
                    doc_type = DocumentType.EDUCATION_CERTIFICATE
                    name = docs.get('education_cert_number')
                    if validated_data.pop('is_education_cert_verified', None):
                        user.education_cert_number = docs.get('education_cert_number')
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'passport_image':
                    doc_type = DocumentType.PASSPORT
                    name = docs.get('passport_number')
                    if validated_data.pop('is_passport_verified', None):
                        user.passport_number = docs.get('passport_number')
                        status = DocumentVerificationStatus.VERIFIED
                else:
                    continue

                doc = Document.objects.get_or_create(
                    user=user,
                    type=doc_type,
                )[0]
                doc.file = value
                doc.name = name
                doc.status = status
                if kyc_submitted_at:
                    doc.created_at = kyc_submitted_at

                # If the KYC is closed from the operation's side.
                if kyc_closed_at:
                    # If the document has been verified.
                    if doc.status == DocumentVerificationStatus.VERIFIED:
                        doc.verified_at = kyc_closed_at
                        if isinstance(kyc_closed_by, User):
                            doc.verified_by = kyc_closed_by
                        else:
                            doc.remarks = kyc_closed_by
                    # If the KYC has been rejected.
                    elif doc.status == DocumentVerificationStatus.DISCARDED:
                        doc.discarded_at = kyc_closed_at
                        if isinstance(kyc_closed_by, User):
                            doc.verified_by = kyc_closed_by
                        else:
                            doc.remarks = kyc_closed_by

                doc.save(
                    update_fields=[
                        'file',
                        'name',
                        'status',
                        'created_at',
                        'verified_at',
                        'verified_by',
                        'discarded_at',
                        'discarded_by',
                        'remarks',
                    ]
                )

            try:
                photo = Document.objects.get(
                    user=user, type=DocumentType.PHOTO, status=DocumentVerificationStatus.VERIFIED
                ).file
                user.photo = photo
            except Document.DoesNotExist:
                pass

        if docs.get('gstin_number'):
            user.gstin_number = docs.get('gstin_number')

        if not user.pan_number:
            user.is_pan_verified = False
        if not user.aadhaar_number:
            user.is_aadhaar_verified = False
        if not user.drivers_license_number:
            user.is_dl_verified = False
        if not user.gstin_number:
            user.is_gst_verified = False
        if not user.passport_number:
            user.is_passport_verified = False
        if not user.bank_account_number or not user.ifsc_code:
            user.is_bank_account_verified = False

        if source:
            user.unique_code = user.create_unique_code(
                prefix='EM' if user.user_type == UserTypes.EMPLOYEE else source
            )

        user.save()
        # set WhatsApp notification enabled when the user created.
        try:
            notification_status = services.set_whatsapp_opt_in_out(user)
            if notification_status:
                user.whatsapp_notification_enabled = notification_status
                user.save()
            logger.info(
                "[AMS][Backfill] Response from communication_preferences service for user[%s]: %s",
                user.unique_code,
                notification_status,
            )
        except Exception as e:
            logger.exception(
                "[AMS][Backfill] Exception in communication_preferences service for user[%s]: %s",
                user.unique_code,
                str(e),
            )

        return user

    def update(self, instance, validated_data):
        # We take the address data out from the validated_data
        # dictinary to update the user address data later on.
        primary_address = validated_data.pop('primary_address', {})
        secondary_address = validated_data.pop('secondary_address', {})
        gstin_address = validated_data.pop('gstin_address', {})
        docs = validated_data.pop('docs', {})

        ref_code = try_or(lambda: validated_data.pop('referral_code', 'DIRECT').upper(), 'DIRECT')

        # Here, we pop the source out of the dictionary, as, no changes are
        # allowed to the unique code once a user has been registered.
        validated_data.pop('source', None)

        kyc_submitted_at = validated_data.pop('kyc_submitted_at', None)
        kyc_closed_at = validated_data.pop('kyc_closed_at', None)
        kyc_closed_by = validated_data.pop('kyc_closed_by', None)
        pos_document_status = validated_data.pop('pos_document_status', None)
        bank_document_status = validated_data.pop('bank_document_status', None)

        # If bank account details are not present, ignore `bank_name` and
        # `bank_branch` fields.
        if (
            not docs.get('ifsc_code')
            or not docs.get('bank_account_number')
            or not validated_data.get('is_bank_account_verified')
        ):
            validated_data.pop('bank_name', None)
            validated_data.pop('bank_branch', None)

        # Try to look for the KYC accepting operations personnel in the
        # database, if not found, save the email ID of the same in
        # the remarks of the document.
        if kyc_closed_by:
            try:
                kyc_closed_by = User.objects.get(
                    hashed_email=data_mask.hash_field(UserManager.normalize_email(kyc_closed_by))
                )
            except User.DoesNotExist:
                pass

        # Check if a referral code was provided and it exists in the system.
        if ref_code not in ('DIRECT', ''):
            try:
                u = get_user_model().objects.get(unique_code=ref_code)
            except get_user_model().DoesNotExist:
                try:
                    u = get_user_model().objects.get(referral_code=ref_code)
                except get_user_model().DoesNotExist:
                    # Raise a validation error if the referral code provided
                    # doesn't exist.
                    raise serializers.ValidationError('Invalid referral code.')

        # Pop the password out of the dictinary to manually update the
        # hash in the database.
        password = validated_data.pop('password', None)

        if validated_data.get('self_referral_code'):
            validated_data.update({'referral_code': validated_data.pop('self_referral_code')})
        else:
            try_or(lambda: validated_data.pop('self_referral_code'), '')

        # For the keys remaining in `validated_data`, we will set them on
        # the current `User` instance one at a time.
        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        # Create a reporting instance if a referral code was provided.
        if ref_code not in ('DIRECT', ''):
            UserReporting.objects.create(
                start_date=timezone.now_local(only_date=True),
                manager=u,
                reportee=instance,
            )

        # Manually set the password hash so that the user's password
        # remains the same as before.
        if password:
            instance.password = password

        if primary_address:
            if not instance.primary_address:
                primary_add_obj = Address.objects.create(**primary_address)
                instance.primary_address = primary_add_obj
            else:
                for (key, value) in primary_address.items():
                    setattr(instance.primary_address, key, value)
            instance.primary_address.save()

        if secondary_address:
            if not instance.secondary_address:
                secondary_add_obj = Address.objects.create(**secondary_address)
                instance.secondary_address = secondary_add_obj
            else:
                for (key, value) in secondary_address.items():
                    setattr(instance.secondary_address, key, value)
            instance.secondary_address.save()

        if gstin_address:
            if not instance.gstin_address:
                gstin_add_obj = Address.objects.create(**gstin_address)
                instance.gstin_address = gstin_add_obj
            else:
                for (key, value) in gstin_address.items():
                    setattr(instance.gstin_address, key, value)
            instance.gstin_address.save()

        if docs:
            for key, value in docs.items():
                status = DocumentVerificationStatus.PENDING
                if key == 'aadhaar_card_image':
                    doc_type = DocumentType.AADHAAR
                    name = docs.get('aadhaar_number')
                    if (
                        validated_data.pop('is_aadhaar_verified', None)
                        or pos_document_status == DocumentVerificationStatus.VERIFIED
                    ):
                        instance.is_aadhaar_verified = True
                        instance.aadhaar_number = docs.get('aadhaar_number')
                        status = DocumentVerificationStatus.VERIFIED
                    elif pos_document_status == DocumentVerificationStatus.DISCARDED:
                        status = DocumentVerificationStatus.DISCARDED
                elif key == 'pan_card_image':
                    doc_type = DocumentType.PAN
                    name = docs.get('pan_number')
                    if (
                        validated_data.pop('is_pan_verified', None)
                        or pos_document_status == DocumentVerificationStatus.VERIFIED
                    ):
                        instance.is_pan_verified = True
                        instance.pan_number = docs.get('pan_number')
                        status = DocumentVerificationStatus.VERIFIED
                    elif pos_document_status == DocumentVerificationStatus.DISCARDED:
                        status = DocumentVerificationStatus.DISCARDED
                elif key == 'gst_cert_image':
                    doc_type = DocumentType.GST_CERT
                    name = docs.get('gstin_number')
                    if validated_data.pop('is_gst_verified', None):
                        instance.gstin_number = docs.get('gstin_number')
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'cancelled_cheque_image':
                    doc_type = DocumentType.CANCELLED_CHEQUE
                    name = 'Account Number: {}, IFSC Code: {}'.format(
                        docs.get('bank_account_number'), docs.get('ifsc_code')
                    )
                    if (
                        validated_data.pop('is_bank_account_verified', None)
                        or bank_document_status == DocumentVerificationStatus.VERIFIED
                    ):
                        instance.is_bank_account_verified = True
                        instance.bank_account_number = docs.get('bank_account_number')
                        instance.ifsc_code = docs.get('ifsc_code')
                        status = DocumentVerificationStatus.VERIFIED
                    elif bank_document_status == DocumentVerificationStatus.DISCARDED:
                        status = DocumentVerificationStatus.DISCARDED
                elif key == 'photo':
                    doc_type = DocumentType.PHOTO
                    name = ''
                    if validated_data.pop('is_photo_verified', None):
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'drivers_license_image':
                    doc_type = DocumentType.DRIVERS_LICENSE
                    name = docs.get('drivers_license_number')
                    if validated_data.pop('is_dl_verified', None):
                        instance.drivers_license_number = docs.get('drivers_license_number')
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'education_cert_image':
                    doc_type = DocumentType.EDUCATION_CERTIFICATE
                    name = docs.get('education_cert_number')
                    if validated_data.pop('is_education_cert_verified', None):
                        instance.education_cert_number = docs.get('education_cert_number')
                        status = DocumentVerificationStatus.VERIFIED
                elif key == 'passport_image':
                    doc_type = DocumentType.PASSPORT
                    name = docs.get('passport_number')
                    if validated_data.pop('is_passport_verified', None):
                        instance.passport_number = docs.get('passport_number')
                        status = DocumentVerificationStatus.VERIFIED
                else:
                    continue

                try:
                    doc = Document.objects.get_or_create(
                        user=instance,
                        type=doc_type,
                    )[0]
                except Document.MultipleObjectsReturned:
                    documents = Document.objects.filter(
                        user=instance,
                        type=doc_type,
                    )
                    non_discarded_docs = documents.exclude(
                        status=DocumentVerificationStatus.DISCARDED
                    )
                    if non_discarded_docs.exists():
                        doc = non_discarded_docs.first()
                    else:
                        doc = documents.first()

                doc.file = value
                doc.name = name
                doc.status = status
                doc.verified_at = None
                doc.verified_by = None
                doc.discarded_at = None
                doc.discarded_by = None
                doc.remarks = None

                # Update the document creation date if the `kyc_submitted_at`
                # is received.
                if kyc_submitted_at:
                    doc.created_at = kyc_submitted_at

                # If the KYC is closed from the operation's side.
                if kyc_closed_at:
                    # If the document has been verified.
                    if doc.status == DocumentVerificationStatus.VERIFIED:
                        doc.verified_at = kyc_closed_at
                        if isinstance(kyc_closed_by, User):
                            doc.verified_by = kyc_closed_by
                        else:
                            doc.remarks = kyc_closed_by
                    # If the KYC has been rejected.
                    elif doc.status == DocumentVerificationStatus.DISCARDED:
                        doc.discarded_at = kyc_closed_at
                        if isinstance(kyc_closed_by, User):
                            doc.discarded_by = kyc_closed_by
                        else:
                            doc.remarks = kyc_closed_by

                doc.save(
                    update_fields=[
                        'file',
                        'name',
                        'status',
                        'created_at',
                        'verified_at',
                        'verified_by',
                        'discarded_at',
                        'discarded_by',
                        'remarks',
                    ]
                )

            try:
                photo = Document.objects.get(
                    user=instance,
                    type=DocumentType.PHOTO,
                    status=DocumentVerificationStatus.VERIFIED,
                ).file
                instance.photo = photo
            except Document.DoesNotExist:
                pass

        if docs.get('gstin_number'):
            instance.gstin_number = docs.get('gstin_number')

        if not instance.pan_number:
            instance.is_pan_verified = False
        if not instance.aadhaar_number:
            instance.is_aadhaar_verified = False
        if not instance.drivers_license_number:
            instance.is_dl_verified = False
        if not instance.gstin_number:
            instance.is_gst_verified = False
        if not instance.passport_number:
            instance.is_passport_verified = False
        if not instance.bank_account_number or not instance.ifsc_code:
            instance.is_bank_account_verified = False

        instance.save()

        return instance


class UserReportingSerializer(serializers.ModelSerializer):
    manager = serializers.CharField(source='manager.unique_code')
    reportee = serializers.CharField(source='reportee.unique_code')

    class Meta:
        model = UserReporting
        fields = ('start_date', 'end_date', 'manager', 'reportee', 'reporting_type')


class BulkUserSerializer(UserSerializer, serializers.HyperlinkedModelSerializer):
    partners_referral_code = serializers.SerializerMethodField()

    def get_partners_referral_code(self, obj):
        try:
            reporting = obj.get_current_reporting(reporting_type=ReportingType.PARTNER_PARTNER)
            return reporting.manager.referral_code
        except Exception:
            return None

    class Meta:
        model = User
        fields = (
            'first_name',
            'middle_name',
            'last_name',
            'primary_address',
            'secondary_address',
            'aadhaar_number',
            'pan_number',
            'gstin_number',
            'bank_account_number',
            'gi_pos_certified_date',
            'li_pos_certified_date',
            'ifsc_code',
            'bank_name',
            'bank_branch',
            'pos_code',
            'created_at',
            'referral_code',
            'partners_referral_code',
        )


class UserClusterSerializer(serializers.ModelSerializer):
    user = serializers.CharField(source='user.unique_code')
    cluster_code = serializers.CharField(source='cluster.cluster_code')
    cluster_name = serializers.CharField(source='cluster.cluster_name')
    start_date = serializers.CharField(source='cluster.start_date')
    end_date = serializers.CharField(source='cluster.end_date')

    class Meta:
        model = UserCluster
        fields = ('user', 'cluster_code', 'cluster_name', 'start_date', 'end_date')


class ClusterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cluster
        fields = (
            'cluster_code',
            'cluster_name',
            'start_date',
            'end_date',
        )
