import csv
import os
import tempfile

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files import File
from django.core.management.base import BaseCommand
from django.db import IntegrityError
from django.db.models import Exists, OuterRef

from ams.accounts.models import Cluster, UserCluster, UserTypes
from ams.utils import email, timezone

UserModel = get_user_model()


class Command(BaseCommand):
    """
        Usage: ./manage.py usercluster_mapping
    """

    help = 'User Cluster Mapping'

    def handle(self, *args, **options):
        is_error = False
        context = {}
        partner_data = []
        clusters = Cluster.objects.filter(end_date=None)
        for cluster in clusters:
            criteria_list = cluster.cluster_data
            # get all user who are not mapped to any active cluster
            users = UserModel.objects.filter(
                primary_address__pincode__isnull=False,
                user_type=UserTypes.PARTNER,
                is_referral_active=False,
                **criteria_list
            ).annotate(not_in_cluster=~Exists(UserCluster.objects.filter(
                user=OuterRef('pk'), cluster__end_date=None
            ))).filter(not_in_cluster=True)
            print(
                f'Total partner found for cluster({cluster.cluster_code}): {users.count()}'
            )
            context.update({
                cluster.cluster_name: users.count()
            })

            for user in users:
                try:
                    UserCluster.objects.create(
                        user=user, cluster=cluster
                    )
                    msg = 'Mapped'
                except IntegrityError:
                    msg = f'User({user.unique_code}) already mapped with cluster({user.clusters.get(cluster__end_date=None).cluster.cluster_code})'
                except Exception as e:
                    is_error = True
                    msg = f'User not mapped: {user.unique_code} due to {e}'
                partner_data.append({
                    'partner': user.pos_code,
                    'cluster_code': cluster.cluster_code,
                    'cluster_name': cluster.cluster_name,
                    'status': msg
                })
                print(msg)

        temp_files = self.create_temp_file(partner_data)
        body = self.set_context_in_template(context, kwargs={'is_error': is_error})
        self.send_report(body, attachments=[temp_files])

    def set_context_in_template(self, context, template_string=None, **kwargs):
        if not template_string:
            template_string = ''
        template_string += f'''
            <div><p>Total Partners mapped on {timezone.now_local(only_date=True)}: {str(sum(context.values()))}</p>
            <p>Any error: {kwargs.get('is_error')}</p></div>
            <table style="border-collapse: collapse;">
                <tr>
        '''
        for key in context.keys():
            template_string += f'''
                <th style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{key}</th>
            '''
        template_string += '</tr><tr>'
        for value in context.values():
            template_string += f'''
                <td style="border: 1px solid #dddddd;text-align: left;padding: 8px;">{str(value)}</td>
            '''
        template_string += '</tr></table>'
        return template_string

    def create_temp_file(self, content):
        file_path = os.path.join(
            tempfile.gettempdir(), 'PartnerClusterMapping'
        )
        csv_file = tempfile.NamedTemporaryFile(
            delete=False, prefix=file_path,
            suffix='.csv', mode='r+'
        )
        csv_columns = content[0].keys() if content else []
        writer = csv.DictWriter(
            csv_file,
            lineterminator='\n',
            quoting=csv.QUOTE_MINIMAL,
            fieldnames=csv_columns
        )
        writer.writeheader()

        for row in content:
            try:
                writer.writerow(row)
            except Exception:
                continue
        return csv_file.name

    def send_report(self, body, recipient_email=None, attachments=[]):
        """
        Send the status report to the mapping.
        """
        if not attachments:
            return
        if recipient_email:
            recipient_email = [recipient_email]
        else:
            recipient_email = settings.DEFAULT_MAPPING_STATUS_EMAIL
        email_status = email.send(
            recipient_email,
            f'Partner Cluster Mapping {timezone.now_local(only_date=True)}',
            html_body=body,
            attachments=list(map(lambda x: File(open(x)), attachments))
        )
        print('Email Sent Status: %s' % email_status)
