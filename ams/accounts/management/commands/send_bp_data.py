"""
Custom Command to send All Active BP data to OPS Team.
"""

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from ams.accounts.models import UserReporting
from ams.utils import create_csv_n_send

UserModel = get_user_model()

USER_TYPE = {
    '1': 'Employee',
    '2': 'Partner',
    '3': 'Aggregator',
    '4': 'Customer',
    '5': 'Corporate',
    '6': 'Pyramid Manager'
}


class Command(BaseCommand):
    """
        Usage: ./manage.py send_bp_data
    """

    help = 'All BP Data'

    def add_arguments(self, parser):
        parser.add_argument('--email')

    def handle(self, *args, **options):
        email = options.get('email')
        self.email = [i.strip() for i in email.split(',')] if email else []

        # All active BP
        all_bp = UserModel.objects.filter(
            referral_code__startswith='BPO',
            is_referral_active=True,
            is_active=True
        )

        all_active_bp = []
        for bp in all_bp:
            all_active_bp.append({
                'BP EI Code': bp.pos_code,
                'BP Referral Code': bp.referral_code,
                'BP Name': bp.get_full_name(),
                'BP User Type': USER_TYPE.get(str(bp.user_type), 'Not Set')
            })

        # All BP's with active reportings count.
        bp_with_active_reporting = UserReporting.objects.filter(
            manager__referral_code__startswith='BPO', end_date=None,
        )

        bp_reportings = []
        for bp in bp_with_active_reporting:
            bp_reportings.append({
                'BP EI Code': bp.manager.pos_code,
                'BP Referral Code': bp.manager.referral_code,
                'Partner EI code': bp.reportee.pos_code,
                'Partner Name': bp.reportee.get_full_name(),
            })

        temp_files = [
            create_csv_n_send.create_temp_file('All Active BP', all_active_bp),
            create_csv_n_send.create_temp_file(
                'BP with Active Reporting', bp_reportings
            )
        ]
        body = f'''
            <p>Please find the attached BP data.</p>
            <p><b>{temp_files[0].replace('/tmp/', '')}</b>: Contains data of all active BPs.</p>
            <p><b>{temp_files[1].replace('/tmp/', '')}</b>: Contains data of all BPs and their mapped partners.</p>
        '''
        create_csv_n_send.send_report(
            body=body,
            subject='BP Data',
            to=settings.SEND_BP_REPORT,
            cc=settings.DEFAULT_MAPPING_STATUS_EMAIL + self.email,
            attachments=temp_files
        )
