import csv

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand
from django.core.files import File
from django.conf import settings

from ams.accounts.models import UserReporting, ReportingType, UserTypes
from ams.utils import email

UserModel = get_user_model()


class Command(BaseCommand):
    """
    This command fetch all the in_active employee that have
    reporting either as a manager or reportee and send these
    employee data to HR.
    """
    def handle(self, *args, **options):
        # all in_active mangers having reportings.
        in_active_manager = UserReporting.objects.filter(
            manager__is_active=False,
            manager__user_type=UserTypes.EMPLOYEE,
            reporting_type=ReportingType.EMPLOYEE,
            end_date=None
            ).values_list('manager___email', flat=True)

        # all in_active reportee having reportings.
        in_active_reportee = UserReporting.objects.filter(
            reportee__is_active=False,
            reportee__user_type=UserTypes.EMPLOYEE,
            reporting_type=ReportingType.EMPLOYEE,
            end_date=None
            ).values_list('reportee___email', flat=True)

        # all in_active_employee that are part of reportings.
        inactive_employee = in_active_manager.union(in_active_reportee)

        if inactive_employee:
            header = ['User Email id']
            with open('/tmp/report_of_inactive_employee.csv', 'w', newline='') as csv_file:
                dict_writer = csv.DictWriter(csv_file, header)
                dict_writer.writeheader()
                for data in inactive_employee:
                    dict_writer.writerow({'User Email id': data})

            print('Total inactive_employees that have reportings: ', len(inactive_employee))

            with open('/tmp/report_of_inactive_employee.csv', 'r') as csv_file:
                attachment_file = File(csv_file)

            try:
                email.send(
                    to=settings.ALL_HR_MAIL,
                    subject='Report of inactive mappings',
                    html_body=f'<h4>Hi,<br><h4>'
                              f'Kindly refer attached file which contains emails of “Inactive” employees '
                              f'but their mapping still existing in "AMS system".<br>'
                              f'Please check and take appropriate action in this matter.<br><br>'
                              f'Feel free to connect on "techsuppport@renewbuy.com" regarding any query of concern.<br> <br>'
                              f'Regards, <br> Renewbuy Support',
                    attachments=[attachment_file]
                )
                print('detail: Email sent successfully.')

            except Exception as e:
                print(f'Unable to send email at the moment due to - {e} . Please try again later.')

        else:
            print('detail: No inactive mappings exist')
