"""
Custom Command to set user type of blank users as partners and
also set EI Code.
"""
import json

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from ams.accounts.models import UserTypes

UserModel = get_user_model()


class Command(BaseCommand):
    """
        Usage: ./manage.py set_usertype_n_poscode
    """

    help = 'Set User Type as partner and pos code for blank users.'

    url = settings.RENEWBUY_DOMAIN['URL'] + 'api/v2/auth/register_executive/'
    headers = {
        'Content-Type': 'application/json',
        'API-SECRET-KEY': settings.RENEWBUY_DOMAIN['API-SECRET-KEY'],
        'App-ID': settings.RENEWBUY_DOMAIN['APP-ID']
    }

    def handle(self, *args, **options):

        users = UserModel.objects.filter(
            referral_code__startswith='BPO',
            is_active=True, user_type=None
        )
        for user in users:
            user.user_type = UserTypes.PARTNER
            user.save()
            if not user.pos_code:
                self.set_pos_code_in_rb(user)

        # set pos code in RB
        users = UserModel.objects.filter(
            is_active=True, pos_code=None,
            user_type=UserTypes.PARTNER
        )
        for user in users:
            self.set_pos_code_in_rb(user)

    def set_pos_code_in_rb(self, user):
        payload = {
            'email': user.decrypted_email,
            'password': settings.GUPSHUP_CONF['CREDENTIALS']['PASSWORD'],
            'referral_code': 'DIRECT'
        }
        response = requests.post(
            url=self.url,
            headers=self.headers,
            data=json.dumps(payload)
        )
        if response.status_code not in [200, 201]:
            print(
                'EI code not updated for user: %s. Reason: %s' %
                (user.unique_code, response.status_code)
            )
