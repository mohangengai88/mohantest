import csv
from django.conf import settings
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand
from django.db.models import Q

from ams.accounts.models import User
from ams.utils import email, try_or


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--date')
        parser.add_argument('--start_date')

    def handle(self, *args, **options):
        """
        Sample usage: ./manage.py rt_doc_verification_mis --date='dd/mm/yyyy'
        """
        try:
            # start_date = options.get('start_date')
            # end_date = options.get('date')
            # if not end_date:
            #     print('Please provide a date(dd/mm/yyyy) argument.')
            #     return
            # if start_date and not isinstance(start_date, date):
            #     start_date = datetime.strptime(start_date, '%d/%m/%Y').date()
            # if not isinstance(end_date, date):
            #     end_date = datetime.strptime(end_date, '%d/%m/%Y').date()

            fields = [
                'AMS ID', 'First Name', 'Last Name', 'PAN Verification Status',
                'Bank Detail Verification Status', 'GST Verification Status',
                'Aadhaar Verification Status', 'Date of Registration',
                'Assigned Referral Code',
            ]

            # if not start_date:
            #     start_date = end_date.replace(day=1)

            users = User.objects.filter(
                Q(unique_code__startswith='RT') |
                Q(unique_code__startswith='AG'),
                referral_code__isnull=False,
            )

            if users:
                print('Generating MIS for RT Document Verification Status.')
                filename = 'rt_doc_verification_mis.csv'
                csv_file = default_storage.open(filename, 'w')
                csv_writer = csv.DictWriter(csv_file, fieldnames=fields)
                csv_writer.writeheader()

                for user in users:
                    print(user)

                    csv_writer.writerow({
                        fields[0]: try_or(lambda: user.unique_code, ''),
                        fields[1]: try_or(lambda: user.first_name, ''),
                        fields[2]: try_or(lambda: user.last_name, ''),
                        fields[3]: try_or(lambda: user.is_pan_verified, ''),
                        fields[4]: try_or(
                            lambda: user.is_bank_account_verified, ''
                        ),
                        fields[5]: try_or(lambda: user.is_gst_verified, ''),
                        fields[6]: try_or(lambda: user.is_aadhaar_verified, ''),
                        fields[7]: try_or(
                            lambda: user.created_at.strftime('%d-%m-%Y'), ''
                        ),
                        fields[8]: try_or(lambda: user.referral_code, ''),
                    })

                csv_file.close()

                if default_storage.exists(filename):
                    csv_file = default_storage.open(filename, 'r')
                    attachment_file = File(csv_file)

                    subject = 'RT Document Verification Status MIS'
                    body = ''
                    to = [
                        'mohit.sharma@renewbuy.com',
                    ]
                    cc = [
                        'maulik.jain@renewbuy.com',
                    ]

                    if settings.IS_DEV_ENV:
                        email.send('maulik.jain@renewbuy.com', subject,
                                   body, attachments=[attachment_file], )
                    else:
                        email.send(to, subject, body,
                                   attachments=[attachment_file], cc=cc, )

                    print('RT Document Verification Status MIS sent.')
                else:
                    print('Error while generating MIS.')

            else:
                print('No details for generating an MIS found.')

        except Exception as e:
            print(e)
