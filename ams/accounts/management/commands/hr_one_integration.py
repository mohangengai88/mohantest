"""
    Response format from Hr-one api.
    {
        responseResult:[
            {
                "employeeCode": "10001",
                "emailAddress": "xxxxxxx@renewbuy.com",
                "reportingManagerCode": "20002",
                ...
            },
            {
                "employeeCode": "20002",
                "emailAddress": "xxxxxxx@renewbuy.com",
                "reportingManagerCode": "20003",
                ...
            },
            ...
        ]
    }
"""
import json
import logging

import requests
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from datetime import datetime

from ams.accounts.managers import UserManager
from ams.accounts.models import ReportingType, UserReporting, UserTypes
from ams.utils import data_mask, timezone
from simple_history.utils import update_change_reason

logger = logging.getLogger(__name__)

UserModel = get_user_model()


class Command(BaseCommand):
    """
       This command call hr-one api and get all employess data
       and then call ami service to set new employee reportings.
    """

    def change_user_status_in_rb(self, user):
        try:
            data = json.dumps({
                'passkey': settings.RB_PARTNER_CREATION_PASS,
                'email': user._email,
                'is_active': user.is_active
            })
            response = requests.post(
                f"{settings.RENEWBUY_DOMAIN['URL']}/api/v1/accounts/change_user_status/",
                headers={
                    'API-SECRET-KEY': settings.RENEWBUY_DOMAIN['API-SECRET-KEY'],
                    'APP-ID': settings.RENEWBUY_DOMAIN['APP-ID'],
                    'Content-Type': 'application/json'
                },
                data=data,
                timeout=2  # Setting 2 seconds long timeout.
            )

            if not response.status_code == 200:
                print('Unable to deactivate user in RB: %s' % user)

        except requests.exceptions.ReadTimeout:
            print(
                f'Unable to change status of user({user}) in RB because of request timeout.'
            )

        except Exception as e:
            print(
                f'Unable to change status for {user} in RB: {e}'
            )

    def handle(self, *args, **options):

        url = settings.HRONE_CONF['URL']
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        data = {
            'AccessKey': settings.HRONE_CONF['ACCESS_KEY']
        }

        employees_data = []

        # call hr-one api to get all employees data.
        employees_data = requests.post(
            url=url,
            data=data,
            headers=headers
        )
        try:
            employees_data = employees_data.json().get('responseResult')
        except Exception as e:
            print('Unable to fetch data from HR-One API: %s', str(e))

        # result_data stores all the employess data.
        # result_data is a dictionary of dictionaries in which 'employeeCode'
        # is a key and for this key 'employee information' store as value.
        result_data = {}
        for data in employees_data:
            result_data.update({
              data.get('employeeCode'): data
            })

        inactive_employee = []

        for empl_code, emp_data in result_data.items():
            try:
                emp_email = emp_data.get('emailAddress')
                manager_code = emp_data.get('reportingManagerCode')
                leaving_date = emp_data.get('dateofLeaving')

                # Employee status `In-Active` in AMS if he left.
                if leaving_date:
                    leaving_date = datetime.strptime(leaving_date, '%d/%m/%Y').date()

                    if leaving_date <= timezone.now_local(only_date=True):
                        try:
                            employee = UserModel.objects.get(
                                hashed_email=data_mask.hash_field(
                                    UserManager.normalize_email(emp_email)
                                )
                            )
                            if employee.is_active and employee.user_type == UserTypes.EMPLOYEE:
                                employee.is_active = False
                                employee.save(update_fields=['is_active'])
                                inactive_employee.append(employee)
                                self.change_user_status_in_rb(employee)
                        except UserModel.DoesNotExist:
                            print(f'Employee does not exist in AMS from this email: {emp_email}')

                        continue

                if manager_code:
                    # fetch manager_email from employees_data.
                    try:
                        manager_data = result_data.get(manager_code)
                        manager_email = manager_data.get('emailAddress')
                        manager_leaving_date = manager_data.get('dateofLeaving')
                    except Exception:
                        print(f'Detail of manager code({manager_code}) does not exist')
                        continue

                    try:
                        reportee = UserModel.objects.get(
                            hashed_email=data_mask.hash_field(
                                UserManager.normalize_email(emp_email)
                            )
                        )
                        # mark active those employees which are in_active in AMS,
                        # buy active in HR-One system.
                        if not reportee.is_active and not leaving_date:
                            try:
                                reportee.is_active = True
                                reportee.save()
                                self.change_user_status_in_rb(reportee)
                            except Exception:
                                print(f'unable to active user: {reportee}')

                    except UserModel.DoesNotExist:
                        print('Reportee User Does not exist: %s' % emp_email)
                        continue

                    try:
                        manager = UserModel.objects.get(
                            hashed_email=data_mask.hash_field(
                                UserManager.normalize_email(manager_email)
                            )
                        )
                        # mark active those employees which are in_active in AMS,
                        # buy active in HR-One system.
                        if not manager.is_active and not manager_leaving_date:
                            try:
                                manager.is_active = True
                                manager.save()
                                self.change_user_status_in_rb(manager)
                            except Exception:
                                print(f'unable to active user: {manager}')

                    except UserModel.DoesNotExist:
                        print('Manager User Does not exist: %s' % manager_email)
                        continue

                    if manager.is_active and reportee.is_active:
                        start_date_emp = timezone.now_local(only_date=True)
                        current_month_start = timezone.get_current_month_start().date()
                        _, prev_month_end_date = timezone.get_prev_month_boundaries()

                        current_reporting = reportee.get_reporting_on_date(
                            as_on_date=start_date_emp,
                            reporting_type=ReportingType.EMPLOYEE,
                        )
                        if not current_reporting:
                            new_reporting = UserReporting.objects.create(
                                manager=manager,
                                reportee=reportee,
                                start_date=current_month_start,
                                reporting_type=ReportingType.EMPLOYEE
                            )
                            update_change_reason(new_reporting, "Requested from HROne")
                            print(
                                'New Manager %s allocated To %s'
                                % (manager.unique_code, reportee.unique_code)
                            )

                        else:  # current manager same as new manager
                            if current_reporting.manager.unique_code == manager.unique_code:
                                print(
                                    'Reportee[%s] already reports to new manager[%s]'
                                    % (reportee.unique_code, manager.unique_code)
                                )
                            elif current_reporting.start_date == current_month_start:
                                # current reporting starts from current month then
                                # update the manager only.
                                current_reporting.manager = manager
                                current_reporting.save(update_fields=['manager'])
                                new_reporting_reason = 'New Manager %s allocated To %s' % \
                                    (manager.unique_code, reportee.unique_code)
                                update_change_reason(current_reporting, "Requested from HROne")
                                print(new_reporting_reason)
                            else:
                                # end the current reporting and create the new reporting
                                # with new manager.
                                current_reporting.end_date = prev_month_end_date.date()
                                current_reporting.save(update_fields=['end_date'])
                                update_change_reason(current_reporting, "Requested from HROne")
                                new_reporting = UserReporting.objects.create(
                                    manager=manager,
                                    reportee=reportee,
                                    start_date=current_month_start,
                                    reporting_type=ReportingType.EMPLOYEE
                                )
                                update_change_reason(new_reporting, "Requested from HROne")
                                print(
                                    'New Manager %s allocated To %s'
                                    % (manager.unique_code, reportee.unique_code)
                                )
                else:
                    print(f'Manager Code not found: "{manager_code}"')

            except Exception as e:
                print(
                    'AMS-HRONE Error[reportee: %s and manager: %s]: %s'
                    % (emp_email, manager_code, str(e))
                )

        if inactive_employee:
            print('Mark employee inactive requested from HR-One: ')
            print(inactive_employee)
