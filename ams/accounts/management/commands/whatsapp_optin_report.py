import csv
import os
import tempfile

from django.core.management.base import BaseCommand
from django.core.files import File
from django.conf import settings
from ams.accounts.models import User
from django.utils.encoding import smart_str
from ams.utils import email


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--status', type=bool)

    def handle(self, *args, **options):
        """
            Send Email containing WHATSAPP_OPT-IN/OUT report to
            the designated recipients depending on the
            status asked.
        """
        status = options.get('status', False)

        if not isinstance(status, bool):
            print("[AMS] Please provide bool value(True/False)")
            raise Exception("[AMS] Status not provided.")

        users = User.objects.filter(
            whatsapp_notification_enabled=status
        )
        file_path = os.path.join(
            tempfile.gettempdir(), 'whatsApp_status_report.csv'
        )
        csv_file = tempfile.NamedTemporaryFile(
            delete=False, prefix=file_path,
            suffix='.csv', mode='r+'
        )
        csv_columns = ['name', 'pos_code', 'whatsapp_status']
        writer = csv.DictWriter(
            csv_file,
            lineterminator="\n",
            quoting=csv.QUOTE_MINIMAL,
            fieldnames=csv_columns
        )
        writer.writeheader()

        for user in users:
            try:
                writer.writerow(
                    {
                        "name": smart_str(user.get_full_name()),
                        "pos_code": smart_str(user.pos_code),
                        "whatsapp_status": smart_str(
                            user.whatsapp_notification_enabled
                        )
                    }
                )
            except Exception as e:
                print("[AMS] error while fetching user details: %s" % str(e))

        email_status = email.send(
            settings.WHATSAPP_OPTIN_REPORT_RECIPENT,
            "Whatsapp OPT-IN/OUT Status",
            html_body="PFA",
            attachments=[File(csv_file)]
        )
        print(
            "[AMS] Email status for WhatsApp Opt report: %s" % email_status
        )
