from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.db.models import Q

from ams.accounts.models import UserReporting, ReportingType
from ams.utils import email

UserModel = get_user_model()


class Command(BaseCommand):
    """"
        This command send an email to the manager for
        the approval of new mapping request.
    """
    help = 'Send an email to manager for approval of new mapping.'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        suggested_by = UserReporting.objects.filter(
            ~Q(suggested_by=None),
            end_date=None
        ).values_list('suggested_by__unique_code', flat=True)

        if suggested_by:
            suggested_by = list(set(suggested_by))

            # Getting manager of manager(that suugest new reportings).
            reportings = UserReporting.objects.select_related(
                'manager', 'reportee').filter(
                reportee__unique_code__in=suggested_by,
                reporting_type=ReportingType.EMPLOYEE,
                end_date=None
            )

            reportings = set(reportings)

            for reporting in reportings:
                try:
                    # send email to all supervisor.
                    to = reporting.manager.decrypted_email
                    print(f'send to: {to}')
                    subject = 'Approve new mapping request.'
                    content = f'''
                        <h4>Hi {reporting.manager.get_full_name()},</h4><br>
                        <p>New mapping request are pending for your approval.<p>
                        <p>Click <a href="{settings.DOSSIER_DASHBOARD_URL}manageteam/">here</a> to approve/reject.</p><br>
                        <p>Regards</p>
                        <p>Team Renewbuy</p>
                    '''

                    email.send(
                        to=to,
                        subject=subject,
                        html_body=content
                    )
                except Exception:
                    print(
                        f'Unable to send email to: {to}'
                    )

        else:
            print('No new request raised for partner mapping.')
