from datetime import datetime
import json

from django.core.management.base import BaseCommand

from ams.accounts.models import Cluster


class Command(BaseCommand):
    """
        Usage: ./manage.py create_cluster
    """

    help = 'Create Cluster'

    def add_arguments(self, parser):
        parser.add_argument('json', type=str)

    def handle(self, *args, **options):
        '''
        Create Cluster from given json file.
        >>> Json format:
            [
                {
                    "cluster_name": "East1",
                    "group_by": "pincode",
                    "cluster_data": {
                        "primary_address__pincode__in": [
                            "121001", "121002", "121003", "121004", "121005"
                        ]
                    },
                    "start_date": "01-06-2020",
                    "end_date": null
                },
                ...
            ]
        '''
        if not options.get('json'):
            return 'Please provide json file path.'

        clusters_defination = json.load(open(options.get('json')))
        for cluster_data in clusters_defination:
            cluster_data.update({
                'start_date': datetime.strptime(
                    cluster_data.get('start_date'), "%d-%m-%Y"
                )
            })
            cluster, _ = Cluster.objects.get_or_create(**cluster_data)
            print(
                f'Cluster Created: {cluster.cluster_code}'
            )
