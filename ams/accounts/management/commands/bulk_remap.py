import csv
from datetime import datetime
import json
import os
import tempfile
import re

import requests
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.files import File
from django.core.management.base import BaseCommand
from django.db.models import Q
from simple_history.utils import update_change_reason

from ams.accounts.helper import manager_column_header, reporting_type_dict
from ams.accounts.managers import UserManager
from ams.accounts.models import ReportingType, User, UserReporting, UserTypes
from ams.utils import data_mask, email, sms, timezone
from ams.utils.simple_history import update_history_user


class Command(BaseCommand):
    help = 'Remap managers for a list of reportees'

    LOG_DEBUG = 'DEBUG'
    LOG_INFO = 'INFO'
    LOG_WARN = 'WARN'
    LOG_ERROR = 'ERROR'

    error_list = []

    sms_context_dict = {
        ReportingType.PARTNER_TELE: 'Tele',
        ReportingType.PARTNER_FIELD: 'Motor',
        ReportingType.PARTNER_HEALTH_FRM: 'Health',
        ReportingType.PARTNER_LIFE_FRM: 'Life'
    }

    def __init__(self, *args, **kwargs):
        self.error_list = []

        if settings.IS_DEV_ENV:
            self.reporting_change_history_user = {
                # Sachin Saini (Test)
                ReportingType.PARTNER_FIELD: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_TELE: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_KEY_RELATIONSHIP_FRM: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_ACTIVATION_FRM: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_ACQUISITION_FRM: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_HEALTH_FRM: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_LIFE_FRM: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_RENEWAL_TRM: User.objects.get(unique_code='RT00012202'),
                ReportingType.EMPLOYEE: User.objects.get(unique_code='RT00012202'),
                ReportingType.PARTNER_PARTNER: User.objects.get(unique_code='RT00012202'),
            }
        else:
            self.reporting_change_history_user = {
                # Mohit Sharma (Operations)
                ReportingType.PARTNER_FIELD: User.objects.get(unique_code='EM00003205'),
                # Ranjay Singh
                ReportingType.PARTNER_TELE: User.objects.get(unique_code='RB00120904'),
                # Kumar Priyawart
                ReportingType.PARTNER_KEY_RELATIONSHIP_FRM: User.objects.get(unique_code='RB00136522'),
                # Kumar Priyawart
                ReportingType.PARTNER_ACTIVATION_FRM: User.objects.get(unique_code='RB00136522'),
                # Kumar Priyawart
                ReportingType.PARTNER_ACQUISITION_FRM: User.objects.get(unique_code='RB00136522'),
                # Kumar Priyawart
                ReportingType.PARTNER_HEALTH_FRM: User.objects.get(unique_code='RB00136522'),
                # Kumar Priyawart
                ReportingType.PARTNER_LIFE_FRM: User.objects.get(unique_code='RB00136522'),
                # Kumar Priyawart
                ReportingType.PARTNER_RENEWAL_TRM: User.objects.get(unique_code='RB00136522'),
                # Priya Singh (HR)
                ReportingType.EMPLOYEE: User.objects.get(unique_code='RB00103852'),
                # Shweta Sharma (TA)
                ReportingType.EMPLOYEE: User.objects.get(unique_code='EM00003203'),
            }
        super().__init__(*args, **kwargs)

    def add_arguments(self, parser):
        parser.add_argument(
            'type',
            type=str,
            choices=[
                'FRM', 'TRM', 'KRFRM', 'ACTFRM', 'AQFRM', 'HFRM', 'LFRM',
                'RENEWALTRM', 'EMP', 'PARTNER', 'PARTNEREMP'
            ]
        )
        parser.add_argument('file', type=str)

    def log(self, loglevel, msg):
        final_msg = '[%s] %s: %s' % (datetime.now().isoformat(), loglevel, msg)
        if loglevel == self.LOG_ERROR:
            # Add extra newlines, so ERRORs are more visible
            self.error_list.append(msg)
            final_msg = '\n%s\n' % final_msg
            self.stdout.write(final_msg)
        else:
            self.stdout.write(final_msg)

    def print_error_list(self):
        if not self.error_list:
            self.log(self.LOG_INFO, 'No Errors Found')
            return
        self.log(self.LOG_WARN, 'List of errors:')
        for error in set(self.error_list):
            self.log(self.LOG_WARN, '%s' % error)

    def send_mapping_to_rb(self, data):
        data.update({
            'passkey': settings.RB_PARTNER_CREATION_PASS
        })
        response = requests.post(
            '{}api/v1/accounts/reporting_update/'.format(settings.RENEWBUY_DOMAIN['URL']),
            headers={
                'API-SECRET-KEY': settings.RENEWBUY_DOMAIN['API-SECRET-KEY'],
                'APP-ID': settings.RENEWBUY_DOMAIN['APP-ID'],
                'Content-Type': 'application/json'
            },
            data=json.dumps(data),
            timeout=2  # Setting 2 seconds long timeout.
        )
        if response.status_code != 200:
            return False
        return True

    def remove_partner_employee_mapping(self, new_reporting, reason, approval):
        if new_reporting.reporting_type == ReportingType.PARTNER_FIELD:
            current_month_start = timezone.get_current_month_start().date()
            _, prev_month_end_date = timezone.get_prev_month_boundaries()
            try:
                p_e_reporting = new_reporting.reportee.get_current_reporting(
                    reporting_type=ReportingType.PARTNER_EMPLOYEE
                )
                if p_e_reporting.start_date == current_month_start:
                    p_e_reporting.delete()
                else:
                    p_e_reporting.end_date = prev_month_end_date.date()
                    p_e_reporting.save()
                    update_change_reason(
                        instance=p_e_reporting,
                        reason=f'Removed due to FRM mapped by ops team. Reason: {reason}. Approval by: {approval}'
                    )
                    update_history_user(
                        instance=p_e_reporting,
                        history_user=self.reporting_change_history_user
                    )
            except Exception:
                pass

    def map_rm(self, reporting_type, filepath):
        count = 0
        start_date_emp = timezone.now_local(only_date=True)
        errors = set()
        try:
            self.reporting_change_history_user = self.reporting_change_history_user.get(reporting_type)
            status, recipient_email = self.is_valid_email(filepath)
            if status:
                self.reporting_change_history_user = User.objects.get(
                    hashed_email=data_mask.hash_field(
                        UserManager.normalize_email(recipient_email)
                    )
                )
        except Exception:
            pass
        if (
            settings.BULK_SEND_NOTIFICATION_FOR_BULK_REMAP and
            settings.SEND_NOTIFICATION_BULK_REMAP
        ):
            notification_partners = []
        else:
            notification_partners = None
        mapping_status_list = []
        deleted_mapping = []
        with open(filepath, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                count += 1
                # AMS ID / POS Code / Email ID
                executive_code = row.get('Partner ID', row.get('Reportee')).strip()
                new_manager_email = row[manager_column_header.get(reporting_type)].strip()
                approval = row['Approved By'].strip()
                reason = row['Reason for Remapping'].strip()

                current_reporting = None
                new_reporting = None

                _, prev_month_end_date = timezone.get_prev_month_boundaries()
                current_month_start = timezone.get_current_month_start().date()

                if not executive_code:
                    continue
                try:
                    executive = User.objects.get(
                        Q(unique_code__iexact=executive_code) |
                        Q(pos_code__iexact=executive_code) |
                        Q(
                            hashed_email=data_mask.hash_field(
                                UserManager.normalize_email(executive_code)
                            )
                        )
                    )
                except User.DoesNotExist:
                    error = f'ERROR: User with this code does not exist {executive_code}'
                    self.log(self.LOG_ERROR, error)
                    errors.add(error)
                    row.update({
                        'status': error
                    })
                    mapping_status_list.append(row)
                    continue

                try:
                    if new_manager_email.upper() == 'TO_BE_REMOVED':
                        current_reporting = executive.get_reporting_on_date(
                            as_on_date=start_date_emp,
                            reporting_type=reporting_type,
                        )
                        if current_reporting and current_reporting.start_date == current_month_start:
                            current_reporting.delete()
                            deleted_mapping.append({
                                'manager__unique_code': current_reporting.manager.unique_code,
                                'reportee__unique_code': current_reporting.reportee.unique_code,
                                'reporting_type': current_reporting.reporting_type,
                                'start_date': current_reporting.start_date.strftime('%Y-%m-%d'),
                                'end_date': current_reporting.start_date.strftime('%Y-%m-%d'),
                            })
                            self.log(
                                self.LOG_INFO,
                                f'Removed reporting manager for code {executive_code}'
                            )

                            change_message = (f'Removed reporting. Reason: {reason}. '
                                              f'Approval By: {approval}. and in Rb: {status}')
                            row.update({
                                'status': change_message
                            })
                            mapping_status_list.append(row)
                        elif current_reporting:
                            current_reporting.end_date = prev_month_end_date.date()
                            current_reporting.save(update_fields=['end_date'])

                            self.log(
                                self.LOG_INFO,
                                f'Removed reporting manager for code {executive_code}'
                            )

                            change_message = (f'Removed reporting. Reason: {reason}. '
                                              f'Approval By: {approval}.')
                            row.update({
                                'status': change_message
                            })
                            mapping_status_list.append(row)

                            # Update the details of the last historical record.
                            update_change_reason(
                                instance=current_reporting,
                                reason=change_message,
                            )
                            update_history_user(
                                instance=current_reporting,
                                history_user=self.reporting_change_history_user,
                            )
                    else:
                        try:
                            manager = User.objects.get(
                                Q(referral_code=new_manager_email) |
                                Q(
                                    hashed_email=data_mask.hash_field(
                                        UserManager.normalize_email(new_manager_email)
                                    )
                                )
                            )
                        except Exception:
                            error = f'ERROR: User does not exist {new_manager_email}'
                            self.log(self.LOG_ERROR, error)
                            errors.add(error)
                            row.update({
                                'status': error
                            })
                            mapping_status_list.append(row)
                            continue
                        # Check the manager is RM or not
                        new_reporting_type = reporting_type
                        append_change_msg = ''
                        if manager.reportee.filter(
                            reporting_type=ReportingType.EMPLOYEE,
                            end_date=None
                        ).exists():
                            if executive.user_type == UserTypes.PARTNER:
                                new_reporting_type = ReportingType.PARTNER_EMPLOYEE
                                append_change_msg = ' (P-E Mapping as manager is not RM.)'
                            elif executive.user_type == UserTypes.EMPLOYEE:
                                new_reporting_type = ReportingType.EMPLOYEE
                        p_e_reporting = None
                        if reporting_type != new_reporting_type:
                            p_e_reporting = executive.get_reporting_on_date(
                                as_on_date=start_date_emp,
                                reporting_type=new_reporting_type,
                            )

                        current_reporting = executive.get_reporting_on_date(
                            as_on_date=start_date_emp,
                            reporting_type=reporting_type,
                        )
                        if not current_reporting and not p_e_reporting:
                            self.log(
                                self.LOG_INFO,
                                f'New manager {manager} allocated to {executive}'
                            )
                            new_reporting = UserReporting.objects.create(
                                reportee=executive,
                                manager=manager,
                                start_date=current_month_start,
                                reporting_type=new_reporting_type,
                            )
                            row.update({
                                'status': f'New manager {manager} allocated to {executive}' + append_change_msg
                            })
                            mapping_status_list.append(row)
                        else:
                            current_manager = current_reporting.manager if current_reporting else p_e_reporting.manager
                            if current_manager._email.lower() == manager._email.lower():
                                self.log(
                                    self.LOG_INFO,
                                    f'Not updating code {executive_code}, '
                                    f'because current and new manager is the same {current_manager}'
                                    f'{append_change_msg}'
                                )
                                row.update({
                                    'status': f'Not updating code {executive_code}, '
                                    f'because current and new manager is the same {current_manager}'
                                    f'{append_change_msg}'
                                })
                                mapping_status_list.append(row)
                                self.remove_partner_employee_mapping(current_reporting, reason, approval)
                                continue
                            else:
                                self.log(
                                    self.LOG_INFO,
                                    f'Changing manager for code {executive_code} to {manager}'
                                    f'{append_change_msg}'
                                )

                                if not current_reporting:
                                    current_reporting = p_e_reporting
                                if current_reporting.start_date == current_month_start:
                                    current_reporting.manager = manager
                                    current_reporting.reporting_type = new_reporting_type
                                    current_reporting.save(
                                        update_fields=['manager', 'reporting_type']
                                    )
                                    update_change_reason(
                                        instance=current_reporting,
                                        reason=f'New manager {manager} allocated to {executive}{append_change_msg}'
                                    )
                                    update_history_user(
                                        instance=current_reporting,
                                        history_user=self.reporting_change_history_user,
                                    )
                                    row.update({
                                        'status': f'New manager {manager} allocated to {executive}{append_change_msg}'
                                    })
                                    mapping_status_list.append(row)
                                    self.remove_partner_employee_mapping(current_reporting, reason, approval)
                                    continue
                                else:
                                    current_reporting.end_date = prev_month_end_date.date()
                                    current_reporting.save(update_fields=['end_date'])
                                    update_change_reason(
                                        instance=current_reporting,
                                        reason=f'Mapping end due to new manager({manager}) assigned. Reason: {reason}. Approval By: {approval}.',
                                    )
                                    update_history_user(
                                        instance=current_reporting,
                                        history_user=self.reporting_change_history_user,
                                    )
                                    new_reporting = UserReporting.objects.create(
                                        reportee=executive,
                                        manager=manager,
                                        start_date=current_month_start,
                                        reporting_type=new_reporting_type,
                                    )
                                    row.update({
                                        'status': f'New manager {manager} allocated to {executive}{append_change_msg}'
                                    })
                                    mapping_status_list.append(row)
                        if reporting_type in self.sms_context_dict.keys():
                            if settings.BULK_SEND_NOTIFICATION_FOR_BULK_REMAP:
                                notification_partners.append((
                                    new_reporting,
                                    reason
                                ))
                            elif settings.SEND_NOTIFICATION_BULK_REMAP:
                                self.send_notification((new_reporting, reason))
                        change_message = (f'Added reporting to {manager}. '
                                          f'Start Date: {current_month_start}. '
                                          f'Reason: {reason}. Approval By: {approval}.')

                        # Update the details of the last historical record.
                        update_change_reason(
                            instance=new_reporting,
                            reason=change_message,
                        )
                        update_history_user(
                            instance=new_reporting,
                            history_user=self.reporting_change_history_user,
                        )
                        # Remove existing P-E mapping if partner come to map with FRM.
                        self.remove_partner_employee_mapping(new_reporting, reason, approval)

                except (ValidationError, ValueError) as error:
                    self.log(self.LOG_ERROR, error)
                    error_with_partner_code_for_logging = '{} - {}'.format(executive, error)
                    errors.add(error_with_partner_code_for_logging)
                    row.update({
                        'status': error_with_partner_code_for_logging
                    })
                    mapping_status_list.append(row)
                    continue
                except Exception as error:
                    self.log(self.LOG_ERROR, error)
                    error_with_partner_code_for_logging = '{} - {}'.format(executive, error)
                    errors.add(error_with_partner_code_for_logging)
                    row.update({
                        'status': error_with_partner_code_for_logging
                    })
                    mapping_status_list.append(row)
                    continue

        if deleted_mapping:
            self.send_mapping_to_rb({'data': deleted_mapping})
        for e in errors:
            self.log(self.LOG_ERROR, e)
        return notification_partners, mapping_status_list

    def send_notification(self, partner_list):
        """
            Send Email and SMS notification when RM mapping change.
        """
        # Check partner is an instance of UserReporting or list.
        partner_list = [i for i in partner_list if i]

        self.log(
            self.LOG_INFO,
            "[NOTIFICATION] Sending to {} partner whose RM's changed.".format(len(partner_list))
        )

        for reporting, reason in partner_list:
            reportee_email = reporting.reportee.decrypted_email
            reportee_mobile = reporting.reportee.decrypted_mobile
            if 'resign' in reason:
                reason_to_send = 'Resigned'
            else:
                reason_to_send = 'Organisation level resource movement'

            user_reportings = UserReporting.objects.filter(
                reportee=reporting.reportee,
                reporting_type__in=[
                    ReportingType.PARTNER_TELE,
                    ReportingType.PARTNER_FIELD,
                    ReportingType.PARTNER_LIFE_FRM,
                    ReportingType.PARTNER_HEALTH_FRM
                ],
                end_date=None
            )
            reportee_fname = reporting.reportee.first_name.capitalize() if reporting.reportee.first_name else ''
            reportee_lname = reporting.reportee.last_name.capitalize() if reporting.reportee.last_name else ''
            context = {
                'partner_name': reportee_fname + ' ' + reportee_lname,
                'domain': settings.DOMAIN,
                'reason': reason_to_send,
                'data': [],
                'tele': {}
            }

            for user_reporting in user_reportings:
                first_name = (user_reporting.manager.first_name or '').capitalize()
                last_name = (user_reporting.manager.last_name or '').capitalize()
                if user_reporting.reporting_type == ReportingType.PARTNER_TELE:
                    context['tele'].update({
                        'name': first_name + ' ' + last_name,
                        'email': user_reporting.manager.decrypted_email,
                        'mobile': user_reporting.manager.caller_id,
                        'reportee_type': 'tele',
                        'product_name': 'Tele'
                    })
                if user_reporting.reporting_type == ReportingType.PARTNER_FIELD:
                    context['data'].append({
                        'name': first_name + ' ' + last_name,
                        'email': user_reporting.manager.decrypted_email,
                        'reportee_type': 'frm',
                        'product_name': 'Motor'
                    })
                if user_reporting.reporting_type == ReportingType.PARTNER_LIFE_FRM:
                    context['data'].append({
                        'name': first_name + ' ' + last_name,
                        'email': user_reporting.manager.decrypted_email,
                        'reportee_type': 'Life_frm',
                        'product_name': 'Life'
                    })
                if user_reporting.reporting_type == ReportingType.PARTNER_HEALTH_FRM:
                    context['data'].append({
                        'name': first_name + ' ' + last_name,
                        'email': user_reporting.manager.decrypted_email,
                        'reportee_type': 'Health_frm',
                        'product_name': 'Health'
                    })
                if user_reporting.reporting_type == ReportingType.PARTNER_EMPLOYEE:
                    context['data'].append({
                        'name': first_name + ' ' + last_name,
                        'email': user_reporting.manager.decrypted_email,
                        'reportee_type': 'General Manager',
                        'product_name': 'DIRECT'
                    })
            try:
                # Send email
                email.send_from_template(
                    to=reportee_email,
                    subject='Changed',
                    template='bulk_remap_notification.html',
                    context=context,
                    from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                )
            except Exception as email_error:
                self.log(
                    self.LOG_ERROR,
                    '[NOTIFICATION] Error in sending EMAIL. %s' % str(email_error)
                )
            try:
                # Send SMS
                msg = '''
                    Hi {FNAME}, we have updated your Service support and you can reach out to your {trm_frm_details}for any insurance assistance. Reason for change: {REASON}. Team RenewBuy
                '''
                trm_details = ''
                frm_details = ''
                if context.get('tele', {}).get('name'):
                    trm_details += 'new Telephonic Relationship Manager - {TRM_NAME} '.format(
                        TRM_NAME=context.get('tele', {}).get('name').capitalize()
                    )
                if context.get('tele', {}).get('mobile'):
                    number = context.get('tele', {}).get('mobile')
                    trm_details += '%s ' % number
                if (
                    reporting.manager.first_name and
                    self.sms_context_dict.get(user_reporting.reporting_type) != 'Tele'
                ):
                    last_name = ' ' + last_name if last_name else ''
                    frm_details += 'Field Relationship Manager({PRODUCT_NAME}) - {FRM_NAME} '.format(
                        PRODUCT_NAME=self.sms_context_dict.get(reporting.reporting_type),
                        FRM_NAME=first_name + last_name
                    )
                if trm_details and frm_details:
                    trm_frm_details = ' or '.join([trm_details, frm_details])
                elif trm_details:
                    trm_frm_details = trm_details
                elif frm_details:
                    trm_frm_details = frm_details
                reportee_fname = reporting.reportee.first_name.capitalize() if reporting.reportee.first_name else ''
                reportee_lname = ' ' + reporting.reportee.last_name.capitalize() if reporting.reportee.last_name else ''
                msg = msg.format(
                    FNAME=reportee_fname + reportee_lname,
                    trm_frm_details=trm_frm_details,
                    REASON=reason_to_send
                )
                sms.send(
                    number=reportee_mobile,
                    message=msg
                )
            except Exception as sms_error:
                self.log(
                    self.LOG_ERROR,
                    '[NOTIFICATION] Error in sending SMS. %s' % str(sms_error)
                )

    def handle(self, *args, **options):
        type_of_mapping = options.get('type').upper()
        filepath = options.get('file')

        if not filepath.endswith('.csv'):
            self.log(self.LOG_ERROR, 'File should be of CSV type')
            return 1

        try:
            temp_file_names = []
            split_file_names = self.split_files(filepath)
            for files in split_file_names:
                partners_to_notify, mapping_status_list = self.map_rm(
                    reporting_type_dict[type_of_mapping],
                    files
                )
                temp_file_names.append(
                    self.create_temp_file(mapping_status_list)
                )
            if settings.BULK_SEND_NOTIFICATION_FOR_BULK_REMAP and partners_to_notify:
                self.send_notification(partners_to_notify)
            # Check if file_name contains an email or not.
            # if contains, send mail to that email.
            status, recipient_email = self.is_valid_email(filepath)
            if status:
                self.send_mapping_report(
                    recipient_email,
                    temp_file_names
                )
        except KeyError:
            self.log(
                self.LOG_ERROR, f'Invalid type of mapping {type_of_mapping}'
            )
            return 1

        self.print_error_list()

    def create_temp_file(self, content):
        file_path = os.path.join(
            tempfile.gettempdir(), 'Mapping_status_report.csv'
        )
        csv_file = tempfile.NamedTemporaryFile(
            delete=False, prefix=file_path,
            suffix='.csv', mode='r+'
        )
        csv_columns = content[0].keys() if content else []
        writer = csv.DictWriter(
            csv_file,
            lineterminator='\n',
            quoting=csv.QUOTE_MINIMAL,
            fieldnames=csv_columns
        )
        writer.writeheader()

        for row in content:
            try:
                writer.writerow(row)
            except Exception:
                continue
        return csv_file.name

    def send_mapping_report(self, recipient_email, attachments=[]):
        """
        Send the status report to the mapping requester.
        """
        if not attachments:
            return
        if recipient_email:
            recipient_email = [recipient_email]
        else:
            recipient_email = settings.DEFAULT_MAPPING_STATUS_EMAIL
        email_status = email.send(
            recipient_email,
            'Mapping Status - %s' % timezone.now_local(only_date=True),
            html_body='PFA',
            attachments=list(map(lambda x: File(open(x)), attachments)),
            cc=settings.DEFAULT_MAPPING_STATUS_EMAIL
        )
        print('Email Sent Status: %s' % email_status)

    def is_valid_email(self, file_name):
        try:
            email = file_name.split('-')[-2]
            regex = r'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
            return bool(re.match(
                regex,
                email
            )), email
        except IndexError:
            return False, ''

    def split_files(self, full_file_path, rows_per_csv=1000):
        """
        method that would split the `example.csv` into
        smaller CSV files of n rows each (with header included)
        if example.csv has 401 rows for instance,
        this creates 3 files in same directory:
        - `example_1.csv` (row 1 - 200)
        - `example_2.csv` (row 201 - 400)
        - `example_3.csv` (row 401)
        """

        file_name = os.path.splitext(full_file_path)[0]
        output_file_name = []
        with open(full_file_path) as infile:
            reader = csv.DictReader(infile)
            header = reader.fieldnames
            rows = [row for row in reader]
            pages = []

            row_count = len(rows)
            start_index = 0
            # here, we slice the total rows into pages,
            # each page having [row_per_csv] rows
            while start_index < row_count:
                pages.append(rows[start_index: start_index + rows_per_csv])
                start_index += rows_per_csv

            for i, page in enumerate(pages):
                with open('{}_{}.csv'.format(file_name, i + 1), 'w+') as outfile:
                    writer = csv.DictWriter(outfile, fieldnames=header)
                    writer.writeheader()
                    for row in page:
                        writer.writerow(row)

                print(
                    'DONE splitting {} into {} files'.format(file_name, len(pages))
                )
                print('split file names : %s' % outfile.name)
                output_file_name.append(outfile.name)
        return output_file_name
