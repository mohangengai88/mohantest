import csv
import requests
from datetime import date, datetime
from django.conf import settings
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils.crypto import get_random_string
from rest_framework import status

from ams.accounts.models import User
from ams.utils import email, try_or
from ams.utils.data_mask import generate_hash


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--start_date')
        parser.add_argument('--end_date')

    def handle(self, *args, **options):
        """
        Sample usage: ./manage.py t1_retail_data
        """
        start_date = options.get('start_date')
        end_date = options.get('end_date')
        if not end_date:
            print('Please provide a date(dd/mm/yyyy) argument.')
            return
        if start_date and not isinstance(start_date, date):
            start_date = datetime.strptime(start_date, '%d/%m/%Y').date()
        if not isinstance(end_date, date):
            end_date = datetime.strptime(end_date, '%d/%m/%Y').date()
        if not start_date:
            start_date = end_date.replace(day=1)
        users = User.objects.filter(
            ~Q(agency_name__icontains='direct'),
            unique_code__startswith='RT',
            referral_code__isnull=True,
            created_at__date__range=(start_date, end_date)
        )
        try:
            accounts_fields = [
                'Registered Date', 'Code', 'First Name', 'Last Name',
                'Gender', 'Mobile Number', 'POS Completed',
                'Referral Code', 'RM Name', 'ASM Name',
            ]

            random_string = get_random_string(20)
            rb_settings = settings.RENEWBUY_DOMAIN
            headers = {
                'API-SECRET-KEY': rb_settings.get('API-SECRET-KEY'),
                'app-id': rb_settings.get('APP-ID'),
            }
            data = {
                "date_range": {
                    "start_date": start_date.strftime('%d/%m/%Y'),
                    "end_date": end_date.strftime('%d/%m/%Y'),
                },
                "pass_key": "&uBU;VTWL*w!x8+v",
                "hash_key": random_string
            }
            url = rb_settings.get('URL') + '/api/v2/executives/hierarchy/'
            hierarchy_response = requests.post(
                url=url,
                json=data,
                headers=headers
            )
            if hierarchy_response.status_code != status.HTTP_200_OK:
                print('Unable to get response from renewbuy\'s server.')
                print(hierarchy_response.text)
                return
            hierarchy_json = hierarchy_response.json()
            if len(hierarchy_json) == 0:
                print('Did not find any data from the response.')
                return

            print('Generating Accounts data.')
            accounts_filename = 'Accounts/t1_accountsRT.csv'
            accounts_csv = default_storage.open(accounts_filename, 'w')
            accounts_writer = csv.DictWriter(accounts_csv, fieldnames=accounts_fields)
            accounts_writer.writeheader()

            for user in users:
                digest = generate_hash(random_string, user.decrypted_email)
                user_data = hierarchy_json.get(str(digest), '')
                accounts_writer.writerow({
                    accounts_fields[0]: try_or(
                        lambda: user.created_at.strftime('%d/%m/%Y'),
                        ''
                    ),
                    accounts_fields[1]: try_or(
                        lambda: user_data['code'],
                        user.pos_code or user.unique_code or ''
                    ),
                    accounts_fields[2]: user.first_name,
                    accounts_fields[3]: try_or(lambda: user.last_name, ''),
                    accounts_fields[4]: try_or(
                        lambda: 'M' if int(user.gender) == 1 else 'F',
                        'M'
                    ),
                    accounts_fields[5]: '',
                    accounts_fields[6]: try_or(
                        lambda: 'Yes' if user_data['is_pos_certified'] else 'No',
                        'Yes' if user.gi_pos_certified_date else 'No'
                    ),
                    accounts_fields[7]: try_or(lambda: user.agency_name, ''),
                    accounts_fields[8]: try_or(
                        lambda: user_data['rm_name'],
                        ''
                    ),
                    accounts_fields[9]: try_or(
                        lambda: user_data['asm_name'],
                        ''
                    ),
                })
            accounts_csv.close()

            if default_storage.exists(accounts_filename):
                accounts_csv = default_storage.open(accounts_filename, 'r')
                accounts_file = File(accounts_csv)

                subject = 'Account registration Data for Retail.'
                body = 'Total {ind} accounts registered for retail products.'
                html_body = body.format(
                    ind=len(users),
                )
                to = [
                    'maulik.jain@renewbuy.com',
                ]

                if settings.IS_DEV_ENV:
                    email.send(
                        'maulik.jain@renewbuy.com', subject, html_body,
                        attachments=[accounts_file]
                    )
                else:
                    email.send(
                        to, subject, html_body,
                        attachments=[accounts_file],
                    )

                print('Accounts data sent.')
            else:
                print('Error while generating Accounts data')
        except Exception as e:
            print(e)
