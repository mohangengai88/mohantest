import csv
from datetime import date, datetime
from django.conf import settings
from django.core.files import File
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand

from ams.accounts.models import User
from ams.utils import email, try_or


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--date')
        parser.add_argument('--start_date')

    def handle(self, *args, **options):
        """
        Sample usage: ./manage.py ta_registration_mis --date='dd/mm/yyyy'
        """
        try:
            start_date = options.get('start_date')
            end_date = options.get('date')
            if not end_date:
                print('Please provide a date(dd/mm/yyyy) argument.')
                return
            if start_date and not isinstance(start_date, date):
                start_date = datetime.strptime(start_date, '%d/%m/%Y').date()
            if not isinstance(end_date, date):
                end_date = datetime.strptime(end_date, '%d/%m/%Y').date()

            fields = [
                'Agency Name', 'Referral Code', 'Referee Name', 'First Name',
                'Last Name', 'Email ID', 'Mobile #', 'Category', 'GST', 'PAN #',
                'PAN Verification Status', 'City', 'State', 'Pin Code',
                'Address', 'Date of Registration',
            ]

            if not start_date:
                start_date = end_date.replace(day=1)

            users = User.objects.filter(
                unique_code__startswith='TA',
                created_at__date__range=(start_date, end_date),
            )

            if users:
                print('Generating MIS for TA registrations.')
                filename = f'ta_registration_mis_{end_date.strftime("%d-%m-%Y")}.csv'
                csv_file = default_storage.open(filename, 'w')
                csv_writer = csv.DictWriter(csv_file, fieldnames=fields)
                csv_writer.writeheader()

                for user in users:
                    print(user)

                    if user.manager.exists():
                        reporting = user.manager.filter(end_date=None).last()
                        if reporting:
                            manager = reporting.manager

                    csv_writer.writerow({
                        fields[0]: try_or(lambda: user.agency_name, ''),
                        fields[1]: try_or(lambda: manager.unique_code, ''),
                        fields[2]: try_or(lambda: manager.get_full_name(), ''),
                        fields[3]: try_or(lambda: user.first_name, ''),
                        fields[4]: try_or(lambda: user.last_name, ''),
                        fields[5]: try_or(lambda: user.decrypted_email, ''),
                        fields[6]: try_or(lambda: user.decrypted_mobile, ''),
                        fields[7]: try_or(lambda: user.unique_code, ''),
                        fields[8]: try_or(lambda: user.gstin_number, ''),
                        fields[9]: try_or(lambda: user.pan_number, ''),
                        fields[10]: try_or(lambda: user.is_pan_verified, ''),
                        fields[11]: try_or(
                            lambda: user.primary_address.city.city_name, ''
                        ),
                        fields[12]: try_or(
                            lambda: user.primary_address.city.state.state_name,
                            ''
                        ),
                        fields[13]: try_or(
                            lambda: user.primary_address.pincode, ''
                        ),
                        fields[14]: try_or(
                            lambda: user.primary_address.get_full_address(), ''
                        ),
                        fields[15]: try_or(
                            lambda: user.created_at.strftime('%d-%m-%Y'), ''
                        ),
                    })

                csv_file.close()

                if default_storage.exists(filename):
                    csv_file = default_storage.open(filename, 'r')
                    attachment_file = File(csv_file)

                    subject = f'TravAssured Registration MIS for {end_date.strftime("%d-%m-%Y")}'
                    body = ''
                    to = [
                        'tarun.garg@renewbuy.com',
                        'vipin.kumar@renewbuy.com',
                        'support@travassured.com',
                    ]
                    cc = [
                        'maulik.jain@renewbuy.com',
                        'vijay.tiwari@renewbuy.com',
                        'nalin.bhuraria@renewbuy.com',
                        'shweta@travassured.com',
                    ]

                    if settings.IS_DEV_ENV:
                        email.send('maulik.jain@renewbuy.com', subject,
                                   body, attachments=[attachment_file],
                                   from_email=settings.TRAVASSURED_FROM_EMAIL)
                    else:
                        email.send(to, subject, body,
                                   attachments=[attachment_file], cc=cc,
                                   from_email=settings.TRAVASSURED_FROM_EMAIL)

                    print('TravAssured registrations MIS sent.')
                else:
                    print('Error while generating registrations MIS.')

            else:
                print('No details for generating an MIS found.')

        except Exception as e:
            print(e)
