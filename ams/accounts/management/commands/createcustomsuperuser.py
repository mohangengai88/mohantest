from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from ams.accounts.models import UserTypes


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.UserModel = get_user_model()

    def handle(self, *args, **options):
        if self.UserModel.objects.count() == 0:
            # Create all admin superuser accounts.
            for user in settings.ADMINS:
                name = user[0].replace(' ', '')
                email = user[1]
                print('Creating account for %s (%s)' % (name, email))
                self.UserModel.objects.create_superuser(
                    _email=email,
                    first_name=name,
                    password='root',
                    user_type=UserTypes.EMPLOYEE
                )
        else:
            print('Admin accounts can only be initialized if no users exist')
