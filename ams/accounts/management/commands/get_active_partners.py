import json
import logging

import requests

from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from glue.registry import call

from ams.accounts.models import UserReporting, ReportingType
from ams.utils import flatten


logger = logging.getLogger(__name__)

UserModel = get_user_model()


class Command(BaseCommand):
    """
        This command get all the business of partners through calling
        dossier api then filter activated partners from them and
        then create new reporting for them.
     """

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        # active partners to store all the active data
        active_partners = []

        acquistion_partners = UserReporting.objects.filter(
            reporting_type=ReportingType.PARTNER_ACQUISITION_FRM
        ).values_list('reportee__unique_code')

        # flatten() use to convert a list of tuple into a list of element.
        acquistion_partners = list(flatten(acquistion_partners))

        headers = {'Content-Type': 'application/json'}

        # partners_business_list get all partners business through
        # calling dossier api.
        try:
            partners_business_list = requests.post(
                url=settings.PARTNER_ACTIVATION_RULE['PARTNER_BUSINESS_DATA_API'],
                data=json.dumps({'users': acquistion_partners}),
                headers=headers
            )
            # here response_object.json() stores the response data.
            partners_business_list = partners_business_list.json()
        except Exception:
            print(f'Unable to fetch data from Dossier Api')

        for partner_business in partners_business_list:
            partner_code = partner_business.get('unique_code')
            total_policies = partner_business.get('total_policies')
            total_business = partner_business.get('total_business')

            try:
                partner = UserModel.objects.get(unique_code=partner_code)

                if partner and total_business and total_policies:
                    # partner activation requirements.
                    if (
                        total_business >= settings.PARTNER_ACTIVATION_RULE['TOTAL_BUSINESS'] or
                        total_policies >= settings.PARTNER_ACTIVATION_RULE['TOTAL_POLICY']
                    ):
                        # append only those partners which are activated.
                        active_partners.append(partner)
            except UserModel.DoesNotExist:
                print(
                    'Partner(%s) & their policy & business may does not exist' %
                    partner_code
                )

        # call PartnerMapping Service to set reporting for active partners.
        call(
            'accounts',
            'partner_mapping',
            data={
                'active_partners': active_partners
            }
        )
