import csv
from datetime import datetime

from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand
from requests import get

from ams.accounts.managers import UserManager
from ams.accounts.models import ReportingType, User, UserReporting
from ams.utils.data_mask import hash_field


class Command(BaseCommand):
    """
    Sample usage:
        ./manage.py renewbuy_reporting_import --url='<FILE_URL>'
        dev_file_url = 'https://rb-media-dev.s3.ap-south-1.amazonaws.com/media/rb_reportings.csv'
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.reporting_type_dict = {
            'FIELD': ReportingType.PARTNER_FIELD,
            'TELE': ReportingType.PARTNER_TELE,
            'EMPLOYEE': ReportingType.EMPLOYEE,
            'HEALTH_EXP': ReportingType.HEALTH_EXPERT,
        }

    def add_arguments(self, parser):
        parser.add_argument('--url')

    def handle(self, *args, **options):
        input_fields = ['MANAGER_EMAIL', 'REPORTEE_EMAIL', 'START_DATE',
                        'END_DATE', 'REPORTING_TYPE', 'SAME_START_DATE',
                        'SAME_START_END_DATE']
        fields = ['MANAGER_EMAIL', 'REPORTEE_EMAIL', 'START_DATE', 'END_DATE',
                  'REPORTING_TYPE', 'ERROR']

        f = default_storage.open('erroneous_reportings.csv', 'w')
        writer = csv.DictWriter(f, fieldnames=fields)
        writer.writeheader()

        url = options.get('url')
        if not url:
            print('Please provide a file URL as argument.')
            return

        with open('/tmp/renewbuy_reporting.csv', 'wb') as file:
            response = get(url)
            file.write(response.content)

        with open('/tmp/renewbuy_reporting.csv', 'r') as file:
            reader = csv.DictReader(file)
            sorted_list = sorted(reader, key=lambda row: (row['REPORTEE_EMAIL'], row['START_DATE']))

        with open('/tmp/sorted_renewbuy_reporting.csv', 'w') as file:
            swriter = csv.DictWriter(file, fieldnames=input_fields)
            swriter.writeheader()
            for row in sorted_list:
                swriter.writerow(row)

        with open('/tmp/sorted_renewbuy_reporting.csv', 'r') as file:
            reader = csv.DictReader(file)
            for row in reader:
                try:
                    manager_email = row['MANAGER_EMAIL']
                    reportee_email = row['REPORTEE_EMAIL']
                    start_date = row['START_DATE']
                    end_date = row['END_DATE']
                    reporting_type = row['REPORTING_TYPE']

                    print(manager_email, reportee_email, start_date, end_date)

                    manager = User.objects.get(
                        hashed_email=hash_field(
                            UserManager.normalize_email(manager_email)
                        )
                    )
                    reportee = User.objects.get(
                        hashed_email=hash_field(
                            UserManager.normalize_email(reportee_email)
                        )
                    )

                    reporting, created = UserReporting.objects.get_or_create(
                        manager=manager,
                        reportee=reportee,
                        start_date=datetime.strptime(start_date, '%Y-%m-%d').date(),
                        reporting_type=self.reporting_type_dict[reporting_type]
                    )
                    if end_date:
                        reporting.end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
                        reporting.save()

                    print(reporting.id, created)

                except Exception as e:
                    writer.writerow({
                        fields[0]: manager_email,
                        fields[1]: reportee_email,
                        fields[2]: start_date,
                        fields[3]: end_date,
                        fields[4]: reporting_type,
                        fields[5]: e
                    })

            f.close()

            return default_storage.url('erroneous_reportings.csv')
