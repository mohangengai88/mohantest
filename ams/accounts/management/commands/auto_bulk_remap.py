"""
Description of File name saved which are uploaded by user.

{reporting_type}_mapping_{original_file_name}-{email}-{counter}.csv

where:
    reporting_type: Reporting type for which the mapping is selected.
    original_file_name: file name uploaded by user
    email: Email of uploader
    counter: Counts the no. of same files uploaded by user on same day.

`_mapping` must be in mapping file name.
"""

import os
import traceback

from django.core.management import call_command
from django.core.management.base import BaseCommand

from ams.utils import timezone


class Command(BaseCommand):
    help = 'Remap managers for a list of reportees'

    def add_arguments(self, parser):
        parser.add_argument('dir')

    def handle(self, *args, **options):
        d = timezone.now_local(only_date=True)
        override_dir = options.get('dir')
        if not override_dir:
            override_dir = '/mapping/{}/{}/{}'.format(str(d.year).zfill(2), str(d.month).zfill(2), str(d.day).zfill(2))

        try:
            files = os.listdir(override_dir)
        except FileNotFoundError:
            print(f'No mapping requests to be processed for {d}')
            return

        for file_name in files:
            if '_mapping' in file_name:
                filepath = f'{override_dir}/{file_name}'
                type_of_mapping = file_name.split('_')[0]

                print(f'Processing file: {filepath}')

                try:
                    call_command(
                        'bulk_remap', type_of_mapping.upper(), filepath
                    )
                except Exception:
                    print(traceback.format_exc())
