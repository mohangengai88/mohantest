import csv

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.files import File
from django.contrib.auth import get_user_model
from django.forms.models import model_to_dict
from django.db.models import Q

from simple_history.utils import update_change_reason

from ams.accounts.models import ReportingType, UserReporting
from ams.accounts.management.commands import bulk_remap
from ams.utils import create_csv_n_send, email, timezone

UserModel = get_user_model()


def get_active_manager(manager):
    # recursive function to get active manager of provided manager.
    active_manager = None
    managers = manager.manager.filter(end_date=None)

    if managers:
        # if only one manager exist of deactivated manager.
        if managers.count() == 1:
            try:
                manager = managers.get().manager
                # if manager is_active.
                if manager.is_active:
                    active_manager = manager
                else:
                    active_manager = get_active_manager(manager)
            except Exception:
                print(f'this manager({manager}) has more than one manger')

        else:  # multiple manager may exist for diff-diff porduct.
            manager_1 = manager_2 = None

            for reporting in managers:
                if reporting.reporting_type == ReportingType.PARTNER_EMPLOYEE:
                    manager_1 = reporting.manager
                    break

                if reporting.reporting_type == ReportingType.PARTNER_FIELD:
                    manager_2 = reporting.manager

            if manager_1:
                active_manager = manager_1 if manager_1.is_active else get_active_manager(manager_1)

            elif manager_2:
                active_manager = manager_2 if manager_2.is_active else get_active_manager(manager_2)

            else:
                print(f'This in_active BP_partner({manager.unique_code})'
                      f'does not have any manager or may have but not related to field manager & above')

    else:
        print(f'This in_active user({manager.unique_code})'
              f'does not have any manager.')

    return active_manager


class Command(BaseCommand):
    """"
        This script will do automate movement of mapping when any
        employeee terminate(in_active) then their reportees(only partner)
        will automatically mapped to the manager of terminated employee.
    """
    def handle(self, *args, **options):
        # `RM` reporting types related with product OR `bp_partner`.
        reporting_types = [ReportingType.PARTNER_FIELD,
                           ReportingType.PARTNER_HEALTH_FRM,
                           ReportingType.PARTNER_LIFE_FRM,
                           ReportingType.PARTNER_EMPLOYEE,
                           ReportingType.EMPLOYEE
                           ]

        updated_reporting = []

        # all reportings in which manager is 'inactive'
        # but reporting still exist.
        previous_reportings = UserReporting.objects.filter(
            Q(reporting_type__in=reporting_types) |
            Q(reporting_type=ReportingType.PARTNER_PARTNER),
            manager__is_active=False,
            end_date=None
        )

        # store data in file.
        if previous_reportings:
            try:
                lis = previous_reportings.values(
                    'reportee__unique_code', 'manager__unique_code',
                    'start_date', 'end_date', 'reporting_type'
                )
                keys = lis[0].keys()
                with open(
                    '/tmp/terminate-{}.csv'.format(timezone.now_local(only_date=True)), 'w'
                ) as output_file:
                    dict_writer = csv.DictWriter(output_file, keys)
                    dict_writer.writeheader()
                    dict_writer.writerows(lis)
            except Exception:
                raise Exception('Unable to make backup of previous reportings')

            print(f'Total reportings update in which inactive user as a manager: '
                  f'{previous_reportings.count()}')

            # reportees list that doesn't have any manager.
            unmapped_reportee_list = []
            # list of mangers & reportee that will get mail.
            manager_mail_list = []
            reportee_mail_list = []

            for reporting in previous_reportings:
                prev_manager = None

                # Move reportee above only when reportee's is `Active`.
                if not reporting.reportee.is_active:
                    try:
                        if reporting.reporting_type == ReportingType.EMPLOYEE:
                            pass
                        else:
                            if reporting.start_date == timezone.get_current_month_start().date():
                                reporting.delete()
                            else:
                                reporting.end_date = timezone.get_prev_month_boundaries()[1].date()
                                reporting.save()

                                updated_reporting.append([reporting.reportee.unique_code,
                                                          reporting.start_date,
                                                          reporting.end_date,
                                                          reporting.manager.unique_code,
                                                          None, None
                                                          ])
                                # Update the details of the last historical record.
                                reason = f'Reporting ended due to termination of manager{reporting.manager}'

                                update_change_reason(
                                    instance=reporting,
                                    reason=reason,
                                )
                        continue
                    except Exception as e:
                        print(f'Unable to end the reporting because - {e}')
                        continue

                print('Changing mapping for: ', model_to_dict(reporting))

                # getting active manager of in_active(terminated) employee.
                manager = get_active_manager(reporting.manager)

                # if there is no manager available in
                # above level then log that reportee
                if not manager:
                    if reporting.reportee.manager.filter(end_date=None).exclude(id=reporting.id):
                        pass
                    else:  # data rows of csv file
                        unmapped_reportee_list.append([reporting.reportee.unique_code])

                    try:
                        if reporting.start_date == timezone.get_current_month_start().date():
                            reporting.delete()
                        else:
                            reporting.end_date = timezone.get_prev_month_boundaries()[1].date()
                            reporting.save()

                            updated_reporting.append([reporting.reportee.unique_code,
                                                      reporting.start_date,
                                                      reporting.end_date,
                                                      reporting.manager.unique_code,
                                                      None, None
                                                      ])

                            # Update the details of the last historical record.
                            reason = f'Reporting ended due to termination of manager{reporting.manager}'

                            update_change_reason(
                                instance=reporting,
                                reason=reason,
                            )
                    except Exception as e:
                        print(f'Unable to end the reporting because - {e}')

                    continue

                else:
                    rm_reporting_types = [
                        ReportingType.PARTNER_FIELD,
                        ReportingType.PARTNER_HEALTH_FRM,
                        ReportingType.PARTNER_LIFE_FRM
                    ]
                    # IF employee-2-employee mapping.
                    if reporting.reporting_type == ReportingType.EMPLOYEE:
                        reporting_type = ReportingType.EMPLOYEE

                    # if manger is `ASM` or above designation then set reporting_type
                    # 'Partner-To-Employee` else set `product-specific` reporting_type
                    # with `RM`.
                    elif manager.reportee.filter(
                        reporting_type=ReportingType.EMPLOYEE, end_date=None
                    ):
                        reporting_type = ReportingType.PARTNER_EMPLOYEE
                    elif manager.reportee.filter(
                        reporting_type=ReportingType.PARTNER_PARTNER, end_date=None
                    ):
                        reporting_type = ReportingType.PARTNER_PARTNER
                    else:
                        try:
                            reporting_type = manager.reportee.filter(
                                reporting_type__in=rm_reporting_types,
                                end_date=None
                                ).first().reporting_type
                        except Exception:
                            reporting_type = ReportingType.PARTNER_EMPLOYEE

                    # if previous reporting start_date and the new reporting
                    # start_date is same then update the manager & reporting_type
                    # of previous reporting.
                    if reporting.start_date == timezone.get_current_month_start().date():
                        try:
                            prev_manager = reporting.manager
                            reporting.manager = manager
                            reporting.reporting_type = reporting_type
                            reporting.save()

                            updated_reporting.append([reporting.reportee.unique_code,
                                                      reporting.start_date,
                                                      reporting.start_date,
                                                      prev_manager.unique_code,
                                                      reporting.manager.unique_code,
                                                      reporting.start_date
                                                      ])
                            print(
                                'Mapping Updated[%s] on same date[manager=%s, r_type=%s]'
                                % (reporting.id, reporting.manager.unique_code,
                                    reporting_type)
                                )

                            reason = f'Mapping Updated[{reporting.id}] on same date[manager={reporting.manager.unique_code}, '
                            f'r_type={reporting_type}] due to termination of manager {prev_manager}'

                            # Update the details of the last historical record.
                            update_change_reason(
                                instance=reporting,
                                reason=reason,
                            )

                            # all managers that will get an email when new reportee assign.
                            manager_mail_list.append((
                                manager,
                                prev_manager
                            ))

                            reportee_mail_list.append((
                                reporting, 'resign'
                            ))

                        except Exception as e:
                            print(
                                'Unable to set user reporting for %s: %s' %
                                (reporting.reportee, e)
                            )
                    # if previous reporting start_date and the new reporting
                    # start_date is not same then end the previous reporting
                    # & create new reporting.
                    else:
                        try:
                            try:
                                prev_manager = reporting.manager
                                # end previous reporting.
                                reporting.end_date = timezone.get_prev_month_boundaries()[1].date()
                                reporting.save()
                                print(
                                    'Mapping Updated[%s] on end date only [manager=%s, r_type=%s]'
                                    % (reporting.id, reporting.manager.unique_code,
                                        reporting_type)
                                )
                                reason = f'Mapping ended due to termination of manager {reporting.manager}'

                                # Update the details of the last historical record.
                                update_change_reason(
                                    instance=reporting,
                                    reason=reason,
                                )

                            except Exception as e:
                                print(f'unable to end reporting: {reporting.id} because {e}')

                            # create new reporting.
                            user_reporting = UserReporting.objects.create(
                                manager=manager,
                                reporting_type=reporting_type,
                                reportee=reporting.reportee,
                                start_date=timezone.get_current_month_start().date()
                            )
                            if user_reporting.id:
                                print(
                                    'Mapping Created[%s]' % user_reporting.id
                                )
                                # Update the details of the last historical record.
                                reason = f"""New reporting created due to termination of
                                         reportee manager {reporting.manager}"""

                                updated_reporting.append([reporting.reportee.unique_code,
                                                          reporting.start_date,
                                                          reporting.end_date,
                                                          prev_manager.unique_code,
                                                          user_reporting.manager.unique_code,
                                                          user_reporting.start_date
                                                          ])

                                update_change_reason(
                                    instance=user_reporting,
                                    reason=reason,
                                )
                                manager_mail_list.append((
                                    manager,
                                    prev_manager
                                ))

                                reportee_mail_list.append((
                                    user_reporting, 'resign'
                                ))

                        except Exception as e:
                            print(
                                'Unable to set user reporting for reportee %s: %s' %
                                (reporting.reportee, e)
                            )

            # log reportee that does not have any manager
            # due to inactivation of current manager.
            if unmapped_reportee_list:
                email_sent = create_csv_n_send.create_and_send(
                    title='Reportee\'s that does not have any manager due to inactivation of current manager',
                    data=unmapped_reportee_list,
                    headers=['User Unique Code'],
                    to=settings.OPS_EMAIL + settings.MIS_EMAIL,
                    body='<h4>Hello Team,<br><h4>'
                         'Kindly refer attached file which contains Unique_codes of reportees '
                         'which does not have any manager<br>'
                         'Please check and take appropriate action in this matter.<br><br>'
                         'Regards, <br> Renewbuy Support',
                    cc=settings.DEFAULT_MAPPING_STATUS_EMAIL
                )
                if not email_sent:
                    print('Unable to send email at the moment.')

            # send termination email to reportee.
            if reportee_mail_list:
                try:
                    obj = bulk_remap.Command()
                    obj.send_notification(reportee_mail_list)
                except Exception as e:
                    print(f'Exception Raised: {e}')

            # send an email to all managers.
            if manager_mail_list:
                new_managers = {}
                for manager, prev_manager in set(manager_mail_list):
                    new_managers.update({
                        manager: []
                    })
                for manager, prev_manager in set(manager_mail_list):
                    inactive_manager = new_managers.get(manager, [])
                    reportees_data = prev_manager.reportee.filter(
                        end_date=timezone.get_prev_month_boundaries()[1].date(),
                        reportee__is_active=True
                        ).values(
                            'manager__first_name',
                            'manager__unique_code',
                            'reportee__first_name',
                            'reportee__unique_code'
                        )
                    reportees_count = reportees_data.count()

                    inactive_manager.append({
                        'prev_manager': prev_manager,
                        'reportees_count': reportees_count,
                        'reportees_data': reportees_data
                        })

                    new_managers.update({
                        manager: inactive_manager
                    })

                for manager, reporting_data in new_managers.items():
                    to = manager.decrypted_email

                    try:
                        reportees_data = []
                        attachment_file = []

                        for data in reporting_data:
                            reportees_data += list(data.get('reportees_data', []))

                        if reportees_data:
                            file_name = create_csv_n_send.create_temp_file(
                                'reporting_data-{}.csv'.format(timezone.now_local(only_date=True)),
                                reportees_data
                                )

                            attachment_file = list(map(lambda x: File(open(x)), [file_name]))

                        context = {
                            'salutation_name': manager.get_full_name(),
                            'reporting_data': reporting_data,
                            'leaving_date': timezone.get_yesterday_boundaries()[1].date()
                        }

                        email_sent = email.send_from_template(
                            to=to,
                            subject='New Mapping Updates',
                            template='mappings/termination_mail_to_manager.html',
                            context=context,
                            attachments=attachment_file,
                            from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                        )
                        if not email_sent:
                            print(f'Unable to sent an email to: {to}')
                    except Exception as e:
                        print(f'[ERROR]: Not able to sent an email at the moment beacause: {e}')

        # all reportings in which reportee is 'inactive' but
        # reporting still exist.
        previous_reportings = UserReporting.objects.filter(
            Q(reporting_type__in=reporting_types) |
            Q(reporting_type=ReportingType.PARTNER_PARTNER),
            reportee__is_active=False,
            end_date=None
        )

        for reporting in previous_reportings:
            if reporting.start_date == timezone.get_current_month_start().date():
                reporting.delete()
            else:
                try:  # end previous reporting.
                    reporting.end_date = timezone.get_prev_month_boundaries()[1].date()
                    reporting.save()

                    updated_reporting.append([reporting.reportee.unique_code,
                                              reporting.start_date,
                                              reporting.end_date,
                                              reporting.manager.unique_code,
                                              None, None
                                              ])

                    reason = f'Reporting ended due to termination of reportee {reporting.reportee}'

                    # Update the details of the last historical record.
                    update_change_reason(
                        instance=reporting,
                        reason=reason,
                    )

                except Exception as e:
                    print(f'unable to end reporting: {reporting.id} because {e}')

        # ALl updated reportings send to `HR`.
        if updated_reporting:
            try:
                email_sent = create_csv_n_send.create_and_send(
                    title='Report of updated reportings',
                    data=updated_reporting,
                    headers=[
                        'Reportee',
                        'Mapping start date',
                        'Mapping End date',
                        'Previous Manager',
                        'Current Manager',
                        'New Mapping start date'
                    ],
                    to=settings.HR_GENERAL_MAIL,
                    body='<h4>Hi,<br><h4>'
                         'Kindly refer attached file which contains data of updated reportings '
                         'due to termination of their manager.<br>'
                         'Please check and take appropriate action in this matter.<br><br>'
                         'Feel free to connect on "techsuppport@renewbuy.com" regarding any query of concern.<br> <br>'
                         'Regards, <br> Renewbuy Support',
                    cc=settings.DEFAULT_MAPPING_STATUS_EMAIL
                )
                print('detail: Email sent successfully.')
            except Exception as e:
                print('updated Reportings: ', updated_reporting)
                print(f'Unable to send email at the moment due to - {e} . Please try again later.')
