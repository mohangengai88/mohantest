from ams.accounts.models import User, BetaFeature
from django.core.management.base import BaseCommand
import csv


class Command(BaseCommand):
    """
    Beta Feature Mapping
    """
    help = "Auto mapping of users with beta feature"

    def add_arguments(self, parser):
        parser.add_argument(
            "--file",
            help="File path of CSV file."
        )

    def handle(self, *args, **options):
        file_name = options.get(
            'file',
            ''
        )
        with open(file_name, 'rt') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                feature_code = row['Feature_code']
                user = row['User']
                print(user, feature_code)
                try:
                    user = User.objects.get(unique_code=user)
                    B1 = BetaFeature.objects.get(feature_code=feature_code)
                    user.beta_enrollment.add(B1)
                    print("User mapped with beta featues successfully")

                except Exception as e:
                    print(e)
