import csv
import logging

from django.core.management.base import BaseCommand

from ams.accounts.models import User
from ams.utils import data_mask as security

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Mapping of Dialer Agent id'

    def add_arguments(self, parser):
        parser.add_argument(
            "--file",
            help="Provide the path of csv file."
        )

    def handle(self, *args, **options):
        file = options['file']

        with open(file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)

            for row in reader:
                user_email = row['Email'].strip()
                dialer_id = row['Dialer_id'].strip()
                user_obj = User.objects.filter(
                    hashed_email=security.hash_field(user_email.lower())
                ).first()

                if user_obj:
                    user_obj.dialer_agent_id = dialer_id
                    user_obj.save(update_fields=['dialer_agent_id'])
                    print("Successfully added dialer agent ID for: %s" % user_email)

                else:
                    print("Email not found: %s" % user_email)
