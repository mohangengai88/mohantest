import csv

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.db.models import Q

from ams.accounts.managers import UserManager
from ams.accounts.models import UserPincodeMapping
from ams.data.models import City
from ams.utils.data_mask import hash_field

UserModel = get_user_model()


class Command(BaseCommand):
    """
        This command update the pincode of given user from
        csv file into database.

        Usage: python manage.py update_pincode --file='<file_path>'
    """

    help = '''Custom command to update pincode for user.'''

    def add_arguments(self, parser):
        parser.add_argument(
            '--file',
            help='Please provide csv file path.'
        )

    def handle(self, *args, **options):
        filepath = options.get('file')
        with open(filepath, mode='r') as csv_file:
            reader = csv.DictReader(csv_file)

            for row in reader:
                email = row.get('mail')
                pincode = row.get('pincode')
                city = row.get('city')
                if not (email and pincode):
                    continue
                try:
                    user = UserModel.objects.filter(
                        Q(hashed_email=hash_field(
                            UserManager.normalize_email(email)
                        ))
                    ).first()
                except UserModel.DoesNotExist:
                    print(f'User({email}) Does not Exists.')

                if user:
                    # if pincode available of the user.
                    if pincode:
                        # if pincode available then update the user
                        # pincode in the database.
                        try:
                            city = City.objects.get(
                                city_name__iexact=city,
                                state__state_name__iexact=row.get('state')
                            )
                        except City.DoesNotExist:
                            city = None
                        try:
                            up, is_created = UserPincodeMapping.objects.get_or_create(
                                user=user, pincode=pincode, city=city
                            )
                            if is_created:
                                print(
                                    f'User[{user.unique_code}]-Pincode[{pincode}] mapping created.'
                                )
                        except Exception:
                            print(
                                f'Unable to update pincode[{pincode}] for the user{user.unique_code}'
                            )
                    else:
                        print(f'No pincode for this user({user.unique_code})')
