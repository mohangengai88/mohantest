import requests
from django.conf import settings
from rest_framework import status

from ams.accounts.exceptions import RbSignupException
from ams.accounts.models import ReportingType


manager_column_header = {
    ReportingType.PARTNER_FIELD: 'New FRM (Email)',
    ReportingType.PARTNER_TELE: 'New TRM (Email)',
    ReportingType.PARTNER_KEY_RELATIONSHIP_FRM: 'New KRFRM (Email)',
    ReportingType.PARTNER_ACTIVATION_FRM: 'New ACTFRM (Email)',
    ReportingType.PARTNER_ACQUISITION_FRM: 'New AQFRM (Email)',
    ReportingType.PARTNER_HEALTH_FRM: 'New HFRM (Email)',
    ReportingType.PARTNER_LIFE_FRM: 'New LFRM (Email)',
    ReportingType.PARTNER_RENEWAL_TRM: 'New RETRM (Email)',
    ReportingType.EMPLOYEE: 'New Manager (Email)',
    ReportingType.PARTNER_PARTNER: 'New Manager (Email)',
    ReportingType.PARTNER_EMPLOYEE: 'New PEMP (Email)',
}
reporting_type_dict = {
    'FRM': ReportingType.PARTNER_FIELD,
    'TRM': ReportingType.PARTNER_TELE,
    'KRFRM': ReportingType.PARTNER_KEY_RELATIONSHIP_FRM,
    'ACTFRM': ReportingType.PARTNER_ACTIVATION_FRM,
    'AQFRM': ReportingType.PARTNER_ACQUISITION_FRM,
    'HFRM': ReportingType.PARTNER_HEALTH_FRM,
    'LFRM': ReportingType.PARTNER_LIFE_FRM,
    'RENEWALTRM': ReportingType.PARTNER_RENEWAL_TRM,
    'EMP': ReportingType.EMPLOYEE,
    'PARTNER': ReportingType.PARTNER_PARTNER,
    'PARTNEREMP': ReportingType.PARTNER_EMPLOYEE,
}


def rb_signup_helper(**kwargs):
    """
        It triggers sign up requests to rb partner portal.
        :param kwargs, keyword arguments of partner details
        :return None or Response Object if status code will not be 200 or 201.
    """
    rb_signup_url = settings.RENEWBUY_DOMAIN.get('URL') + 'api/v1/accounts/signup/'
    partner_code = kwargs.pop('agency_name')
    kwargs.update({
        'is_executive': True,
        'type': 'public',
        'partner_code': partner_code,
        'send_otp': '=:U.d*47_^i~;5O'
    })
    try:
        rb_signup_response = requests.post(url=rb_signup_url, data=kwargs)

        if rb_signup_response.status_code not in [status.HTTP_200_OK,
                                                  status.HTTP_201_CREATED]:
            return rb_signup_response
    except Exception:
        raise RbSignupException(
            detail='Connection refused by the remote server.',
        )
