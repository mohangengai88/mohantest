import logging
from collections import Iterable
from datetime import datetime, timedelta
from functools import wraps
import re

import requests
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import PermissionDenied
from django.core.exceptions import ValidationError as DjangoValidationError
from django.db.models import DateField, F, IntegerField, Q, QuerySet, Value, Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.functions import Cast, Coalesce, Greatest, Least
from django.http.request import QueryDict
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from glue.models import APIKey
from glue.service import BaseService
from rest_framework.exceptions import (
    AuthenticationFailed, NotAuthenticated, ValidationError
)
from simple_history.utils import update_change_reason

from ams.accounts import helper
from ams.accounts.backends import JWTAuthentication
from ams.accounts.exceptions import (
    InvalidLoginException, InvalidUserTypeException,
    OptUserException, NotificationAPIException,
    RbSignupException, UserRequiredException
)
from ams.accounts.managers import UserManager
from ams.accounts.models import (
    Channel, Cluster, PartnerToEmployeeReportingType, ReportingType,
    UserCluster, UserConsent, UserPincodeMapping, UserReporting,
    UserTypes
)
from ams.accounts.serializers import (
    BackfillSerializer, BulkUserSerializer, ClusterSerializer,
    DocsSerializer, LoginSerializer, PasswordResetSerializer,
    RegistrationSerializer, UserClusterSerializer, UserSerializer
)
from ams.data.models import City, PolicyAndContract
from ams.otp.models import SMSDevice
from ams.utils import (
    clean_empty, email, flatten, sms, timezone, try_or
)
from ams.utils.data_mask import hash_field
from ams.utils.db import ExtractDaysFromPGInterval
from ams.utils.tasks import create_pdf_and_send

logger = logging.getLogger(__name__)

UserModel = get_user_model()

sensitive_post_parameters_m = method_decorator(
    sensitive_post_parameters(
        'password', 'old_password', 'new_password1', 'new_password2'
    )
)


def dashboard_login_permission(function):
    @wraps(function)
    def has_dashboard_login_permission(self, *args, **kwargs):
        if not self.user.id:
            return redirect('dash_login')

        if not (self.user.user_type == UserTypes.EMPLOYEE and self.user.is_staff):
            raise PermissionDenied()

        return function(self, *args, **kwargs)

    return has_dashboard_login_permission


def authenticate(request):
    # Jugaad authentication. To be made part of Glue in the near future.
    if isinstance(request.user, AnonymousUser):
        raise NotAuthenticated

    try:
        if isinstance(request.user, str):
            request.META = {
                'HTTP_AUTHORIZATION': f'Token {request.user}'
            }
        auth = JWTAuthentication().authenticate(request)
    except AttributeError:
        raise AuthenticationFailed

    if not auth:
        raise AuthenticationFailed

    return auth[0]  # Return the user instance.


class AuthenticationService(BaseService):
    """
    Example usage:
    ```
    call(
        'accounts',
        'authenticate',
        data={
            'username': '<Username>',
            'password': '<Password>'
        }
    )
    ```
    """
    is_global = True

    def validate(self):
        serializer = LoginSerializer(data=self.request.data)
        if serializer.is_valid(raise_exception=True):
            self.validated_data = serializer.data
            return True
        return False

    def run(self, **kwargs):
        # If validation is passed, then
        # return the serialized login response
        if self.validated_data:
            return self.validated_data
        raise InvalidLoginException


class LogoutService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'logout_everywhere',
            user='<Token>'
        )
        ```
    """
    is_global = True

    def run(self, **kwargs):
        if self.request.user:
            self.request.user.update_token_secret_key()
        return {'detail': 'Successfully logged out of all devices.'}


class PasswordResetService(BaseService):
    """
    Calls Django Auth PasswordResetForm save method.
    Accepts the following POST parameters: email
    Returns the success/fail message.
    """
    is_global = True

    def run(self, *args, **kwargs):
        # Create a serializer with request.data
        serializer = PasswordResetSerializer(
            data=self.request.data,
            context={'request': self.request}
        )
        serializer.is_valid(raise_exception=True)

        serializer.save()

        # Return the success message.
        return {'detail': 'Password reset e-mail has been sent.'}


class EmployeeSearchService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'employee_search',
            data = {
                'query': '<Search query>'
            }
        )
        ```
    """

    def run(self, **kwargs):
        if self.request.query_params:
            search = self.request.query_params.get('query', '')
        else:
            search = self.request.data.get('query', '')

        users = UserModel.objects.filter(
            Q(first_name__icontains=search) | Q(last_name__icontains=search) |
            Q(middle_name__icontains=search) | Q(unique_code__icontains=search) |
            Q(hashed_email=hash_field(UserManager.normalize_email(search))),
            user_type=UserTypes.EMPLOYEE, is_active=True
        )
        results = []
        for user in users:
            user_dict = {
                'data': user.id,
                'value': user.get_full_name() + ' - ' + user._email,
            }
            results.append(user_dict)
        return {'suggestions': results}


class ReferralSearchService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'referral_search',
            data = {
                'query': '<Search query>'
            }
        )
        ```
    """

    def run(self, **kwargs):
        if self.request.query_params:
            search = self.request.query_params.get('query', '')
        else:
            search = self.request.data.get('query', '')

        try:
            UserModel.objects.get(
                referral_code=search, is_referral_active=True
            )
            return True
        except UserModel.DoesNotExist:
            return False


class RegistrationService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'register',
            data = {
                'email': '',
                'password': '',
                'referral_code': '',
                'first_name': '',
                'middle_name': '',
                'last_name': '',
                'user_type': '',
                'unique_code': '',
                'mobile': '',
                'gender': '',
                'dob': '',
                'agency_name': '',
                'occupation_type': '',
                'aadhaar_number': '',
                'pan_number': '',
                'gstin_number': '',
                'bank_account_number': '',
                'ifsc_code': '',
                'drivers_license_number': '',
                'education_cert_number': '',
                'passport_number': '',
                'primary_address_line1': '',
                'primary_address_line2': '',
                'primary_address_line3': '',
                'primary_city': '',
                'primary_pincode': '',
                'secondary_address_line1': '',
                'secondary_address_line2': '',
                'secondary_address_line3': '',
                'secondary_city': '',
                'secondary_pincode': '',
                'source': '',
            },
            files = {
                'aadhaar_card_image': '',
                'pan_card_image': '',
                'gst_cert_image': '',
                'cancelled_cheque_image': '',
                'drivers_license_image': '',
                'education_cert_image': '',
                'passport_image': '',
                'photo': '',
            }
        )
        ```
    """
    is_global = True

    def validate(self):
        user_data = self.request.data

        # Do not allow Employee user to be created via this method.
        if user_data.get('user_type'):
            if int(user_data.get('user_type')) == UserTypes.EMPLOYEE:
                raise InvalidUserTypeException

        serializer_data = {
            'email': user_data.get('email', None),
            'password': user_data.get('password', None),
            'referral_code': user_data.get('referral_code', None),
            'first_name': user_data.get('first_name', None),
            'middle_name': user_data.get('middle_name', None),
            'last_name': user_data.get('last_name', None),
            'user_type': user_data.get('user_type', None),
            'unique_code': user_data.get('unique_code', None),
            'mobile': user_data.get('mobile', None),
            'gender': user_data.get('gender', None),
            'dob': user_data.get('dob', None),
            'agency_name': user_data.get('agency_name', None),
            'occupation_type': user_data.get('occupation_type', None),
            'marital_status': user_data.get('marital_status', None),
            'date_of_anniversary': user_data.get('date_of_anniversary', None),
            'insurance_business': user_data.get('insurance_business', None),
            'preferred_lang_written': user_data.get('preferred_lang_written', None),
            'preferred_lang_spoken': user_data.get('preferred_lang_spoken', None),
            'educational_qualification': user_data.get('educational_qualification', None),
            'docs': {
                'aadhaar_number': user_data.get('aadhaar_number', None),
                'aadhaar_card_image': user_data.get('aadhaar_card_image', None),
                'pan_number': user_data.get('pan_number', None),
                'pan_card_image': user_data.get('pan_card_image', None),
                'gstin_number': user_data.get('gstin_number', None),
                'gst_cert_image': user_data.get('gst_cert_image', None),
                'bank_account_number': user_data.get('bank_account_number', None),
                'ifsc_code': user_data.get('ifsc_code', None),
                'cancelled_cheque_image': user_data.get('cancelled_cheque_image', None),
                'drivers_license_number': user_data.get('drivers_license_number', None),
                'drivers_license_image': user_data.get('drivers_license_image', None),
                'education_cert_number': user_data.get('education_cert_number', None),
                'education_cert_image': user_data.get('education_cert_image', None),
                'passport_number': user_data.get('passport_number', None),
                'passport_image': user_data.get('passport_image', None),
                'photo': user_data.get('photo', None),
            },
            'primary_address': {
                'address_line1': user_data.get('primary_address_line1', None),
                'address_line2': user_data.get('primary_address_line2', None),
                'address_line3': user_data.get('primary_address_line3', None),
                'city_id': user_data.get('primary_city', None),
                'pincode': user_data.get('primary_pincode', None),
            },
            'secondary_address': {
                'address_line1': user_data.get('secondary_address_line1', None),
                'address_line2': user_data.get('secondary_address_line2', None),
                'address_line3': user_data.get('secondary_address_line3', None),
                'city_id': user_data.get('secondary_city', None),
                'pincode': user_data.get('secondary_pincode', None),
            },
            'gstin_address': {
                'address_line1': user_data.get('gstin_address_line1', None),
                'address_line2': user_data.get('gstin_address_line2', None),
                'address_line3': user_data.get('gstin_address_line3', None),
                'city_id': user_data.get('gstin_city', None),
                'pincode': user_data.get('gstin_pincode', None),
            },
            'source': user_data.get('source', None),
        }
        clean_serializer_data = clean_empty(serializer_data)

        glue_api_key_obj = APIKey.objects.get(
            api_key=self.request.META['HTTP_API_KEY'],
            api_secret_key=self.request.META['HTTP_SECRET_KEY']
        )

        source = glue_api_key_obj.app_id.strip().split(' ')[0]

        if source in settings.VALID_SOURCES:
            clean_serializer_data.update({
                'source': source
            })

        if not (clean_serializer_data.get('email') or
                clean_serializer_data.get('mobile')):
            raise ValidationError(
                'An email or mobile number is required for registration.'
            )
        serializer = RegistrationSerializer(data=clean_serializer_data)
        serializer.is_valid(raise_exception=True)

        if clean_serializer_data.get('source') == 'RT':
            logger.info(
                'Registering retailer in RenewBuy: %s',
                clean_serializer_data
            )
            helper_response = helper.rb_signup_helper(**clean_serializer_data)
            if helper_response is not None:
                logger.info(
                    'Error while registering retailer in RenewBuy: %s',
                    helper_response.json()
                )
                raise RbSignupException(
                    detail=helper_response.json(),
                    status_code=helper_response.status_code
                )
            # user already created from Renewbuy through Backfill
            # getting the instance of user.
            user_instance = UserModel.objects.get(
                hashed_email=hash_field(
                    UserManager.normalize_email(clean_serializer_data.get('email'))
                )
            )
            # Pop the email to prevent it from updation
            clean_serializer_data.pop('email')

            # Update the serializer with instance
            serializer = RegistrationSerializer(user_instance, data=clean_serializer_data)
            serializer.is_valid(raise_exception=True)

        logger.info('New registration request: %s', clean_serializer_data)

        self.validated_data = serializer
        return True

    def run(self, **kwargs):
        # If validation is passed, then, return the serialized response
        if self.validated_data:
            # Validated data will be a serializer instance in this case.
            self.validated_data.save()

            user = self.validated_data.instance

            # Its an implementation to bypass user that are
            # already created in Renewbuy and AMS through `rb_helper`.
            # It will update the source and create unique code
            # based on it and update its reporting.
            if self.request.data.get('source', '').startswith('RT'):
                source = self.request.data.get('source')
                ref_code = self.request.data.get('referral_code', 'DIRECT').upper()

                # It will make sure that a confirmation/verification
                # email must be sent
                user.is_email_verified = False

                # Create the unique code again, with right source
                user.unique_code = user.create_unique_code(prefix=source)

                try:
                    channel = Channel.objects.get(code=source)
                    user.channel = channel
                except Channel.DoesNotExist:
                    pass

                # Check whether Referral code is not `Direct` and it does not having
                # any reporting manager mapped.
                if ref_code not in ('DIRECT', '') and not user.get_current_reporting():
                    manager = None
                    try:
                        manager = UserModel.objects.get(unique_code=ref_code)
                    except UserModel.DoesNotExist:
                        try:
                            manager = UserModel.objects.get(referral_code=ref_code)
                        except UserModel.DoesNotExist:
                            logger.exception(
                                'Invalid referral code. : %s', ref_code
                            )
                    if manager:
                        # Create a reporting instance if a referral code was provided.
                        try:
                            UserReporting.objects.create(
                                start_date=timezone.now_local(only_date=True),
                                manager=manager,
                                reportee=user,
                            )
                        except Exception as e:
                            logger.exception('Unable to set user reporting for %s: %s',
                                             user, e)
                user.save()

            # Storing pan number and aadhaar number of digital partner.
            if user.unique_code.startswith('DP'):
                user.pan_number = self.request.data.get('pan_number')
                user.aadhaar_number = self.request.data.get('aadhaar_number')
                user.save(update_fields=['_pan_number', '_aadhaar_number'])

            if user.decrypted_email:
                # Send welcome and confirmation email.
                user.send_email_verification()
            if user.mobile:
                # TODO: Remove this block once the frontend SDK is in use.
                if self.request.data.get('source') in ('TA', 'RT'):
                    user.is_mobile_verified = True
                    user.save(update_fields=['is_mobile_verified'])
                elif self.request.data.get('source') == 'DP':
                    # Don't send OTP on mobile if the user is coming from
                    # DP channel.
                    pass
                else:
                    # Send welcome message.
                    # Send OTP for mobile number verification.
                    sms_device_instance = SMSDevice.objects.get_or_create(
                        user=user,
                        number=user.decrypted_mobile
                    )[0]
                    try:
                        sms_device_instance.generate_challenge()
                    except Exception:
                        logger.exception(
                            'Unable to send an OTP to: %s', user.unique_code
                        )

            # Return data of the serializer.
            return self.validated_data.data
        return None


def get_single_manager(supervisors):
    """
        Recursive function to get a single manager in above Hierarchy.
    """
    manager = []
    if len(supervisors) == 1:
        manager.extend(supervisors)
        return manager
    else:
        supervisor_list = set()
        for supervisor in supervisors:
            # get manager of current user.
            # assuming that single employee(asm & above)
            # reports to only single manager.
            supervisor_list.add(
                supervisor.get_current_reporting(
                    reporting_type=ReportingType.EMPLOYEE
                ).manager
            )
        supervisor_list = list(supervisor_list)
        return get_single_manager(supervisor_list)


def get_manager(relation_manager_list):
    """
        return single manager list.
    """
    manager = []
    # when only one RM found.
    if len(relation_manager_list) == 1:
        manager.extend(relation_manager_list)
    # when more than one RM found.
    else:
        reporting_list = []
        rm_reporting_types = [
            ReportingType.PARTNER_FIELD,
            ReportingType.PARTNER_HEALTH_FRM,
            ReportingType.PARTNER_LIFE_FRM
        ]
        for rm in relation_manager_list:
            reportings = rm.reportee.filter(
                reporting_type__in=rm_reporting_types,
                end_date=None
            )
            # as all reportings are same for single manager so we
            # just need to append for any first index reportings.
            reporting_list.append(reportings.first().reporting_type)
        for reporting_type in rm_reporting_types:
            if reporting_list.count(reporting_type) == 1:
                index = reporting_list.index(reporting_type)
                manager.append(relation_manager_list[index])
        if not manager:
            manager = get_single_manager(relation_manager_list)
    return manager


class PartnerMappingService(BaseService):
    """
        service for doing all active partner mapping on the basis
        of their pincode.
    """

    def validate(self):
        partners = self.request.data.get('active_partners')
        if not partners:
            raise ValidationError('Please provide activated partners.')

        if isinstance(self.request.data, QueryDict):
            # Setting the QueryDict as mutable otherwise
            # the operations being done below won't be possible.
            self.request.data._mutable = True

        if isinstance(partners, str):
            partners = [s.strip() for s in partners.split(',')]

        elif not isinstance(partners, Iterable):
            raise ValidationError(
                'Please provide the `active_partners` argument as a `list` like object '
                'or a comma-separated string of AMS IDs.'
            )

        if isinstance(partners[0], str):
            active_partner = []
            for partner in partners:
                try:
                    active_partner.append(
                        UserModel.objects.get(unique_code=partner)
                    )
                except UserModel.DoesNotExist:
                    logger.exception(
                        '[AMS][MAPPING] User does not exist.'
                    )
            self.request.data.update({
                'partner': active_partner
            })
        elif isinstance(partners[0], UserModel):
            self.request.data.update({
                'partner': partners
            })
        else:
            raise ValidationError('Partner not found.')

        return True

    def run(self):
        rm_reporting_types = [
            ReportingType.PARTNER_FIELD,
            ReportingType.PARTNER_HEALTH_FRM,
            ReportingType.PARTNER_LIFE_FRM
        ]
        managers = []

        for partner in self.request.data.get('partner'):
            try:
                pincode = partner.primary_address.pincode
            except Exception:
                pincode = 'NA'
                logger.exception(
                    '[AMS][]MAPPING] Pincode not available for this partner: %s',
                    partner.unique_code
                )
                continue
            relation_manager = UserPincodeMapping.objects.filter(
                pincode=pincode,
                user__user_type=UserTypes.EMPLOYEE,
                user__reportee__reporting_type__in=rm_reporting_types,
                user__is_active=True
            )

            # if RM exists.
            if relation_manager.exists():
                # this list holds only those rm which doesn't
                # have any employee reportee.
                only_rm = []
                relation_manager = list(set([rm.user for rm in relation_manager]))

                for rm in relation_manager:
                    # check rm doesn't hold any employee reportee.
                    if rm.reportee.filter(
                        reporting_type=ReportingType.EMPLOYEE, end_date=None
                    ).exists():
                        continue
                    else:
                        only_rm.append(rm)
                managers = get_manager(only_rm)

            else:
                # If Rm not exists. Now we are fetching the last employee of given pincode
                users = UserModel.objects.filter(
                    Q(reportee=None) |
                    Q(reportee__reporting_type=ReportingType.PARTNER_EMPLOYEE) &
                    ~Q(reportee__reporting_type=ReportingType.EMPLOYEE),
                    user_type=UserTypes.EMPLOYEE,
                    is_active=True
                )
                relation_manager = UserPincodeMapping.objects.filter(
                    user__in=users, pincode=pincode
                )
                if relation_manager.exists():
                    relation_manager = list(set([rm.user for rm in relation_manager]))
                    managers = get_single_manager(relation_manager)
                else:
                    logger.exception(f'Manager not availavle for this pincode({pincode})')

            # if manager found then done mapping.
            if managers:
                # end the previous acquistion reportings with the partner.
                try:
                    UserReporting.objects.filter(
                        reportee=partner,
                        reporting_type=ReportingType.PARTNER_ACQUISITION_FRM
                    ).update(end_date=timezone.get_prev_month_boundaries()[1].date())
                except Exception:
                    logger.exception(
                        'This reportee(%s) did not reports to any manager.',
                        partner.unique_code
                    )

                for manager in managers:
                    try:
                        UserReporting.objects.create(
                            start_date=timezone.get_current_month_start().date(),
                            manager=manager,
                            reportee=partner,
                            reporting_type=ReportingType.PARTNER_EMPLOYEE,
                        )

                        # if new reporting set with RM then send email to rm's manager.
                        if manager.reportee.filter(reporting_type__in=rm_reporting_types, end_date=None):
                            if not manager.reportee.filter(
                                reporting_type=ReportingType.EMPLOYEE,
                                end_date=None
                            ):
                                if manager.manager.filter(end_date=None).exists():
                                    to = manager.manager.filter(end_date=None).first()
                                else:
                                    to = manager  # if manager is at top
                        # send email to direct reporting manager.
                        else:
                            to = manager

                        context = {
                            'salutation_name': to.get_full_name(),
                            'click_url': f'{settings.DOSSIER_DASHBOARD_URL}manageteam/'
                        }
                        try:
                            email.send_from_template(
                                to=to.decrypted_email,
                                subject='New Mapping Request.',
                                template='mappings/view_new_mappings.html',
                                context=context,
                                from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                            )
                        except Exception:
                            logger.exception(
                                f'[AMS][]MAPPING]Unable to send an email: {to.decrypted_email}'
                            )

                    except Exception as e:
                        logger.exception(
                            'Unable to set user reporting for %s: %s',
                            partner, str(e)
                        )
            else:
                logger.info(f'No manager available for this pincode({pincode}),'
                            f'so reporting does not change for partner({partner}) from aquistion frm.')

        return True


class MappingApproverViewService(BaseService):
    """"
        Showing view for the mapping that suggested by system or
        by some other employee where supervisor will accepts that mapping
        for manager and reportee.

        Example usage:
        ```
        call(
            'accounts',
            'approve_mapping',
            user=<User Token>
        )
        ```
    """

    def validate(self):
        login_user = self.request.user
        if int(login_user.user_type) == UserTypes.EMPLOYEE:
            return True
        else:
            raise InvalidUserTypeException

    def run(self):
        approval_data = []
        login_user = self.request.user
        reportings = login_user.reportee.filter(
            reporting_type=ReportingType.EMPLOYEE,
            end_date=None
        )

        if reportings:
            for relation_manager in reportings:
                if relation_manager.reportee.reportee.filter(
                    reporting_type=ReportingType.PARTNER_EMPLOYEE,
                    suggested_by=None,
                    end_date=None
                ):
                    if not relation_manager.reportee.reportee.filter(
                        reporting_type=ReportingType.EMPLOYEE,
                        end_date=None
                    ):
                        approval_data.append({
                            'name': '{} ({})'.format(
                                relation_manager.reportee.get_full_name(),
                                relation_manager.reportee.unique_code
                            ),
                            'current_partners': relation_manager.reportee.reportee.filter(
                                ~Q(reporting_type=ReportingType.PARTNER_EMPLOYEE), end_date=None
                            ).count(),
                            'new_mappings': relation_manager.reportee.reportee.filter(
                                reporting_type=ReportingType.PARTNER_EMPLOYEE,
                                suggested_by=None,
                                end_date=None
                            ).count(),
                            'request_raised_by': 'System Raised',
                            'request_raised_code': None
                        })
            # if any suggested_reportings_by the reportee of login user.
            for reporting in reportings:
                suggested_reportings = UserReporting.objects.filter(
                    suggested_by=reporting.reportee,
                    end_date=None
                )
                # if any new suggeted reportings.
                if suggested_reportings:
                    # To get new reportings corresponding to unique manger.
                    suggested_reportings = suggested_reportings.values('manager').annotate(
                        newly_mapped=Count('manager__unique_code')
                    )
                    for reports in suggested_reportings:
                        manager_id = reports.get('manager')

                        try:
                            manager = UserModel.objects.filter(id=manager_id).first()
                        except UserModel.DoesNotExist:
                            logger.exception(
                                f'User({manager_id})does not exist'
                            )

                        approval_data.append({
                            'name': '{} ({})'.format(
                                manager.get_full_name(),
                                manager.unique_code,
                            ),
                            'current_partners': manager.reportee.filter(
                                suggested_by=None, end_date=None
                            ).count(),
                            'new_mappings': reports.get('newly_mapped'),
                            'request_raised_by': reporting.reportee.get_full_name(),
                            'request_raised_code': reporting.reportee.unique_code
                        })

        if approval_data:
            return approval_data
        else:
            return {'response': 'You don\'t have any mapping for approval.'}


class MappingDetailApprovalViewService(BaseService):
    """
        Get all partners whose mapping not accepted.

        Example usage:
        ```
        call(
            'accounts',
            'reportee_list',
            data={
                'unique_code': '<ID>',
                'request_raised': '<ID> or null',
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('unique_code'):
            raise UserRequiredException
        try:
            self.request.data['manager'] = UserModel.objects.get(
                unique_code=self.request.data.get('unique_code')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException
        return True

    def run(self):
        approval_needed = []
        suggested_by = self.request.data.get('request_raised')
        manager = self.request.data.get('manager')

        if suggested_by is None:
            unapproved_partner = manager.reportee.filter(
                reporting_type=ReportingType.PARTNER_EMPLOYEE,
                suggested_by=None, end_date=None
            )
            for partner in unapproved_partner:
                try:
                    pincode = partner.reportee.primary_address.pincode
                except Exception:
                    pincode = 'NA'
                approval_needed.append({
                    'name': partner.reportee.get_full_name(),
                    'unique_code': partner.reportee.unique_code,
                    'pincode': pincode
                })
        else:
            unapproved_partner = manager.reportee.filter(
                reporting_type=ReportingType.PARTNER_EMPLOYEE,
                suggested_by__unique_code=suggested_by,
                end_date=None
            )

            for partner in unapproved_partner:
                try:
                    pincode = partner.reportee.primary_address.pincode
                except Exception:
                    pincode = 'NA'
                approval_needed.append({
                    'name': partner.reportee.get_full_name(),
                    'unique_code': partner.reportee.unique_code,
                    'pincode': pincode
                })

        return approval_needed


class MappingAcceptOrRejectService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for accepting mapping of the partner.
        On accepting the mapping by supervisor, new "permanent-reporting"
        will create between manager & reportee and on rejection reporting
        will create with the supervisor that reject the suggested mapping.

        Example usage:
        ```
        call(
            'accounts',
            'mapping_allocation',
            data={
                'reportee': ['partner_code'],
                'manager': '<ID>',
                'is_accepted': true or false
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('reportee') or not \
                self.request.data.get('manager'):
            raise UserRequiredException
        try:
            self.request.data.update({
                'manager': UserModel.objects.get(
                    unique_code=self.request.data.get('manager')
                )
            })
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not isinstance(self.request.data.get('is_accepted'), bool):
            raise ValidationError(
                'Please send bool value for \'is_accepted\''
            )

        return True

    def run(self):
        partner_codes = self.request.data.get('reportee')

        if not isinstance(partner_codes, list):
            partner_codes = [partner_codes]

        login_user = self.request.user
        is_accepted = self.request.data.get('is_accepted')
        manager = self.request.data.get('manager')

        if is_accepted:
            rm_reporting_types = [
                        ReportingType.PARTNER_FIELD,
                        ReportingType.PARTNER_HEALTH_FRM,
                        ReportingType.PARTNER_LIFE_FRM
                    ]

            for partner_unique_code in partner_codes:
                try:
                    reporting = manager.reportee.get(
                        reportee__unique_code=partner_unique_code,
                        end_date=None
                    )
                except UserReporting.DoesNotExist:
                    logger.exception(
                        f'Reporting Does Not Exist for the reportee unique_code {partner_unique_code}'
                        )
                    continue

                if reporting:
                    context = {
                        'partner_name': reporting.reportee.get_full_name(),
                        'domain': settings.DOMAIN,
                        'reason': 'Organization level resource movement',
                        'data': [],
                        'tele': {}
                    }

                    suggest_by = reporting.suggested_by

                    manager_reporting_type = manager.reportee.filter(
                        reporting_type__in=rm_reporting_types,
                        end_date=None
                    )

                    # if new mapping accepted then set temporary reporting_type
                    # with manager.
                    if manager.reportee.filter(reporting_type=ReportingType.EMPLOYEE, end_date=None):
                        reporting.reporting_type = ReportingType.PARTNER_EMPLOYEE

                    # if new mapping accepted then set product specific
                    # with new manager(if it is RM).
                    elif manager_reporting_type:
                        reporting.reporting_type = manager_reporting_type.first().reporting_type

                    # set default PARTNER_FIELD reporting_type with RM.
                    else:
                        reporting.reporting_type = ReportingType.PARTNER_FIELD

                    reporting.start_date = timezone.get_current_month_start().date()
                    reporting.suggested_by = None

                    try:
                        reporting.save(update_fields=['reporting_type', 'start_date', 'suggested_by'])
                    except Exception as e:
                        logger.exception(f'Unable to save reporting{reporting} because {e}')

                    if reporting.reporting_type in rm_reporting_types:
                        if reporting.reporting_type == ReportingType.PARTNER_FIELD:
                            context['data'].append({
                                'name': manager.get_full_name(),
                                'email': manager.decrypted_email,
                                'reportee_type': 'frm',
                                'product_name': 'Motor'
                            })

                        elif reporting.reporting_type == ReportingType.PARTNER_LIFE_FRM:
                            context['data'].append({
                                'name': manager.get_full_name(),
                                'email': manager.decrypted_email,
                                'reportee_type': 'frm',
                                'product_name': 'Life'
                            })
                        else:
                            context['data'].append({
                                'name': manager.get_full_name(),
                                'email': manager.decrypted_email,
                                'reportee_type': 'frm',
                                'product_name': 'Health'
                            })
                        # send email
                        try:
                            # Send email to partner.
                            email.send_from_template(
                                to=reporting.reportee.decrypted_email,
                                subject='New Manager Assigned',
                                template='bulk_remap_notification.html',
                                context=context,
                                from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                            )
                        except Exception as email_error:
                            logger.exception(
                                '[NOTIFICATION] Error in sending EMAIL. %s' % str(email_error)
                            )
                    # send email()
                    if suggest_by:
                        # send email to reporting manager.
                        to = manager.decrypted_email
                        context = {
                            'salutation_name': manager.get_full_name(),
                            'assignee_name': suggest_by.get_full_name(),
                            'new_partner_count': len(partner_codes)
                        }

                        email.send_from_template(
                            to=to,
                            subject='New Mapping Updates',
                            template='mappings/mapping_updates.html',
                            context=context,
                            from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                        )
                        # send email to user who suggest the mapping.
                        context = {
                            'salutation_name': suggest_by.get_full_name(),
                            'click_url': f'{settings.DOSSIER_DASHBOARD_URL}manageteam'
                        }
                        email.send_from_template(
                            to=suggest_by.decrypted_email,
                            subject='Mapping Request Approved.',
                            template='mappings/request_approved.html',
                            context=context,
                            from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                        )
                    else:
                        context = {
                            'salutation_name': manager.get_full_name(),
                            'assignee_name': login_user.get_full_name(),
                            'new_partner_count': len(partner_codes)
                        }

                        email.send_from_template(
                            to=manager.decrypted_email,
                            subject='New Mapping Updates',
                            template='mappings/mapping_updates.html',
                            context=context,
                            from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                        )
        else:  # Mapping rejected
            for partner_unique_code in partner_codes:
                try:
                    reporting = manager.reportee.get(
                        reportee__unique_code=partner_unique_code
                    )
                except UserReporting.DoesNotExist:
                    logger.exception(
                        f'Reporting Does Not Exist for the reportee unique_code {partner_unique_code}'
                        )
                    continue

                # again create mapping with same ASM.
                suggested_by = reporting.suggested_by
                if suggested_by:
                    try:
                        reporting.manager = suggested_by
                        reporting.suggested_by = None
                        reporting.save()
                        # send email
                        to = suggested_by.decrypted_email
                        context = {
                            'salutation_name': suggested_by.get_full_name(),
                            'click_url': f'{settings.DOSSIER_DASHBOARD_URL}manageteam/'
                        }
                        email.send_from_template(
                            to=to,
                            subject='Mapping Request Rejected.',
                            template='mappings/request_rejected.html',
                            context=context,
                            from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                        )
                    except Exception as e:
                        logger.exception(
                            '[AMS][MAPPING] Unable to set user reporting for %s: %s',
                            suggested_by, e
                        )
                else:
                    try:
                        reporting.manager = login_user
                        reporting.start_date = timezone.get_current_month_start().date()
                        reporting.save()
                        # send email
                        to = login_user.decrypted_email
                        context = {
                            'salutation_name': login_user.get_full_name(),
                            'new_partner_count': len(partner_codes),
                            'click_url': f'{settings.DOSSIER_DASHBOARD_URL}manageteam/'
                        }
                        email.send_from_template(
                            to=login_user.decrypted_email,
                            subject='New Mapping Updates',
                            template='mappings/direct_reportings.html',
                            context=context,
                            from_email=settings.RENEWBUY_PARTNERS_FROM_EMAIL
                        )
                    except Exception as e:
                        logger.exception(
                            '[AMS][MAPPING] Unable to set user reporting for %s: %s',
                            login_user, str(e)
                        )

        return {'response': 'Your response submitted successfully'}


class GetUnmappedPartnerService(BaseService):
    """
        Manager request(suggest) the new mapping
        for his reportee(only_partner).

        Example Usage:
        ```
        call(
            'accounts,
            'new_mapping_request',
            user='<User Token>'
        )
        ```
    """

    def validate(self):
        login_user = self.request.user
        if int(login_user.user_type) == UserTypes.EMPLOYEE:
            return True
        else:
            raise InvalidUserTypeException

    def run(self):
        # partner mapping holds data for partner and manager mapping
        # i.e.,requested by supervisor.
        partner_mapping = {}
        login_user = self.request.user
        motor_manager = []
        health_manager = []
        life_manager = []
        motor_partner = []
        health_partner = []
        life_partner = []
        partner_data = []
        manager_data = []
        unmapped_partner = []

        # unmapped partner that directly reports to manager on temporary basis.
        unmapped_partner_reporting = login_user.reportee.filter(
            reporting_type=ReportingType.PARTNER_EMPLOYEE,
            suggested_by=None, end_date=None
        )
        for reporting in unmapped_partner_reporting:
            unmapped_partner.append(reporting.reportee)

        reportings = login_user.reportee.filter(
            reporting_type=ReportingType.EMPLOYEE,
            end_date=None,
            reportee__is_active=True
        )
        if reportings:
            # To identify login user is asm or csm.
            flag = False
            for reporting in reportings:
                if reporting.reportee.reportee.filter(reporting_type=ReportingType.EMPLOYEE, end_date=None):
                    flag = True
                    break
            # if flag is true, login user is csm or above.
            if flag:
                if unmapped_partner:
                    for reporting in reportings:
                        manager_data.append({
                            'name': reporting.reportee.get_full_name(),
                            'unique_code': reporting.reportee.unique_code,
                        })

                    for partner in unmapped_partner:
                        try:
                            pincode = partner.primary_address.pincode
                        except Exception:
                            pincode = 'NA'
                        partner_data.append({
                            'name': partner.get_full_name(),
                            'unique_code': partner.unique_code,
                            'pincode': pincode
                        })

                partner_mapping['partner'] = partner_data
                partner_mapping['manager'] = manager_data
                partner_mapping['designation'] = 'csm'
            # if ASM login.
            else:
                for reporting in reportings:
                    partner_reportings = reporting.reportee.reportee.filter(
                        reporting_type__in=[
                            ReportingType.PARTNER_FIELD,
                            ReportingType.PARTNER_HEALTH_FRM,
                            ReportingType.PARTNER_LIFE_FRM
                        ],
                        suggested_by=None, end_date=None
                    )
                    if partner_reportings:
                        for partner_reporting in partner_reportings:
                            if partner_reporting.reporting_type == ReportingType.PARTNER_FIELD:
                                motor_manager.append(partner_reporting.manager)
                                motor_partner.append(partner_reporting.reportee)

                            elif partner_reporting.reporting_type == ReportingType.PARTNER_HEALTH_FRM:
                                health_manager.append(partner_reporting.manager)
                                health_partner.append(partner_reporting.reportee)

                            elif partner_reporting.reporting_type == ReportingType.PARTNER_LIFE_FRM:
                                life_manager.append(partner_reporting.manager)
                                life_partner.append(partner_reporting.reportee)
                    else:
                        continue

                # partner dose not have field RM.
                not_motor = list(set(health_partner) - set(motor_partner))
                not_motor.extend(list(set(life_partner) - set(motor_partner)))
                not_motor.extend(unmapped_partner)
                # get those partner which have temporary reporting RM
                # but reporting doesn't accepted till now.
                unmapped_reportee = UserReporting.objects.filter(
                    reportee__in=not_motor,
                    manager__in=motor_manager, end_date=None
                ).values_list('reportee')
                unmapped_reportee = list(flatten(unmapped_reportee))
                unmapped_reportee = UserModel.objects.filter(id__in=unmapped_reportee)
                # removing those partner which have temporary reporting.
                not_motor = set(not_motor) - set(unmapped_reportee)

                # partner does not have health RM.
                not_health = list(set(motor_partner) - set(health_partner))
                not_health.extend(list(set(life_partner) - set(health_partner)))
                not_health.extend(unmapped_partner)
                # get those partner which have temporary reporting RM
                # but reporting doesn't accepted till now.
                unmapped_reportee = UserReporting.objects.filter(
                    reportee__in=not_health,
                    manager__in=health_manager, end_date=None
                ).values_list('reportee')
                unmapped_reportee = list(flatten(unmapped_reportee))
                unmapped_reportee = UserModel.objects.filter(id__in=unmapped_reportee)
                # removing those partner which have temporary reporting.
                not_health = set(not_health) - set(unmapped_reportee)

                # partner does not have life RM.
                not_life = list(set(health_partner) - set(life_partner))
                not_life.extend(list(set(motor_partner) - set(life_partner)))
                not_life.extend(unmapped_partner)
                # get those partner which have temporary reporting RM
                # but reporting doesn't accepted till now.
                unmapped_reportee = UserReporting.objects.filter(
                    reportee__in=not_life,
                    manager__in=life_manager, end_date=None
                ).values_list('reportee')
                unmapped_reportee = list(flatten(unmapped_reportee))
                unmapped_reportee = UserModel.objects.filter(id__in=unmapped_reportee)
                # removing those partner which have temporary reporting.
                not_life = set(not_life) - set(unmapped_reportee)

                # Append partner data
                not_mapped_motor_partner = []
                not_mapped_health_partner = []
                not_mapped_life_partner = []
                for partner in not_motor:
                    try:
                        pincode = partner.primary_address.pincode
                    except Exception:
                        pincode = 'NA'
                    not_mapped_motor_partner.append(
                        {
                            'name': partner.get_full_name(),
                            'unique_code': partner.unique_code,
                            'pincode': pincode
                        }
                    )
                for partner in not_health:
                    try:
                        pincode = partner.primary_address.pincode
                    except Exception:
                        pincode = 'NA'
                    not_mapped_health_partner.append(
                        {
                            'name': partner.get_full_name(),
                            'unique_code': partner.unique_code,
                            'pincode': pincode
                        }
                    )
                for partner in not_life:
                    try:
                        pincode = partner.primary_address.pincode
                    except Exception:
                        pincode = 'NA'
                    not_mapped_life_partner.append(
                        {
                            'name': partner.get_full_name(),
                            'unique_code': partner.unique_code,
                            'pincode': pincode
                        }
                    )
                # Append manager data
                motor_manager_data = []
                health_manager_data = []
                life_manager_data = []
                for manager in list(set(motor_manager)):
                    motor_manager_data.append({
                        'name': manager.get_full_name(),
                        'product': 'motor',
                        'unique_code': manager.unique_code,
                        'current_partner': manager.reportee.filter(
                            end_date=None
                        ).count(),
                    })
                for manager in list(set(health_manager)):
                    health_manager_data.append({
                        'name': manager.get_full_name(),
                        'product': 'Health',
                        'unique_code': manager.unique_code,
                        'current_partner': manager.reportee.filter(end_date=None).count(),
                    })
                for manager in list(set(life_manager)):
                    life_manager_data.append({
                        'name': manager.get_full_name(),
                        'product': 'Life',
                        'unique_code': manager.unique_code,
                        'current_partner': manager.reportee.filter(end_date=None).count(),
                    })
                partner_mapping['motor'] = {
                    'partners': not_mapped_motor_partner,
                    'managers': motor_manager_data
                }
                partner_mapping['health'] = {
                    'partners': not_mapped_health_partner,
                    'managers': health_manager_data
                }
                partner_mapping['life'] = {
                    'partners': not_mapped_life_partner,
                    'managers': life_manager_data
                }
                partner_mapping['designation'] = 'asm'

            return partner_mapping

        else:
            return {'response': 'You don\'t have any reportee for suggestion.'}


class SuggestMappingService(BaseService):
    """
        This service will only be accessible to the internal
        systems for requesting mapping of the partner.
        After suggesting new mapping for his reportee(only_partner),
        new "temporary-mapping" will create between suggested
        manager & reportee.

        Example usage:
        ```
        call(
            'accounts',
            'mapping_suggest',
            data={
                'manager_code': '<ID>',
                'partner_list': ['partner_code'],
                'is_submit': true or false
            }
        )
        '''
    """

    def validate(self):
        login_user = self.request.user
        if int(login_user.user_type == UserTypes.EMPLOYEE):
            mapping_data = self.request.data
            if not mapping_data.get('manager_code') or not \
                    mapping_data.get('partner_list'):
                raise UserRequiredException
            try:
                self.request.data.update({
                    'manager_code': UserModel.objects.get(
                        unique_code=mapping_data.get('manager_code')
                    )
                })
            except UserModel.DoesNotExist:
                raise UserRequiredException
            if not isinstance(self.request.data.get('is_submit'), bool):
                raise ValidationError('Please send bool value for "is_submit" key')
            return True

    def run(self):
        login_user = self.request.user
        is_submit = self.request.data.get('is_submit')
        manager = self.request.data.get('manager_code')
        partners = self.request.data.get('partner_list')

        if is_submit:
            for partner_code in partners:
                # if manager suggest a new mapping for his reportee(partner).
                if login_user.reportee.filter(reportee__unique_code=partner_code, end_date=None):
                    reporting = login_user.reportee.get(reportee__unique_code=partner_code)
                    try:
                        reporting.manager = manager
                        reporting.reporting_type = ReportingType.PARTNER_EMPLOYEE
                        reporting.suggested_by = login_user
                        reporting.save()
                    except Exception as e:
                        logger.exception(
                            '[AMS][MAPPING] Unable to set user reporting for %s: %s',
                            manager, e
                        )

                else:
                    # if manager suggest a new product RM for partner.
                    try:
                        try:
                            partner = UserModel.objects.get(unique_code=partner_code)
                        except UserModel.DoesNotExist:
                            logger.exception(
                                '[AMS][MAPPING]No user exist for this unique_code(%s)',
                                partner_code
                            )
                        # create new mapping with new product RM.
                        UserReporting.objects.create(
                            start_date=timezone.get_current_month_start().date(),
                            manager=manager,
                            reportee=partner,
                            reporting_type=ReportingType.PARTNER_EMPLOYEE,
                            suggested_by=login_user,
                        )
                    except Exception as e:
                        logger.exception(
                            '[AMS][MAPPING] Unable to set user reporting for %s: %s',
                            partner, e
                        )

        # send email to login user manager for approval using celery.
        return {'response': 'Your request submitted successfully'}


class ProfileUpdationService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'update_profile',
            data = {
                'email': '',
                'password': '',
                'first_name': '',
                'middle_name': '',
                'last_name': '',
                'user_type': '',
                'unique_code': '',
                'mobile': '',
                'gender': '',
                'dob': '',
                'agency_name': '',
                'occupation_type': '',
                'aadhaar_number': '',
                'pan_number': '',
                'gstin_number': '',
                'bank_account_number': '',
                'ifsc_code': '',
                'drivers_license_number': '',
                'education_cert_number': '',
                'passport_number': '',
                'primary_address_line1': '',
                'primary_address_line2': '',
                'primary_address_line3': '',
                'primary_city': '',
                'primary_pincode': '',
                'secondary_address_line1': '',
                'secondary_address_line2': '',
                'secondary_address_line3': '',
                'secondary_city': '',
                'secondary_pincode': '',
                'source': '',
            },
            files = {
                'aadhaar_card_image': '',
                'pan_card_image': '',
                'gst_cert_image': '',
                'cancelled_cheque_image': '',
                'drivers_license_image': '',
                'education_cert_image': '',
                'passport_image': '',
                'photo': '',
            }
        )
        ```
    """
    is_global = True

    def validate(self):
        self.request.user = authenticate(self.request)
        user_data = self.request.data

        serializer_data = {
            # 'email': user_data.get('email', None),
            # 'password': user_data.get('password', None),
            # 'referral_code': user_data.get('referral_code', None),
            'first_name': user_data.get('first_name', None),
            'middle_name': user_data.get('middle_name', None),
            'last_name': user_data.get('last_name', None),
            'user_type': user_data.get('user_type', None),
            # 'unique_code': user_data.get('unique_code', None),
            # 'mobile': user_data.get('mobile', None),
            'gender': user_data.get('gender', None),
            'dob': user_data.get('dob', None),
            'agency_name': user_data.get('agency_name', None),
            'occupation_type': user_data.get('occupation_type', None),
            'marital_status': user_data.get('marital_status', None),
            'date_of_anniversary': user_data.get('date_of_anniversary', None),
            'insurance_business': user_data.get('insurance_business', None),
            'preferred_lang_written': user_data.get('preferred_lang_written', None),
            'preferred_lang_spoken': user_data.get('preferred_lang_spoken', None),
            'educational_qualification': user_data.get('educational_qualification', None),
            'docs': {
                'aadhaar_number': user_data.get('aadhaar_number', None),
                'aadhaar_card_image': user_data.get('aadhaar_card_image', None),
                'pan_number': user_data.get('pan_number', None),
                'pan_card_image': user_data.get('pan_card_image', None),
                'gstin_number': user_data.get('gstin_number', None),
                'gst_cert_image': user_data.get('gst_cert_image', None),
                'bank_account_number': user_data.get('bank_account_number', None),
                'ifsc_code': user_data.get('ifsc_code', None),
                'cancelled_cheque_image': user_data.get('cancelled_cheque_image', None),
                'drivers_license_number': user_data.get('drivers_license_number', None),
                'drivers_license_image': user_data.get('drivers_license_image', None),
                'education_cert_number': user_data.get('education_cert_number', None),
                'education_cert_image': user_data.get('education_cert_image', None),
                'passport_number': user_data.get('passport_number', None),
                'passport_image': user_data.get('passport_image', None),
                'photo': user_data.get('photo', None),
            },
            'primary_address': {
                'address_line1': user_data.get('primary_address_line1', None),
                'address_line2': user_data.get('primary_address_line2', None),
                'address_line3': user_data.get('primary_address_line3', None),
                'city_id': user_data.get('primary_city', None),
                'pincode': user_data.get('primary_pincode', None),
            },
            'secondary_address': {
                'address_line1': user_data.get('secondary_address_line1', None),
                'address_line2': user_data.get('secondary_address_line2', None),
                'address_line3': user_data.get('secondary_address_line3', None),
                'city_id': user_data.get('secondary_city', None),
                'pincode': user_data.get('secondary_pincode', None),
            },
            'gstin_address': {
                'address_line1': user_data.get('gstin_address_line1', None),
                'address_line2': user_data.get('gstin_address_line2', None),
                'address_line3': user_data.get('gstin_address_line3', None),
                'city_id': user_data.get('gstin_city', None),
                'pincode': user_data.get('gstin_pincode', None),
            },
        }

        clean_serializer_data = clean_empty(serializer_data)
        serializer = UserSerializer(
            instance=self.request.user,
            data=clean_serializer_data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        self.validated_data = serializer
        return True

    def run(self):
        # If validation is passed, then,
        # return the serialized response
        if self.validated_data:
            # Validated data will be a serializer instance in this case.
            self.validated_data.save()

            # Return data of the serializer.
            return self.validated_data.data
        return None


class ProfileFetchingService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'fetch_profile',
            user=<User Token>
        )
        ```
    """
    is_global = True

    def validate(self):
        self.request.user = authenticate(self.request)
        return True

    def run(self):
        serializer = UserSerializer(self.request.user)
        return serializer.data


class FeatureListService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'feature_list',
            user=<User Token>
        )
        ```
    """
    is_global = True

    def validate(self):
        self.request.user = authenticate(self.request)
        return True

    def run(self):
        return_dict = {
            'features': []
        }
        if self.request.user.channel and self.request.user.channel.code == 'RT':
            return_dict.update({
                'features': ['tp_only']
            })
        return return_dict


class DataBackfillService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'data_backfill',
            data = {
                'email': '',
                'password': '',
                'referral_code': '',
                'first_name': '',
                'middle_name': '',
                'last_name': '',
                'user_type': '',
                'unique_code': '',
                'mobile': '',
                'gender': '',
                'dob': '',
                'dialer_agent_id': '',
                'agency_name': '',
                'occupation_type': '',
                'aadhaar_number': '',
                'pan_number': '',
                'gstin_number': '',
                'bank_account_number': '',
                'ifsc_code': '',
                'drivers_license_number': '',
                'education_cert_number': '',
                'passport_number': '',
                'primary_address_line1': '',
                'primary_address_line2': '',
                'primary_address_line3': '',
                'primary_city': '',
                'primary_pincode': '',
                'secondary_address_line1': '',
                'secondary_address_line2': '',
                'secondary_address_line3': '',
                'secondary_city': '',
                'secondary_pincode': '',
                'source': '',
            },
            files = {
                'aadhaar_card_image': '',
                'pan_card_image': '',
                'gst_cert_image': '',
                'cancelled_cheque_image': '',
                'drivers_license_image': '',
                'education_cert_image': '',
                'passport_image': '',
                'photo': '',
            }
        )
        ```
    """

    def validate(self):
        user_data = self.request.data
        if hasattr(self.request, 'FILES'):
            user_data.update(self.request.FILES)

        serializer_data = {
            'email': user_data.get('email', None),
            'password': user_data.get('password', None),
            'referral_code': user_data.get('referral_code', None),
            'self_referral_code': user_data.get('self_referral_code', None),
            'first_name': user_data.get('first_name', None),
            'middle_name': user_data.get('middle_name', None),
            'last_name': user_data.get('last_name', None),
            'user_type': user_data.get('user_type', None),
            'unique_code': user_data.get('unique_code', None),
            'mobile': user_data.get('mobile', None),
            'gender': user_data.get('gender', None),
            'dob': user_data.get('dob', None),
            'dialer_agent_id': user_data.get('dialer_agent_id', None),
            'agency_name': user_data.get('agency_name', None),
            'is_referral_active': user_data.get('is_referral_active', None),
            'is_email_verified': user_data.get('is_email_verified', None),
            'is_mobile_verified': user_data.get('is_mobile_verified', None),
            'is_photo_verified': user_data.get('is_photo_verified', None),
            'is_aadhaar_verified': user_data.get('is_aadhaar_verified', None),
            'is_pan_verified': user_data.get('is_pan_verified', None),
            'is_bank_account_verified': user_data.get('is_bank_account_verified', None),
            'is_dl_verified': user_data.get('is_dl_verified', None),
            'is_education_cert_verified': user_data.get('is_education_cert_verified', None),
            'is_gst_verified': user_data.get('is_gst_verified', None),
            'is_passport_verified': user_data.get('is_passport_verified', None),
            'pos_code': user_data.get('pos_code', None),
            'gi_pos_certified_date': user_data.get('gi_pos_certified_date', None),
            'li_pos_certified_date': user_data.get('li_pos_certified_date', None),
            'li_pos_status': user_data.get('li_pos_status', None),
            'gi_pos_status': user_data.get('gi_pos_status', None),
            'occupation_type': user_data.get('occupation_type', None),
            'marital_status': user_data.get('marital_status', None),
            'date_of_anniversary': user_data.get('date_of_anniversary', None),
            'insurance_business': user_data.get('insurance_business', None),
            'preferred_lang_written': user_data.get('preferred_lang_written', None),
            'preferred_lang_spoken': user_data.get('preferred_lang_spoken', None),
            'educational_qualification': user_data.get('educational_qualification', None),
            'kyc_submitted_at': user_data.get('kyc_submitted_at', None),
            'kyc_closed_at': user_data.get('kyc_closed_at', None),
            'kyc_closed_by': user_data.get('kyc_closed_by', None),
            'pos_document_status': user_data.get('pos_document_status', None),
            'bank_document_status': user_data.get('bank_document_status', None),
            'bank_name': user_data.get('bank_name', None),
            'bank_branch': user_data.get('bank_branch', None),
            'docs': {
                'aadhaar_number': user_data.get('aadhaar_number', None),
                'aadhaar_card_image': user_data.get('aadhaar_card_image', None),
                'pan_number': user_data.get('pan_number', None),
                'pan_card_image': user_data.get('pan_card_image', None),
                'gstin_number': user_data.get('gstin_number', None),
                'gst_cert_image': user_data.get('gst_cert_image', None),
                'bank_account_number': user_data.get('bank_account_number', None),
                'ifsc_code': user_data.get('ifsc_code', None),
                'cancelled_cheque_image': user_data.get('cancelled_cheque_image', None),
                'drivers_license_number': user_data.get('drivers_license_number', None),
                'drivers_license_image': user_data.get('drivers_license_image', None),
                'education_cert_number': user_data.get('education_cert_number', None),
                'education_cert_image': user_data.get('education_cert_image', None),
                'passport_number': user_data.get('passport_number', None),
                'passport_image': user_data.get('passport_image', None),
                'photo': user_data.get('photo', None),
            },
            'source': user_data.get('source', None),
        }

        if serializer_data.get('is_gst_verified') in [True, 'True']:
            serializer_data['is_gst_verified'] = True
        elif serializer_data.get('is_gst_verified') in [False, 'False']:
            serializer_data['is_gst_verified'] = False
        else:
            serializer_data.pop('is_gst_verified')

        if serializer_data.get('gi_pos_status') not in ['0', False, None, '']:
            serializer_data['gi_pos_status'] = True
        else:
            serializer_data['gi_pos_status'] = False

        if user_data.get('primary_city'):
            primary_cities = City.objects.filter(
                city_name__iexact=user_data.get('primary_city')
            )
            if primary_cities.exists():
                primary_city = primary_cities.first()

                serializer_data.update({
                    'primary_address': {
                        'address_line1': user_data.get('primary_address_line1', None),
                        'address_line2': user_data.get('primary_address_line2', None),
                        'address_line3': user_data.get('primary_address_line3', None),
                        'city_id': primary_city.id,
                        'pincode': user_data.get('primary_pincode', None),
                    }
                })

        if user_data.get('secondary_city'):
            secondary_cities = City.objects.filter(
                city_name__iexact=user_data.get('secondary_city')
            )
            if secondary_cities.exists():
                secondary_city = secondary_cities.first()

                serializer_data.update({
                    'secondary_address': {
                        'address_line1': user_data.get('secondary_address_line1', None),
                        'address_line2': user_data.get('secondary_address_line2', None),
                        'address_line3': user_data.get('secondary_address_line3', None),
                        'city_id': secondary_city.id,
                        'pincode': user_data.get('secondary_pincode', None),
                    }
                })

        if user_data.get('gstin_city'):
            gstin_cities = City.objects.filter(
                city_name__iexact=user_data.get('gstin_city')
            )
            if gstin_cities.exists():
                gstin_city = gstin_cities.first()

                serializer_data.update({
                    'gstin_address': {
                        'address_line1': user_data.get('gstin_address_line1', None),
                        'address_line2': user_data.get('gstin_address_line2', None),
                        'address_line3': user_data.get('gstin_address_line3', None),
                        'city_id': gstin_city.id,
                        'pincode': user_data.get('gstin_pincode', None),
                    }
                })

        # If the email/unique_code already exists in the system. Update the
        # user's details.
        try:
            try:
                user = UserModel.objects.get(
                    unique_code=user_data.get('unique_code', None)
                )
                serializer_data.pop('unique_code')
            except UserModel.DoesNotExist:
                user = UserModel.objects.get(
                    hashed_email=hash_field(
                        UserManager.normalize_email(
                            user_data.get('email')
                        )
                    )
                )
        except UserModel.DoesNotExist:
            user = None
        # Process the data received, ignore all the garbage data that doesn't
        # adhere to the basic data format validations.
        doc_serializer = DocsSerializer(
            data=serializer_data.get('docs', {}),
            context={"user": user}
        )
        if not doc_serializer.is_valid():
            for key in doc_serializer.errors.keys():
                serializer_data.get('docs', {}).pop(key, None)

        # `is_****_verified` cannot be true if the corresponding document
        # number doens't exist.
        if not serializer_data.get('docs', {}).get('aadhaar_number', None):
            serializer_data.pop('is_aadhaar_verified', None)
        if not serializer_data.get('docs', {}).get('pan_number', None):
            serializer_data.pop('is_pan_verified', None)
        if not serializer_data.get('docs', {}).get('drivers_license_number', None):
            serializer_data.pop('is_dl_verified', None)
        if not serializer_data.get('docs', {}).get('passport_number', None):
            serializer_data.pop('is_passport_verified', None)
        if not serializer_data.get('docs', {}).get('gstin_number', None):
            serializer_data.pop('is_gst_verified', None)
        if not serializer_data.get('docs', {}).get('bank_account_number', None):
            serializer_data.pop('is_bank_account_verified', None)
        if not serializer_data.get('docs', {}).get('ifsc_code', None):
            serializer_data.pop('is_bank_account_verified', None)

        if user:  # Update
            serializer = BackfillSerializer(
                instance=user,
                data=serializer_data,
                partial=True
            )
        else:  # Create
            serializer = BackfillSerializer(
                data=serializer_data
            )
        serializer.is_valid(raise_exception=True)
        self.validated_data = serializer
        return True

    def run(self, **kwargs):

        # If validation is passed, then return the serialized response
        if self.validated_data:
            # Validated data will be a serializer instance in this case.

            user = self.validated_data.save()

            if self.request.data.get('is_active') in ('True', 'true', '1'):
                user.is_active = True
            else:
                user.is_active = False
            if self.request.data.get('is_staff') in ('True', 'true', '1'):
                user.is_staff = True
            else:
                user.is_staff = False
            if self.request.data.get('date_joined') and not user.unique_code.startswith('TA'):
                user.created_at = datetime.strptime(
                    self.request.data.get('date_joined'), '%Y-%m-%dT%H:%M:%SZ'
                )

            user.save()

            # Return data of the serializer.
            return self.validated_data.data
        return None


class InternalCustomerRegistrationService(BaseService):
    """
        Service to let the internal transactional systems create user accounts
        for customers consuming their services if one doesn't already exist in
        the system.
    """

    def validate(self):
        user_data = self.request.data

        serializer_data = {
            'email': user_data.get('email', None),
            'first_name': user_data.get('first_name', None),
            'middle_name': user_data.get('middle_name', None),
            'last_name': user_data.get('last_name', None),
            'user_type': UserTypes.CUSTOMER,  # Only allow users of type `Customer` to be created.
            'unique_code': user_data.get('unique_code', None),
            'mobile': user_data.get('mobile', None),
            'gender': user_data.get('gender', None),
            'dob': user_data.get('dob', None),
            'agency_name': user_data.get('agency_name', None),
            'occupation_type': user_data.get('occupation_type', None),
            'docs': {
                'aadhaar_number': user_data.get('aadhaar_number', None),
                'aadhaar_card_image': user_data.get('aadhaar_card_image', None),
                'pan_number': user_data.get('pan_number', None),
                'pan_card_image': user_data.get('pan_card_image', None),
                'gstin_number': user_data.get('gstin_number', None),
                'gst_cert_image': user_data.get('gst_cert_image', None),
                'bank_account_number': user_data.get('bank_account_number', None),
                'cancelled_cheque_image': user_data.get('cancelled_cheque_image', None),
                'drivers_license_number': user_data.get('drivers_license_number', None),
                'drivers_license_image': user_data.get('drivers_license_image', None),
                'education_cert_number': user_data.get('education_cert_number', None),
                'education_cert_image': user_data.get('education_cert_image', None),
                'passport_number': user_data.get('passport_number', None),
                'passport_image': user_data.get('passport_image', None),
                'photo': user_data.get('photo', None),
            },
            'primary_address': {
                'address_line1': user_data.get('primary_address_line1', None),
                'address_line2': user_data.get('primary_address_line2', None),
                'address_line3': user_data.get('primary_address_line3', None),
                'city_id': user_data.get('primary_city', None),
                'pincode': user_data.get('primary_pincode', None),
            },
            'secondary_address': {
                'address_line1': user_data.get('secondary_address_line1', None),
                'address_line2': user_data.get('secondary_address_line2', None),
                'address_line3': user_data.get('secondary_address_line3', None),
                'city_id': user_data.get('secondary_city', None),
                'pincode': user_data.get('secondary_pincode', None),
            },
            'gstin_address': {
                'address_line1': user_data.get('gstin_address_line1', None),
                'address_line2': user_data.get('gstin_address_line2', None),
                'address_line3': user_data.get('gstin_address_line3', None),
                'city_id': user_data.get('gstin_city', None),
                'pincode': user_data.get('gstin_pincode', None),
            },
            'source': user_data.get('source', None),
        }
        clean_serializer_data = clean_empty(serializer_data)
        serializer = UserSerializer(data=clean_serializer_data)
        serializer.is_valid(raise_exception=True)
        self.validated_data = serializer
        return True

    def run(self, **kwargs):
        # If validation is passed, return the serialized response
        if self.validated_data:
            # Validated data will be a serializer instance in this case.
            self.validated_data.save()

            user = self.validated_data.instance

            if user.decrypted_email:
                # Send welcome and confirmation email.
                user.send_email_verification()

            # Return data of the serializer.
            return self.validated_data.data
        return None


class InternalProfileUpdationService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for updating the profile of the user.
        Takes the AMS ID of the user as an argument to verify
        which user to perform operations on.

        Example usage:
        ```
        call(
            'accounts',
            'update_profile_with_id',
            data={
                'user_id': '<AMS ID/Email ID>',
                'gi_pos_certified_date': '<Date (dd/mm/yyyy)>',
                'li_pos_certified_date': '<Date (dd/mm/yyyy)>',
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        user_data = self.request.data

        serializer_data = {
            'gi_pos_certified_date': user_data.get(
                'gi_pos_certified_date',
                try_or(lambda: self.request.user.gi_pos_certified_date, None)
            ),
            'li_pos_certified_date': user_data.get(
                'li_pos_certified_date',
                try_or(lambda: self.request.user.li_pos_certified_date, None)
            ),
        }

        clean_serializer_data = clean_empty(serializer_data)
        serializer = UserSerializer(
            instance=self.request.user,
            data=clean_serializer_data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        self.validated_data = serializer
        return True

    def run(self):
        # If validation is passed, then,
        # return the serialized response
        if self.validated_data:
            # Validated data will be a serializer instance in this case.
            self.validated_data.save()

            # Return data of the serializer.
            return self.validated_data.data
        return None


class RecentReportingChangeFetchService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'recent_reporting_updates',
            data={
                'days': '1',
                'hours': '12'
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data:
            self.request.data = {
                'hours': 1
            }

        try:
            request_data = dict(self.request.data)
            for k, v in self.request.data.items():
                request_data.update({
                    k: int(v)
                })
            self.request.data = request_data
        except Exception:
            raise ValidationError(
                'Please provide correct parameters to be passed in a `timedelta` method.'
            )

        return True

    def run(self):
        try:
            user_reportings = UserReporting.objects.filter(
                modified_at__gte=timezone.now_local() - timedelta(**self.request.data)
            )
        except TypeError:
            raise ValidationError(
                'Please provide correct parameters to be passed in a `timedelta` method.'
            )

        if user_reportings:
            return user_reportings.values('start_date', 'end_date',
                                          'manager__unique_code', 'reportee__unique_code',
                                          'reporting_type')
        return []


class SetUserReporting(BaseService):
    """
        Example Usage:
        ```
        call(
            'accounts',
            'set_user_reporting',
            data={
                'manager': '<AMS_ID>',
                'reportee': '<AMS_ID>',
                'start_date': '<Date (dd/mm/yyyy)>',
                'reporting_type': '<ReportingType>'  # Defaults to None.
            }
        )
        ```
    """

    def validate(self):
        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            date_for_check = timezone.now_local(only_date=True)

            if self.request.data.get('start_date'):
                date_for_check = datetime.strptime(
                    self.request.data.get('start_date'), '%d/%m/%Y'
                ).date()

            if self.request.data.get('reporting_type'):
                self.request.data.update({
                    'reporting_type': int(self.request.data.get('reporting_type'))
                })

            if not self.request.data.get('reporting_type') in PartnerToEmployeeReportingType.__dict__.values() \
                    and not self.request.data.get('reporting_type') in ReportingType.__dict__.values():
                raise ValidationError(
                    'Please provide a valid reporting type value.'
                )

            if self.request.data.get('manager') == self.request.data.get('reportee'):
                raise ValidationError('Reportee cannot report to themselves.')

            self.request.data.update({
                'start_date': date_for_check,
                'manager': UserModel.objects.get(
                    Q(unique_code=self.request.data.get('manager')) |
                    Q(referral_code=self.request.data.get('manager'),
                      is_referral_active=True)
                ),
                'reportee': UserModel.objects.get(
                    unique_code=self.request.data.get('reportee')
                ),
            })
        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )
        except UserModel.DoesNotExist:
            raise ValidationError(
                'Please provide a valid AMS ID.'
            )

        return True

    def run(self):
        try:
            manager = self.request.data.get('manager')
            reportee = self.request.data.get('reportee')
            start_date = self.request.data.get('start_date')
            reporting_type = self.request.data.get('reporting_type')
            if (
                (
                    manager.user_type == UserTypes.EMPLOYEE or
                    reportee.user_type == UserTypes.EMPLOYEE
                ) and
                reporting_type is None
            ):
                raise InvalidUserTypeException()

            UserReporting.objects.get_or_create(
                manager=manager,
                reportee=reportee,
                start_date=start_date,
                reporting_type=reporting_type,
            )

            if not reporting_type:
                managers = get_managers_list_on_date(reportee, start_date, reporting_type=None)
                managers = UserModel.objects.filter(id__in=[m.id for m in flatten(managers)])

                part_of_bp_corporate = False

                # If any of the managers in the upward hierarchy is a pyramid manager,
                # update the unique_code of this user and set the pyramid manager's referral
                # code as the prefix.
                if managers.filter(user_type=UserTypes.PYRAMID_MANAGER).exists():
                    part_of_bp_corporate = True
                    manager = managers.get(user_type=UserTypes.PYRAMID_MANAGER)

                prefix = manager.referral_code

                # Only update the `unique_code` of this user if the prefix isn't the
                # referral code of the manager and the user was created within 5 minutes of
                # this request, update the `referral_code` if the manager is a pyramid manager.
                if (not reportee.unique_code.startswith(prefix) and
                        reportee.created_at > timezone.now_local() - timedelta(minutes=5)) or \
                        part_of_bp_corporate:
                    # Calculate the suffix, which would be the count of the people directly or
                    # indirectly reporting to this manager.
                    suffix = 0
                    latest_user = None
                    try:
                        latest_user = UserModel.objects.filter(
                            unique_code__startswith=prefix
                        ).latest('unique_code')
                    except UserModel.DoesNotExist:
                        pass
                    else:
                        suffix = int(latest_user.unique_code.replace(prefix, ''))
                    finally:
                        suffix = suffix + 1

                    if (not reportee == latest_user and
                            reportee.created_at > timezone.now_local() - timedelta(minutes=5)):
                        reportee.unique_code = f'{prefix}{suffix}'

                    # Set the referral code of this reportee only if it belongs to
                    # the BP corporate tree.
                    if part_of_bp_corporate and settings.GENERATE_REFERAL:
                        reportee.referral_code = f'{prefix}{suffix}'
                        reportee.is_referral_active = True

                    reportee.save(update_fields=['unique_code',
                                                 'referral_code',
                                                 'is_referral_active'])
            return reportee.unique_code

        except DjangoValidationError as e:
            raise ValidationError(str(e))


class InternalProfileFetchingService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for fetching the profile of the user.
        Takes the AMS ID/Email/Pos Code of the user as an argument to verify
        which user to fetch profile for.

        Example usage:
        ```
        call(
            'accounts',
            'fetch_profile_with_id',
            data={
                'user_id': '<AMS ID/Email ID/Pos Code>',
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                Q(unique_code=self.request.data.get('user_id')) |
                Q(hashed_email=hash_field(
                    UserManager.normalize_email(
                        self.request.data.get('user_id')
                    )
                )) |
                Q(pos_code=self.request.data.get('user_id'))
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException
        return True

    def run(self):
        serializer = UserSerializer(self.request.user)
        return serializer.data


def get_managers_list_on_date(users, date, **extra_kwargs):
    """
        Returns a list of the managing hierarchy above the user
    """
    hierarchy_list = []

    if not isinstance(users, Iterable):
        users = [users]

    for user in users:
        managers_on_date = user.manager.filter(
            Q(end_date__gte=date) | Q(end_date=None),
            start_date__lte=date,
            **extra_kwargs
        )
        if managers_on_date.exists():
            managers_on_date = UserModel.objects.filter(
                id__in=managers_on_date.values_list('manager', flat=True)
            )
            hierarchy_list.extend(get_managers_list_on_date(managers_on_date, date, **extra_kwargs))
            hierarchy_list.append(managers_on_date)

    return hierarchy_list


class ReportingHierarchyListService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        Example usage:
        ```
        call(
            'accounts',
            'reporting_hierarchy',
            data={
                'user_id': '<ID>',
                'as_on': '<Date (dd/mm/yyyy)>'
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.data or not self.request.data.get('as_on'):
            raise ValidationError(
                'Please supply the `as_on` argument to fetch reporting for.'
            )
        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'as_on': datetime.strptime(
                    self.request.data.get('as_on'), '%d/%m/%Y'
                ).date(),
            })
        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )

        return True

    def run(self):
        managing_hierarachy_list = []
        manager_list = [self.request.user]

        manager_list.extend(get_managers_list_on_date(
            self.request.user,
            self.request.data.get('as_on')
        ))

        manager_list = flatten(manager_list)

        for manager in set(list(manager_list)):
            managing_hierarachy_list.append({
                'code': manager.unique_code,
                'name': manager.get_full_name(),
            })

        return managing_hierarachy_list


def get_reportings_list_on_date(users, date, **extra_kwargs):
    """
        Returns a list of the reporting hierarchy above
        the given user_id.
    """
    hierarchy_list = []

    if not isinstance(users, Iterable):
        users = [users]

    for user in users:
        managers_on_date = user.manager.filter(
            Q(end_date__gte=date) | Q(end_date=None),
            start_date__lte=date,
            **extra_kwargs
        )
        if managers_on_date.exists():
            hierarchy_list.append(managers_on_date)
            managers = UserModel.objects.filter(
                id__in=managers_on_date.values_list('manager', flat=True)
            )

            hierarchy_list.extend(get_reportings_list_on_date(managers, date, **extra_kwargs))

    return hierarchy_list


class ReportingHierarchyDetailListService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for getting immediate managers(with reporting type)
        of a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        Example usage:
        ```
        call(
            'accounts',
            'reporting_hierarchy_with_detail',
            data={
                'user_id': '<ID>',
                'as_on': '<Date (dd/mm/yyyy)>'
                }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.data or not self.request.data.get('as_on'):
            raise ValidationError(
                'Please supply the `as_on` argument to fetch reporting for.'
            )
        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'as_on': datetime.strptime(
                    self.request.data.get('as_on'), '%d/%m/%Y'
                ).date(),
            })
        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )

        return True

    def run(self):
        # returns the complete reporting hierarchy
        # above the given user_id.

        manager_hierarachy_list = []
        reporting_list = []
        reporting_hierarchy_data = {}

        reporting_list.extend(get_reportings_list_on_date(
            self.request.user,
            self.request.data.get('as_on')
        ))
        user_data = {
            'code': self.request.user.unique_code,
            'name': self.request.user.get_full_name(),
        }

        # flatten() convert a multi-dimension list
        # into one dimension list.
        reporting_list = list(flatten(reporting_list))

        for reporting in set(reporting_list):
            manager_hierarachy_list.append({
                'code': reporting.manager.unique_code,
                'name': reporting.manager.get_full_name(),
                'reporting_type': reporting.reporting_type,
                'start_date': reporting.start_date,
                'end_date': reporting.end_date,
                'reportee_code': reporting.reportee.unique_code,
                'reportee_name': reporting.reportee.get_full_name(),
            })

        reporting_hierarchy_data['user'] = user_data
        reporting_hierarchy_data['reporting_hierarchy'] = manager_hierarachy_list

        return reporting_hierarchy_data


class ReportingManagerService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        Example usage:
        ```
        call(
            'accounts',
            'reporting_manager',
            data={
                'user_id': '<ID>',
                'as_on': '<Date (dd/mm/yyyy)>'
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.data or not self.request.data.get('as_on'):
            raise ValidationError(
                'Please supply the `as_on` argument to fetch reporting for.'
            )
        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'as_on': datetime.strptime(
                    self.request.data.get('as_on'), '%d/%m/%Y'
                ).date(),
            })
        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )

        return True

    def run(self):
        reporting = self.request.user.get_reporting_on_date(
            self.request.data.get('as_on')
        )
        if reporting:
            return {
                'code': reporting.manager.unique_code,
                'name': reporting.manager.get_full_name()
            }
        return {}


class GetAllReportingManager(BaseService):
    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS ID of the user and return current managers
        of given AMS ID

        Example usage:
        ```
        call(
            'accounts',
            'current_reporting_managers',
            data={
                'user_id': '<ID>'
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException
        return True

    def run(self):
        reportings = self.request.user.get_current_and_future_reportings()
        result = []
        for reporting in reportings:
            result.append({
                'reportee__unique_code': reporting.reportee.unique_code,
                'manager__unique_code': reporting.manager.unique_code,
                'name': reporting.manager.get_full_name(),
                'start_date': reporting.start_date,
                'end_date': reporting.end_date,
                'reporting_type': reporting.reporting_type
            })
        return result


class ReportingCheckService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS IDs of the user and the reportee and checks whether
        they have a relation on some date.

        Example usage:
        ```
        call(
            'accounts',
            'reporting_check',
            data={
                'user_id': '<ID>',
                'reportee': '<ID>',
                'as_on': '<Date (dd/mm/yyyy)>', # Defaults to today's date
                'is_dialer': True, # to use AMS_Dialer service
            }
        )
        ```
    """
    is_global = True

    def validate(self):
        if not self.request.data.get('user_id') and not self.request.user:
            raise UserRequiredException

        if not self.request.user or isinstance(self.request.user, AnonymousUser):
            try:
                self.request.user = UserModel.objects.get(
                    unique_code=self.request.data.get('user_id')
                )
            except UserModel.DoesNotExist:
                raise UserRequiredException

        try:

            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            date_for_check = timezone.now_local(only_date=True)

            if self.request.data.get('as_on'):
                date_for_check = datetime.strptime(
                    self.request.data.get('as_on'), '%d/%m/%Y'
                ).date()

            try:
                reportee = UserModel.objects.get(
                    unique_code=self.request.data.get('reportee')
                )
            except UserModel.DoesNotExist:
                raise ValidationError(
                    'Please provide a valid AMS ID of the reportee.'
                )

            self.request.data.update({
                'as_on': date_for_check,
                'manager': self.request.user,
                'reportee': reportee,
            })

        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )

        return True

    def run(self):

        manager_list = []

        manager_list.extend(get_managers_list_on_date(
            self.request.data.get('reportee'),
            self.request.data.get('as_on')
        ))

        manager_list = list(flatten(manager_list))

        if self.request.data.get('manager') in manager_list:

            if self.request.data.get('is_dialer') and self.request.user.dialer_agent_id:
                agent_id = self.request.data.get('manager').dialer_agent_id
                mobile = self.request.data.get('reportee').decrypted_mobile
                pos_code = self.request.data.get('reportee').pos_code

                if not mobile:
                    raise ValidationError(
                        '{} doesn\'t have a mobile number.'.format(
                            self.request.data.get('reportee').unique_code
                        )
                    )

                return call_dialer(agent_id, mobile, pos_code)

            if self.request.data.get('is_dialer'):
                return self.request.data.get('reportee').decrypted_mobile

            return True

        return False


def call_dialer(agent_id, mobile=None, pos_code=''):
    """
        This method will take the 'dialer agent ID', 'mobile number'
         and 'Executive code' and initiate
        the call or return the appropriate response.
        :param agent_id: Dialer agent id
        :param mobile: Mobile no of partner / customer
        :return: call response  ( response from dialer API )
    """

    agent_parameters = {
        "uniqueid": pos_code,
        "agent_id": agent_id,
        "phone_num": mobile,
        "resFormat": 3,  # format 3 = JSON
    }

    try:

        status_response = requests.get(
            settings.DIALER_API_CHECK_AGENT_STATUS,
            params=agent_parameters
        )
        status_response_json = status_response.json()

        if status_response_json['response']['state'] in ('CLOSURE', 'PREVIEW_FAIL'):
            raise ValidationError('Please disconnect the current call from dialer.')

        elif status_response_json['response']['state'] == 'INCALL':
            raise ValidationError('Agent is already in call.')

        elif status_response_json['response']['state'] == 'NOT_LOGGED_IN':
            raise ValidationError('Please login to the Dialer.')

        else:
            call_response = requests.get(
                settings.DIALER_API_CALL_CONNECT,
                params=agent_parameters
            )

            if call_response.json()['response']['status'] == 'FAIL':
                raise ValidationError('Call Failed')

    except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout):
        raise ValidationError('Unable to connect to the dialer.')

    return {
        'response': 'Call Initiated....'
    }


class ImmediateReportingService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        Example usage:
        ```
        call(
            'accounts',
            'immediate_user_reporting',
            data={
                'user_id': '<ID>',
                'as_on': '<Date (dd/mm/yyyy)>'
            }
        )
        ||
        call(
            'accounts',
            'immediate_user_reporting',
            data={
                'user_id': '<ID>',
                'start_date': '<Date (dd/mm/yyyy)>'
                'end_date': '<Date (dd/mm/yyyy)>'
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.data or not \
                self.request.data.get('as_on') and \
                (not self.request.data.get('start_date') or not self.request.data.get('end_date')):
            raise ValidationError(
                'Please supply either `as_on` or date range (`start_date`, `end_date`) arguments.'
            )
        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise
                # the operations being done below won't be possible.
                self.request.data._mutable = True

            if self.request.data.get('as_on'):
                self.request.data.update({
                    'as_on': datetime.strptime(self.request.data.get('as_on'), '%d/%m/%Y').date(),
                })
            else:
                self.request.data.update({
                    'start_date': datetime.strptime(self.request.data.get('start_date'),
                                                    '%d/%m/%Y').date(),
                    'end_date': datetime.strptime(self.request.data.get('end_date'),
                                                  '%d/%m/%Y').date(),
                })
        except ValueError:
            raise ValidationError('Please provide the dates in proper format (dd/mm/yyyy).')

        return True

    def run(self):
        all_reportings = self.request.user.reportee.all()

        if self.request.data.get('as_on'):
            reportings_on_date = all_reportings.filter(
                Q(end_date__gte=self.request.data.get('as_on')) | Q(end_date=None),
                start_date__lte=self.request.data.get('as_on'),
            ).values_list('reportee__unique_code', flat=True)

            return reportings_on_date

        today = timezone.now_local(only_date=True)
        start_date = self.request.data.get('start_date')
        end_date = self.request.data.get('end_date')

        reportings_within_date_range = self.request.user.reportee.annotate(
            latest_start_date=Greatest('start_date', Value(start_date)),
            earliest_end_date=Coalesce(
                Least('end_date' or today, Value(end_date)),
                end_date or F('end_date')
            )
        ).annotate(
            diff=(Cast(F('earliest_end_date'), DateField()) -
                  Cast(F('latest_start_date'), DateField()))
        ).annotate(
            diff_days=ExtractDaysFromPGInterval(F('diff'), output_field=IntegerField())
        ).annotate(
            overlap=Greatest(0, F('diff_days') + 1)
        ).filter(overlap__gt=0).values_list('reportee__unique_code', 'start_date',
                                            'end_date', 'reporting_type')

        return reportings_within_date_range


def get_all_nodes_of_downward_hierarchy_tree(user_unique_codes, as_on, include_filter={}, exclude_filter={}):
    if isinstance(user_unique_codes, str):
        user_unique_codes = [user_unique_codes]
    elif isinstance(user_unique_codes, QuerySet):
        user_unique_codes = list(user_unique_codes)

    direct_reportings_on_date = UserReporting.objects.filter(
        Q(end_date__gte=as_on) | Q(end_date=None),
        start_date__lte=as_on,
        manager__unique_code__in=user_unique_codes,
        **include_filter
    ).exclude(**exclude_filter).values_list('reportee__unique_code', flat=True)

    if direct_reportings_on_date:
        user_unique_codes.extend(
            get_all_nodes_of_downward_hierarchy_tree(
                user_unique_codes=direct_reportings_on_date,
                as_on=as_on,
                include_filter=include_filter,
                exclude_filter=exclude_filter
            )
        )

    return user_unique_codes


class PartnersListService(BaseService):

    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        include_bp_partner: return all BP's with thier partners
        return_type: return unique_code or pos_code, default is unique_code

        Example usage:
        ```
        call(
            'accounts',
            'partners_list',
            data={
                'users': '[<ID_1>, <ID_2>, ...]',
                'as_on': '<Date (dd/mm/yyyy)>',
                'include_bp_partner': True/False(Optional),
                'return_type': 'unique_code/pos_code'(optional, default is unique_code)
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data or not \
                self.request.data.get('as_on'):
            raise ValidationError('Please supply `as_on` argument.')

        users = self.request.data.get('users', [])

        if isinstance(users, str):
            users = [s.strip() for s in users.split(',')]
        elif not isinstance(users, list):
            raise ValidationError(
                'Please provide the `users` argument as a `list` like object '
                'or a comma-separated string of AMS IDs.'
            )

        if any(s in ('', ',', '.') for s in users):
            users = list(filter(lambda a: a not in ('', ',', '.'), users))

        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise
                # the operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'users': users,
                'as_on': datetime.strptime(self.request.data.get('as_on'), '%d/%m/%Y').date(),
            })
        except ValueError:
            raise ValidationError('Please provide the `as_on` date in proper format (dd/mm/yyyy).')

        # Set default return_type.
        if self.request.data.get('return_type') not in ('pos_code', 'unique_code'):
            self.request.data['return_type'] = 'unique_code'

        return True

    def run(self):
        partners = {}
        exclude_bp_partner = {}
        if self.request.data.get("include_bp_partner"):
            users = UserModel.objects.filter(
                user_type=UserTypes.PARTNER,
                referral_code__startswith="BPO"
            ).values_list('unique_code', flat=True)
        else:
            users = self.request.data.get('users')
            # exclude BP
            exclude_bp_partner = {
                "reporting_type": ReportingType.PARTNER_PARTNER
            }
        for user in users:
            partner_list = get_all_nodes_of_downward_hierarchy_tree(
                user_unique_codes=user,
                as_on=self.request.data.get('as_on'),
                exclude_filter=exclude_bp_partner
            )

            partners_list = UserModel.objects.filter(
                unique_code__in=partner_list,
                pos_code__isnull=False
            ).values_list(
                self.request.data.get('return_type'),
                flat=True
            )
            try:
                if self.request.data.get('return_type') == 'pos_code':
                    user = UserModel.objects.get(unique_code=user).pos_code

                partners.update({
                    user: partners_list
                })
            except UserModel.DoesNotExist:
                logger.exception("%s: User Does not Exist.", user)

        return partners


class SendEmailService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for sending emails to the users.
        Takes the AMS ID of the user as an argument to verify
        which user to send email to.

        Example usage:
        ```
        call(
            'accounts',
            'send_email',
            data={
                'user_id': ''<AMS_ID/Pos code/email>',
                'cc': '<CC>',
                'bcc': '<BCC>',
                'subject': '<Subject>',
                'content': '<Body>'
            },
            files=[
                ('attachments', 'f1'),
                ('attachments', 'f2'),
                ...
            ]
        )
        ```
    """

    def is_valid_email(self, email):
        try:
            regex = r'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
            return bool(re.match(regex, email))
        except Exception:
            return False

    def validate(self):
        if not (self.request.data.get('user_id') or self.request.data.get('email')):
            raise UserRequiredException
        if self.request.data.get('user_id'):
            try:
                self.request.user = UserModel.objects.get(
                    Q(unique_code=self.request.data.get('user_id')) |
                    Q(hashed_email=hash_field(
                        UserManager.normalize_email(
                            self.request.data.get('user_id')
                        )
                    )) |
                    Q(pos_code=self.request.data.get('user_id'))
                )
            except UserModel.DoesNotExist:
                raise UserRequiredException

            if not self.request.user.decrypted_email:
                raise ValidationError(
                    'Unable to send email to the user. '
                    'Given user does not have an email ID associated with it.'
                )
        elif not self.is_valid_email(self.request.data.get('email')):
            raise UserRequiredException

        if not self.request.data.get('subject'):
            raise ValidationError('Please provide the subject (`subject`) line of the email.')

        if not self.request.data.get('content'):
            raise ValidationError(
                'Please provide the content (`content`) that is to be sent to the user.'
            )

        return True

    def run(self):
        email_sent = email.send(
            to=self.request.data.get('email') or self.request.user.decrypted_email,
            cc=self.request.data.get('cc', []),
            bcc=self.request.data.get('bcc', []),
            subject=self.request.data.get('subject'),
            html_body=self.request.data.get('content'),
            attachments=self.request.files.getlist('attachments', []) if self.request.files else [],
        )

        if not email_sent:
            raise ValidationError('Unable to send email at the moment. Please try again later.')

        return {'detail': 'Email sent successfully.'}


class SendSMSService(BaseService):
    """
        Internal service to send SMS to a user.
        Accepts AMS ID to verify what user to send SMS to.

        Example usage:
            ```
            call(
                'accounts',
                'send_sms',
                data={
                    'user_id': '<AMS_ID/Pos code>' or <mobile>,
                    'content': '<SMS Content>',
                }
            )
            ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException

        if self.request.data.get('user_id').isdigit():
            if len(self.request.data.get('user_id')) != 10:
                raise UserRequiredException
        else:
            try:
                self.request.user = UserModel.objects.get(
                    Q(unique_code=self.request.data.get('user_id')) |
                    Q(pos_code=self.request.data.get('user_id'))
                )
            except UserModel.DoesNotExist:
                raise UserRequiredException

            if self.request.user.mobile:
                self.request.data.update({
                    'user_id': self.request.user.decrypted_mobile
                })
            else:
                raise ValidationError(
                    'Unable to send SMS to the user. Given user does not have a mobile number associated with it.'
                )

        if not self.request.data.get('content'):
            raise ValidationError(
                'Please provide the content (`content`) that is to be sent to the user.'
            )

        return True

    def run(self):
        sms.send(
            self.request.data.get('user_id'),
            self.request.data.get('content')
        )

        return {
            'detail': f'Trying to send SMS to: {self.request.data.get("user_id")}'
        }


class SendOTPService(BaseService):
    """
        Example usage:
            ```
            call(
                'accounts',
                'send_otp',
                data={
                    'user_id': '<ID>'
                }
            )
            ```
    """
    is_global = True

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.user.mobile:
            raise ValidationError(
                'Unable to send an OTP to the user. '
                'Given user does not have a mobile number associated with it.'
            )

        return True

    def run(self):
        instance = SMSDevice.objects.get_or_create(
            user=self.request.user,
            number=self.request.user.decrypted_mobile
        )[0]
        try:
            instance.generate_challenge()
        except Exception:
            message = f'Unable to send an OTP to: {self.request.user.unique_code}'
            logger.exception(message)
            raise ValidationError(message)

        return {
            'detail': f'Trying to send OTP to: {self.request.user.mobile}'
        }


class VerifyOTPService(BaseService):
    """
        Example usage:
            ```
            call(
                'accounts',
                'verify_otp',
                data={
                    'user_id': '<ID>',
                    'otp_token': '<OTP>',
                }
            )
            ```
    """
    is_global = True

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                unique_code=self.request.data.get('user_id')
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.user.mobile:
            raise ValidationError(
                'Unable to verify OTP sent to the user. '
                'Given user does not have a mobile number associated with it.'
            )

        if not self.request.data.get('otp_token'):
            raise ValidationError(
                'Please provide the OTP token (`otp_token`) that is to be validated.'
            )

        return True

    def run(self):
        try:
            instance = SMSDevice.objects.get(
                user=self.request.user,
                number=self.request.user.decrypted_mobile
            )
        except SMSDevice.DoesNotExist:
            raise ValidationError('No OTP was sent to this user\'s mobile number.')

        if not instance:
            raise ValidationError('No OTP was sent to this user\'s mobile number.')

        if not instance.verify_token(token=self.request.data.get('otp_token')):
            raise ValidationError('Enter a valid OTP.')

        # Check if any other user had this mobile as their verified contact,
        # mark their's as unverified.

        users = UserModel.objects.filter(
            hashed_mobile=hash_field(self.request.user.decrypted_mobile),
            is_mobile_verified=True
        )
        for user in users:
            user.is_mobile_verified = False
            user.save(update_fields=['is_mobile_verified'])

        # Set this user's mobile as verified.
        self.request.user.is_mobile_verified = True
        self.request.user.is_active = True
        self.request.user.save(update_fields=['is_mobile_verified', 'is_active'])

        return {'detail': 'OTP verified successfully.'}


class CommunicationPreferenceService(BaseService):
    """
        Example usage:
            ```
            call(
                'accounts',
                'communication_preferences',
                data={
                    "whatsapp_notification_enabled": True/False
                }
            )
            ```
    """
    is_global = True

    def validate(self):
        self.request.user = authenticate(self.request)
        phone_number = self.request.user.decrypted_mobile
        if not phone_number and self.request.user.is_mobile_verified:
            raise ValidationError(
                "Given user does not have a mobile number associated with it."
            )
        if isinstance(self.request.data, QueryDict):
            # Setting the QueryDict as mutable otherwise the
            # operations being done below won't be possible.
            self.request.data._mutable = True
        # In Query Dict object, values are in string format.
        if self.request.data.get(
            'whatsapp_notification_enabled'
        ) not in ["True", "False", True, False]:
            raise ValidationError(
                "Please provide valid request data."
            )
        return True

    def run(self):
        if self.request.data.get('whatsapp_notification_enabled') in [True, "True"]:
            notification_status = True
        else:
            notification_status = False
        if notification_status:
            status = set_whatsapp_opt_in_out(
                self.request.user
            )
            logger.info(
                "[AMS][WHATSAPP] notification status[Unique Code: %s]: %s",
                self.request.user.unique_code, status
            )
        # to prevent user not to update any other values of user model,
        # if request data contains any other field data.
        clean_serializer_data = clean_empty({
            "whatsapp_notification_enabled": notification_status
        })
        serializer = UserSerializer(
            instance=self.request.user,
            data=clean_serializer_data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return {
            'whatsapp_notification_enabled': serializer.data.get(
                "whatsapp_notification_enabled"
            )
        }


def set_whatsapp_opt_in_out(user):
    """
    function used to set the WhatsApp opt-IN status.
    params: user object
    return: True if opt-IN change
    """
    api_url = settings.GUPSHUP_CONF['URLS']['WHATSAPP_OPT_IN_OUT_API']
    user_id = settings.GUPSHUP_CONF['CREDENTIALS']['USER_ID']
    password = settings.GUPSHUP_CONF['CREDENTIALS']['PASSWORD']
    phone_number = user.decrypted_mobile
    if len(phone_number) == 10:
        phone_number = "91" + phone_number
    elif phone_number.startswith("+91"):
        phone_number = phone_number.replace("+", "")
    url = api_url.format(
        user_id=user_id,
        password=password,
        phone_number=phone_number
    )
    response = requests.get(url=url)
    if response.status_code != 200:
        raise OptUserException
    else:
        try:
            parsed_content = response.json()
            content = parsed_content.get(
                'data', {}
            ).get('response_messages', {})[0]
            if content.get('status', '') == "success":
                response_text = "User OPT-IN successfully."
            elif content.get('status', '') == "error":
                response_text = content.get('details', '')
            logger.info(
                "[AMS] WhatsApp OPT-IN/OUT API response[Unique Code: %s]: %s",
                user.unique_code, response_text
            )
        except Exception as e:
            logger.exception(
                "[AMS] WhatsApp API error[Unique code: %s]: %s",
                user.unique_code, response.text
            )
            raise NotificationAPIException(
                f"WhatsApp API error({e}) for Unique code: {user.unique_code}"
            )
    return True


class BulkPartnerDetailsService(BaseService):
    """
    Service used to return the bulk partner details with its BP referral code
        Example usage:
            ```
            call(
                'accounts',
                'bulk_partner_details',
                data={
                    "page": <int>,
                    "record_per_page": <int>
                }
            )
            ```
    """

    def validate(self):
        if (
            self.request.data.get("record_per_page") is None or
            self.request.data.get("record_per_page") > settings.DEFAULT_PAGINATOR_SIZE
        ):
            self.request.data.update({
                "record_per_page": settings.DEFAULT_PAGINATOR_SIZE
            })
        return True

    def run(self):

        partners = UserModel.objects.filter(
            user_type=UserTypes.PARTNER
        ).exclude(referral_code__startswith="BPO").order_by('id')
        total_records = partners.count()

        paginator = Paginator(partners, self.request.data.get("record_per_page"))
        page = self.request.data.get('page', 1)

        if page > paginator.num_pages:
            return {}
        has_next = False
        try:
            has_next = paginator.page(page).has_next()
            partners = paginator.page(page)
        except (PageNotAnInteger, EmptyPage):
            partners = paginator.page(1)

        serializer = BulkUserSerializer(partners, many=True)

        return {
            'data': serializer.data,
            'hasNext': has_next,
            'total_records': total_records
        }


class ReportingsOnDateRangeService(BaseService):
    """
    This service get all the reportings between
    the provided dates.

        Example usage:
        ```
        call(
            'accounts',
            'reportings_on_daterange',
            data={
                'start_date': '%d/%m/%Y',
                'end_date': '%d/%m/%Y
            }
        )
        ```
    """

    def validate(self):
        if self.request.data.get('start_date') and self.request.data.get('end_date'):
            try:
                if isinstance(self.request.data, QueryDict):
                    self.request.data._mutable = True

                start_date = datetime.strptime(
                    self.request.data.get('start_date'), '%d/%m/%Y'
                ).date()

                end_date = datetime.strptime(
                    self.request.data.get('end_date'), '%d/%m/%Y'
                ).date()

                self.request.data.update({
                    'start_date': start_date,
                    'end_date': end_date
                })
            except ValueError:
                raise ValidationError(
                    'Please provide the dates in proper format (dd/mm/yyyy).'
                )

            return True

        else:
            return False

    def run(self):
        start_date = self.request.data.get('start_date')
        end_date = self.request.data.get('end_date')
        user_reportings = UserReporting.objects.filter(
            modified_at__date__range=(start_date, end_date)
        )

        if user_reportings:
            return user_reportings.values('start_date', 'end_date',
                                          'manager__unique_code', 'reportee__unique_code',
                                          'reporting_type')
        return []


class FetchClusterMapping(BaseService):
    """
    This service get all the Partners of given cluster.

        Example usage:
        ```
        call(
            'accounts',
            'bulk_partners_from_cluster',
            data={
                'cluster_code': <Cluster_ID>,
                'page': <int>,
                'record_per_page': <int>
            }
        )
        ```
    """

    def validate(self):
        try:
            self.request.data.update({
                'cluster': Cluster.objects.get(
                    cluster_code=self.request.data.get('cluster_code')
                )
            })
        except Cluster.DoesNotExist:
            raise ValidationError(
                'Please provide valid cluster Code'
            )
        if (
            self.request.data.get('record_per_page') is None
            or self.request.data.get('record_per_page') > settings.DEFAULT_PAGINATOR_SIZE
        ):
            self.request.data.update({
                'record_per_page': settings.DEFAULT_PAGINATOR_SIZE
            })
        return True

    def run(self):
        cluster = self.request.data.get('cluster')
        partners = cluster.users.prefetch_related(
            'user', 'cluster'
        ).order_by('id')
        total_records = partners.count()

        paginator = Paginator(
            partners, self.request.data.get('record_per_page')
        )
        page = self.request.data.get('page', 1)

        if page > paginator.num_pages:
            return {}
        has_next = False
        # has_next = True if page < paginator.num_pages else False
        try:
            has_next = paginator.page(page).has_next()
            partners = paginator.page(page)
        except (PageNotAnInteger, EmptyPage):
            partners = paginator.page(1)

        serializer = UserClusterSerializer(partners, many=True)

        return {
            'data': serializer.data,
            'hasNext': has_next,
            'total_records': total_records
        }


class FetchPartnerClusterMapping(BaseService):
    """
    This service get all the Partners of given cluster.
    `include_inactive`: if True return partner with active cluster else all cluster

        Example usage:
        ```
        call(
            'accounts',
            'get_cluster_with_partner_id',
            data={
                'user_id': <AMS_ID>or<Pos_code>or<EmailID>,
                'include_inactive': <True/False>
            }
        )
        ```
    """

    def validate(self):

        if not self.request.data.get('user_id'):
            raise UserRequiredException

        if isinstance(self.request.data, QueryDict):
            # Setting the QueryDict as mutable otherwise the
            # operations being done below won't be possible.
            self.request.data._mutable = True

        if self.request.data.get('include_inactive') is None:
            self.request.data.update({
                'include_inactive': False
            })
        users = self.request.data.get('user_id', [])
        if isinstance(users, str):
            users = [s.strip() for s in users.split(',')]
        elif not isinstance(users, list):
            raise ValidationError(
                'Please provide the `users` argument as a `list` like object '
                'or a comma-separated string of AMS IDs.'
            )

        if any(s in ('', ',', '.') for s in users):
            users = list(filter(lambda a: a not in ('', ',', '.'), users))

        if len(users) > settings.DEFAULT_PAGINATOR_SIZE:
            raise ValidationError('Maximum users limit reached.')

        self.request.data.update({
            'users': users
        })
        return True

    def run(self):
        users_list = [
            hash_field(UserManager.normalize_email(email))
            for email in self.request.data.get('users')
        ]
        user_cluster = UserCluster.objects.select_related(
            'user', 'cluster').filter(
                Q(user__unique_code__in=self.request.data.get('users')) |
                Q(user__hashed_email__in=users_list) |
                Q(user__pos_code__in=self.request.data.get('users')) |
                Q(user__referral_code__in=self.request.data.get('users'))
        )
        if self.request.data.get('include_inactive'):
            if user_cluster.count() == 0:
                return []
        else:  # Active cluster only
            user_cluster = user_cluster.filter(cluster__end_date=None)

        serializer = UserClusterSerializer(user_cluster, many=True)
        return serializer.data


class GetPartnerReportingsService(BaseService):
    """
        This service gets the `frm`, `trm`, `fasm`
        & 'tasm` of given partner.

        Example usage:
        ```
        call(
            'accounts',
            'get_partner_reportings',
            data={
                'partner_unique_codes': list[<AMS_ID>],
                'as_on': '<Date (dd/mm/yyyy)>'
            }
        )
        ```
    """
    def validate(self):
        if not self.request.data.get('partner_unique_codes'):
            raise UserRequiredException

        if not self.request.data or not self.request.data.get('as_on'):
            raise ValidationError(
                'Please supply the `as_on` argument to fetch reporting for.'
            )

        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'as_on': datetime.strptime(
                    self.request.data.get('as_on'), '%d/%m/%Y'
                ).date(),
            })
        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )

        if len(self.request.data.get('partner_unique_codes')) > settings.DEFAULT_PAGINATOR_SIZE:
            raise ValidationError('Maximum users limit reached(i.e., 1000)')

        return True

    def run(self):
        reporting_data = {}

        partner_unique_codes = self.request.data.get('partner_unique_codes')

        as_on = self.request.data.get('as_on')

        reportings = UserReporting.objects.select_related(
            'manager', 'reportee').filter(
            Q(reporting_type=ReportingType.PARTNER_FIELD) |
            Q(reporting_type=ReportingType.PARTNER_TELE) |
            Q(reporting_type=ReportingType.PARTNER_PARTNER) |
            Q(reporting_type=ReportingType.PARTNER_EMPLOYEE),
            Q(end_date=None) | Q(end_date__gte=as_on),
            start_date__lte=as_on,
            reportee__unique_code__in=partner_unique_codes
        )
        reportings_data = reportings.values('reportee').annotate(
            reporting=Count('reportee__unique_code')
        )

        managers = reportings.values_list('manager', flat=True)

        manager_reportings = UserReporting.objects.select_related(
            'manager', 'reportee').filter(
                Q(end_date=None) | Q(end_date__gte=as_on),
                start_date__lte=as_on,
                reportee__id__in=managers,
                reporting_type=ReportingType.EMPLOYEE,
            )

        for data in reportings_data:
            frm_mail = trm = frm = trm_mail = ''
            fasm_mail = tasm_mail = bp_mail = ''

            partner_reportings = reportings.filter(
                reportee__id=data.get('reportee')
            )

            for reporting in partner_reportings:
                if reporting.reporting_type == ReportingType.PARTNER_FIELD:
                    frm = reporting.manager
                    frm_mail = frm.decrypted_email
                    try:
                        fasm_mail = manager_reportings.filter(
                            reportee=frm
                        ).first().manager.decrypted_email
                    except Exception:
                        logger.exception(
                            f'frm({frm}) does not have any manager'
                        )

                elif reporting.reporting_type == ReportingType.PARTNER_TELE:
                    trm = reporting.manager
                    trm_mail = trm.decrypted_email
                    try:
                        tasm_mail = manager_reportings.filter(
                            reportee=trm
                        ).first().manager.decrypted_email
                    except Exception:
                        logger.exception(
                            f'trm({trm}) does not have any manager'
                        )

                elif reporting.reporting_type == ReportingType.PARTNER_PARTNER:
                    bp_mail = reporting.manager.decrypted_email

            partner = partner_reportings.first().reportee

            if not frm:
                try:
                    fasm_mail = partner_reportings.filter(
                        reporting_type=ReportingType.PARTNER_EMPLOYEE
                    ).first().manager.decrypted_email
                except Exception:
                    logger.exception(
                        f'partner({partner}) does not have any manager'
                    )

            reporting_data.update({
                partner.unique_code: {
                    'frm': frm_mail,
                    'trm': trm_mail,
                    'fasm': fasm_mail,
                    'tasm': tasm_mail,
                    'bp': bp_mail
                }
            })

        return reporting_data


class DirectReportingDetailService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for getting direct parters(with reporting type)
        of a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        Example usage:
        ```
        call(
            'accounts',
            'direct_reportings_with_detail',
            data={
                'user_id': '<ID>',
                'as_on': '<Date (dd/mm/yyyy)>'
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException
        try:
            self.request.user = UserModel.objects.get(
                Q(unique_code=self.request.data.get('user_id')) |
                Q(hashed_email=hash_field(
                    UserManager.normalize_email(
                        self.request.data.get('user_id')
                    )
                )) |
                Q(pos_code=self.request.data.get('user_id'))
            )
        except UserModel.DoesNotExist:
            raise UserRequiredException

        if not self.request.data or not self.request.data.get('as_on'):
            raise ValidationError(
                'Please supply the `as_on` argument to fetch reporting for.'
            )
        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise the
                # operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'as_on': datetime.strptime(
                    self.request.data.get('as_on'), '%d/%m/%Y'
                ).date(),
            })
        except ValueError:
            raise ValidationError(
                'Please provide the dates in proper format (dd/mm/yyyy).'
            )

        return True

    def run(self):
        manager_hierarachy_list = []
        date = self.request.data.get('as_on')

        managers_on_date = self.request.user.reportee.filter(
            Q(end_date__gte=date) | Q(end_date=None),
            start_date__lte=date,
            reporting_type__in=[
                i for i in PartnerToEmployeeReportingType.__dict__.values() if type(i) == int
            ]
        )

        for reporting in managers_on_date:
            manager_hierarachy_list.append({
                'unique_code': reporting.reportee.unique_code,
                'pos_code': reporting.reportee.pos_code,
                'name': reporting.reportee.get_full_name(),
                'reporting_type': reporting.reporting_type,
                'start_date': reporting.start_date,
                'end_date': reporting.end_date
            })

        return manager_hierarachy_list


class ReportingManagerDetailsService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for immediate reportings to a user.
        Takes the AMS ID of the user and the date range to
        fetch the user reporting in that time frame.

        Example usage:
        ```
        call(
            'accounts',
            'reporting_manager_details',
            data/json={
                'users': '[<ID_1>, <ID_2>, ...]',
                'as_on': '<Date (dd/mm/yyyy)>'
                'reporting_type': <reporting code>
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('users'):
            raise UserRequiredException

        if not self.request.data.get('as_on'):
            raise ValidationError('Please supply `as_on` argument.')

        users = self.request.data.get('users', [])

        if isinstance(users, str):
            users = [s.strip() for s in users.split(',')]
        elif not isinstance(users, list):
            raise ValidationError(
                'Please provide the `users` argument as a `list` like object '
                'or a comma-separated string of AMS IDs.'
            )

        if any(s in ('', ',', '.') for s in users):
            users = list(filter(lambda a: a not in ('', ',', '.'), users))

        try:
            if isinstance(self.request.data, QueryDict):
                # Setting the QueryDict as mutable otherwise
                # the operations being done below won't be possible.
                self.request.data._mutable = True

            self.request.data.update({
                'users': users,
                'as_on': datetime.strptime(self.request.data.get('as_on'), '%d/%m/%Y').date(),
            })
        except ValueError:
            raise ValidationError('Please provide the `as_on` date in proper format (dd/mm/yyyy).')

        if not self.request.data.get('reporting_type'):
            self.request.data.update({
                'reporting_type': ReportingType.PARTNER_HEALTH_FRM
            })

        return True

    def run(self):
        user_reportings = UserReporting.objects.filter(
            Q(reportee__unique_code__in=self.request.data.get('users')) |
            Q(reportee__pos_code__in=self.request.data.get('users')),
            reporting_type=self.request.data.get('reporting_type'),
            end_date=None
        ).values('reportee__pos_code', 'manager___email', 'manager___mobile',
                 'manager__first_name', 'manager__last_name')

        reporting_list = {}

        for reporting in user_reportings:
            reporting_list.update({
                reporting.get('reportee__pos_code', ''): {
                    'health_expert_email': reporting.get('manager___email') or '',
                    'health_expert_mobile': reporting.get('manager___mobile') or 'Not Available',
                    'health_expert_full_name': '{fname} {lname}'.format(
                        fname=reporting.get('manager__first_name') or '',
                        lname=reporting.get('manager__last_name') or '',
                    )
                }
            })

        return reporting_list


class BulkProfileFetchingService(BaseService):
    """
        This service will only be accessbile to the internal
        systems for fetching the profile of the user.
        Takes the AMS ID/Email/Pos Code of the users as an argument to verify
        which user to fetch profile for.

        Example usage:
        ```
        call(
            'accounts',
            'bulk_fetch_profile_with_id',
            data/json={
                'user_id': '[<AMS ID/Email ID/Pos Code>, ...]',
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('user_id'):
            raise UserRequiredException

        users = self.request.data.get('user_id', [])

        if isinstance(users, str):
            users = [s.strip() for s in users.split(',')]
        elif not isinstance(users, list):
            raise ValidationError(
                'Please provide the `users` argument as a `list` like object '
                'or a comma-separated string of AMS IDs.'
            )
        if isinstance(self.request.data, QueryDict):
            # Setting the QueryDict as mutable otherwise the
            # operations being done below won't be possible.
            self.request.data._mutable = True

        if any(s in ('', ',', '.') for s in users):
            users = list(filter(lambda a: a not in ('', ',', '.'), users))

        if len(users) > settings.DEFAULT_PAGINATOR_SIZE:
            raise ValidationError('Maximum user_id limit reached.')

        self.request.data.update({
            'users': users
        })
        return True

    def run(self):
        users = self.request.data.get('users')

        # didn't use email due to optimization of API.
        users = UserModel.objects.prefetch_related(
            'primary_address', 'secondary_address', 'gstin_address'
        ).filter(
            Q(unique_code__in=users) |
            Q(pos_code__in=users) |
            Q(referral_code__in=users)
        )
        serializer = UserSerializer(users, many=True)
        return serializer.data


class FetchClusterDetails(BaseService):
    """
        This service will only be accessbile to the internal
        systems for fetching the cluster details of the cluster.
        Takes the `is_active` as optional args which filter the cluster status.

        Example usage:
        ```
        call(
            'accounts',
            'fetch_cluster_details',
            json={
                'is_active': <True/False> (*optional)
            }
        )
        ```
    """

    def run(self):
        if self.request.data.get('is_active') is not None:
            cluster = Cluster.objects.filter(
                end_date__isnull=self.request.data.get('is_active')
            )
        else:
            cluster = Cluster.objects.all()
        serializer = ClusterSerializer(cluster, many=True)
        return serializer.data


class FetchConsentStatusService(BaseService):
    """
    Service used to get the policy or contract.
    it returns user consent status for each policy.
        Example usage:
        ```
        call(
            'accounts',
            'fetch_user_consent_status',
            data={
                'consent_doc': '<consent_doc>,<consent_doc>...' or '<short_name>,<short_name>,...'
            }
        )
        ```
    """

    def validate(self):
        self.request.user = authenticate(self.request)

        if isinstance(self.request.data, QueryDict):
            # Setting the QueryDict as mutable otherwise the
            # operations being done below won't be possible.
            self.request.data._mutable = True

        consent_doc = self.request.data.get('consent_doc', [])
        if isinstance(consent_doc, str):
            consent_doc = [s.strip() for s in consent_doc.split(',')]
        elif not isinstance(consent_doc, list):
            raise ValidationError(
                'Please provide the `consent_doc` argument as a `list` like object '
                'or a comma-separated string of Consent IDs.'
            )

        if any(s in ('', ',', '.') for s in consent_doc):
            consent_doc = list(filter(lambda a: a not in ('', ',', '.'), consent_doc))

        self.request.data.update({
            'consent_doc': consent_doc
        })
        return True

    def run(self):
        # Send Whatsapp consent everytime
        consent_details = []
        consent_status = []
        try:
            if self.request.data.get('consent_doc'):
                search_query = {
                    'consent_doc__consent_doc__in': self.request.data.get('consent_doc')
                }
            else:
                # to return all consent of user.
                search_query = {}
            consents = UserConsent.objects.filter(
                Q(**search_query),
                user=self.request.user
            )
            for consent in consents:
                consent_status.append(consent.is_accepted)
                consent_details.append({
                    'consent_doc': consent.consent_doc.consent_doc,
                    'is_accepted': consent.is_accepted,
                    'short_name': consent.consent_doc.short_name,
                    'consent_at': consent.created_at,
                    'platform': consent.platform,
                })
        except Exception as e:
            raise ValidationError(str(e))

        return {
            'is_accepted': all(consent_status) if consent_status else False,
            'user': self.request.user.unique_code,
            'consent_details': consent_details
        }


class CreateUserConsentService(BaseService):
    """
    Service used to get the policy or contract.
    It creates User consent for the given policies.
        Example usage:
        ```
        call(
            'accounts',
            'update_user_consent',
            json={
                "platform": 1/2,
                "consent_details": [
                    {
                        "consent_doc": <consent_doc>,
                        "is_accepted": <True/False>
                    },
                    {
                        "consent_doc": <consent_doc>,
                        "is_accepted": <True/False>
                    },...
                ]
            }
        )
        ```
    """

    def validate(self):
        self.request.user = authenticate(self.request)
        if not isinstance(
            self.request.data.get('consent_details'), Iterable
        ):
            raise ValidationError(
                'Please provide the consent details.'
            )

        # check for data is correct or not.
        for i in self.request.data.get('consent_details'):
            if 'consent_doc' not in i.keys():
                raise ValidationError('Please provide the correct consent format.')
            if (
                'is_accepted' not in i.keys() and
                not isinstance(i.get('is_accepted'), bool)
            ):
                raise ValidationError('Please provide the correct consent format.')

        try:
            consent = []
            for detail in self.request.data.get('consent_details'):
                consent.append((
                    PolicyAndContract.objects.get(
                        consent_doc=detail.get('consent_doc')
                    ),
                    detail.get('is_accepted')
                ))
            self.request.data.update({
                'consent_details': consent
            })
        except PolicyAndContract.DoesNotExist:
            raise ValidationError(
                'Consent document not found: %s' % detail.get('consent_doc')
            )

        try:
            ip = self.request.META.get('HTTP_X_FORWARDED_FOR', '').split(',')
            self.request.data.update({
                'ip_address': ip[0] if ip else None
            })
        except Exception as e:
            raise ValidationError(e)

        return True

    def run(self):
        consent_details = []
        consent_status = []
        send_consent_notification = False
        try:
            for consent, status in self.request.data.get('consent_details'):
                user_consent, is_created = UserConsent.objects.get_or_create(
                    consent_doc=consent,
                    user=self.request.user
                )
                user_consent.is_accepted = status
                user_consent.ip_address = self.request.data.get('ip_address')
                user_consent.platform = self.request.data.get('platform')
                user_consent.save(
                    update_fields=['is_accepted', 'ip_address', 'platform']
                )
                user_consent.refresh_from_db()
                update_change_reason(
                    instance=user_consent,
                    reason=f'Change from IP: {self.request.META.get("HTTP_X_FORWARDED_FOR", "")}'
                )
                consent_status.append(user_consent.is_accepted)
                consent_details.append({
                    'consent_doc': user_consent.consent_doc.consent_doc,
                    'is_accepted': user_consent.is_accepted,
                    'consent_at': user_consent.created_at,
                    'ip_address': user_consent.ip_address
                })
                # set flag not to send email for whatsapp
                if user_consent.consent_doc.short_name != 'WhatsApp':
                    send_consent_notification = True

            if send_consent_notification:
                # Create template for Consent Doc.
                address = city = pincode = ''
                if self.request.user.primary_address:
                    address = self.request.user.primary_address.get_full_address()
                    pincode = self.request.user.primary_address.pincode
                    city = self.request.user.primary_address.city.city_name if self.request.user.primary_address.city else ''
                elif self.request.user.secondary_address:
                    address = self.request.user.secondary_address.get_full_address()
                    pincode = self.request.user.secondary_address.pincode
                    city = self.request.user.secondary_address.city.city_name if self.request.user.secondary_address.city else ''
                context = {
                    'agreement_data': {
                        'partner_name': self.request.user.get_full_name(),
                        'address': address,
                        'exam_pass_date': '',
                        'pincode': pincode,
                        'city': city,
                        'type': 'Consent Document'
                    }
                }
                # Call celery for creating attachments and sending email.
                create_pdf_and_send.delay(
                    to=self.request.user.unique_code,
                    subject='Partner Agreement',
                    email_template='posp_email.html',
                    email_context={'user_name': self.request.user.get_full_name()},
                    pdf_tempalte='agreement.html',
                    pdf_context=context,
                    consent_details=consent_details,
                    filename='POSP T&C.pdf',
                    send_from_spear=settings.SEND_EMAIL_FROM_SPEAR
                )
        except Exception as e:
            raise ValidationError(str(e))

        return {
            'is_accepted': all(consent_status) if consent_status else False,
            'platform': self.request.data.get('platform'),
            'user': self.request.user.unique_code,
            'consent_details': consent_details
        }
