# Generated by Django 2.2.5 on 2020-08-28 10:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0027_cluster_historicalcluster_historicalusercluster_usercluster'),
    ]

    operations = [
        migrations.CreateModel(
            name='BetaFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('feature_name', models.CharField(blank=True, max_length=50)),
                ('feature_code', models.CharField(blank=True, max_length=20, unique=True)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='user',
            name='beta_enrollment',
            field=models.ManyToManyField(to='accounts.BetaFeature'),
        ),
    ]
