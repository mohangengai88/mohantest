from collections import OrderedDict

from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import (AuthenticationForm, PasswordResetForm,
                                       ReadOnlyPasswordHashField,
                                       SetPasswordForm)
from django.utils.translation import ugettext_lazy as _

from ams.accounts.managers import UserManager
from ams.accounts.models import User
from ams.utils.data_mask import hash_field

UserModel = get_user_model()


class AMSAuthenticationForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        pass


class AMSPasswordResetForm(PasswordResetForm):

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        """
        Send a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        user = context.get('user')
        if 'TA' in user.unique_code:
            from_email = settings.TRAVASSURED_FROM_EMAIL
        super().send_mail(subject_template_name, email_template_name,
                          context, from_email, to_email, html_email_template_name=None)

    def get_users(self, email):
        """Given an email, return matching user(s) who should receive a reset.

        This allows subclasses to more easily customize the default policies
        that prevent inactive users and users with unusable passwords from
        resetting their password.
        """
        active_users = UserModel._default_manager.filter(**{
            'hashed_email__iexact': hash_field(
                UserManager.normalize_email(
                    email
                )
            ),
            'is_active': True,
        })
        return (u for u in active_users if u.has_usable_password())


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    error_messages = {
        'duplicate_email': _('A user with that email already exists.'),
        'password_mismatch': _('The two password fields didn\'t match.'),
    }

    hashed_email = forms.CharField(
        label=_('Email'), max_length=128,
        error_messages={
            'required': 'Enter the Email ID.'
        }
    )

    password1 = forms.CharField(
        label=_('Password'),
        min_length=6,
        error_messages={
            'required': 'Enter New Password.',
            'min_length': 'Password should have minimum 6 characters.'
        },
        widget=forms.PasswordInput)

    password2 = forms.CharField(
        label=_('Password confirmation'),
        widget=forms.PasswordInput,
        help_text=_('Enter the same password as above, for verification.'))

    class Meta:
        model = UserModel
        fields = ('hashed_email',)

    def clean_hashed_email(self):
        # Since User.email is unique, this check is redundant,
        # but it sets a nicer error message than the ORM.
        email = self.cleaned_data['hashed_email']
        try:
            User._default_manager.get(
                hashed_email=hash_field(UserManager.normalize_email(email))
            )
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.email = self.cleaned_data['hashed_email']
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """
        A form for updating users. Includes all the fields on
        the user, but replaces the password field with admin's
        password hash display field.
    """
    password = ReadOnlyPasswordHashField(label=_('Password'),
                                         help_text=_('Raw passwords are not stored, so there is no way to see '
                                                     'this user\'s password, but you can change the password '
                                                     'using <a href=\'../password/\'>this form</a>.'))

    class Meta:
        model = UserModel
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.cleaned_data['password']

    def clean_first_name(self):
        """
            First name validations.
        """
        first_name = self.cleaned_data['first_name']

        if first_name in ('', None):
            raise forms.ValidationError('First name cannot be blank or None')

        return first_name


class PasswordReset(forms.Form):
    new_password = forms.CharField(
        max_length=128,
        min_length=6,
        error_messages={'required': 'Please enter your new password.',
                        'min_length': 'Password should have minimum 6 characters.'},
        widget=forms.TextInput(
            attrs={'type': 'password',
                   'class': 'form-control',
                   'placeholder': 'New Password'}
        )
    )
    confirmed_password = forms.CharField(
        max_length=128,
        error_messages={'required': 'Please enter your new confirmation password.'},
        widget=forms.TextInput(
            attrs={'type': 'password',
                   'class': 'form-control',
                   'placeholder': 'Confirmation Password'}
        )
    )

    def clean(self):
        cleaned_data = self.cleaned_data
        new_password = cleaned_data.get('new_password')
        confirmed_password = cleaned_data.get('confirmed_password')
        if new_password and confirmed_password and new_password != confirmed_password:
            self.add_error('confirmed_password', 'The two password fields didn\'t match.')

        return cleaned_data


class PasswordChangeForm(SetPasswordForm):
    """
    A form that lets a user change their password by entering their old
    password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _('Your old password was entered incorrectly.Please enter it again.'),
        'password_same_as_old': _('New Password must be different from Old Password.'),
        'min_length': _(' Password should have minimum 6 characters. '),
        'alpha_pwd': _('The new password must contain at least one letter and at least one digit or punctuation character.'),
        'password_mismatch': _('New Password does not match the Confirm Password.'),
    })
    old_password = forms.CharField(label=_('Old password'),
                                   widget=forms.PasswordInput)

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data['old_password']
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

    MIN_LENGTH = 6  # Set The Minumum Password Length

    def clean_new_password1(self):
        """
        Validates that the new_password1 field is min length 6 and not same as the Old Pasword.
        """
        password1 = self.cleaned_data.get('new_password1')
        old_password = self.cleaned_data.get('old_password')

        # At least MIN_LENGTH long
        if len(password1) < self.MIN_LENGTH:
            raise forms.ValidationError(
                self.error_messages['min_length'],
                code='min_length',
            )

        # At least one letter and one non-letter

        # first_isalpha = password1[0].isalpha()
        # if all(c.isalpha() == first_isalpha for c in password1):
        #     raise forms.ValidationError(
        #             self.error_messages['alpha_pwd'],
        #             code='alpha_pwd',
        #     )

        # Checks the Old Password with new password , It must be different from the Old One
        if password1 and old_password:
            if password1 == old_password:
                raise forms.ValidationError(
                    self.error_messages['password_same_as_old'],
                    code='password_same_as_old',
                )
        return password1


PasswordChangeForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)
