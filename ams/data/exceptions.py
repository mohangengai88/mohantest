from rest_framework.exceptions import APIException


class InvalidUUIDException(APIException):
    status_code = 400
    default_detail = 'Invalid UUID. Please enter a valid UUID.'


class TransactionDoesNotExistException(APIException):
    status_code = 400
    default_detail = 'Transaction with this UUID not found. Please enter a valid UUID.'
