from rest_framework import serializers

from ams.accounts.validators import PinCodeValidator
from ams.data.models import (
    Address,
    City,
    PolicyAndContract,
    State,
    Transaction
)


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = ('state_name', 'gst_code')
        read_only_fields = ('state_name', 'gst_code')


class CitySerializer(serializers.ModelSerializer):
    city = serializers.CharField(source='city_name')
    state = StateSerializer(read_only=True)

    class Meta:
        model = City
        fields = ('id', 'city', 'state')
        read_only_fields = ('id', 'city', 'state')


class AddressSerializer(serializers.ModelSerializer):
    pincode = serializers.CharField(
        max_length=6, min_length=6, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'min_length': 'Pin code length must be 6 digits.',
            'max_length': 'Pin code length must be 6 digits.',
        },
        validators=[PinCodeValidator()]
    )
    address_line1 = serializers.CharField(
        max_length=128, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'max_length': 'Address length must be 128 characters.',
        }
    )
    address_line2 = serializers.CharField(
        max_length=128, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'max_length': 'Address length must be 128 characters.',
        }
    )
    address_line3 = serializers.CharField(
        max_length=128, required=False,
        allow_null=True, allow_blank=True, default=None,
        error_messages={
            'max_length': 'Address length must be 128 characters.',
        }
    )
    city = CitySerializer(read_only=True)
    city_id = serializers.PrimaryKeyRelatedField(
        queryset=City.objects.all(), write_only=True, source='city'
    )

    class Meta:
        model = Address
        fields = ('id', 'address_line1', 'address_line2',
                  'address_line3', 'pincode', 'city', 'city_id')


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('uuid', 'data', 'doc1', 'doc2', 'doc3', 'doc4',
                  'doc5', 'doc6',)
        read_only_fields = ('uuid',)


class PolicyAndContractSerializer(serializers.ModelSerializer):

    class Meta:
        model = PolicyAndContract
        fields = (
            'consent_doc',
            'title',
            'short_name',
            'doc_type',
            'file',
            'released_date',
            'version',
        )
