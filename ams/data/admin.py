from django.contrib import admin

from ams.data.models import (
    Address,
    City,
    PolicyAndContract,
    State,
    Transaction
)

admin.site.register(Address)


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    list_display = ('id', 'state_name', 'gst_code')


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'city_name', 'state', 'is_active')
    search_fields = ('city_name', 'state__state_name')


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'data', 'doc1', 'doc2', 'doc3',
                    'doc4', 'doc5', 'doc6',)


@admin.register(PolicyAndContract)
class PolicyAndContractAdmin(admin.ModelAdmin):
    list_display = ('title', 'released_date', 'version')
    readonly_fields = ('consent_doc',)
