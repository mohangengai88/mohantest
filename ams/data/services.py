import logging
import uuid

from glue.service import BaseService
from django.db.models import Q
from django.http.request import QueryDict
from rest_framework.exceptions import ValidationError

from ams.data.exceptions import (InvalidUUIDException,
                                 TransactionDoesNotExistException)
from ams.data.models import City, PolicyAndContract, Transaction
from ams.data.serializers import (
    PolicyAndContractSerializer, TransactionSerializer
)


logger = logging.getLogger(__name__)


class CitySearchService(BaseService):
    """
        Example usage:
        ```
        call(
            'accounts',
            'search_city',
            data = {
                'city': '<Search city>'
            }
        )
        ```
    """
    is_global = True

    def run(self, **kwargs):
        if self.request.query_params:
            city = self.request.query_params.get('city', '')
        else:
            city = self.request.data.get('city', '')

        cities = City.objects.filter(city_name__icontains=city)
        results = []
        for c in cities:
            city_dict = {
                'id': c.id,
                'city': c.city_name
            }
            results.append(city_dict)
        return results


class TransactionDataInputService(BaseService):
    """
        Service to store might be confidential data on behalf of the
        various internal transactional engines.
    """

    def validate(self):
        if not isinstance(self.request.data.get('uuid'), uuid.UUID):
            try:
                uuid.UUID(self.request.data.get('uuid'))
            except ValueError:
                raise InvalidUUIDException
        return True

    def run(self):
        if hasattr(self.request, 'files'):
            self.request.data.update(self.request.files)
        transaction = Transaction.objects.get_or_create(
            uuid=self.request.data.get('uuid', None)
        )[0]
        serializer = TransactionSerializer(
            instance=transaction,
            data=self.request.data,
            partial=True
        )

        serializer.is_valid(raise_exception=True)
        serializer.save()

        return {'detail': 'Transaction data updated successfully.'}


class TransactionDataFetchService(BaseService):
    """
        Service to fetch confidential transactional data stored by
        internal systems.
    """

    def validate(self):
        if not isinstance(self.request.data.get('uuid'), uuid.UUID):
            try:
                uuid.UUID(self.request.data.get('uuid'))
            except ValueError:
                raise InvalidUUIDException
        return True

    def run(self):
        try:
            transaction = Transaction.objects.get_or_create(
                uuid=self.request.data.get('uuid')
            )[0]
            serializer = TransactionSerializer(transaction)
            return serializer.data
        except Transaction.DoesNotExist:
            raise TransactionDoesNotExistException


class PolicyAndContractFetchService(BaseService):
    """
    Service used to get the policy or contract.
    By Default it returns latest version of policy
    based on given `consent_doc` or `title`.
        Example usage:
        ```
        call(
            'accounts',
            'get_contract',
            data={
                'consent_doc': '<consent_doc>, <consent_doc>, ...'
                'version': '', <Optional>
            }
        )
        ```
    """

    def validate(self):
        if not self.request.data.get('consent_doc'):
            return ValidationError('Please provide document identifiers.')

        if isinstance(self.request.data, QueryDict):
            # Setting the QueryDict as mutable otherwise the
            # operations being done below won't be possible.
            self.request.data._mutable = True

        consent_doc = self.request.data.get('consent_doc', [])
        if isinstance(consent_doc, str):
            consent_doc = [s.strip() for s in consent_doc.split(',')]
        elif not isinstance(consent_doc, list):
            raise ValidationError(
                'Please provide the `consent_doc` argument as a `list` like object '
                'or a comma-separated string of Consent IDs.'
            )

        if any(s in ('', ',', '.') for s in consent_doc):
            consent_doc = list(filter(lambda a: a not in ('', ',', '.'), consent_doc))

        try:
            self.request.data.update({
                'contract': PolicyAndContract.objects.filter(
                    Q(short_name__in=consent_doc) |
                    Q(consent_doc__in=consent_doc)
                )
            })
        except PolicyAndContract.DoesNotExist:
            raise ValidationError('No Record Found.')

        return True

    def run(self):
        return PolicyAndContractSerializer(
            self.request.data.get('contract'), many=True
        ).data
