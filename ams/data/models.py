import uuid

from django.contrib.postgres.fields.jsonb import JSONField
from django.db import models
from simple_history.models import HistoricalRecords

from ams.base.models import TimeStampModel
from ams.base.validators.form_validators import file_extension_validator
from ams.utils import timezone


def document_upload(instance, filename):
    """
    Stores the attachment in a 'per private-documents/module-type/yyyy/mm/dd' folder.
    :param instance, filename
    :returns ex: private-documents/User-DrivingLicense/2016/03/30/filename
    """
    today = timezone.get_today_start()
    return 'private-documents/{model}-{type}/{year}/{month}/{day}/{filename}.{extn}'.format(
        model=instance._meta.model_name,
        type=instance.type, year=today.year, month=today.month,
        day=today.day, filename=uuid.uuid4(), extn=filename.split('.')[-1]
    )


def file_upload(instance, filename):
    """
    Stores the attachment in a 'per private-documents/module-type/yyyy/mm/dd' folder.
    :param instance, filename
    :returns ex: private-documents/User-DrivingLicense/2016/03/30/filename
    """
    today = timezone.get_today_start()
    return 'transaction-documents/{model}/{year}/{month}/{day}/{filename}.{extn}'.format(
        model=instance._meta.model_name, year=today.year, month=today.month,
        day=today.day, filename=uuid.uuid4(), extn=filename.split('.')[-1]
    )


def upload_image(instance, image):
    """
    Stores the attachment in a 'per ams-gallery/module-type/yyyy/mm/dd' folder.
    :param instance, filename
    :returns ex: ams-gallery/User-profile/2016/03/30/filename
    """
    today = timezone.get_today_start()
    return 'ams-gallery/{model}/{year}/{month}/{day}/{pk}_{image}'.format(
        model=instance._meta.model_name,
        year=today.year, month=today.month,
        day=today.day, pk=instance.pk, image=image,
    )


class State(models.Model):
    state_name = models.CharField(max_length=128, blank=True, null=True)
    gst_code = models.CharField(max_length=2, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.state_name


class City(models.Model):
    city_name = models.CharField(max_length=128, blank=True, null=True)
    state = models.ForeignKey(State, blank=True, null=True, default=None, on_delete=models.PROTECT)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.city_name


class Address(models.Model):
    address_line1 = models.CharField(max_length=128, blank=True, null=True)
    address_line2 = models.CharField(max_length=128, blank=True, null=True)
    address_line3 = models.CharField(max_length=128, blank=True, null=True)
    city = models.ForeignKey(
        City, blank=True, null=True, default=None, on_delete=models.PROTECT
    )
    pincode = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Addresses'

    def get_full_address(self):
        address = self.address_line1
        if self.address_line2:
            address = '{} {}'.format(address, self.address_line2)
            if self.address_line3:
                address = '{} {}'.format(address, self.address_line3)
        return address


class Transaction(models.Model):
    uuid = models.UUIDField(editable=False)
    data = JSONField(blank=True, null=True)

    doc1 = models.FileField(
        validators=[file_extension_validator], upload_to=file_upload,
        blank=True, null=True, default=None
    )
    doc2 = models.FileField(
        validators=[file_extension_validator], upload_to=file_upload,
        blank=True, null=True, default=None
    )
    doc3 = models.FileField(
        validators=[file_extension_validator], upload_to=file_upload,
        blank=True, null=True, default=None
    )
    doc4 = models.FileField(
        validators=[file_extension_validator], upload_to=file_upload,
        blank=True, null=True, default=None
    )
    doc5 = models.FileField(
        validators=[file_extension_validator], upload_to=file_upload,
        blank=True, null=True, default=None
    )
    doc6 = models.FileField(
        validators=[file_extension_validator], upload_to=file_upload,
        blank=True, null=True, default=None
    )


class DocType:
    FILE = 1
    HTML = 2


class PolicyAndContract(TimeStampModel):
    DOC_TYPE_CHOICES = (
        (DocType.FILE, 'File'),
        (DocType.HTML, 'Html'),
    )
    consent_doc = models.PositiveSmallIntegerField(
        unique=True, blank=True, null=True
    )
    title = models.CharField(max_length=50, blank=True, null=True)
    short_name = models.CharField(max_length=12, blank=True, null=True)
    doc_type = models.PositiveSmallIntegerField(
        choices=DOC_TYPE_CHOICES, blank=True, null=True
    )
    file = models.FileField(
        upload_to=file_upload,
        blank=True, null=True, default=None
    )
    released_date = models.DateField()
    version = models.CharField(max_length=5)

    history = HistoricalRecords(
        history_change_reason_field=models.TextField(null=True)
    )

    def __str__(self):
        return f'{self.short_name}({self.version})'

    def save(self, *args, **kwargs):
        if self.file.name:
            self.doc_type = DocType.HTML if 'html' in self.file.name else DocType.FILE
        else:
            self.doc_type = None

        super().save(*args, **kwargs)
        if not self.consent_doc:
            self.consent_doc = self.pk
        super().save()
