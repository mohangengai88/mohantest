import logging
from pprint import pformat

import requests
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

from ams.utils.tasks import send_communications_task

logger = logging.getLogger(__name__)


def send_communications(event=None,
                        context={},
                        user_id=None,
                        user_type=None,
                        direct_to=[],
                        direct_mobile=[],
                        direct_cc=[],
                        direct_bcc=[],
                        attachments={}):
    """
    This method perform all commmunication related action on any event in async
    """
    if settings.IS_DEV_ENV:
        response = send_communications_sync(
            event=event,
            context=context,
            user_id=user_id,
            user_type=user_type,
            direct_to=direct_to,
            direct_mobile=direct_mobile,
            direct_cc=direct_cc,
            direct_bcc=direct_bcc,
            attachments=attachments)
        return response
    else:
        response = send_communications_task.delay(
            event=event,
            context=context,
            user_id=user_id,
            user_type=user_type,
            direct_to=direct_to,
            direct_mobile=direct_mobile,
            direct_cc=direct_cc,
            direct_bcc=direct_bcc,
            attachments=attachments)
        return response


def send_communications_sync(event=None,
                             context={},
                             user_id=None,
                             user_type=None,
                             direct_to=[],
                             direct_mobile=[],
                             direct_cc=[],
                             direct_bcc=[],
                             attachments={}):
    """
    This method perform all commmunication related action on any event
    """

    if user_type not in ["direct", "partner", "employee", "customer"]:

        raise ImproperlyConfigured(
            "Unknown User Type ::: {}".format(user_type))

        return

    communication_api_url = settings.SPEAR.get("SPEAR_COMMUNICATION_API")
    headers = settings.SPEAR.get("HEADERS")
    data = {
        "event": event,
        "context": context,
        "user_id": user_id,
        "user_type": user_type,
        "direct_to": direct_to,
        "direct_mobile": direct_mobile,
        "direct_cc": direct_cc,
        "direct_bcc": direct_bcc,
        "attachments": attachments
    }

    try:

        logger.info(
            "Sending commmunications to user with user id %s for event '%s', Data ::: \n %s",
            user_id, event, pformat(data))

        response = requests.post(
            communication_api_url, headers=headers, json=data)
        response.raise_for_status()
        return True

    except requests.exceptions.HTTPError as err:
        error_detail = err
        logger.info(
            'Unable to send commmunications to user with user id %s for event :::: %s with Data \n %s, \n Error Details :::: %s',
            user_id, event, pformat(data), err)
        return False

    except requests.exceptions.RequestException as e:
        error_detail = e
        logger.info(
            'Unable to send commmunications to user with user id %s for event :::: %s with Data \n %s, \n Error Details :::: %s',
            user_id, event, pformat(data), error_detail)
        return False

    except Exception as e:
        logger.info(
            'Unable to send commmunications for user with user id %s for event %s with Data \n %s, \n because of error ::: %s',
            user_id, event, pformat(data), e)
        return False
