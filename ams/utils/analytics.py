import base64
import json
import logging
import time
import uuid

import requests
from django.conf import settings

from ams.utils import timezone
from ams.utils.tasks import send_event_task

logger = logging.getLogger(__name__)


def send_event(
    event_name,
    data,
    event_vendor='com.renewbuy.motor',
    event_format='jsonschema',
    event_version='1-0-0'
):
    data.update({
        'event_time': '{}Z'.format(
            timezone.now_local().utcnow().isoformat()[:-3]
        )
    })
    logger.info("[Analytics]Event data for %s is  %s", event_name, data)
    send_event_task.delay(
        event_name,
        data,
        event_vendor,
        event_format,
        event_version
    )


def send_event_sync(
    event_name,
    data,
    event_vendor='com.renewbuy.motor',
    event_format='jsonschema',
    event_version='1-0-0'
):
    headers = {
        'Content-Type': 'application/json',
        'Accept': '*/*'
    }
    snowplow_url = '{}/com.snowplowanalytics.snowplow/tp2'.format(
        settings.ANALYTICS.get('SNOWPLOW_COLLECTOR_URL')
    )

    # SnowPlow API call
    _ = requests.post(
        url=snowplow_url,
        json={
            'schema': 'iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-4',
            'data': [{
                'e': 'ue',
                'ue_px': base64.b64encode(json.dumps({
                    'schema': 'iglu:com.snowplowanalytics.snowplow/unstruct_event/jsonschema/1-0-0',
                    'data': {
                        'schema': 'iglu:' + event_vendor + '/' + event_name + '/' + event_format + '/' + event_version,
                        'data': data
                    }
                }).encode('utf-8')).decode("ascii"),
                'tna': 'rb_backend_tracker',
                'tz': 'Asia/Kolkata',
                'dtm': '{}'.format(int(time.mktime(timezone.now_local().timetuple()) * 1000)),
                'lang': 'en-US',
                'cs': 'UTF-8',
                'p': 'web',
                'tv': '0',
            }]
        },
        headers=headers
    )

    # Amplitude API call
    _ = requests.post(
        'https://api.amplitude.com/2/httpapi',
        json={
            'api_key': settings.ANALYTICS.get('AMPLITUDE_API_KEY'),
            'events': [
                {
                    'user_id': data.get('ams_id', str(uuid.uuid4())),
                    'device_id': str(uuid.uuid4()),
                    'event_type': event_name,
                    'event_properties': data
                }
            ]
        },
        headers=headers
    )
