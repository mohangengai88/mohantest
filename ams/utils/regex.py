import re

KYC_DETAILS_REGEX_LIST = [
    re.compile(r'[a-z]{5}\d{4}[a-z]{1}', flags=re.I),  # PAN Regex
    re.compile(r'\d{12}'),  # Aadhaar Regex
    re.compile(r'^[789]\d{9}$'),  # Indian Mobile Number Regex
    re.compile(r'^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$', flags=re.I)  # Email address Regex
]
