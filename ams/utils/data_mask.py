def hash_field(value):
    """
    Takes the value as an argument, returns the hexdigest of the calculated hash
    """
    import hashlib
    import hmac

    from django.conf import settings

    key = settings.MAC_KEY

    if isinstance(value, str):
        value = value.encode('utf-8')
    if isinstance(key, str):
        key = key.encode('utf-8')

    return hmac.new(
        key=key,
        msg=value,
        digestmod=hashlib.sha3_512
    ).hexdigest()


def encrypt_email(email):
    try:
        if email:
            username, domain = email.split('@')
            username = username[:6] + '**'
            domain = domain[:2] + '**' + domain[-3:]
            return '{}@{}'.format(username, domain)
    except Exception as e:
        print(str(e))
        return email


def encrypt_mobile(mobile):
    try:
        if mobile:
            mobile_num = mobile
            mobile_num = mobile_num[:2] + '*' * (len(mobile_num) - 5) + mobile_num[7:]
            return '{}'.format(mobile_num)
    except Exception as e:
        print(str(e))
        return mobile


def encrypt_aadhaar(aadhar):
    try:
        if aadhar:
            aadhaar_num = aadhar
            aadhaar_num = aadhaar_num[:2] + '*' * (len(aadhaar_num) - 4) + aadhaar_num[10:]
            return '{}'.format(aadhaar_num)
    except Exception as e:
        print(str(e))
        return aadhar


def encrypt_pan(pan):
    try:
        if pan:
            pan_num = pan
            pan_num = pan_num[:2] + '*' * (len(pan_num) - 4) + pan_num[8:]
            return '{}'.format(pan_num)
    except Exception as e:
        print(str(e))
        return pan


def encrypt_gstin(gstin):
    try:
        if gstin:
            gstin_num = gstin
            gstin_num = gstin_num[:2] + '*' * (len(gstin_num) - 7) + gstin_num[10:]
            return '{}'.format(gstin_num)
    except Exception as e:
        print(str(e))
        return gstin


def encrypt_bank_acc(bank_acc):
    try:
        if bank_acc:
            bank_acc_num = bank_acc
            bank_acc_num = '*' * (len(bank_acc_num) - 4) + bank_acc_num[-4:]
            return '{}'.format(bank_acc_num)
    except Exception as e:
        print(str(e))
        return bank_acc


def encrypt_ifsc(ifsc):
    try:
        if ifsc:
            ifsc_code = ifsc
            ifsc_code = ifsc_code[:2] + '*' * (len(ifsc_code) - 4) + ifsc_code[-2:]
            return '{}'.format(ifsc_code)
    except Exception as e:
        print(str(e))
        return ifsc


def encrypt_dl(dl):
    try:
        if dl:
            dl_num = dl
            dl_num = dl_num[:2] + '*' * (len(dl_num) - 4) + dl_num[13:]
            return '{}'.format(dl_num)
    except Exception as e:
        print(str(e))
        return dl


def encrypt_edu_cert(edu_cert):
    try:
        if edu_cert:
            edu_cert_num = edu_cert
            edu_cert_num = edu_cert_num[:2] + '*' * (len(edu_cert_num) - 4) + edu_cert_num[10:]
            return '{}'.format(edu_cert_num)
    except Exception as e:
        print(str(e))
        return edu_cert


def encrypt_passport(passport):
    try:
        if passport:
            passport_num = passport
            passport_num = passport_num[:2] + '*' * (len(passport_num) - 3) + passport_num[5:]
            return '{}'.format(passport_num)
    except Exception as e:
        print(str(e))
        return passport


def get_client_browser_ip(request):
    """
    Get The Customer IP where Payment is going to Happen
    """
    try:
        client_address = request.META['HTTP_X_REAL_IP']
        if client_address:
            return client_address
    except Exception:
        client_address = request.META['REMOTE_ADDR']
        if client_address:
            return client_address


def generate_hash(*args):
    import hashlib
    checksum = hashlib.sha256('|'.join(args).encode('utf-8'))
    return checksum.hexdigest().upper()
