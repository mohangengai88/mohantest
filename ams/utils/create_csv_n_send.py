"""
Module to create temp file and send.
Accepted data format:
    list of list: [['John', 20], ['Smith', 80]]
    list of dict: [{'a': 100}, {'a': 90}]
if list of list is provided then headers is must.

"""

import csv
import os
import tempfile

from django.core.files import File

from ams.utils import email, timezone


def create_and_send(
    title, data, headers=[], to=[], body='', cc=[]
):
    if not data:
        print('No data to send')
        return
    try:
        filename = create_temp_file(title, data, headers)
        status = send_report(body, title, to, cc=cc, attachments=[filename])
        print('Email Sent Status: %s' % status)
    except Exception as e:
        print(e)


def create_temp_file(title, content, headers=[]):
    file_path = os.path.join(tempfile.gettempdir(), title)
    csv_file = tempfile.NamedTemporaryFile(
        delete=False, prefix=file_path,
        suffix='.csv', mode='r+'
    )
    if isinstance(content[0], dict):
        writer = csv.DictWriter(
            csv_file,
            lineterminator='\n',
            quoting=csv.QUOTE_MINIMAL,
            fieldnames=content[0].keys()
        )
        writer.writeheader()
        writer.writerows(content)
    elif isinstance(content[0], list):
        writer = csv.writer(
            csv_file,
            lineterminator='\n',
            quoting=csv.QUOTE_MINIMAL
        )
        content = [headers] + content
        writer.writerows(content)

    return csv_file.name


def send_report(body, subject, to=[], cc=[], attachments=[]):
    """
    Send the status report to the mapping requester.
    """
    if not attachments:
        return
    email_status = email.send(
        to,
        f'{subject} - {timezone.now_local(only_date=True)}',
        html_body=body,
        attachments=list(map(lambda x: File(open(x)), attachments)),
        cc=cc
    )
    return email_status
