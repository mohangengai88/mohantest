from django.core.files.base import File
from django.core.files.temp import NamedTemporaryFile
from django.db.models import Q
from django.http import HttpResponse
from django.template.loader import get_template
from html import escape
from io import BytesIO
from xhtml2pdf import pisa

from ams.utils.data_mask import hash_field
from ams.accounts.managers import UserManager


def html_to_pdf_convert(template, context):
    html = template.render(context)
    policy_document_file = NamedTemporaryFile(delete=False)

    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), policy_document_file)
    if not pdf.err:
        return File(policy_document_file)
    return False


def convert_html_to_pdf(template, context, document_owner, filename):
    html = template.render(context)
    from ams.accounts.models import User
    if not isinstance(document_owner, User):
        try:
            document_owner = User.objects.get(
                Q(unique_code=document_owner) |
                Q(hashed_email=hash_field(
                    UserManager.normalize_email(document_owner)
                )) |
                Q(pos_code=document_owner) |
                Q(referral_code=document_owner)
            )
            user = document_owner.pos_code if document_owner.pos_code else document_owner.unique_code
        except Exception:
            user = document_owner

    f = NamedTemporaryFile()
    f.name = '/' + user + '/' + str(filename)
    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), f)

    if not pdf.err:
        return File(f)
    return False


def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()

    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))
