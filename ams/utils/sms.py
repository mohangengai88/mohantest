import json
import logging
import re
import requests
from django.conf import settings

from ams.utils.tasks import (send_otp_notification,
                             send_otp_value_notification,
                             send_sms_notification)

logger = logging.getLogger(__name__)


def smart_send(func):
    def inner(number, message):
        logger.info('Number (%s): Trying to send SMS `%s`', number, message)
        if not message:
            logger.warning('We are sending the SMS with out text to Mobile Number')
            return
        try:
            int(number)
        except ValueError:
            logger.warning('Invalid Mobile Number `%s`', number)
            return
        except TypeError:
            logger.warning('Invalid Mobile Number `%s`', number)
            return

        if len(str(number)) != 10:
            logger.warning('Invalid Mobile Number `%s`', number)
            return
        func(number, message)
    return inner


@smart_send
def send_otp(number, message):
    """
    Send sms function accepting number and message
    :param number: Mobile number
    :param message: Message text
    """
    send_otp_notification.delay(number, message)


@smart_send
def send_otp_value(number, message):
    """
    Send sms function accepting number and message
    :param number: Mobile number
    :param message: Message text
    """
    send_otp_value_notification.delay(number, message)


def send_otp_sync(number, message):
    source_name = 'OTPINFINISOURCE'
    if not settings.SMS_API_ENABLED:
        logger.info('SMS API DISABLED: %s - %s', number, message)
        return True

    source = settings.SMS_CONF.get(source_name)
    url = '{URL}&method=sms&message={SMSBODY}&to={MOBILE}&sender={SENDERID}'.format(
        URL=source.get('URL').format(source.get('OTPKEY')),
        SMSBODY=message,
        MOBILE='91' + number,
        SENDERID=source.get('SENDERID')
    )

    try:
        logger.info('Number (%s): Sending SMS `%s`', number, message)
        response = requests.post(url)
        if not response.status_code == 200:
            raise Exception
    except Exception:
        logger.exception('Unable to connect to the Solutions Infini site')
        return False

    data = json.loads(response.text)
    if data.get('status') == 'OK':
        transid = data.get('data')[0].get('id')
        logger.info('Transaction ID: %s', transid)
        return transid

    logger.warning('Unable to send sms.')
    logger.warning(response.text)
    return False


def send_otp_digi_sync(number, message):
    """
    Send sms function accepting number and message
    :param number: Mobile number
    :param message: Message text
    """
    source_name = 'OTPSOURCE'
    if not settings.SMS_API_ENABLED:
        logger.info('SMS API DISABLED: %s - %s', number, message)
        return True

    source = settings.SMS_CONF.get(source_name)
    url = '{URL}UserId={USERNAME}&pwd={PASSWORD}&Message={SMSBODY}&Contacts={MOBILE}&SenderId={SENDERID}&StartTime='.format(
        URL=source.get('URL'),
        USERNAME=source.get('USERNAME'),
        PASSWORD=source.get('PASSWORD'),
        SMSBODY=message,
        MOBILE='91' + number,
        SENDERID=source.get('SENDERID')
    )

    try:
        logger.info('Number (%s): Sending OTP SMS `%s`', number, message)
        response = requests.get(url)
        logger.debug('Response: %s', response.text)
        m = re.search(r'TransId\s*:\s*(?P<TRANS_ID>[-0-9a-z]+)', response.text, flags=re.I)

        if m:
            transid = m.group('TRANS_ID')
            logger.info('Transaction ID: %s', transid)
            return True
        logger.warning('Unable to send OTP.')
        logger.warning(response.text)
        return False
    except Exception:
        logger.exception('Unable to connect the Digialaya site')
        return False


def send_otp_value_sync(number, message):
    """
    Send sms function accepting number and message
    :param number: Mobile number
    :param message: Message text
    http://api2.myvaluefirst.com/psms/servlet/psms.Eservice2?data=<?xml version='1.0' encoding='ISO-8859-1'?>
    <!DOCTYPE MESSAGE SYSTEM 'http://127.0.0.1:80/psms/dtd/messagev12.dtd'><MESSAGE VER='1.2'>
    <USER USERNAME='d2cxml' PASSWORD='d2ccspl123'/><SMS  UDH='0' CODING='1' TEXT='test' PROPERTY='0' ID='1'><ADDRESS FROM='RNWBUY' TO='91XXXXXXXXXX' SEQ='1' TAG='some clientside random data'/></SMS></MESSAGE>&action=send
    """
    source_name = 'OTPVALUESOURCE'
    if not settings.SMS_API_ENABLED:
        logger.info('SMS API DISABLED: %s - %s', number, message)
        return True

    source = settings.SMS_CONF.get(source_name).get('URL')
    url = source.format(
        SMSBODY=message,
        MOBILE=number
    )
    logger.info(url)
    try:
        logger.info('Number (%s): Sending SMS through ValueFirst `%s`', number, message)
        response = requests.get(url)
        logger.info(response.text)
        return True
    except Exception:
        logger.exception('Unable to connect the Value First site')
        return False


@smart_send
def send(number, message):
    """
    Send sms function accepting number and message
    :param number: Mobile number
    :param message: Message text
    """
    send_sms_notification.delay(number, message)


def send_sync(number, message):
    source_name = 'OTPINFINISOURCE'
    if not settings.SMS_API_ENABLED:
        logger.info('SMS API DISABLED: %s - %s', number, message)
        return True

    source = settings.SMS_CONF.get(source_name)
    url = '{URL}&method=sms&message={SMSBODY}&to={MOBILE}&sender={SENDERID}'.format(
        URL=source.get('URL').format(source.get('KEY')),
        SMSBODY=message,
        MOBILE='91' + number,
        SENDERID=source.get('SENDERID')
    )

    try:
        logger.info('Number (%s): Sending SMS `%s`', number, message)
        response = requests.post(url)
        if not response.status_code == 200:
            raise Exception
    except Exception:
        logger.exception('Unable to connect to the Solutions Infini site')
        return False

    data = json.loads(response.text)
    if data.get('status') == 'OK':
        transid = data.get('data')[0].get('id')
        logger.info('Transaction ID: %s', transid)
        return transid

    logger.warning('Unable to send sms.')
    logger.warning(response.text)
    return False


def send_digi_sync(number, message):
    source_name = 'OTPSOURCE'
    if not settings.SMS_API_ENABLED:
        logger.info('SMS API DISABLED: %s - %s', number, message)
        return True

    source = settings.SMS_CONF.get(source_name)
    url = '{URL}UserId={USERNAME}&pwd={PASSWORD}&Message={SMSBODY}&Contacts={MOBILE}&SenderId={SENDERID}&StartTime='.format(
        URL=source.get('URL'),
        USERNAME=source.get('USERNAME'),
        PASSWORD=source.get('PASSWORD'),
        SMSBODY=message,
        MOBILE='91' + number,
        SENDERID=source.get('SENDERID')
    )

    try:
        logger.info('Number (%s): Sending SMS `%s`', number, message)
        response = requests.get(url)
    except Exception:
        logger.exception('Unable to connect the Digialaya site')
        return False

    m = re.search(r'TransId\s*:\s*(?P<TRANS_ID>[-0-9a-z]+)', response.text, flags=re.I)

    if m:
        transid = m.group('TRANS_ID')
        logger.info('Transaction ID: %s', transid)
        return transid
    logger.warning('Unable to send sms.')
    logger.warning(response.text)
    return False
