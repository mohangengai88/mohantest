import re


def get_first_name(email):
    """
    :param email=foo@example.com:
    :return foo:
    """
    regexStr = r'([a-zA-Z]+)'
    matchobj = re.search(regexStr, email)
    if matchobj:
        return matchobj.group(1)
