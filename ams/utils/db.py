from django.db.models import Func


class ExtractDaysFromPGInterval(Func):
    function = 'EXTRACT'
    template = "%(function)s(day FROM %(expressions)s)"
