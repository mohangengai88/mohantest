import random
import string
import sys
from collections import Iterable

from django.http import HttpResponse
from django.utils.encoding import iri_to_uri


class HttpResponseReload(HttpResponse):
    """
    Copied from an external library called django-annoying.
    Reload page and stay on the same page from where request was made.
    example:
    def simple_view(request):
        if request.POST:
            form = CommentForm(request.POST):
            if form.is_valid():
                form.save()
                return HttpResponseReload(request)
        else:
            form = CommentForm()
        return render_to_response('some_template.html', {'form': form})
    """
    status_code = 302

    def __init__(self, request):
        HttpResponse.__init__(self)
        referer = request.META.get('HTTP_REFERER')
        self['Location'] = iri_to_uri(referer or '/')


def numToWords(num, join=True):
    """words = {} convert an integer number into words"""
    units = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
    teens = ['', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen',
             'seventeen', 'eighteen', 'nineteen']
    tens = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
            'eighty', 'ninety']
    thousands = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion',
                 'quintillion', 'sextillion', 'septillion', 'octillion',
                 'nonillion', 'decillion', 'undecillion', 'duodecillion',
                 'tredecillion', 'quattuordecillion', 'sexdecillion',
                 'septendecillion', 'octodecillion', 'novemdecillion',
                 'vigintillion']
    words = []
    if num == 0:
        words.append('zero')
    else:
        numStr = '%d' % num
        numStrLen = len(numStr)
        groups = int((numStrLen + 2) / 3)
        numStr = numStr.zfill(groups * 3)
        for i in range(0, groups * 3, 3):
            h, t, u = int(numStr[i]), int(numStr[i + 1]), int(numStr[i + 2])
            g = int(groups - (i / 3 + 1))
            if h >= 1:
                words.append(units[h])
                words.append('hundred')
            if t > 1:
                words.append(tens[t])
                if u >= 1:
                    words.append(units[u])
            elif t == 1:
                if u >= 1:
                    words.append(teens[u])
                else:
                    words.append(tens[t])
            else:
                if u >= 1:
                    words.append(units[u])
            if (g >= 1) and ((h + t + u) > 0):
                words.append(thousands[g] + ',')
    if join:
        return ' '.join(words)
    return words


def try_or(fn, default):
    """
    Jugaad for a one liner try:except block.
    Usage: try_or(lambda: request_user.email, None)
    """
    try:
        return fn()
    except Exception:
        return default


def split_address(address):
    address_data = []
    if len(address) < 30:
        return [address, '', '']
    i = 1
    start_point = 0
    n_step = 30
    address_data = ['', '', '']
    while len(address) > 0 and i < 5:  # first while loop code
        if len(address) > 30:
            is_comma_avail = address.rfind(',', i, n_step)
            if is_comma_avail != -1:
                index_comma = address.rfind(',', i, n_step)
                address_data[i - 1] = (address[start_point:index_comma])
                address = address[index_comma + 1:]
                i = i + 1
                # start_point = index_comma
            elif address.rfind(' ', i, n_step * i) != -1:
                index_comma = address.rfind(' ', i, n_step)
                # address_data.append(address[start_point:index_comma])
                address_data[i - 1] = address[start_point:index_comma]
                address = address[index_comma + 1:]
                i = i + 1
            else:
                address_data[i - 1] = address[start_point:n_step]
                address = address[n_step:]
                i = i + 1
        else:
            address_data[i - 1] = address
            break
    return address_data


def clean_empty(d):
    """
    Recursive function to remove all the key value pairs where the value is None.
    """
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (clean_empty(v) for v in d) if v]
    return {k: v for k, v in ((k, clean_empty(v)) for k, v in d.items()) if v not in (None, '', [], {})}


def import_class(path):
    """
    Imports a class based on a full Python path ('pkg.pkg.mod.Class'). This
    does not trap any exceptions if the path is not valid.
    """
    module, name = path.rsplit('.', 1)
    __import__(module)
    mod = sys.modules[module]
    cls = getattr(mod, name)

    return cls


def random_password():
    """Generate a random password """
    random_source = string.ascii_letters + string.digits + string.punctuation
    password = random.choice(string.ascii_lowercase)
    password += random.choice(string.ascii_uppercase)
    password += random.choice(string.digits)
    password += random.choice(string.punctuation)
    password += '@'
    for i in range(5):
        password += random.choice(random_source)
    password_list = list(password)
    random.SystemRandom().shuffle(password_list)
    password = ''.join(password_list)
    return password


def flatten(items):
    """
    Yield items from any nested iterable.
    """
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            yield from flatten(x)
        else:
            yield x
