"""
Custom S3 storage backends to store files in subfolders.
"""
from storages.backends.s3boto3 import S3Boto3Storage


def MediaRootS3Boto3Storage():
    return S3Boto3Storage(location='media')
