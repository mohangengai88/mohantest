import logging

from celery import shared_task
from celery.exceptions import MaxRetriesExceededError, Retry
from django.template.loader import get_template

from ams.utils import pdf_docs

logger = logging.getLogger(__name__)


@shared_task(bind=True, max_retries=3, rate_limit='240/m')
def send_otp_notification(self, number, message):
    try:
        from ams.utils.sms import send_otp_sync

        if send_otp_sync(number, message) is False:
            raise self.retry(countdown=10 * (self.request.retries + 1))
    except Exception:
        raise self.retry(countdown=10 * self.request.retries + 1)


@shared_task(bind=True, max_retries=3, rate_limit='240/m')
def send_otp_value_notification(self, number, message):
    try:
        from ams.utils.sms import send_otp_value_sync

        if send_otp_value_sync(number, message) is False:
            raise self.retry(countdown=10 * (self.request.retries + 1))
    except Exception:
        raise self.retry(countdown=10 * (self.request.retries + 1))


@shared_task(bind=True, max_retries=3, rate_limit='120/m', ignore_result=True)
def send_sms_notification(self, number, message):
    try:
        from ams.utils.sms import send_sync

        if send_sync(number, message) is False:
            raise self.retry(countdown=10 * (self.request.retries + 1))
    except Exception:
        raise self.retry(countdown=10 * (self.request.retries + 1))


@shared_task(bind=True)
def send_event_task(
    self,
    event_name,
    data,
    event_vendor='com.renewbuy.motor',
    event_format='jsonschema',
    event_version='1-0-0',
):
    try:
        from ams.utils.analytics import send_event_sync

        send_event_sync(event_name, data, event_vendor, event_format, event_version)
    except Exception as e:
        logger.exception(
            '[Analytics]Unable to send tracker events: %s for event name: %s',
            e,
            event_name,
        )


@shared_task(bind=True, max_retries=3, rate_limit='120/m', ignore_result=True)
def send_email_from_template_task(self, to, subject, template, context={}, *kwargs):
    try:
        from ams.utils.email import send_from_template

        if send_from_template(to, subject, template, context, *kwargs) is False:
            raise self.retry(countdown=10 * (self.request.retries + 1))
    except Exception:
        raise self.retry(countdown=10 * (self.request.retries + 1))


@shared_task(bind=True, max_retries=5, ignore_result=True)
def send_communications_task(
    self,
    event,
    context,
    user_id,
    user_type,
    direct_to,
    direct_mobile,
    direct_cc,
    direct_bcc,
    attachments,
):
    try:
        from ams.utils.spear import send_communications_sync

        if (
            send_communications_sync(
                event,
                context,
                user_id,
                user_type,
                direct_to,
                direct_mobile,
                direct_cc,
                direct_bcc,
                attachments,
            )
            is False
        ):

            retry_time = 120 * (self.request.retries + 1)
            logger.info(
                "\n Retry Countdown ::: %s, retrying after ::: %s seconds \n",
                (self.request.retries + 1),
                retry_time,
            )
            raise self.retry(countdown=retry_time)
        else:
            return True
    except Retry:
        pass
    except MaxRetriesExceededError:
        logger.info("\n Max Tries Reached ::: Sending backup communications")
        return False
    except Exception as e:
        logger.exception(
            "Couldn't able to send communications because of exception ::: {}".format(e)
        )


@shared_task(bind=True, max_retries=3, rate_limit='120/m', ignore_result=True)
def create_pdf_and_send(
    self,
    to,
    subject,
    email_template,
    email_context,
    pdf_tempalte,
    pdf_context={},
    consent_details=[],
    filename='',
    send_from_spear=False,
):
    """
    Task used to create pdf and send email with template and pdf as attachment.
    """
    from ams.accounts.models import UserConsent

    try:
        from django.contrib.auth import get_user_model

        user = get_user_model().objects.get(unique_code=to)
        for consent_dict in consent_details:
            file = pdf_docs.convert_html_to_pdf(
                get_template(pdf_tempalte),
                context=pdf_context,
                document_owner=to,
                filename=filename or 'POSP T&C.pdf',
            )
            consent = UserConsent.objects.get(
                user=user,
                consent_doc__consent_doc=consent_dict.get('consent_doc')
            )
            consent.file = file
            consent.save()

        email_status = False
        data_email_dict = {
            'user_name': user.get_full_name()
        }

        # send via spear
        if send_from_spear:
            event = 'EV_posp_agreement'
            try:
                from ams.utils.spear import send_communications

                email_status = send_communications(
                    event=event,
                    context=data_email_dict,
                    user_type="direct",
                    direct_to=user.decrypted_email,
                    attachments={
                        'aggreement_copy': UserConsent.objects.get(
                            user=user,
                            consent_doc__consent_doc=consent_details[0].get(
                                'consent_doc'
                            ),
                        ).file.url
                    },
                )
            except Exception:
                logger.exception(
                    'Unable to send agreement email via spear for user %s',
                    user.unique_code,
                )
                email_status = False

        # if failed aur email not send, then send with ams email core module.
        if email_status is False:
            from ams.utils.email import send_from_template
            pdf_file = pdf_docs.convert_html_to_pdf(
                get_template(pdf_tempalte),
                context=pdf_context,
                document_owner=to,
                filename=filename or 'POSP T&C.pdf',
            )
            logger.info(
                'Sending agreement email via core ams module for user %s',
                user.unique_code,
            )
            if not send_from_template(
                to=user.decrypted_email,
                subject=subject,
                template=email_template,
                context=email_context,
                attachments=[pdf_file]
            ):
                raise self.retry(countdown=10 * (self.request.retries + 1))
    except Exception as e:
        print(e)
        raise self.retry(countdown=10 * (self.request.retries + 1))
