from django.conf import settings


def allowed_user_context_processor(request):
    return {
        "bulk_mapping_allowed_user": settings.BULK_MAPPING_VIEW_ALLOWED_USERS
    }
