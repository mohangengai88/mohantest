from datetime import timedelta

from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def read_setting(name, default=''):
    """
    :param name: setting name in dot separated format e.g. read_setting INSURERS.RELIANCE.PAYMENT_URL
    :return Value of the config defined in settings
    """
    setting_parts = name.split('.')

    if not hasattr(settings, setting_parts[0]):
        return default

    setting_val = getattr(settings, setting_parts[0])
    if len(setting_parts) > 1:
        for setting_part in setting_parts[1:]:
            if setting_part not in setting_val:
                return default
            setting_val = setting_val[setting_part]
    if not setting_val:
        return default
    return setting_val


@register.filter
def add_one_day(value):
    return value + timedelta(days=1)


@register.simple_tag
def call_method(obj, method_name, *args):
    method = getattr(obj, method_name)
    return method(*args)
