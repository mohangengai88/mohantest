from django.conf import settings
from simple_history.models import HistoricalRecords


def add_history_ip_address(sender, **kwargs):
    history_instance = kwargs['history_instance']

    # thread.request for use only when the simple_history middleware is on and enabled
    if settings.IS_DEV_ENV:
        history_instance.ip_address = HistoricalRecords.thread.request.META.get('HTTP_X_REAL_IP')
    else:
        history_instance.ip_address = HistoricalRecords.thread.request.META.get(
            'HTTP_X_FORWARDED_FOR',
            HistoricalRecords.thread.request.META.get('HTTP_X_REAL_IP')
        )


def update_history_user(instance, history_user):
    attrs = {}
    model = type(instance)
    manager = instance if instance.id is not None else model
    for field in instance._meta.fields:
        value = getattr(instance, field.attname)
        if field.primary_key is True:
            if value is not None:
                attrs[field.attname] = value
        else:
            attrs[field.attname] = value
    record = manager.history.filter(**attrs).order_by("-history_date").first()
    record.history_user = history_user
    record.save()
