var user_token = ""
var profile_data;
var indexid = 1;

var documentType = {
    1: "pan_card_image",
    2: "aadhaar_card_image",
    3: "gst_cert_image",
    4: "PHOTO",
    5: "cancelled_cheque_image",
    6: "drivers_license_image",
    7: "education_cert_image",
    8: "passport_image"
};
var docVerificationStatus = {
    1: "PENDING",
    2: "VERIFIED",
    3: "DISCARDED"
}
function render(form_fields) {
    var newclass = '';
    var template = "";
    var field = form_fields.fields
    for (var key in field) {
        indexid++;
        var validations = "";
        if (field[key].required) {
            validations += " required"
        }
        if (field[key].maxlength) {
            validations += " maxlength=" + field[key].maxlength
        }
        if (field[key].minlength) {
            validations += " minlength=" + field[key].minlength
        }
        if (field[key].pattern) {
            validations += " pattern=" + field[key].pattern
        }
        // if(field[key].pattern_suggestion){
        //     validations += " data-suggestion="+field[key].pattern_suggestion
        // }
        if (field[key].disable) {
            validations += " disable=" + field[key].disable
        }
        if (field[key].dependency_field) {
            validations += " dependency_field=" + field[key].dependency_field
        }
        if (field[key].type == "select") {
            var this_option = ""
            field[key].options.forEach(element => {
                if (element.value == field[key].value) {
                    this_option += '<option value="' + element.value + '" selected >'
                } else {
                    if (element.value == 0) {
                        this_option += '<option disabled="disabled" value="' + element.value + '" >'
                    } else {
                        this_option += '<option value="' + element.value + '" >'
                    }
                }
                this_option += element.name + '</option>'
            });
            template += "<!--  input field -->"
                + '<div class="' + field[key].class + ' display_only input_container">'
                + '<div class="fromfieldwrapper">'
                + '<div class="formWrap">'
                + '<span class="icons">' + field[key].icon + '</span>'
                + '<div class="materialfiled">'
                + '<select disabled ' + validations + ' name="' + field[key].name + '">'
                + this_option
                + '</select>'
                + '<label for="' + field[key].name + '" class="label">' + field[key].label + '</label>'
                // +           '<hr/>'
                // +           '<label class="focused" for="'+field[key].label+'">'+field[key].label+'</label>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<!-- input field end -->'

        } else if (field[key].type == "file") {
            template += "<!--  input field -->"
                + '<div class="' + field[key].class + ' display_only file_container input_container">'
                + '<div class="fromfieldwrapper ' + field[key].status + '">'
                + '<div class="formWrap">'
                + '<span class="icons">' + field[key].icon + '</span>'
                + '<div class="materialfiled">'
                + '<div class="imageWrap">'
                + '<div class="text1">Upload your ' + field[key].placeholder + '<span style="color:#0880ea;">(max 5MB)</span></div>'
                + '<div class="text2">You can upload image in jpeg & png format</div>'
                + '</div>'
                + '<input type="file" status="' + field[key].status + '" disabled ' + validations + ' name="' + field[key].name + '" placeholder="' + field[key].placeholder + '" value="' + field[key].value + '" />'
                + '<img class="imagename hide" style="height:150px; width:150px; margin-bottom:10px;" class="' + field[key].name + '" src="' + dummyImage + '" %}" />'
                + '<div class="imagename hide">Updated ' + field[key].placeholder + '</div>'
                + '<div class="status">' + field[key].status + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<!-- input field end -->'
        } else if (field[key].type == "radio") {
            var this_radioBox = ""
            field[key].options.forEach(element => { 
                var is_checked = field[key].intial_value == element.value?"checked":""
                this_radioBox += '<div class="col-md-2"> <input type="radio"  disabled ' + validations + ' name="' + element.name  +  '" value="' + element.value  + '"  id="' + element.id + '" '+ is_checked +'/>' +  element.label +'</div>' 
            });
            template += "<!--  input field -->"
                + '<div class="' + field[key].class + ' display_only input_container">'
                + '<div class="fromfieldwrapper label_wrap">'
                + '<div class="formWrap">'
                + '<fieldset class="materialfiled radio_btn_wrap">'
                + '<div class="row">'
                + '<label for="' + field[key].name + '" class="label col-md-4">' + field[key].label + '</label>'
                + this_radioBox
                + '</div>'
                + '</fieldset>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<!-- input field end -->'
         }
        else {
            template += "<!--  input field -->"
                + '<div class="' + field[key].class + ' display_only input_container">'
                + '<div class="fromfieldwrapper">'
                + '<div class="formWrap">'
                + '<span class="icons">' + field[key].icon + '</span>'
                + '<fieldset class="materialfiled">'
                + '<input type="text" disabled ' + validations + ' name="' + field[key].name + '" placeholder="' + field[key].placeholder + '" value="' + field[key].value + '" />'
                + '<label for="' + field[key].name + '" class="label">' + field[key].label + '</label>'
                + '</fieldset>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<!-- input field end -->'
        }
    }

    var template_conatainer = ''
        + '<div class="col-md-12 section_container readonly">'
        + '    <div class="sub_header" style="cursor: pointer;">'
        + '      <div class="titleLeft">                      '
        + '        <div class="title">                        '
        + '           <span class="headicon" style="color:#a5a3a3;font-size: 28px;">' + form_fields.icon + '</span>     '
        + '           <span class="headfiled">' + form_fields.header + '</span>   '
        + '        </div>';
    if (form_fields.editable) {
        template_conatainer +=
            '        <div class="edit_container hide">'
            + '            <div class="edit edit_button">'
            + '                <span class="editIcon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span> <span style="color: #0a70bb;">Edit</span>'
            + '            </div>'
            + '        </div>'
            + '     </div>      '
            + '     <div class="titleRight"><spna class="plusminusIcon collapsed" data-toggle="collapse" href="#collapse' + indexid + '"><i class="fa fa-plus" aria-hidden="true"></i></span></div>'
    }
    template_conatainer +=
        '    </div>'
        + '    <div class="form_conatiner panel-collapse collapse" style="paddind:0px 10px;" id="collapse' + indexid + '">'
        + '        <form enctype="multipart/form-data">'
        + '            <div class="row" id="profile_doc">'
        + template
        + '            </div>'
        + '            <div class="editwrap hide" style="text-align: right;">'
        + '                  <div class="edit cancel">'
        + '                      Cancel'
        + '                  </div>'
        + '                  <div class="edit save">'
        + '                      Save'
        + '                   </div>'
        + '            </div>                '
        + '        </form>'
        + '    </div>'
        + '</div>'

    return template_conatainer
}

$(document).on('change', ".martitalStatus", function (e) {
    if (e.target.value == 1) {
        $('.anniversaryDate').addClass('hide');
    } else {
        $('.anniversaryDate').removeClass('hide');
    }
});

$(document).on('click', ".plusminusIcon", function () {
    var $edit = $(this).parents('.section_container').find('.edit_container');
    $($edit).toggleClass("hide");
});
$(document).on('click', ".edit_button", function () {
    parent = $(this).parents(".section_container").eq(0)
    parent.removeClass('readonly').addClass('editmode');
    parent.find('.editwrap').removeClass('hide')
    //$('.editwrap').removeClass('hide')
    parent.find(".input_container").each(function () {
        var attrName = $(this).find('input , select').attr('disable');
        if ($(this).find('input').val() != '' && $(this).find('select').val() != 0) {
            $(this).addClass("filledFiled");
        } else {
            $(this).removeClass("filledFiled");
        }
        if (attrName == "true") {
            $(this).find('input, select').attr("disabled", true)
        } else {
            $(this).find('input, select').attr("disabled", false)
            $(this).removeClass("display_only");
        }
    })
})
$(document).on('click', ".cancel", function () {
    render_form(profile_data);

    parent = $(this).parents(".section_container").eq(0)
    parent.addClass('readonly').removeClass('editmode')
    parent.find(".input_container").each(function () {
        $(this).addClass("display_only")
        $(this).find('input, select').attr("disabled", true)
    })
})


$(document).on('click', ".save", function (e) {

    e.preventDefault();
    form = $(this).parents(".section_container").find("form").eq(0)
    if (validateForm(form[0])) {
        return false
    }
    $(".loader").show()
    parent = $(this).parents(".section_container").eq(0);
    $(form[0]).find(".input_container").each(function () {

        var inputElement = $(this).find('input[type="text"]');
        var inputValue = $(inputElement).val();

        if (inputValue) {
            inputValue = escapeHtml(inputValue);
            $(inputElement).val(inputValue);
        }

    });
    formdata = new FormData(form[0]);
    //formdata.append('photo', $('#profileImage').prop('files')[0]);
    var whatsApp = $(this).parents(".form_conatiner").find(".input_container").find('input[name="whatsapp_status"]:checked').val();
    if (whatsApp) {
        whatsAppprofileUpdate(whatsApp,true);
    }
    else {
        profileUpdate(formdata, true);
    }


});

function escapeHtml(inputNodeValue) {
    inputNodeValue = inputNodeValue.replace(/&/g, "&amp;");
    inputNodeValue = inputNodeValue.replace(/</g, "&lt;");
    inputNodeValue = inputNodeValue.replace(/>/g, "&gt;");
    inputNodeValue = inputNodeValue.replace(/"/g, "&quot;");
    inputNodeValue = inputNodeValue.replace(/'/g, "&#039;");
    return inputNodeValue;
}

$(document).ready(function () {
    sdk()
    //action();
});
function profileUpdate(formdata, is_true) {
    $.ajax({
        type: "POST",
        processData: false,
        contentType: false,
        mimeType: "multipart/form-data",
        data: formdata,
        url: '/api/v1/update_profile/',
        headers: {
            "Api-key": CURRENT_ENV_API_KEY,
            "secret-key": CURRENT_ENV_SECRET_KEY,
            "Authorization": "Token " + user_token
        },
        success: function (result) {
            if (is_true) {
                profile_data = JSON.parse(result)
                render_form(profile_data);
                profilecomplete(profile_data);
                if (profile_data.marital_status == 1) {
                    $('.anniversaryDate').addClass('hide')
                }
                if (profile_data.photo && profile_data.gi_pos_certified_date) {
                    $('#profileImage').attr("disabled", true);
                    $('.profile_edit').hide();
                }
                $('.section_container').find(".input_container").each(function () {
                    if ($(this).find('input').val() != '' && $(this).find('select').val() != 0) {
                        $(this).addClass("readonlyFilled");
                    } else {
                        $(this).removeClass("readonlyFilled");
                    }
                });
                parent.addClass('readonly').removeClass('editmode');
                $('.plusminusIcon').eq(0).click();

            }
        },
        error: function (result) {
            showAlertBox(result.responseText, "red");
            $(".loader").hide()
            return false
        }
    });
}

function whatsAppprofileUpdate(dataJson,is_true) {
    var data_json = {
        "platform": 1,
        "consent_details": [
            {
                "consent_doc": 1,  // whatsApp
                "is_accepted":JSON.parse(dataJson)
            }
        ]
    }
    $.ajax({
        type: "POST",
        processData: false,
        data:JSON.stringify(data_json),
        contentType: "application/json",
        url: '/api/v1/update_user_consent/',
        headers: {
            "Api-key": CURRENT_ENV_API_KEY,
            "secret-key": CURRENT_ENV_SECRET_KEY,
            "Authorization": "Token " + user_token
        },
        success: function (result) {
            if (is_true) {
                profile_data = {
                    'whatsapp_notification_enabled': result['is_accepted']
                };
                render_form(profile_data);
                profilecomplete(profile_data);
            }
        },
        error: function (result) {
            showAlertBox(result.responseText, "red");
            $(".loader").hide()
            return false
        }
    });
}

function initial_datepicker() {
    $(".datepicker.anniversaryDate input").datepicker({
        dateFormat: 'yy-mm-dd', changeYear: true, minDate: '+1D -76y', changeMonth: true, yearRange: '-76 : +0'
    });

    $(".datepicker.dob input").datepicker({
        defaultDate: new Date(),
        dateFormat: 'yy-mm-dd', changeYear: true, maxDate: '-18y', minDate: '+1D -76y', changeMonth: true, yearRange: '-76 : -18'
    });
}

function profilecomplete(result) {
    var $filledfield = 0;
    var $emptyfiled = 0;
    var $inputType = 0;
    if (result['photo']) {
        $filledfield = 1
    } else {
        $emptyfiled = 1
    }
    $("#filedsDetails input ,#filedsDetails select").each(function (index, item) {
        if ($(item).attr('type') == 'file' && $(item).parents('docImage')) {
            $inputType++
        }
        if ($(item).val() == '') {
            $emptyfiled++
        } else {
            $filledfield++
        }

    })
    var $totalVal = $filledfield + $emptyfiled - $inputType;
    var progressbar = $filledfield / $totalVal * 100;
    $("#profilebar").progressbar({
        value: progressbar
    });
    var percentvalue = Math.round(progressbar);
    $('.progressbarvalue').html(percentvalue + '%');
    $(".progressvalue").css("width", progressbar + "%");
}

$(document).on("focus", ".datepicker input", function () {
    initial_datepicker()
});

$(document).on("change", "#mainbarWrap input[type='file']", function (e) {
    $(this).parents('.file_container').find('.imageWrap').hide();
    $(this).parents('.file_container').find('.icons').hide();
    readURL(e);
});

function readURL(event) {
    if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        $(event.target).parent().find('.bg-danger').remove();
        reader.onload = function (e) {
            $(event.target).parent().find('img').attr('src', e.target.result);
        }
        if (event.target.files[0].name.substr(-4) == ".pdf" || event.target.files[0].name.substr(-4) == ".jpg" || event.target.files[0].name.substr(-5) == ".jpeg" || event.target.files[0].name.substr(-4) == ".png") {
            $(event.target).parents('.file_container').find('.imageWrap').hide();
            $(event.target).parents('.file_container').find('.imagename').removeClass('hide');
            if (event.target.files[0].name.substr(-4) == ".pdf") {
                $(event.target).parent().find('img').attr('src', path);
            } else {
                reader.readAsDataURL(event.target.files[0]);
            }
        } else {
            $(event.target).parent().append("<label class='bg-danger'> Please upload jpg/jpeg/png or pdf files. </label>")
        }

    }
}
function setprofile(url) {
    var formData = new FormData();
    formData.append('photo', url);
    profileUpdate(formData, false)
}



function profilecrop() {
    $("#profileImage").finecrop({
        viewHeight: 500,
        cropWidth: 200,
        cropHeight: 200,
        cropInput: 'inputImage',
        cropOutput: 'profile_img',
        zoomValue: 50,
        setprofile: setprofile
    });

}

$(document).on("keypress", "input", function () {
    if ($(this).parents('.input_container').find('.error').length !== 0) {
        $(this).parents('.input_container').find('.error').remove();
    }
    var $this = $(this)
    floatInput($this)
});



$(document).on("click", "label", function () {
    var Input = $(this).parent('.materialfiled').find('input');
    $(Input).focus();
});

$(document).on("focus", ".datepicker input", function () {
    $(this).parents('.input_container').addClass("readonlyFilled");
});

function validateForm(form) {
    var error = false
    $(form).find("input", "select").each(function () {
        var maxlength = $(this).attr("maxlength")
        var minlength = $(this).attr("minlength")
        var required = $(this).attr("required")
        var pattern = $(this).attr("pattern");
        var dependency_field = $(this).attr("dependency_field");
        var regex = new RegExp(pattern)
        //var pattern_suggestion = $(this).attr("data-suggestion")
        if (required && ($(this).val() == "")) {
            showAlert("This Field is required", this)
            error = true
        }
        if (dependency_field && $(this).val() != "") {
            var string = String(dependency_field);
            var dependencyField = string.split(",");
            $.each(dependencyField, function (key, value) {
                let status = $("input[name='" + value + "']").attr("status");
                if ($("input[name='" + value + "']").val() == "" && status != 'VERIFIED') {
                    var $this = $("input[name='" + value + "']");
                    showAlert("This Field is required", $this)
                    error = true
                }
            });
        }
        var is_disabled = $(this).attr("disabled")
        if (maxlength && $(this).val().length > maxlength && $(this).val().length != 0 && is_disabled != 'disabled') {
            showAlert("Max length can not be greater than " + maxlength, this)
            error = true
        }
        if (minlength && $(this).val().length < minlength && $(this).val().length != 0 && is_disabled != 'disabled') {
            showAlert("Min length can not be less than " + minlength, this)
            error = true
        }
        if ($(this).val() != '' && pattern && is_disabled != 'disabled') {
            if (pattern && !regex.test($(this).val())) {
                showAlert("Please enter a valid format.", this)
                error = true
            }
        }
    });
    return error
}
function showAlert(text, ele) {
    $(ele).parents(".formWrap").eq(0).find(".error").remove()
    $(ele).parent(".materialfiled").append("<div class='error'>" + text + "</div>")
}
function sdk() {
    elem = document.getElementById("login_sdk")
    var sdk = new RB_AMS_SDK({
        'userInfo': elem,
        'islogIn': initiate_user,
        'UserlogOut': logOut,
        "amsurl": DOMAIN,
        'loginBoxStyle1': true,
        'widgetColor': '#ef663c',
        'widgetFont': '14px'
    })
}
function initiate_user(response) {
    if (response && response.token) {
        user_token = response.token
        $.ajax({
            type: "GET",
            url: '/api/v1/fetch_profile/',
            contentType: "application/json",
            headers: {
                "Api-key": CURRENT_ENV_API_KEY,
                "secret-key": CURRENT_ENV_SECRET_KEY,
                "Authorization": "Token " + user_token
            },
            success: function (result) {
                profile_data = result
                render_form(profile_data);
                if (profile_data.marital_status == 1) {
                    $('.anniversaryDate').addClass('hide')
                }
                if (result.photo && result.gi_pos_certified_date) {
                    $('#profileImage').attr("disabled", true);
                    $('.profile_edit').hide();
                }
                var unique_code = result.unique_code;
                unique_code = unique_code.toString();
                profilecomplete(result);
                $('.plusminusIcon').eq(0).click();
                profilecrop();
                if (unique_code.indexOf('TA') > -1) {
                    $('#logo').attr('src', TA_LOGO)
                    $('#logoLink').attr('href', TA_DOMAIN)
                } else {
                    if (unique_code.indexOf('RT') > -1) {
                        $('#logoLink').attr('href', RETAIL_DOMAIN)
                    } else {
                        $('#logoLink').attr('href', RENEWBUY_DOMAINCODE)
                    }
                    $('#logo').attr('src', RB_LOGO)
                }

                $('.section_container').find(".input_container").each(function () {
                    if ($(this).find('input').val() != '' && $(this).find('select').val() != 0) {
                        $(this).addClass("readonlyFilled");
                    } else {
                        $(this).removeClass("readonlyFilled");
                    }
                });

            },
            error: function (result) {
                showAlertBox(result.responseJSON.detail.error[0], "red");
                return false
            }
        });
    } else {
        logOut()
    }
}

function floatInput($this) {
    if ($($this).val() != '') {
        $($this).parents('.input_container').addClass("readonlyFilled");
    } else {
        $($this).parents('.input_container').removeClass("readonlyFilled");
    }
}

function logOut() {
    current_locaion = window.location.href
    window.location.href = DOMAIN + "?next=" + current_locaion
}

function render_form(result) {
    for (var key in result) {
        if (bank_details.fields[key] && result[key]) {
            bank_details.fields[key].value = result[key]
        }
        if (account_details.fields[key] && result[key]) {
            account_details.fields[key].value = result[key]
        }
        if (basicInfo.fields[key] && result[key]) {
            basicInfo.fields[key].value = result[key]
        }
        if (business_detail.fields[key] && result[key]) {
            business_detail.fields[key].value = result[key]
        }
        if (education.fields[key] && result[key]) {
            education.fields[key].value = result[key]
        }
        if (communication_details.fields[key] && result[key]) {
            communication_details.fields[key].value = result[key]
        }
        if (KYC.fields[key] && result[key]) {
            KYC.fields[key].value = result[key]
        }
        if (result['pan_number']) {
            KYC.fields.pan_number.disable = true;
        }
        if (result['aadhaar_number']) {
            KYC.fields.aadhaar_number.disable = true;
        }
        if (result['passport_number']) {
            KYC.fields.passport_number.disable = true;
        }
        if (result['gstin_number']) {
            business_detail.fields.gstin_number.disable = true;
        }
        if (result['drivers_license_number']) {
            KYC.fields.drivers_license_number.disable = true;
        }

        if (result['education_cert_number']) {
            education.fields.education_cert_number.disable = true;
        }

        if (result['bank_account_number']) {
            bank_details.fields.bank_account_number.disable = true;
        }

        if (result['ifsc_code']) {
            bank_details.fields.ifsc_code.disable = true;
        }

        if (result['email']) {
            communication_details.fields.email.disable = true;
        }

        if (result['mobile']) {
            communication_details.fields.mobile.disable = true;
        }

        if (result['educational_qualification']) {
            education.fields.educational_qualification.disable = true;
        }
        if (result['first_name'] && result.gi_pos_certified_date) {
            account_details.fields.first_name.disable = true;
        }

        if (result['last_name'] && result.gi_pos_certified_date) {
            account_details.fields.last_name.disable = true;
        }

        if (result['middle_name'] && result.gi_pos_certified_date) {
            account_details.fields.middle_name.disable = true;
        }
    }
    for (var key in result.doc_verification_status) {
        var statusvalue = result.doc_verification_status[key]['type'];
        var statusvalue1 = documentType[statusvalue];
        var statusvalue2 = result.doc_verification_status[key]['status']
        if (statusvalue1 in education['fields']) {
            education['fields'][statusvalue1].status = docVerificationStatus[statusvalue2]
        } else if (statusvalue1 in KYC['fields']) {
            KYC['fields'][statusvalue1].status = docVerificationStatus[statusvalue2]
        } else if (statusvalue1 in bank_details['fields']) {
            bank_details['fields'][statusvalue1].status = docVerificationStatus[statusvalue2]
        } else if (statusvalue1 in business_detail['fields']) {
            business_detail['fields'][statusvalue1].status = docVerificationStatus[statusvalue2]
        }
    }
    for (var key in result["gstin_address"]) {
        var val = result["gstin_address"]
        if (business_detail.fields['gstin_' + key] && val[key]) {
            business_detail.fields['gstin_' + key].value = val[key]
        }
        if (val['city'] != '' && val['city'] != null) {
            business_detail.fields['gstin_city'].value = val['city']['id'];
            business_detail.fields['gstin_cityid'].value = val['city']['city'];
        }

    }
    for (var key in result["primary_address"]) {
        var val = result["primary_address"]
        if (communication_details.fields['primary_' + key] && val[key]) {
            communication_details.fields['primary_' + key].value = val[key]
        }
        if (val['city'] != '' && val['city'] != null) {
            communication_details.fields['primary_city'].value = val['city']['id'];
            communication_details.fields['primary_cityid'].value = val['city']['city']
        }
    }
    for (var key in result["secondary_address"]) {
        var val = result["secondary_address"]
        if (communication_details.fields['secondary_' + key] && val[key]) {
            communication_details.fields['secondary_' + key].value = val[key]
        }
        if (val['city'] != '' && val['city'] != null) {
            communication_details.fields['secondary_city'].value = val['city']['id'];
            communication_details.fields['secondary_cityid'].value = val['city']['city']
        }
    }
    comm_preferences.fields.whatsapp_opt.intial_value = result['whatsapp_notification_enabled']
    if (result.photo) {
        $("#profile_img").attr("src", result.photo)
    }
    var name = result.first_name ? result.first_name : ""
    name += result.middle_name ? " " + result.middle_name : ""
    name += result.last_name ? " " + result.last_name : ""
    $(".pro_code").text(result.unique_code)
    $(".pro_name").text(name)
    $("#primary_form").html("");
    forms = [basicInfo, account_details, communication_details, business_detail, education, KYC, bank_details, comm_preferences]
    forms.forEach(element => {
        $("#primary_form").append(render(element))
    });
    $(".loader").hide()
    $("#load_text").text("Updating Profile")
}


$(document).on("keypress", ".city input", function (e) {
    city_search($(this), $(this).val())
})

function city_search(ele, city) {
    $(ele).autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/api/v1/search_city",
                dataType: "json",
                data: {
                    city: city
                },
                headers: {
                    "Api-key": CURRENT_ENV_API_KEY,
                    "secret-key": CURRENT_ENV_SECRET_KEY,
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        console.log(item)
                        return { label: item.city, value: item.id }
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            $(ele).val(ui.item.label);
            $(ele).parents('.city').next('.cityId').find('input').val(ui.item.value);
            return false;
        }
    });
}
