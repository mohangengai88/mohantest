
	var windowUrl = window.location.href;
	//var currentLogo = windowUrl.includes("travassured.com");
	var data_json;
	var url;
	
	function initialLoading(){
		var windowUrl = window.location.href
		if(localStorage.getItem('items')){
			var originHostname = window.location.href.slice(window.location.href.indexOf('?') + 6);
			originHostname = originHostname.split('/')[2];
			originHostname = originHostname.split('?')[0];
			originHostname = originHostname.split('.');
			originHostname = '.'+ originHostname[originHostname.length - 2] + '.' + originHostname[originHostname.length - 1]
			if(windowUrl.indexOf('next') > -1){
				if(authorizedDomains.indexOf(originHostname) > -1){
				    var url = window.location.href.slice(window.location.href.indexOf('?') + 6).split('&');
				    window.location.href = url;
				}
				else{
					var url = DOMAIN + 'profile';
					window.location.href = url;
				}
				
			} else{
				var url = DOMAIN + 'profile';
				window.location.href = url;
			}
		}
		if(windowUrl.indexOf("travassured.com") > -1){
			$('.mainLogo img').attr('src', travLogo);
		} else{
			$('.mainLogo img').attr('src', renewbuyLogo)
		}

	}

	if(!($('.form-group input').val() == '')){
		$(this).parents('.form-group').addClass('focused')
	} else{
		$(this).parents('.form-group').removeClass('focused')
	}

	$('.form-group input').focus(function(){
			$(this).parents('.form-group').addClass('focused');
	});

	$('.form-group label').click(function(){
		$(this).parents('.form-group').addClass('focused');
	})


	$('.form-group input').blur(function(){
		var inputValue = $(this).val();
		if ( inputValue == "" ) {
		    $(this).removeClass('filled');
		    $(this).parents('.form-group').removeClass('focused');  
		} else {
		    $(this).addClass('filled');
		}
	}) 

	function resetNewPassword(){
		$('.signInWrapper').hide();
		$('.forgotWrap').show();
	}

	function backToLogin(){
		$('.signInWrapper').show();
		$('.forgotWrap').hide();
		$('.sucessMsg').hide();

	}

	function forgotpassward(){
		if (validate()) {
			return false;
		} else {
			var email = $('#passwordemail').val();
			var data_json = {
				"email": email,
			}
			$.ajax({
				type: "POST",
				url: '/api/v1/password_reset/',
				data: JSON.stringify(data_json),
				contentType: "application/json",
				headers: {
					'Api-key': CURRENT_ENV_API_KEY,
					'secret-key': CURRENT_ENV_SECRET_KEY,
				},
				success: function (result) {
					$('.signInWrapper').hide();
					$('.forgotWrap').hide();
					$('.sucessMsg').show();
				},
				error: function (result) {
					showAlertBox(result.responseJSON.detail.error[0], "red");
					return false
				}
			});

		}
	}

	function validate() {
		var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var error = false
		var $error = $(".error");
		var password = $("#password").val();
		$error.text("");
		if(!($("#forgotWrapper").is(":hidden"))){
			var $form = $('#passwordsubmit').parents('form');
			var email = $("#passwordemail").val();
		} else{
			var $form = $('#submitbutton').parents('form')
			var email = $("#email").val();
		}
		
		$($form).find('input').each(function () {
			if($(this).val() == ''){
				var $error = $(this).parent('.form-group').next('.error');
				$error.text("This is required field.");
				$error.css("color", "red");
				error = true;
			}
			if (!emailPattern.test(email) && $(this).val() != '') {
				var $error = $(this).parent('.form-group').next('.error');
				$error.text("Please enter a valid email.");
				$error.css("color", "red");
				error = true;
			}

		});		
		return error;
	}

	$(document).bind('keypress', function(e) {
	    if(e.keyCode==13){
	    	if(!($("#forgotWrapper").is(":hidden"))){
				forgotpassward()
			} else{
				accountlogin()
			}
	     }
	});

	function accountlogin() {
		if (validate()) {
			return false;
		} else {
			var email = $('#email').val();
			var password = $('#password').val();
			var data_json = {
				"username": email,
				"password": password,
			}
			$.ajax({
				type: "POST",
				url: '/api/v1/authenticate/',
				data: JSON.stringify(data_json),
				contentType: "application/json",
				headers: {
					'Api-key': CURRENT_ENV_API_KEY,
					'secret-key': CURRENT_ENV_SECRET_KEY,
				},
				success: function (result) {
					var windowUrl = window.location.href
					localStorage.setItem('items', JSON.stringify(result));
					var originHostname = window.location.href.slice(window.location.href.indexOf('?') + 6);
					originHostname = originHostname.split('/')[2];
					originHostname = originHostname.split('?')[0];
					originHostname = originHostname.split('.');
					originHostname = '.'+ originHostname[originHostname.length - 2] + '.' + originHostname[originHostname.length - 1]
					if(windowUrl.indexOf('next') > -1){
						if(authorizedDomains.indexOf(originHostname) > -1){
							var url = window.location.href.slice(window.location.href.indexOf('?') + 6).split('&');
							window.location.href = url;
						}
						else{
							var url = DOMAIN + 'profile';
							window.location.href = url;
						}

					} else{
						var url = DOMAIN + 'profile';
						window.location.href = url;
					}
				},
				error: function (result) {
					showAlertBox(result.responseJSON.detail.error[0], "red");
					return false
				}
			});

		}
	}
	function showError(text){
		$("#error_text").html(text)
	}
