var drivingLicense = "^[A-Za-z]{2}\\d{13}$";
var passportPattern = "^[A-Za-z]{1}\\d{7}$";
var panNumberPattern = "^[A-Za-z]{3}[CPHFATBLJGcphfatbljg]{1}[A-Za-z]{1}\\d{4}[A-Za-z]{1}$";
var aadhaarPattern = "^[1-9]{1}\\d{11}$";
var gstinPattern = "^\\d{2}[A-Za-z]{3}[CPHFATBLJGcphfatbljg]{1}[A-Za-z]{1}\\d{4}[A-Za-z]{1}\\d{1}[Zz]{1}[0-9A-Za-z]{1}$";
var voterPattern = "^[A-Za-z]{3}\\d{7}$";
var ifscPattern = "^[A-Za-z]{4}[A-Za-z0-9]{7}$";
var namePattern = "^[A-Za-z]+$";

var account_details = {
    header: "User Account Details", 
    icon: "<i class='fa fa-user-circle-o'></i>",
    editable: true, 
    fields:{
        unique_code: {
            type: "text",
            class: "col-md-6 disable",
            icon: "<i class='fa fa-barcode'></i>",
            name: "unique_code",
            placeholder: "",
            value: "",
            label: "Unique code"
        },
        first_name: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-user'></i>",
            name: "first_name",
            placeholder: "",
            value: "",
            minlength: 3,
            maxlength: 60,
            label: "First Name",
            pattern:namePattern
        },
        middle_name: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-user'></i>",
            name: "middle_name",
            placeholder: "",
            value: "",
            minlength: 3,
            maxlength: 60,
            label: "Middle Name",
            pattern:namePattern
        },
        last_name: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-user'></i>",
            name: "last_name",
            placeholder: "",
            value: "",
            minlength: 3,
            maxlength: 60,
            label: "Last Name",
            pattern:namePattern
        },
    } 
}
var basicInfo = {
    header: "Basic Information",
    editable: true,
    icon: "<i class='fa fa-user'></i>",
    fields: {
        gender: {
            type: "select",
            class: "col-md-6",
            icon: "<i class='fa fa-venus-mars'></i>",
            name: "gender",
            placeholder: "",
            value: "",
            label: "Gender",
            options: [
                { name:"Please select Gender", value: "0"},
                { name: "Male", value: "1" },
                { name: "Female", value: "2" },
            ]
        },
        dob: {
            type: "text",
            class: "col-md-6 datepicker dob",
            icon: "<i class='fa fa-birthday-cake'></i>",
            name: "dob",
            placeholder: "",
            value: "",
            label: "Date of Birth"
        },
        marital_status:{
            type: "select",
            class: "col-md-6 martitalStatus",
            icon: "<i class='fa fa-venus-double'></i>",
            name: "marital_status",
            placeholder: "",
            value: "",
            label: "Marital status",
            options: [
                { name: "Married", value: "2" },
                { name: "Single", value: "1" }
            ]
        },
        date_of_anniversary:{
            type: "text",
            class: "col-md-6 datepicker anniversaryDate",
            icon: "<i class='fa fa-birthday-cake'></i>",
            name:"date_of_anniversary",
            placeholder:"",
            label: "Date of anniversary",
            value:""
        },
        preferred_lang_written:{
            type: "select",
            class: "col-md-6",
            icon: "<i class='fa fa-pencil-square-o'></i>",
            name: "preferred_lang_written",
            placeholder: "",
            value: "",
            label: "Preferred Lang Written",
            options: [
                { name:"Select preferred written language", value: "0"},
                { name: "English", value: "1" },
                { name: "Hindi", value: "2" }
            ]
        },
        preferred_lang_spoken:{
            type: "select",
            class: "col-md-6",
            icon: "<i class='fa fa-language'></i>",
            name: "preferred_lang_spoken",
            placeholder: "",
            value: "",
            label: "Preferred Lang Spoken",
            options: [
                { name:"Select preferred spoken language", value: "0"},
                { name: "English", value: "1" },
                { name: "Hindi", value: "2" },
            ]
        }
    }
}


var communication_details = {
    header: "Communication Details",
    icon: "<i class='fa fa-phone'></i>",
    editable: true, 
    fields:{
        email: {
            type: "email",
            class: "col-md-6",
            icon: "<i class='fa fa-envelope'></i>",
            name: "email",
            placeholder: "",
            value: "",
            label: "Email",
            disable: false
        },
        mobile: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-mobile'></i>",
            name: "mobile",
            placeholder: "",
            value: "",
            minlength: 10,
            maxlength: 10,
            disable: false,
            label: "Mobile"
        },
        primary_address_line1: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card'></i>",
            name: "primary_address_line1",
            placeholder: "",
            value: "",
            label: "Primary Address Line 1"
        },
        primary_address_line2: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card'></i>",
            name: "primary_address_line2",
            placeholder: "",
            value: "",
            label: "Primary Address Line 2"
        },
        primary_address_line3: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card'></i>",
            name: "primary_address_line3",
            placeholder: "",
            value: "",
            label: "Primary Address Line 3"
        },
        primary_cityid: {
            type: "text",
            class: "col-md-6 city ui-widget",
            icon: "<i class='fa fa-map-marker'></i>",
            name: "primary_cityid",
            placeholder: "",
            value: "",
            label: "Primary City",
            options:[]
        },
        primary_city: {
            type: "text",
            class: "col-md-6 cityId cityhide",
            icon: "<i class='fa fa-map-marker'></i>",
            name: "primary_city",
            placeholder: "",
            value: "",
            label: "Primary City Id",
            options:[]
        },
        primary_pincode: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-location-arrow'></i>",
            name: "primary_pincode",
            placeholder: "",
            value: "",
            label: "Primary Pincode",
            options:[]
        },
        secondary_address_line1: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card-o'></i>",
            name: "secondary_address_line1",
            placeholder: "",
            value: "",
            label: "Secondary Address Line 1"
        },
        secondary_address_line2: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card-o'></i>",
            name: "secondary_address_line2",
            placeholder: "",
            value: "",
            label: "Secondary Address Line 2"
        },
        secondary_address_line3: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card-o'></i>",
            name: "secondary_address_line3",
            placeholder: "",
            value: "",
            label: "Secondary Address Line 3"
        },
        secondary_cityid: {
            type: "text",
            class: "col-md-6 city ui-widget",
            icon: "<i class='fa fa-map-marker'></i>",
            name: "secondary_cityid",
            placeholder: "",
            value: "",
            label: "secondary city",
            options:[]
        },
        secondary_city: {
            type: "text",
            class: "col-md-6 cityhide cityId",
            icon: "<i class='fa fa-map-marker'></i>",
            name: "secondary_city",
            placeholder: "",
            value: "",
            label: "Primary City Id",
            options:[]
        },
        secondary_pincode: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-location-arrow'></i>",
            name: "secondary_pincode",
            placeholder: "",
            label: "Secondary Pincode",
            value: ""
        }
    } 
}
var business_detail = {
    header: "Business Details",
    icon: "<i class='fa fa-line-chart'></i>",
    editable: true, 
    fields:{
        agency_name: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-building-o'></i>",
            name: "agency_name",
            placeholder: "",
            value: "",
            label: "Agency Name",
            minlength: 3
        },
        occupation_type:{
            type: "select",
            class: "col-md-6",
            icon: "<i class='fa fa-briefcase'></i>",
            name: "occupation_type",
            placeholder: "",
            value: "",
            label: "Occupation Type",
            options: [
                { name:"Select occupation", value: "0"},
                { name: "Travel agency", value: "1" },
                { name: "Forex agency", value: "2" },
                { name: "Visa consultant", value: "3" },
                { name: "Online travel agent", value: "4" },
                { name: "Travel aggregator", value: "5" },
                { name: "General sales agent", value: "6" },
                { name: "Regional sales agent", value: "7" },
                { name: "Business partner", value: "8" },
                { name: "Other", value: "9" },
                { name: "Chemist", value: "10" },
                { name: "Salaried", value: "11" },
                { name: "Agriculture", value: "12" },
                { name: "Professional", value: "13" },
                { name: "Business owner", value: "14" },
                { name: "Self employed", value: "15" },
                { name: "House wife", value: "16" },
                { name: "Student", value: "17" },
                { name: "Insurance agent", value: "18" },
                { name: "Car dealer", value: "19" },
                { name: "Dealer", value: "20" },
                { name: "Max agnet", value: "21" },
                { name: "Garage workshop owner", value: "22" },
                { name: "Financial service", value: "23" }

            ]
        },

        insurance_business:{
            type: "select",
            class: "col-md-6",
            icon: "<i class='fa fa-briefcase'></i>",
            name: "insurance_business",
            placeholder: "",
            value: "",
            label: "Insurance Business",
            options: [
                { name:"Select Insurance business", value: "0"},
                { name: "Motor insurance", value: "1" },
                { name: "Life insurance", value: "2" },
                { name: "Health insurance", value: "3" },
                { name: "General insurance", value: "4" },
                { name: "Non insurance", value: "5" }
            ]
        },
        gstin_number:{
            type:"text",
            class: "col-md-6 docfield",
            icon: "<i class='fa fa-id-card-o'></i>",
            name:"gstin_number",
            disable:false,
            placeholder:"",
            value:"",
            pattern:gstinPattern,
            minlength: 15,
            maxlength: 15,
            label: "GSTIN Number",
            pattern_suggestion:"29AAAPL1234C2Z5"
        },
        gstin_address_line1: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card'></i>",
            name: "gstin_address_line1",
            placeholder: "",
            label: "GSTIN Address Line 1",
            value: ""
        },
        gstin_address_line2: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card'></i>",
            name: "gstin_address_line2",
            placeholder: "",
            value: "",
            label: "GSTIN Address Line 2"

        },
        gstin_address_line3: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-address-card'></i>",
            name: "gstin_address_line3",
            placeholder: "",
            value: "",
            label: "GSTIN Address Line 3"
        },
        gstin_cityid: {
            type: "text",
            class: "col-md-6 city ui-widget",
            icon: "<i class='fa fa-map-marker'></i>",
            name: "gstin_cityid",
            placeholder: "",
            value: "",
            label: "GSTIN City",
            options:[]
        },
        gstin_city : {
            type: "text",
            class: "col-md-6 cityId cityhide",
            icon: "<i class='fa fa-map-marker'></i>",
            name: "gstin_city",
            placeholder: "",
            value: "",
            label: "GSTIN City ID",
            options:[]
        },

        gstin_pincode: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-location-arrow'></i>",
            name: "gstin_pincode",
            placeholder: "",
            label: "GSTIN pincode",
            value: ""
        },
        gst_cert_image: {
            type:"file",
            class: "col-md-6 docImage",
            icon: "<i class='fa fa-upload'></i>",
            name:"gst_cert_image",
            placeholder:"",
            value:"",
            label: "",
            status: ""
        }

    }
}
var education = {
    header: "Education Details",
    editable: true,
    icon: "<i class='fa fa-graduation-cap'></i>", 
    fields: {
        educational_qualification:{
            type: "select",
            class: "col-md-6",
            icon: "<i class='fa fa-graduation-cap'></i>",
            name: "educational_qualification",
            disable :false,
            placeholder: "", 
            value: "",
            label: "Educational qualification",
            options: [
                { name:"Select your qualification", value: "0"},
                { name: "Below high school", value: "1" },
                { name: "High school", value: "2" },
                { name: "Intermediate", value: "3" },
                { name: "Graduate", value: "4" },
                { name: "Post Graduate", value: "5" },
                { name: "Illiterate", value: "6" },
                { name: "Other", value: "7" }
            ]
        },
        education_cert_number:{
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-id-card-o'></i>",
            name: "education_cert_number",
            placeholder: "",
            disable :false,
            value: "" ,
            label: "Education cert number",
            dependency_field : ['education_cert_image']       
        },
        education_cert_image: {
            type:"file",
            class: "col-md-6",
            icon: "<i class='fa fa-upload'></i>",
            name:"education_cert_image",
            placeholder:"Educational cert image",
            value:"",
            status: "",
            label: "",
            dependency_field : ['education_cert_number']
        }
    }
}
var KYC = {
    header: "KYC Details",
    editable: true,
    icon: "<i class='fa fa-id-badge'></i>",
    fields: {
        aadhaar_number:{
            type:"text",
            class: "col-md-6 docfield",
            icon: "<i class='fa fa-id-card-o'></i>",
            name:"aadhaar_number",
            placeholder:"",
            value:"",
            disable :false,
            pattern:aadhaarPattern,
            maxlength:12,
            minlength:12,
            label: "Aadhaar Number",
            dependency_field : ['aadhaar_card_image'],
            pattern_suggestion:"000011112222"
        },
        pan_number: {
            type: "text",
            class: "col-md-6 docfield",
            icon: "<i class='fa fa-id-card-o'></i>",
            name: "pan_number",
            placeholder: "",
            disable :false,
            value: "",
            minlength: 10,
            maxlength: 10,
            label: "PAN Number",
            dependency_field : ['pan_card_image'],
            pattern:panNumberPattern,
            pattern_suggestion:"AAAPL1234C"
        },
        aadhaar_card_image:{
            type:"file",
            class: "col-md-6 docImage",
            icon: "<i class='fa fa-upload'></i>",
            name:"aadhaar_card_image",
            dependency_field: ['aadhaar_number'],
            placeholder:"Aadhaar Image",
            value:"",
            label: "",
            status: ""
        },
        pan_card_image: {
            type: "file",
            class: "col-md-6 docImage",
            icon: "<i class='fa fa-upload'></i>",
            name: "pan_card_image",
            dependency_field : ['pan_number'],
            placeholder: "PAN Card Image",
            field_text: true,
            value: "",
            label: "",
            status: ""
        },
        drivers_license_number: {
            type: "text",
            class: "col-md-6 docfield",
            icon: "<i class='fa fa-id-card'></i>",
            name: "drivers_license_number",
            placeholder: "",
            value: "",
            label: "drivers license number",
            dependency_field : ['drivers_license_image'],
            minlength: 15,
            maxlength: 15,
            disable :false,
            pattern:drivingLicense,
            //driving_suggestion:"AAAPL1234C"
        },
        passport_number: {
            type: "text",
            class: "col-md-6 docfield",
            icon: "<i class='fa fa-address-card'></i>",
            name: "passport_number",
            dependency_field: ['passport_image'],
            placeholder: "",
            value: "",
            label: "Passport Number",
            disable :false,
            minlength: 8,
            maxlength: 8,
            pattern:passportPattern,
            //passport_suggestion:"AAAPL1234C"
        },
        drivers_license_image: {
            type: "file",
            class: "col-md-6",
            icon: "<i class='fa fa-upload'></i>",
            name: "drivers_license_image",
            dependency_field: ['drivers_license_number'],
            placeholder: "drivers license image",
            value: "",
            label: "",
            status: ""
            //required: true,
        },
        passport_image: {
            type: "file",
            class: "col-md-6 docImage",
            icon: "<i class='fa fa-upload'></i>",
            dependency_field: ['passport_number'],
            name: "passport_image",
            placeholder: "Passport Image",
            label: "",
            value: "",
            status: ""
        },

    }
}
var bank_details = {
    header: "Bank Details",
    editable: true,
    icon: "<i class='fa fa-university'></i>",
    fields: {
        bank_account_number: {
            type: "text",
            class: "col-md-6 docfield",
            icon: "<i class='fa fa-university'></i>",
            name: "bank_account_number",
            dependency_field: ['cancelled_cheque_image','ifsc_code'],
            placeholder: "",
            value: "",
            label: "Bank account Number",
            disable: false,
            minlength: 6,
            maxlength: 20
        },
        ifsc_code: {
            type: "text",
            class: "col-md-6",
            icon: "<i class='fa fa-university'></i>",
            name: "ifsc_code",
            dependency_field: ['bank_account_number','cancelled_cheque_image'],
            placeholder: "",
            value: "",
            label: "IFSC code",
            disable :false,
            pattern : ifscPattern,
            minlength: 3,
            maxlength: 11,
        },
        cancelled_cheque_image: {
            type: "file",
            class: "col-md-6 docImage",
            icon: "<i class='fa fa-upload'></i>",
            name: "cancelled_cheque_image",
            dependency_field: ['bank_account_number','ifsc_code'],
            placeholder: "Cancelled cheque Image",
            value: "",
            label: "",
            status:""
        }
    }
}
var comm_preferences = {
    header: "Communication Preferences",
    editable: true,
    icon: "<i class='fa fa-bullhorn'></i>",
    fields: {
        whatsapp_opt: {
            type: "radio",
            class: "col-md-12",
            name: "notification_whatsapp",
            value: "",
            label: "Allow whatsapp notifications",
            disable: false,
            intial_value: '',
            options: [
                { id: "Male", value: true ,  name: "whatsapp_status", label:"Yes"},
                { id: "Female", value: false, name: "whatsapp_status",label:"No"},
            ]
        }
    }
}
