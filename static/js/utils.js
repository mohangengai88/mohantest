
function showAlertBox(msg, color) {
	var $alertBox = $("#alertBox");
	$alertBox.find(".content").html(msg.replace("\\n", "<br/>"));
    if (typeof color != "undefined") {
        $("#alertBox .content").css("color", color);
    }
    $alertBox.modal("show");
}
