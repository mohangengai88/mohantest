from django.db.models import Q

from ams.accounts.models import User
from ams.utils import try_or

users = User.objects.filter(
    Q(unique_code__startswith='RT') |
    Q(unique_code__startswith='AG'),
    referral_code__isnull=False
)

fields = ['Partner Code', 'Debit A/c no', 'Beneficiary A/c No', 'Beneficiary A/c No',
          'Beneficiary Name', 'Amount', 'Payment Mode', 'Date', 'IFSC Code',
          'Payable Location Name', 'Print Location name', 'Bene Add 1',
          'Bene Add 2', 'Bene Add 3', 'Bene Add 4', 'Add details 1',
          'Add details 2', 'Add details 3', 'Add details 4', 'Add details 5',
          'Remarks', 'Pos Date', 'Partner Code', 'Email Domain', 'Referral Code']

print(','.join(fields))

for user in users:
    print(
        try_or(lambda: user.unique_code, ''),
        'None',
        try_or(lambda: '~{}'.format(user._bank_account_number), ''),
        try_or(lambda: user._bank_account_number, ''),
        try_or(lambda: user.get_full_name(), ''),
        '-',
        try_or(lambda: 'I' if user._ifsc_code.startswith('ICIC') else 'N', ''),
        'None',
        try_or(lambda: user._ifsc_code, ''),
        'None',
        'None',
        '',
        try_or(lambda: user._pan_number, ''),
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        'Partner Fee',
        '',
        try_or(lambda: user.unique_code, ''), 'renewbuy.com',
        try_or(lambda: user.referral_code, ''),
        sep=','
    )
