#!/bin/bash

# Makemigration
# echo Starting Makemigration.
# python3 manage.py makemigrations

# Migrate
echo Starting Migration.
python3 /code/manage.py migrate &

# Create superuser
echo Creating superuser.
python3 /code/manage.py createcustomsuperuser &

# Collect static files
echo Collecting static files.
python3 /code/manage.py collectstatic --no-input &

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn ams.wsgi:application --bind 0.0.0.0:26000 --workers 5

