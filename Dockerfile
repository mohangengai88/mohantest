# Command for building the container sending the build arguments.
# docker-compose build --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)"

#FROM python:3.6-alpine
FROM python:3.7.4-alpine3.10
ENV PYTHONUNBUFFERED 1

RUN apk update && apk add --no-cache curl-dev \
                                     gcc \
                                     git \
                                     libffi-dev \
                                     musl-dev \
                                     openssh \
                                     postgresql-dev \
                                     vim \
                                     #Pillow dependencies
                                     freetype-dev \
                                     jpeg-dev \
                                     lcms2-dev \
                                     openjpeg-dev \
                                     tcl-dev \
                                     tiff-dev \
                                     tk-dev \
                                     zlib-dev

RUN mkdir /code /var/log/ams

# Copy all the code.
COPY . /code/

WORKDIR /code/

#RUN chmod 777 start1.sh

ADD requirements /code/requirements

RUN pip install -r /code/requirements/pip.txt -U

# Copy all the code.
COPY . /code

